﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class TestStepResponse
	{
		public string TestName { get; set; }
		public int StepId { get; set; }
		public string StepName { get; set; }
		public int TestSequence { get; set; }
	}
}
