﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class TestSessionFailure
	{
		public int TestSessionId { get; set; }
		public int FailureReasonId { get; set; }
	}
}
