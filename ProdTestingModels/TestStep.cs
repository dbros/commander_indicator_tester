﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class TestStep
	{
		public int TestCategoryId { get; set; }
		public int TestId { get; set; }
		public string TestName { get; set; }
		public int StepId { get; set; }
		public string StepName { get; set; }
		public int TestSequence { get; set; }
	}
}
