﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class FixAction
	{
		public int FixActionId { get; set; }
		public string FixActionName { get; set; }
		public string TestCategoryId { get; set; }
	}
}
