﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class TestPartCodeResponse
	{
		public int TestId { get; set; }
		public string PartCode { get; set; }
	}
}
