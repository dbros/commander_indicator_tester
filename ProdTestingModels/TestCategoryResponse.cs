﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class TestCategoryResponse
	{
		public int TestCategoryId { get; set; }
		public string TestCategoryName { get; set; }
	}
}
