﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class TestSessionResultRequest
	{
		public int StepId { get; set; }
		public bool FailPassResult { get; set; }
		public string TextResult { get; set; }
	}
}
