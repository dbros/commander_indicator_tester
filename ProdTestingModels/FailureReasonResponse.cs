﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class FailureReasonResponse
	{
		public int FailureReasonId { get; set; }
		public string FailureReasonName { get; set; }
	}
}
