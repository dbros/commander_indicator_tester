﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class TestResponse
	{
		public int TestId { get; set; }
		public string TestName { get; set; }
	}
}
