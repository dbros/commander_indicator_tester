﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class FixActionResponse
	{
		public int FixActionId { get; set; }
		public string FixActionName { get; set; }
	}
}
