﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper.Contrib.Extensions;


namespace ProdTestingModels
{
	[Table("TestSessionResult")]
	public class TestSessionResult
	{
		[Key]
		public int Id { get; set; }
		public int TestSessionId { get; set; }
		public int StepId { get; set; }
		public bool FailPassResult { get; set; }
		public string TextResult { get; set; }
	}
}
