﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class TestSessionResponse
	{
		public int TestSessionId { get; set; }
		public int TestCategoryId { get; set; }
		public int TestId { get; set; }
		public int TestSessionTypeId { get; set; }
		public string DevId { get; set; }
		public string Version { get; set; }
		public string PartCode { get; set; }
		public string EmployeeBadge { get; set; }
		public string JobTraveller { get; set; }
		public string SerialNo { get; set; }
		public DateTime StartDateTime { get; set; }
		public DateTime EndDateTime { get; set; }
		public bool OverallResult { get; set; }
		public string GetVersion { get; set; }
		public string IAPVersion { get; set; }
		public string GetDevID { get; set; }
		public bool TestIO { get; set; }
		public bool ClustCleanseAir { get; set; }
		public bool ClustCleanseWater { get; set; }
		public bool Pulsation { get; set; }
		public bool AcrRam { get; set; }
		public bool MilklineOP { get; set; }
		public bool Retention { get; set; }
		public bool SmartStartReedSwitch { get; set; }
		public bool ProxSwitch { get; set; }
		public bool Temp { get; set; }
		public bool Probes { get; set; }
		public bool Jetstream { get; set; }
		public bool DivertLineReedSwitch { get; set; }
		public bool TeatSpray { get; set; }
		public bool MilkMeterLeadWire { get; set; }
		public bool MilkMeterLead { get; set; }
		public bool Keypad { get; set; }
		public List<TestSessionResultResponse> TestSessionResultResponse { get; set; }
	}
}
