﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class TestPartCode
	{
		public int TestCategoryId { get; set; }
		public int TestId { get; set; }
		public string PartCode { get; set; }
	}
}
