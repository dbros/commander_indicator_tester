﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class FailureReason
	{
		public int FailureReasonId { get; set; }
		public string FailureReasonName { get; set; }
		public string TestCategoryId { get; set; }
	}
}
