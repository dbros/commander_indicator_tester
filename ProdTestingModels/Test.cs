﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingModels
{
	public class Test
	{
		public int TestId { get; set; }
		public string TestName { get; set; }
		public string TestCategoryId { get; set; }
	}
}
