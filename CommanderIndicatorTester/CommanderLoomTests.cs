﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CAN232_Monitor
{
    public class CommanderLoomTests
    {
        public enum CommanderCommonLoom
        {
            CC_AIR, //CLUST_CLEANSE_AIR,
            CC_WATER, //CLUST_CLEANSE_WATER,
            PULSATION,
            ACR_RAM,
            MILKLINE_OP
        };

        public enum CommanderCommonRotLoom
        {
           RETENTION,
           SMART_START_REED_SWITCH
        };

        public enum ProductLoomTests
        {
            PROX_SWITCH,                //0
            TEMP,                       //1
            PROBES,                     //2
            SMART_START_REED_SWITCH,    //3   
            JETSTREAM,                  //4  
            DIVERT_LINE_OP,             //5
            DIVERT_LINE_REED_SWITCH,    //6
            TEAT_SPRAY,                 //7
            MILK_METER_LEAD_WIRE,             //8
            MILK_METER_LEAD             //9
        };

        public enum ProductNums
        {
            EMPTYTest,
            SSDUTest1,  //1
            SSDUTest2,
            SSDUTest3,
            SSDUTest4,
            SSDUTest5,
            SSDUTest6,
            SSDUTest7,
            SSDUTest8,
            SSDUTest9,
            SSDUTest10,
            SSDUTest11,
            SSDUTest12,
            SSDUTest13,
            SSDUTest14, //14
            SSDUTest15,
            SSDUTest16,
            SSDUTest17,
            SSDUTest18,
            SSDUTest19,
            SSDUTest20,            
            RTYTest1,
            RTYJSTest1,
            RTYTest2,
            RTYJSTest2,
            RTYTest3,
            RTYJSTest3,
            RTYTest4,
            RTYJSTest4,
            RTYTest5,
            RTYJSTest5,
            RTYTest6,
            RTYJSTest6,
            RTYTest7,
            RTYJSTest7,
            RTYTest8,
            RTYJSTest8,
            RTYTest9,
            RTYJSTest9,
            KeyPadTest,
            SSDUTest21,
            SSDUJSTest22,
            SSDUTest23,
            SSDUJSTest24,
        };

        public enum SSDUTest1
        {
            PROX_SWITCH,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD            
        };

        public enum SSDUTest2
        {
            PROX_SWITCH,
            JETSTREAM,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD          
        };

        public enum SSDUTest3
        {
            PROX_SWITCH,            
            DIVERT_LINE_OP,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest4
        {
            PROX_SWITCH,
            JETSTREAM,            
            DIVERT_LINE_OP,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest5
        {
            PROX_SWITCH,            
            DIVERT_LINE_OP,
            DIVERT_LINE_REED_SWITCH,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest6
        {
            PROX_SWITCH,
            JETSTREAM,            
            DIVERT_LINE_OP,
            DIVERT_LINE_REED_SWITCH,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest7
        {
            SMART_START_REED_SWITCH,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest8
        {
            SMART_START_REED_SWITCH,
            JETSTREAM,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest9
        {
            SMART_START_REED_SWITCH,            
            DIVERT_LINE_OP,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest10
        {
            SMART_START_REED_SWITCH,
            JETSTREAM,            
            DIVERT_LINE_OP,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest11
        {
            SMART_START_REED_SWITCH,            
            DIVERT_LINE_OP,
            DIVERT_LINE_REED_SWITCH,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest12
        {
            SMART_START_REED_SWITCH,
            JETSTREAM,            
            DIVERT_LINE_OP,
            DIVERT_LINE_REED_SWITCH,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest13
        {
            PROX_SWITCH,
            TEMP,
            PROBES,
            DIVERT_LINE_OP
        };

        public enum SSDUTest14
        {
            PROX_SWITCH,
            TEMP,
            PROBES,
            JETSTREAM,
            DIVERT_LINE_OP
        };

        public enum SSDUTest15
        {
            TEMP,
            PROBES,
            SMART_START_REED_SWITCH,
            DIVERT_LINE_OP
        };

        public enum SSDUTest16
        {
            TEMP,
            PROBES,
            SMART_START_REED_SWITCH,
            JETSTREAM,
            DIVERT_LINE_OP
        };

        public enum SSDUTest17
        {            
            PROX_SWITCH,
            TEMP,
            PROBES,            
            DIVERT_LINE_OP,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest18
        {
            TEMP,
            PROBES,
            SMART_START_REED_SWITCH,            
            DIVERT_LINE_OP,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest19
        {
            PROX_SWITCH,
            PROBES,
            DIVERT_LINE_OP
        };

        public enum SSDUTest20
        {
            PROBES,
            SMART_START_REED_SWITCH,
            DIVERT_LINE_OP
        };

        public enum SSDUTest21
        {
            PROBES,
            DIVERT_LINE_OP,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUJSTest22
        {
            PROBES,
            JETSTREAM,
            DIVERT_LINE_OP,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum SSDUTest23
        {
            TEMP,
            PROBES,
            DIVERT_LINE_OP,
        };

        public enum SSDUJSTest24
        {
            JETSTREAM,
            TEMP,
            PROBES,
            DIVERT_LINE_OP,
        };

        public enum RTYTest1
        {
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum RTYJSTest1
        {
            JETSTREAM,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum RTYTest2
        {
            TEAT_SPRAY,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD,
        };

        public enum RTYJSTest2
        {
            JETSTREAM,
            TEAT_SPRAY,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD,
        };

        public enum RTYTest3
        {            
            DIVERT_LINE_OP,
            DIVERT_LINE_REED_SWITCH,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum RTYJSTest3
        {
            JETSTREAM,
            DIVERT_LINE_OP,
            DIVERT_LINE_REED_SWITCH,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum RTYTest4
        {            
            DIVERT_LINE_OP,
            DIVERT_LINE_REED_SWITCH,
            TEAT_SPRAY,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum RTYJSTest4
        {
            JETSTREAM,
            DIVERT_LINE_OP,
            DIVERT_LINE_REED_SWITCH,
            TEAT_SPRAY,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum RTYTest5
        {
            TEMP,
            PROBES,            
            DIVERT_LINE_OP,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum RTYJSTest5
        {
            TEMP,
            PROBES,
            JETSTREAM,
            DIVERT_LINE_OP,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum RTYTest6
        {
            TEMP,
            PROBES,            
            DIVERT_LINE_OP,
            TEAT_SPRAY,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum RTYJSTest6
        {
            TEMP,
            PROBES,
            JETSTREAM,
            DIVERT_LINE_OP,
            TEAT_SPRAY,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum RTYTest7
        {
            TEMP,
            PROBES,
            DIVERT_LINE_OP
        };

        public enum RTYJSTest7
        {
            TEMP,
            PROBES,
            JETSTREAM,
            DIVERT_LINE_OP
        };

        public enum RTYTest8
        {
            TEMP,
            PROBES,
            DIVERT_LINE_OP,
            TEAT_SPRAY
        };

        public enum RTYJSTest8
        {
            TEMP,
            PROBES,
            JETSTREAM,
            DIVERT_LINE_OP,
            TEAT_SPRAY
        };

        public enum RTYTest9
        {
            DIVERT_LINE_OP,
            TEAT_SPRAY,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public enum RTYJSTest9
        {
            JETSTREAM,
            DIVERT_LINE_OP,
            TEAT_SPRAY,
            MILK_METER_LEAD_WIRE,
            MILK_METER_LEAD
        };

        public static bool ioRequestSent = false;
        public static string prodSpecificTest = "";

        public static void performCommonLoomsTest(CommanderTestForm commanderTestForm, string testCommonString)
        {
            if (commanderTestForm.factTest.CurrentTestAborted)
            {
                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                return;
            }

            switch (testCommonString)
            {
                //case "CLUST_CLEANSE_AIR":
                case "CC_AIR":
                    {
                        enableOutputCommander(commanderTestForm, Constants.CommanderOutputs.OP_CCAIR);                        
                        break;
                    }
                //case "CLUST_CLEANSE_WATER":
                case "CC_WATER":
                    {
                        enableOutputCommander(commanderTestForm, Constants.CommanderOutputs.OP_CCWAT);
                        break;
                    }
                case "PROX_SWITCH": //For Side by Side or Double Ups only
                    {
                        ioRequestSent = false;
                        commanderTestForm.RequestIO(commanderTestForm.mainForm.DefaultAddr);
                        ioRequestSent = true;
                        Utils.delayThread(500);

                        ioRequestSent = false;
                        commanderTestForm.allOutputs(0);
                        commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, commanderTestForm.mainForm.DefaultAddr, 1);
                        Utils.delay(10);
                        commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, commanderTestForm.mainForm.coolContAddr, 1);
                        //Utils.delay(10);
                        Utils.delayThread(500);
                        
                        ProcessCommTestResp.LastIO_inputs = "";
                        ProcessCommTestResp.LastIO_outputs = "";
                        commanderTestForm.RequestIO(commanderTestForm.mainForm.DefaultAddr);
                        ioRequestSent = true;
                        //Utils.delay(50);
                        Utils.delayThread(500);
                        break;
                    }
                case "TEMP":
                    {
                        string text = commanderTestForm.mainForm.msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_COMMANDER_REQUEST_TEMPERATURE, commanderTestForm.mainForm.DefaultAddr, 1, 0, null);
                        commanderTestForm.serialOut(text);
                        //Utils.delay(10);
                        Utils.delayThread(500);
                        break;
                    }
                case "PROBES": //Conductivity Test
                    {
                        //enableOutputCC(commanderTestForm, Constants.CoolControlOutputs.O2);
                        //commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, commanderTestForm.mainForm.DefaultAddr, 1);
                        //commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, commanderTestForm.mainForm.DefaultAddr, 1);
                        //enableOutputCC(commanderTestForm, Constants.CoolControlOutputs.O2);

                        string text = commanderTestForm.mainForm.msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_CONDUCTIVITY, commanderTestForm.mainForm.DefaultAddr, 1, 0, null);
                        commanderTestForm.serialOut(text);
                        //Utils.delay(10);
                        Utils.delayThread(500);
                        break;
                    }
                case "SMART_START_REED_SWITCH":
                    {
                        ioRequestSent = false;
                        commanderTestForm.allOutputs(0);
                        Utils.delayThread(100);
                        commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, commanderTestForm.mainForm.DefaultAddr, 1);
                        Utils.delayThread(100);
                        commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, commanderTestForm.mainForm.DefaultAddr, 1);
                        Utils.delayThread(100);
                        //commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_RET, commanderTestForm.mainForm.DefaultAddr, 1);
                        commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, commanderTestForm.mainForm.DefaultAddr, 1);
                        Utils.delayThread(100);
                        commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_CCAIR, commanderTestForm.mainForm.DefaultAddr, 1);
                   
                        //Utils.delay(10);
                        Utils.delayThread(100);
                        commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, commanderTestForm.mainForm.coolContAddr, 1);
                        //Utils.delay(10);
                        Utils.delayThread(1000);

                        ProcessCommTestResp.LastIO_inputs = "";
                        ProcessCommTestResp.LastIO_outputs = "";
                       
                        commanderTestForm.RequestIO(commanderTestForm.mainForm.DefaultAddr);
                        ioRequestSent = true;
                        //Utils.delay(35);
                        Utils.delayThread(500);
                        break;
                    }
                case "JETSTREAM":
                    {
                        enableOutputCommander(commanderTestForm, Constants.CommanderOutputs.OP_17);
                        break;
                    }

                case "PULSATION":
                    {
                        enableOutputCommander(commanderTestForm, Constants.CommanderOutputs.PULS_1);
                        //Utils.delay(100);
                        break;
                    }
                case "ACR_RAM":
                    {
                       enableOutputCommander(commanderTestForm, Constants.CommanderOutputs.OP_ACR);  
                       break;
                    }
                case "MILK_METER_LEAD":
                    {
                        if (commanderTestForm.milkLeadTest == null && !(commanderTestForm.factTest.MilkMeterLeadTestComplete))
                            {
                                //performSoftReset(commanderTestForm);

                                ProcessCommTestResp.milkMeterLeadTestAborted = false;
                                commanderTestForm.SendNvrCmdSet(commanderTestForm.mainForm.DefaultAddr, (int)Constants.NVR_COMMANDER.NVR_WEIGHALL_CAL_FACTOR, 500);
                                Utils.delayThread(10);
                                commanderTestForm.SendNvrCmdSet(commanderTestForm.mainForm.DefaultAddr, (int)Constants.NVR_COMMANDER.NVR_PROGRAM_TYPE, 1);
                                Utils.delayThread(10);
                                commanderTestForm.softReset(commanderTestForm.mainForm.DefaultAddr);
                                //Utils.delayThread(2000);
                                Utils.delayThread(8000);
                                commanderTestForm.FactoryTestMode();
                                Utils.delayThread(10);


                                commanderTestForm.milkLeadTest = new MilkLeadTest(commanderTestForm);
                                commanderTestForm.milkLeadTestInitiated = true;
                                commanderTestForm.milkLeadTest.ShowDialog();
                                //commanderTestForm.Hide();
                            }
                                                
                        break;
                    }
                case "MILK_METER_LEAD_WIRE":
                    {
                        //enableOutputCommander(commanderTestForm, Constants.CommanderOutputs.OP_BOTTOM);
                        ioRequestSent = false;
                        commanderTestForm.allOutputs(0);
                        Utils.delay(500);
                        commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_BOTTOM, commanderTestForm.mainForm.DefaultAddr, 1);
                        Utils.delay(500);


                        ProcessCommTestResp.LastIO_inputs = "";
                        ProcessCommTestResp.LastIO_outputs = "";
                        commanderTestForm.RequestIO(commanderTestForm.mainForm.coolContAddr);
                        ioRequestSent = true;
                        Utils.delayThread(500);

                        ioRequestSent = false;
                        commanderTestForm.allOutputs(0);
                        Utils.delay(500);
                        commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_TOP, commanderTestForm.mainForm.DefaultAddr, 1);
                        Utils.delay(500);


                        ProcessCommTestResp.LastIO_inputs = "";
                        ProcessCommTestResp.LastIO_outputs = "";
                        commanderTestForm.RequestIO(commanderTestForm.mainForm.coolContAddr);
                        ioRequestSent = true;
                        Utils.delayThread(500);

                        break;
                    }
                case "MILKLINE_OP":
                    {
                        enableOutputCommander(commanderTestForm, Constants.CommanderOutputs.MAIN_LINE);
                        break;
               
                    }
                case "DIVERT_LINE_OP":
                    {
                        enableOutputCommander(commanderTestForm, Constants.CommanderOutputs.LINE_3);
                        break;
                    }
                case "DIVERT_LINE_REED_SWITCH":
                    {
                        //Turn off Prox switch
                        commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, commanderTestForm.mainForm.DefaultAddr, 1);
                        Utils.delay(10);
                        commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, commanderTestForm.mainForm.coolContAddr, 1);
                        Utils.delay(10);

                        ioRequestSent = false;
                        commanderTestForm.RequestIO(commanderTestForm.mainForm.DefaultAddr);
                        ioRequestSent = true;
                        Utils.delayThread(500);
                        
                        ioRequestSent = false;
                        commanderTestForm.allOutputs(0);
                        commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, commanderTestForm.mainForm.DefaultAddr, 1);
                        commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, commanderTestForm.mainForm.DefaultAddr, 1);
                        //commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_RET, commanderTestForm.mainForm.DefaultAddr, 1);
                        commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, commanderTestForm.mainForm.DefaultAddr, 1);
                        Utils.delay(10);
                        
                        //Turn off Prox switch
                        commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, commanderTestForm.mainForm.coolContAddr, 1);

                        Utils.delayThread(500);
                        commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, commanderTestForm.mainForm.coolContAddr, 1);

                         //Utils.delay(10);
                        Utils.delayThread(1000);
                        ProcessCommTestResp.LastIO_inputs = "";
                        ProcessCommTestResp.LastIO_outputs = "";
                     
                        commanderTestForm.RequestIO(commanderTestForm.mainForm.DefaultAddr);
                        ioRequestSent = true;
                        //Utils.delay(35);
                        Utils.delayThread(500);
                        break;
                    }
                case "RETENTION":
                    {
                        enableOutputCommander(commanderTestForm, Constants.CommanderOutputs.OP_RET);
                        break;
                    }
                case "TEAT_SPRAY":
                    {
                        enableOutputCommander(commanderTestForm, Constants.CommanderOutputs.OP_TSPRAY);
                        break;
                    }
                              
                default: break;
            }
        }

        public static void performCommonLoomsTestNew(Can232 can232Form, string testCommonString)
        {
            if (can232Form.factTest.CurrentTestAborted)
            {
                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                return;
            }

            switch (testCommonString)
            {
                //case "CLUST_CLEANSE_AIR":
                case "CC_AIR":
                    {
                        can232Form.logTest("Sending CC_AIR cmd");
                        enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.OP_CCAIR);                        
                        break;
                    }
                //case "CLUST_CLEANSE_WATER":
                case "CC_WATER":
                    {
                        enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.OP_CCWAT);
                        break;
                    }
                case "PROX_SWITCH": //For Side by Side or Double Ups only
                    {
                        ioRequestSent = false;
                        can232Form.RequestIO(can232Form.DefaultAddr);
                        ioRequestSent = true;
                        Utils.delayThread(500);

                        ioRequestSent = false;
                        can232Form.allOutputs(0);
                        can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, can232Form.DefaultAddr, 1);
                        Utils.delay(10);
                        can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, can232Form.coolContAddr, 1);
                        //Utils.delay(10);
                        Utils.delayThread(500);

                        ProcessCommTestResp.LastIO_inputs = "";
                        ProcessCommTestResp.LastIO_outputs = "";
                        can232Form.RequestIO(can232Form.DefaultAddr);
                        ioRequestSent = true;
                        //Utils.delay(50);
                        Utils.delayThread(500);
                        break;
                    }
                case "TEMP":
                    {
                        string text = can232Form.msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_COMMANDER_REQUEST_TEMPERATURE, can232Form.DefaultAddr, 1, 0, null);
                        can232Form.serialOut(text);
                        //Utils.delay(10);
                        Utils.delayThread(500);
                        break;
                    }
                case "PROBES": //Conductivity Test
                    {
                        //enableOutputCC(commanderTestForm, Constants.CoolControlOutputs.O2);
                        //commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, commanderTestForm.mainForm.DefaultAddr, 1);
                        //commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, commanderTestForm.mainForm.DefaultAddr, 1);
                        //enableOutputCC(commanderTestForm, Constants.CoolControlOutputs.O2);

                        string text = can232Form.msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_CONDUCTIVITY, can232Form.DefaultAddr, 1, 0, null);
                        can232Form.serialOut(text);
                        //Utils.delay(10);
                        Utils.delayThread(500);
                        break;
                    }
                case "SMART_START_REED_SWITCH":
                    {
                        ioRequestSent = false;
                        can232Form.allOutputs(0);
                        Utils.delayThread(100);
                        can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, can232Form.DefaultAddr, 1);
                        Utils.delayThread(100);
                        can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, can232Form.DefaultAddr, 1);
                        Utils.delayThread(100);
                        //commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_RET, commanderTestForm.mainForm.DefaultAddr, 1);
                        can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, can232Form.DefaultAddr, 1);
                        Utils.delayThread(100);
                        can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_CCAIR, can232Form.DefaultAddr, 1);

                        //Utils.delay(10);
                        Utils.delayThread(100);
                        can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, can232Form.coolContAddr, 1);
                        //Utils.delay(10);
                        Utils.delayThread(1000);

                        ProcessCommTestResp.LastIO_inputs = "";
                        ProcessCommTestResp.LastIO_outputs = "";

                        can232Form.RequestIO(can232Form.DefaultAddr);
                        ioRequestSent = true;
                        //Utils.delay(35);
                        Utils.delayThread(500);
                        break;
                    }
                case "JETSTREAM":
                    {
                        enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.OP_17);
                        break;
                    }

                case "PULSATION":
                    {
                        enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.PULS_1);
                        //Utils.delay(100);
                        break;
                    }
                case "ACR_RAM":
                    {
                        enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.OP_ACR);
                        break;
                    }
                case "MILK_METER_LEAD":
                    {
                        if (can232Form.milkLeadTest == null && !(can232Form.factTest.MilkMeterLeadTestComplete))
                        {
                            //performSoftReset(commanderTestForm);

                            ProcessCommTestResp.milkMeterLeadTestAborted = false;
                            can232Form.SendNvrCmdSet(can232Form.DefaultAddr, (int)Constants.NVR_COMMANDER.NVR_WEIGHALL_CAL_FACTOR, 500);
                            Utils.delayThread(10);
                            can232Form.SendNvrCmdSet(can232Form.DefaultAddr, (int)Constants.NVR_COMMANDER.NVR_PROGRAM_TYPE, 1);
                            Utils.delayThread(10);
                            can232Form.softReset(can232Form.DefaultAddr);
                            //Utils.delayThread(2000);
                            Utils.delayThread(8000);
                            can232Form.FactoryTestMode();
                            can232Form.CommanderSecurityHandshake();
                            Utils.delayThread(10); 

                            can232Form.milkLeadTest = new MilkLeadTest(can232Form);
                            can232Form.milkLeadTestInitiated = true;
                            can232Form.milkLeadTest.ShowDialog();
                            //commanderTestForm.Hide();
                        }

                        break;
                    }
                case "MILK_METER_LEAD_WIRE":
                    {
                        //enableOutputCommander(commanderTestForm, Constants.CommanderOutputs.OP_BOTTOM);
                        ioRequestSent = false;
                        can232Form.allOutputs(0);
                        Utils.delay(500);
                        can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_BOTTOM, can232Form.DefaultAddr, 1);
                        Utils.delay(500);


                        ProcessCommTestResp.LastIO_inputs = "";
                        ProcessCommTestResp.LastIO_outputs = "";
                        can232Form.RequestIO(can232Form.coolContAddr);
                        ioRequestSent = true;
                        Utils.delayThread(500);

                        ioRequestSent = false;
                        can232Form.allOutputs(0);
                        Utils.delay(500);
                        can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_TOP, can232Form.DefaultAddr, 1);
                        Utils.delay(500);


                        ProcessCommTestResp.LastIO_inputs = "";
                        ProcessCommTestResp.LastIO_outputs = "";
                        can232Form.RequestIO(can232Form.coolContAddr);
                        ioRequestSent = true;
                        Utils.delayThread(500);

                        break;
                    }
                case "MILKLINE_OP":
                    {
                        enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.MAIN_LINE);
                        break;

                    }
                case "DIVERT_LINE_OP":
                    {
                        enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.LINE_3);
                        break;
                    }
                case "DIVERT_LINE_REED_SWITCH":
                    {
                        //Turn off Prox switch
                        can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, can232Form.DefaultAddr, 1);
                        Utils.delay(10);
                        can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, can232Form.coolContAddr, 1);
                        Utils.delay(10);

                        ioRequestSent = false;
                        can232Form.RequestIO(can232Form.DefaultAddr);
                        ioRequestSent = true;
                        Utils.delayThread(500);

                        ioRequestSent = false;
                        can232Form.allOutputs(0);
                        can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, can232Form.DefaultAddr, 1);
                        can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, can232Form.DefaultAddr, 1);
                        //commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_RET, commanderTestForm.mainForm.DefaultAddr, 1);
                        can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, can232Form.DefaultAddr, 1);
                        Utils.delay(10);

                        //Turn off Prox switch
                        can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, can232Form.coolContAddr, 1);

                        Utils.delayThread(500);
                        can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, can232Form.coolContAddr, 1);

                        //Utils.delay(10);
                        Utils.delayThread(1000);
                        ProcessCommTestResp.LastIO_inputs = "";
                        ProcessCommTestResp.LastIO_outputs = "";

                        can232Form.RequestIO(can232Form.DefaultAddr);
                        ioRequestSent = true;
                        //Utils.delay(35);
                        Utils.delayThread(500);

                        //when this code is in ProcesCommTestResp it throws out timings and a result is not returned
                        if (can232Form.factTest.DivertLineReedSwitchTestPassed)
						{
                            can232Form.setTestStepResult("DIVERT_LINE_REED_SWITCH", "PASS");
                        }
                        else
						{
                            can232Form.setTestStepResult("DIVERT_LINE_REED_SWITCH", "FAIL");
                            can232Form.setTestStepResult("OVERALL", "FAIL");
                        }
                        break;
                    }
                case "RETENTION":
                    {
                        enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.OP_RET);
                        break;
                    }
                case "TEAT_SPRAY":
                    {
                        enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.OP_TSPRAY);
                        break;
                    }

                default: break;
            }
        }

        public static void performCommonLoomsTestSimulate(Can232 can232Form, string testCommonString)
        {
            if (can232Form.factTest.CurrentTestAborted)
            {
                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                return;
            }

            switch (testCommonString)
            {
                //case "CLUST_CLEANSE_AIR":
                case "CC_AIR":
                    {
                        //enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.OP_CCAIR);
                        can232Form.logTest("CC_AIR");
                        can232Form.setTestStepResult("CC_AIR", "PASS");
                        //can232Form.lblTestFeedback.Text = "Testing";
                        //can232Form.lblTestFeedbackText.Text = "CC_AIR";
                        break;
                    }
                //case "CLUST_CLEANSE_WATER":
                case "CC_WATER":
                    {
                        //enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.OP_CCWAT);
                        can232Form.logTest("CC_WATER");
                        can232Form.setTestStepResult("CC_WATER", "PASS");
                        break;
                    }
                case "PROX_SWITCH": //For Side by Side or Double Ups only
                    {
                        //ioRequestSent = false;
                        //can232Form.RequestIO(can232Form.DefaultAddr);
                        //ioRequestSent = true;
                        //Utils.delayThread(500);

                        //ioRequestSent = false;
                        //can232Form.allOutputs(0);
                        //can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, can232Form.DefaultAddr, 1);
                        //Utils.delay(10);
                        //can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, can232Form.coolContAddr, 1);
                        //Utils.delayThread(500);

                        //ProcessCommTestResp.LastIO_inputs = "";
                        //ProcessCommTestResp.LastIO_outputs = "";
                        //can232Form.RequestIO(can232Form.DefaultAddr);
                        //ioRequestSent = true;
                        //Utils.delayThread(500);
                        //break;

                        can232Form.logTest("PROX_SWITCH");
                        can232Form.setTestStepResult("PROX_SWITCH", "PASS");
                        break;
                    }
                case "TEMP":
                    {
                        //string text = can232Form.msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_COMMANDER_REQUEST_TEMPERATURE, can232Form.DefaultAddr, 1, 0, null);
                        //can232Form.serialOut(text);
                        //Utils.delayThread(500);

                        can232Form.logTest("TEMP");
                        can232Form.setTestStepResult("TEMP", "PASS");
                        break;
                    }
                case "PROBES": //Conductivity Test
                    {
                        //string text = can232Form.msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_CONDUCTIVITY, can232Form.DefaultAddr, 1, 0, null);
                        //can232Form.serialOut(text);
                        //Utils.delayThread(500);

                        can232Form.logTest("PROBES");
                        can232Form.setTestStepResult("PROBES", "PASS");
                        break;
                    }
                case "SMART_START_REED_SWITCH":
                    {
                        //ioRequestSent = false;
                        //can232Form.allOutputs(0);
                        //Utils.delayThread(100);
                        //can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, can232Form.DefaultAddr, 1);
                        //Utils.delayThread(100);
                        //can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, can232Form.DefaultAddr, 1);
                        //Utils.delayThread(100);
                        //can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, can232Form.DefaultAddr, 1);
                        //Utils.delayThread(100);
                        //can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_CCAIR, can232Form.DefaultAddr, 1);

                        //Utils.delayThread(100);
                        //can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, can232Form.coolContAddr, 1);
                        //Utils.delayThread(1000);

                        //ProcessCommTestResp.LastIO_inputs = "";
                        //ProcessCommTestResp.LastIO_outputs = "";

                        //can232Form.RequestIO(can232Form.DefaultAddr);
                        //ioRequestSent = true;
                        //Utils.delayThread(500);

                        can232Form.logTest("SMART_START_REED_SWITCH");
                        can232Form.setTestStepResult("SMART_START_REED_SWITCH", "PASS");
                        break;
                    }
                case "JETSTREAM":
                    {
                        //enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.OP_17);
                        can232Form.logTest("JETSTREAM");
                        can232Form.setTestStepResult("JETSTREAM", "PASS");
                        break;
                    }

                case "PULSATION":
                    {
                        //enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.PULS_1);
                        can232Form.logTest("PULSATION");
                        can232Form.setTestStepResult("PULSATION", "PASS");
                        break;
                    }
                case "ACR_RAM":
                    {
                        //enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.OP_ACR);
                        can232Form.logTest("ACR_RAM");
                        can232Form.setTestStepResult("ACR_RAM", "PASS");
                        break;
                    }
                case "MILK_METER_LEAD":
                    {
                        //if (can232Form.milkLeadTest == null && !(can232Form.factTest.MilkMeterLeadTestComplete))
                        //{
                        //    ProcessCommTestResp.milkMeterLeadTestAborted = false;
                        //    can232Form.SendNvrCmdSet(can232Form.DefaultAddr, (int)Constants.NVR_COMMANDER.NVR_WEIGHALL_CAL_FACTOR, 500);
                        //    Utils.delayThread(10);
                        //    can232Form.SendNvrCmdSet(can232Form.DefaultAddr, (int)Constants.NVR_COMMANDER.NVR_PROGRAM_TYPE, 1);
                        //    Utils.delayThread(10);
                        //    can232Form.softReset(can232Form.DefaultAddr);
                        //    Utils.delayThread(8000);
                        //    can232Form.FactoryTestMode();
                        //    Utils.delayThread(10);

                        //    can232Form.milkLeadTest = new MilkLeadTest(can232Form);
                        //    can232Form.milkLeadTestInitiated = true;
                        //    can232Form.milkLeadTest.ShowDialog();
                        //}

                        can232Form.logTest("MILK_METER_LEAD");
                        can232Form.setTestStepResult("MILK_METER_LEAD", "PASS");
                        break;
                    }
                case "MILK_METER_LEAD_WIRE":
                    {
                        //ioRequestSent = false;
                        //can232Form.allOutputs(0);
                        //Utils.delay(500);
                        //can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_BOTTOM, can232Form.DefaultAddr, 1);
                        //Utils.delay(500);

                        //ProcessCommTestResp.LastIO_inputs = "";
                        //ProcessCommTestResp.LastIO_outputs = "";
                        //can232Form.RequestIO(can232Form.coolContAddr);
                        //ioRequestSent = true;
                        //Utils.delayThread(500);

                        //ioRequestSent = false;
                        //can232Form.allOutputs(0);
                        //Utils.delay(500);
                        //can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_TOP, can232Form.DefaultAddr, 1);
                        //Utils.delay(500);

                        //ProcessCommTestResp.LastIO_inputs = "";
                        //ProcessCommTestResp.LastIO_outputs = "";
                        //can232Form.RequestIO(can232Form.coolContAddr);
                        //ioRequestSent = true;
                        //Utils.delayThread(500);

                        can232Form.logTest("MILK_METER_LEAD_WIRE");
                        can232Form.setTestStepResult("MILK_METER_LEAD_WIRE", "PASS");
                        break;
                    }
                case "MILKLINE_OP":
                    {
                        //enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.MAIN_LINE);

                        can232Form.logTest("MILKLINE_OP");
                        can232Form.setTestStepResult("MILKLINE_OP", "PASS");
                        break;

                    }
                case "DIVERT_LINE_OP":
                    {
                        //enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.LINE_3);

                        can232Form.logTest("DIVERT_LINE_OP");
                        can232Form.setTestStepResult("DIVERT_LINE_OP", "PASS");
                        break;
                    }
                case "DIVERT_LINE_REED_SWITCH":
                    {
                        ////Turn off Prox switch
                        //can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, can232Form.DefaultAddr, 1);
                        //Utils.delay(10);
                        //can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, can232Form.coolContAddr, 1);
                        //Utils.delay(10);

                        //ioRequestSent = false;
                        //can232Form.RequestIO(can232Form.DefaultAddr);
                        //ioRequestSent = true;
                        //Utils.delayThread(500);

                        //ioRequestSent = false;
                        //can232Form.allOutputs(0);
                        //can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, can232Form.DefaultAddr, 1);
                        //can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, can232Form.DefaultAddr, 1);
                        //can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, can232Form.DefaultAddr, 1);
                        //Utils.delay(10);

                        ////Turn off Prox switch
                        //can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, can232Form.coolContAddr, 1);

                        //Utils.delayThread(500);
                        //can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, can232Form.coolContAddr, 1);

                        //Utils.delayThread(1000);
                        //ProcessCommTestResp.LastIO_inputs = "";
                        //ProcessCommTestResp.LastIO_outputs = "";

                        //can232Form.RequestIO(can232Form.DefaultAddr);
                        //ioRequestSent = true;
                        //Utils.delayThread(500);

                        can232Form.logTest("DIVERT_LINE_REED_SWITCH");
                        can232Form.setTestStepResult("DIVERT_LINE_REED_SWITCH", "PASS");
                        break;
                    }
                case "RETENTION":
                    {
                        //enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.OP_RET);
                        can232Form.logTest("RETENTION");
                        can232Form.setTestStepResult("RETENTION", "PASS");
                        break;
                    }
                case "TEAT_SPRAY":
                    {
                        //enableOutputCommanderNew(can232Form, Constants.CommanderOutputs.OP_TSPRAY);
                        can232Form.logTest("TEAT_SPRAY");
                        can232Form.setTestStepResult("TEAT_SPRAY", "PASS");
                        break;
                    }

                default: break;
            }
        }

        public static void performSoftReset(CommanderTestForm commanderTestForm)
        {
            commanderTestForm.SendNvrCmdSet(commanderTestForm.mainForm.DefaultAddr, (int)Constants.NVR_COMMANDER.NVR_WEIGHALL_CAL_FACTOR, 500);
            Utils.delayThread(10);
            commanderTestForm.SendNvrCmdSet(commanderTestForm.mainForm.DefaultAddr, (int)Constants.NVR_COMMANDER.NVR_PROGRAM_TYPE, 1);
            Utils.delayThread(10);
            commanderTestForm.softReset(commanderTestForm.mainForm.DefaultAddr);
            Utils.delayThread(2000);
            commanderTestForm.FactoryTestMode();
            Utils.delayThread(10);
        }

        public static void enableOutputCommander(CommanderTestForm commanderTestForm, Constants.CommanderOutputs outPut)
        {
            ioRequestSent = false;
            
            commanderTestForm.allOutputs(0);
            //Utils.delay(10);
            Utils.delayThread(500);
            commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, outPut, commanderTestForm.mainForm.DefaultAddr, 1);
            

            //Utils.delay(10);
            Utils.delayThread(500);
            ProcessCommTestResp.LastIO_inputs = "";
            ProcessCommTestResp.LastIO_outputs = "";
            commanderTestForm.RequestIO(commanderTestForm.mainForm.coolContAddr);
            //commanderTestForm.RequestIO();
            ioRequestSent = true;
            //Utils.delay(300);
            Utils.delayThread(500);
        }

        public static void enableOutputCommanderNew(Can232 can232Form, Constants.CommanderOutputs outPut)
        {
            ioRequestSent = false;

            can232Form.allOutputs(0);
            //Utils.delay(10);
            Utils.delayThread(500);
            can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, outPut, can232Form.DefaultAddr, 1);


            //Utils.delay(10);
            Utils.delayThread(500);
            ProcessCommTestResp.LastIO_inputs = "";
            ProcessCommTestResp.LastIO_outputs = "";
            can232Form.RequestIO(can232Form.coolContAddr);
            //commanderTestForm.RequestIO();
            ioRequestSent = true;
            //Utils.delay(300);
            Utils.delayThread(500);
        }

        public static void enableOutputCC(CommanderTestForm commanderTestForm, Constants.CoolControlOutputs outPut)
        {
            ioRequestSent = false;

            commanderTestForm.allOutputs(0);
            //Utils.delay(10);
            Utils.delayThread(500);
            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, outPut, commanderTestForm.mainForm.coolContAddr, 1);

            //Utils.delay(10);
            Utils.delayThread(500);
            ProcessCommTestResp.LastIO_inputs = "";
            ProcessCommTestResp.LastIO_outputs = "";
            //commanderTestForm.RequestIO(commanderTestForm.mainForm.coolContAddr);
            commanderTestForm.RequestIO(commanderTestForm.mainForm.DefaultAddr);
            //commanderTestForm.RequestIO();
            ioRequestSent = true;
            //Utils.delay(25);
            Utils.delayThread(500);
        }

        public static void selectProductLoomsTest(CommanderTestForm commanderTestForm)
        {
            if (commanderTestForm.factTest.CurrentTestAborted)
            {
                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                return;
            }
            switch (commanderTestForm.Selected_Test)
            {
                case (int)ProductNums.SSDUTest1:
                    {
                        SSDUTest1 testSSDUTest1 = SSDUTest1.MILK_METER_LEAD;

                        for (testSSDUTest1 = 0; testSSDUTest1 <= Enum.GetValues(typeof(SSDUTest1)).Cast<SSDUTest1>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest1.ToString();
                            string testSSDUTest1String = testSSDUTest1.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest1String);
                            testSSDUTest1++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest2:
                    {
                        SSDUTest2 testSSDUTest2 = SSDUTest2.MILK_METER_LEAD;

                        for (testSSDUTest2 = 0; testSSDUTest2 <= Enum.GetValues(typeof(SSDUTest2)).Cast<SSDUTest2>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest2.ToString();
                            string testSSDUTest2String = testSSDUTest2.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest2String);
                            testSSDUTest2++; 
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest3:
                    {
                        SSDUTest3 testSSDUTest3 = SSDUTest3.DIVERT_LINE_OP;

                        for (testSSDUTest3 = 0; testSSDUTest3 <= Enum.GetValues(typeof(SSDUTest3)).Cast<SSDUTest3>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest3.ToString();
                            string testSSDUTest3String = testSSDUTest3.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest3String);
                            testSSDUTest3++;                            
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest4:
                    {
                        SSDUTest4 testSSDUTest4 = SSDUTest4.DIVERT_LINE_OP;

                        for (testSSDUTest4 = 0; testSSDUTest4 <= Enum.GetValues(typeof(SSDUTest4)).Cast<SSDUTest4>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest4.ToString();
                            string testSSDUTest4String = testSSDUTest4.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest4String);
                            testSSDUTest4++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest5:
                    {
                        SSDUTest5 testSSDUTest5 = SSDUTest5.DIVERT_LINE_REED_SWITCH;

                        for (testSSDUTest5 = 0; testSSDUTest5 <= Enum.GetValues(typeof(SSDUTest5)).Cast<SSDUTest5>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest5.ToString();
                            string testSSDUTest5String = testSSDUTest5.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest5String);
                            testSSDUTest5++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest6:
                    {
                        SSDUTest6 testSSDUTest6 = SSDUTest6.DIVERT_LINE_REED_SWITCH;

                        for (testSSDUTest6 = 0; testSSDUTest6 <= Enum.GetValues(typeof(SSDUTest6)).Cast<SSDUTest6>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest6.ToString();
                            string testSSDUTest6String = testSSDUTest6.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest6String);
                            testSSDUTest6++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest7:
                    {
                        SSDUTest7 testSSDUTest7 = SSDUTest7.MILK_METER_LEAD;

                        for (testSSDUTest7 = 0; testSSDUTest7 <= Enum.GetValues(typeof(SSDUTest7)).Cast<SSDUTest7>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest7.ToString();
                            string testSSDUTest7String = testSSDUTest7.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest7String);
                            testSSDUTest7++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest8:
                    {
                        SSDUTest8 testSSDUTest8 = SSDUTest8.MILK_METER_LEAD;

                        for (testSSDUTest8 = 0; testSSDUTest8 <= Enum.GetValues(typeof(SSDUTest8)).Cast<SSDUTest8>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest8.ToString();
                            string testSSDUTest8String = testSSDUTest8.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest8String);
                            testSSDUTest8++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest9:
                    {
                        SSDUTest9 testSSDUTest9 = SSDUTest9.DIVERT_LINE_OP;

                        for (testSSDUTest9 = 0; testSSDUTest9 <= Enum.GetValues(typeof(SSDUTest9)).Cast<SSDUTest9>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest9.ToString();
                            string testSSDUTest9String = testSSDUTest9.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest9String);
                            testSSDUTest9++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest10:
                    {
                        SSDUTest10 testSSDUTest10 = SSDUTest10.DIVERT_LINE_OP;

                        for (testSSDUTest10 = 0; testSSDUTest10 <= Enum.GetValues(typeof(SSDUTest10)).Cast<SSDUTest10>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest10.ToString();
                            string testSSDUTest10String = testSSDUTest10.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest10String);
                            testSSDUTest10++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest11:
                    {
                        SSDUTest11 testSSDUTest11 = SSDUTest11.DIVERT_LINE_REED_SWITCH;

                        for (testSSDUTest11 = 0; testSSDUTest11 <= Enum.GetValues(typeof(SSDUTest11)).Cast<SSDUTest11>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest11.ToString();
                            string testSSDUTest11String = testSSDUTest11.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest11String);
                            testSSDUTest11++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest12:
                    {
                        SSDUTest12 testSSDUTest12 = SSDUTest12.DIVERT_LINE_REED_SWITCH;

                        for (testSSDUTest12 = 0; testSSDUTest12 <= Enum.GetValues(typeof(SSDUTest12)).Cast<SSDUTest12>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest12.ToString();
                            string testSSDUTest12String = testSSDUTest12.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest12String);
                            testSSDUTest12++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest13:
                    {
                        SSDUTest13 testSSDUTest13 = SSDUTest13.DIVERT_LINE_OP;

                        for (testSSDUTest13 = 0; testSSDUTest13 <= Enum.GetValues(typeof(SSDUTest13)).Cast<SSDUTest13>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest13.ToString();
                            string testSSDUTest13String = testSSDUTest13.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest13String);
                            testSSDUTest13++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest14:
                    {
                        SSDUTest14 testSSDUTest14 = SSDUTest14.DIVERT_LINE_OP;

                        for (testSSDUTest14 = 0; testSSDUTest14 <= Enum.GetValues(typeof(SSDUTest14)).Cast<SSDUTest14>().Last();)
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest14.ToString();
                            string testSSDUTest14String = testSSDUTest14.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest14String);
                            testSSDUTest14++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest15:
                    {
                        SSDUTest15 testSSDUTest15 = SSDUTest15.DIVERT_LINE_OP;

                        for (testSSDUTest15 = 0; testSSDUTest15 <= Enum.GetValues(typeof(SSDUTest15)).Cast<SSDUTest15>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest15.ToString();
                            string testSSDUTest15String = testSSDUTest15.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest15String);
                            testSSDUTest15++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest16:
                    {
                        SSDUTest16 testSSDUTest16 = SSDUTest16.DIVERT_LINE_OP;

                        for (testSSDUTest16 = 0; testSSDUTest16 <= Enum.GetValues(typeof(SSDUTest16)).Cast<SSDUTest16>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest16.ToString();
                            string testSSDUTest16String = testSSDUTest16.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest16String);
                            testSSDUTest16++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest17:
                    {
                        SSDUTest17 testSSDUTest17 = SSDUTest17.DIVERT_LINE_OP;

                        for (testSSDUTest17 = 0; testSSDUTest17 <= Enum.GetValues(typeof(SSDUTest17)).Cast<SSDUTest17>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }
                            prodSpecificTest = testSSDUTest17.ToString();
                            string testSSDUTest17String = testSSDUTest17.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest17String);
                            testSSDUTest17++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest18:
                    {
                        SSDUTest18 testSSDUTest18 = SSDUTest18.DIVERT_LINE_OP;

                        for (testSSDUTest18 = 0; testSSDUTest18 <= Enum.GetValues(typeof(SSDUTest18)).Cast<SSDUTest18>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest18.ToString();
                            string testSSDUTest18String = testSSDUTest18.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest18String);
                            testSSDUTest18++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest19:
                    {
                        SSDUTest19 testSSDUTest19 = SSDUTest19.DIVERT_LINE_OP;

                        for (testSSDUTest19 = 0; testSSDUTest19 <= Enum.GetValues(typeof(SSDUTest19)).Cast<SSDUTest19>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest19.ToString();
                            string testSSDUTest19String = testSSDUTest19.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest19String);
                            testSSDUTest19++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest20:
                    {
                        SSDUTest20 testSSDUTest20 = SSDUTest20.DIVERT_LINE_OP;

                        for (testSSDUTest20 = 0; testSSDUTest20 <= Enum.GetValues(typeof(SSDUTest20)).Cast<SSDUTest20>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest20.ToString();
                            string testSSDUTest20String = testSSDUTest20.ToString();

                            performCommonLoomsTest(commanderTestForm, testSSDUTest20String);
                            testSSDUTest20++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest1:
                    {
                        RTYTest1 testRTYTest1 = RTYTest1.MILK_METER_LEAD;

                        for (testRTYTest1 = 0; testRTYTest1 <= Enum.GetValues(typeof(RTYTest1)).Cast<RTYTest1>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest1.ToString();
                            string testRTYTest1String = testRTYTest1.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYTest1String);
                            testRTYTest1++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest1:
                    {
                        RTYJSTest1 testRTYJSTest1 = RTYJSTest1.MILK_METER_LEAD;

                        for (testRTYJSTest1 = 0; testRTYJSTest1 <= Enum.GetValues(typeof(RTYJSTest1)).Cast<RTYJSTest1>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest1.ToString();
                            string testRTYJSTest1String = testRTYJSTest1.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYJSTest1String);
                            testRTYJSTest1++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest2:
                    {
                        RTYTest2 testRTYTest2 = RTYTest2.TEAT_SPRAY;

                        for (testRTYTest2 = 0; testRTYTest2 <= Enum.GetValues(typeof(RTYTest2)).Cast<RTYTest2>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest2.ToString();
                            string testRTYTest2String = testRTYTest2.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYTest2String);
                            testRTYTest2++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest2:
                    {
                        RTYJSTest2 testRTYJSTest2 = RTYJSTest2.TEAT_SPRAY;

                        for (testRTYJSTest2 = 0; testRTYJSTest2 <= Enum.GetValues(typeof(RTYJSTest2)).Cast<RTYJSTest2>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest2.ToString();
                            string testRTYJSTest2String = testRTYJSTest2.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYJSTest2String);
                            testRTYJSTest2++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest3:
                    {
                        RTYTest3 testRTYTest3 = RTYTest3.DIVERT_LINE_REED_SWITCH;

                        for (testRTYTest3 = 0; testRTYTest3 <= Enum.GetValues(typeof(RTYTest3)).Cast<RTYTest3>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest3.ToString();
                            string testRTYTest3String = testRTYTest3.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYTest3String);
                            testRTYTest3++;                         
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest3:
                    {
                        RTYJSTest3 testRTYJSTest3 = RTYJSTest3.DIVERT_LINE_REED_SWITCH;

                        for (testRTYJSTest3 = 0; testRTYJSTest3 <= Enum.GetValues(typeof(RTYJSTest3)).Cast<RTYJSTest3>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest3.ToString();
                            string testRTYJSTest3String = testRTYJSTest3.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYJSTest3String);
                            testRTYJSTest3++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest4:
                    {
                        RTYTest4 testRTYTest4 = RTYTest4.DIVERT_LINE_REED_SWITCH;

                        for (testRTYTest4 = 0; testRTYTest4 <= Enum.GetValues(typeof(RTYTest4)).Cast<RTYTest4>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest4.ToString();
                            string testRTYTest4String = testRTYTest4.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYTest4String);
                            testRTYTest4++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest4:
                    {
                        RTYJSTest4 testRTYJSTest4 = RTYJSTest4.DIVERT_LINE_REED_SWITCH;

                        for (testRTYJSTest4 = 0; testRTYJSTest4 <= Enum.GetValues(typeof(RTYJSTest4)).Cast<RTYJSTest4>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest4.ToString();
                            string testRTYJSTest4String = testRTYJSTest4.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYJSTest4String);
                            testRTYJSTest4++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest5:
                    {
                        RTYTest5 testRTYTest5 = RTYTest5.DIVERT_LINE_OP;

                        for (testRTYTest5 = 0; testRTYTest5 <= Enum.GetValues(typeof(RTYTest5)).Cast<RTYTest5>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest5.ToString();
                            string testRTYTest5String = testRTYTest5.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYTest5String);
                            testRTYTest5++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest5:
                    {
                        RTYJSTest5 testRTYJSTest5 = RTYJSTest5.DIVERT_LINE_OP;

                        for (testRTYJSTest5 = 0; testRTYJSTest5 <= Enum.GetValues(typeof(RTYJSTest5)).Cast<RTYJSTest5>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest5.ToString();
                            string testRTYJSTest5String = testRTYJSTest5.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYJSTest5String);
                            testRTYJSTest5++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest6:
                    {
                        RTYTest6 testRTYTest6 = RTYTest6.TEAT_SPRAY;

                        for (testRTYTest6 = 0; testRTYTest6 <= Enum.GetValues(typeof(RTYTest6)).Cast<RTYTest6>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest6.ToString();
                            string testRTYTest6String = testRTYTest6.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYTest6String);
                            testRTYTest6++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest6:
                    {
                        RTYJSTest6 testRTYJSTest6 = RTYJSTest6.TEAT_SPRAY;

                        for (testRTYJSTest6 = 0; testRTYJSTest6 <= Enum.GetValues(typeof(RTYJSTest6)).Cast<RTYJSTest6>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest6.ToString();
                            string testRTYJSTest6String = testRTYJSTest6.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYJSTest6String);
                            testRTYJSTest6++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest7:
                    {
                        RTYTest7 testRTYTest7 = RTYTest7.DIVERT_LINE_OP;

                        for (testRTYTest7 = 0; testRTYTest7 <= Enum.GetValues(typeof(RTYTest7)).Cast<RTYTest7>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest7.ToString();
                            string testRTYTest7String = testRTYTest7.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYTest7String);
                            testRTYTest7++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest7:
                    {
                        RTYJSTest7 testRTYJSTest7 = RTYJSTest7.DIVERT_LINE_OP;

                        for (testRTYJSTest7 = 0; testRTYJSTest7 <= Enum.GetValues(typeof(RTYJSTest7)).Cast<RTYJSTest7>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest7.ToString();
                            string testRTYJSTest7String = testRTYJSTest7.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYJSTest7String);
                            testRTYJSTest7++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest8:
                    {
                        RTYTest8 testRTYTest8 = RTYTest8.TEAT_SPRAY;

                        for (testRTYTest8 = 0; testRTYTest8 <= Enum.GetValues(typeof(RTYTest8)).Cast<RTYTest8>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest8.ToString();
                            string testRTYTest8String = testRTYTest8.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYTest8String);
                            testRTYTest8++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest8:
                    {
                        RTYJSTest8 testRTYJSTest8 = RTYJSTest8.TEAT_SPRAY;

                        for (testRTYJSTest8 = 0; testRTYJSTest8 <= Enum.GetValues(typeof(RTYJSTest8)).Cast<RTYJSTest8>().Last(); )
                        {
                            if (commanderTestForm.factTest.CurrentTestAborted)
                            {
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest8.ToString();
                            string testRTYJSTest8String = testRTYJSTest8.ToString();

                            performCommonLoomsTest(commanderTestForm, testRTYJSTest8String);
                            testRTYJSTest8++;
                        }
                        break;
                    }

                default: break;
            }
        }

        public static void selectProductLoomsTestNew(Can232 can232Form)
        {
            if (can232Form.factTest.CurrentTestAborted)
            {
                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                return;
            }

            var ssduTest1 = (int)ProductNums.SSDUTest1;
            var ssduTest23 = (int)ProductNums.SSDUTest23;
            switch (can232Form.Selected_Test)
            {
                case (int)ProductNums.SSDUTest1:
                    {
                        SSDUTest1 testSSDUTest1 = SSDUTest1.MILK_METER_LEAD;

                        for (testSSDUTest1 = 0; testSSDUTest1 <= Enum.GetValues(typeof(SSDUTest1)).Cast<SSDUTest1>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest1.ToString();
                            string testSSDUTest1String = testSSDUTest1.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest1String);
                            testSSDUTest1++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest2:
                    {
                        SSDUTest2 testSSDUTest2 = SSDUTest2.MILK_METER_LEAD;

                        for (testSSDUTest2 = 0; testSSDUTest2 <= Enum.GetValues(typeof(SSDUTest2)).Cast<SSDUTest2>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest2.ToString();
                            string testSSDUTest2String = testSSDUTest2.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest2String);
                            testSSDUTest2++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest3:
                    {
                        SSDUTest3 testSSDUTest3 = SSDUTest3.DIVERT_LINE_OP;

                        for (testSSDUTest3 = 0; testSSDUTest3 <= Enum.GetValues(typeof(SSDUTest3)).Cast<SSDUTest3>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest3.ToString();
                            string testSSDUTest3String = testSSDUTest3.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest3String);
                            testSSDUTest3++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest4:
                    {
                        SSDUTest4 testSSDUTest4 = SSDUTest4.DIVERT_LINE_OP;

                        for (testSSDUTest4 = 0; testSSDUTest4 <= Enum.GetValues(typeof(SSDUTest4)).Cast<SSDUTest4>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest4.ToString();
                            string testSSDUTest4String = testSSDUTest4.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest4String);
                            testSSDUTest4++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest5:
                    {
                        SSDUTest5 testSSDUTest5 = SSDUTest5.DIVERT_LINE_REED_SWITCH;

                        for (testSSDUTest5 = 0; testSSDUTest5 <= Enum.GetValues(typeof(SSDUTest5)).Cast<SSDUTest5>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest5.ToString();
                            string testSSDUTest5String = testSSDUTest5.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest5String);
                            testSSDUTest5++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest6:
                    {
                        SSDUTest6 testSSDUTest6 = SSDUTest6.DIVERT_LINE_REED_SWITCH;

                        for (testSSDUTest6 = 0; testSSDUTest6 <= Enum.GetValues(typeof(SSDUTest6)).Cast<SSDUTest6>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest6.ToString();
                            string testSSDUTest6String = testSSDUTest6.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest6String);
                            testSSDUTest6++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest7:
                    {
                        SSDUTest7 testSSDUTest7 = SSDUTest7.MILK_METER_LEAD;

                        for (testSSDUTest7 = 0; testSSDUTest7 <= Enum.GetValues(typeof(SSDUTest7)).Cast<SSDUTest7>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest7.ToString();
                            string testSSDUTest7String = testSSDUTest7.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest7String);
                            testSSDUTest7++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest8:
                    {
                        SSDUTest8 testSSDUTest8 = SSDUTest8.MILK_METER_LEAD;

                        for (testSSDUTest8 = 0; testSSDUTest8 <= Enum.GetValues(typeof(SSDUTest8)).Cast<SSDUTest8>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest8.ToString();
                            string testSSDUTest8String = testSSDUTest8.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest8String);
                            testSSDUTest8++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest9:
                    {
                        SSDUTest9 testSSDUTest9 = SSDUTest9.DIVERT_LINE_OP;

                        for (testSSDUTest9 = 0; testSSDUTest9 <= Enum.GetValues(typeof(SSDUTest9)).Cast<SSDUTest9>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest9.ToString();
                            string testSSDUTest9String = testSSDUTest9.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest9String);
                            testSSDUTest9++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest10:
                    {
                        SSDUTest10 testSSDUTest10 = SSDUTest10.DIVERT_LINE_OP;

                        for (testSSDUTest10 = 0; testSSDUTest10 <= Enum.GetValues(typeof(SSDUTest10)).Cast<SSDUTest10>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest10.ToString();
                            string testSSDUTest10String = testSSDUTest10.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest10String);
                            testSSDUTest10++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest11:
                    {
                        SSDUTest11 testSSDUTest11 = SSDUTest11.DIVERT_LINE_REED_SWITCH;

                        for (testSSDUTest11 = 0; testSSDUTest11 <= Enum.GetValues(typeof(SSDUTest11)).Cast<SSDUTest11>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest11.ToString();
                            string testSSDUTest11String = testSSDUTest11.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest11String);
                            testSSDUTest11++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest12:
                    {
                        SSDUTest12 testSSDUTest12 = SSDUTest12.DIVERT_LINE_REED_SWITCH;

                        for (testSSDUTest12 = 0; testSSDUTest12 <= Enum.GetValues(typeof(SSDUTest12)).Cast<SSDUTest12>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest12.ToString();
                            string testSSDUTest12String = testSSDUTest12.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest12String);
                            testSSDUTest12++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest13:
                    {
                        SSDUTest13 testSSDUTest13 = SSDUTest13.DIVERT_LINE_OP;

                        for (testSSDUTest13 = 0; testSSDUTest13 <= Enum.GetValues(typeof(SSDUTest13)).Cast<SSDUTest13>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest13.ToString();
                            string testSSDUTest13String = testSSDUTest13.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest13String);
                            testSSDUTest13++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest14:
                    {
                        SSDUTest14 testSSDUTest14 = SSDUTest14.DIVERT_LINE_OP;

                        for (testSSDUTest14 = 0; testSSDUTest14 <= Enum.GetValues(typeof(SSDUTest14)).Cast<SSDUTest14>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest14.ToString();
                            string testSSDUTest14String = testSSDUTest14.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest14String);
                            testSSDUTest14++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest15:
                    {
                        SSDUTest15 testSSDUTest15 = SSDUTest15.DIVERT_LINE_OP;

                        for (testSSDUTest15 = 0; testSSDUTest15 <= Enum.GetValues(typeof(SSDUTest15)).Cast<SSDUTest15>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest15.ToString();
                            string testSSDUTest15String = testSSDUTest15.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest15String);
                            testSSDUTest15++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest16:
                    {
                        SSDUTest16 testSSDUTest16 = SSDUTest16.DIVERT_LINE_OP;

                        for (testSSDUTest16 = 0; testSSDUTest16 <= Enum.GetValues(typeof(SSDUTest16)).Cast<SSDUTest16>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest16.ToString();
                            string testSSDUTest16String = testSSDUTest16.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest16String);
                            testSSDUTest16++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest17:
                    {
                        SSDUTest17 testSSDUTest17 = SSDUTest17.DIVERT_LINE_OP;

                        for (testSSDUTest17 = 0; testSSDUTest17 <= Enum.GetValues(typeof(SSDUTest17)).Cast<SSDUTest17>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }
                            prodSpecificTest = testSSDUTest17.ToString();
                            string testSSDUTest17String = testSSDUTest17.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest17String);
                            testSSDUTest17++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest18:
                    {
                        SSDUTest18 testSSDUTest18 = SSDUTest18.DIVERT_LINE_OP;

                        for (testSSDUTest18 = 0; testSSDUTest18 <= Enum.GetValues(typeof(SSDUTest18)).Cast<SSDUTest18>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest18.ToString();
                            string testSSDUTest18String = testSSDUTest18.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest18String);
                            testSSDUTest18++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest19:
                    {
                        SSDUTest19 testSSDUTest19 = SSDUTest19.DIVERT_LINE_OP;

                        for (testSSDUTest19 = 0; testSSDUTest19 <= Enum.GetValues(typeof(SSDUTest19)).Cast<SSDUTest19>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest19.ToString();
                            string testSSDUTest19String = testSSDUTest19.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest19String);
                            testSSDUTest19++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest20:
                    {
                        SSDUTest20 testSSDUTest20 = SSDUTest20.DIVERT_LINE_OP;

                        for (testSSDUTest20 = 0; testSSDUTest20 <= Enum.GetValues(typeof(SSDUTest20)).Cast<SSDUTest20>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest20.ToString();
                            string testSSDUTest20String = testSSDUTest20.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest20String);
                            testSSDUTest20++;
                        }
                        break;
                    }

                case (int)ProductNums.SSDUTest21:
                    {
                        SSDUTest21 testSSDUTest21 = SSDUTest21.DIVERT_LINE_OP;

                        for (testSSDUTest21 = 0; testSSDUTest21 <= Enum.GetValues(typeof(SSDUTest21)).Cast<SSDUTest21>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest21.ToString();
                            string testSSDUTest21String = testSSDUTest21.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest21String);
                            testSSDUTest21++;
                        }
                        break;
                    }

                case (int)ProductNums.SSDUJSTest22:
                    {
                        SSDUJSTest22 testSSDUJSTest22 = SSDUJSTest22.DIVERT_LINE_OP;

                        for (testSSDUJSTest22 = 0; testSSDUJSTest22 <= Enum.GetValues(typeof(SSDUJSTest22)).Cast<SSDUJSTest22>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUJSTest22.ToString();
                            string testSSDUTest21String = testSSDUJSTest22.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest21String);
                            testSSDUJSTest22++;
                        }
                        break;
                    }

                case (int)ProductNums.SSDUTest23:
                    {
                        SSDUTest23 testSSDUTest23 = SSDUTest23.DIVERT_LINE_OP;

                        for (testSSDUTest23 = 0; testSSDUTest23 <= Enum.GetValues(typeof(SSDUTest23)).Cast<SSDUTest23>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest23.ToString();
                            string testSSDUTest23String = testSSDUTest23.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest23String);
                            testSSDUTest23++;
                        }
                        break;
                    }

                case (int)ProductNums.SSDUJSTest24:
                    {
                        SSDUJSTest24 testSSDUJSTest24 = SSDUJSTest24.DIVERT_LINE_OP;

                        for (testSSDUJSTest24 = 0; testSSDUJSTest24 <= Enum.GetValues(typeof(SSDUJSTest24)).Cast<SSDUJSTest24>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUJSTest24.ToString();
                            string testSSDUTest21String = testSSDUJSTest24.ToString();

                            performCommonLoomsTestNew(can232Form, testSSDUTest21String);
                            testSSDUJSTest24++;
                        }
                        break;
                    }

                case (int)ProductNums.RTYTest1:
                    {
                        RTYTest1 testRTYTest1 = RTYTest1.MILK_METER_LEAD;

                        for (testRTYTest1 = 0; testRTYTest1 <= Enum.GetValues(typeof(RTYTest1)).Cast<RTYTest1>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest1.ToString();
                            string testRTYTest1String = testRTYTest1.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYTest1String);
                            testRTYTest1++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest1:
                    {
                        RTYJSTest1 testRTYJSTest1 = RTYJSTest1.MILK_METER_LEAD;

                        for (testRTYJSTest1 = 0; testRTYJSTest1 <= Enum.GetValues(typeof(RTYJSTest1)).Cast<RTYJSTest1>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest1.ToString();
                            string testRTYJSTest1String = testRTYJSTest1.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYJSTest1String);
                            testRTYJSTest1++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest2:
                    {
                        RTYTest2 testRTYTest2 = RTYTest2.TEAT_SPRAY;

                        for (testRTYTest2 = 0; testRTYTest2 <= Enum.GetValues(typeof(RTYTest2)).Cast<RTYTest2>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest2.ToString();
                            string testRTYTest2String = testRTYTest2.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYTest2String);
                            testRTYTest2++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest2:
                    {
                        RTYJSTest2 testRTYJSTest2 = RTYJSTest2.TEAT_SPRAY;

                        for (testRTYJSTest2 = 0; testRTYJSTest2 <= Enum.GetValues(typeof(RTYJSTest2)).Cast<RTYJSTest2>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest2.ToString();
                            string testRTYJSTest2String = testRTYJSTest2.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYJSTest2String);
                            testRTYJSTest2++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest3:
                    {
                        RTYTest3 testRTYTest3 = RTYTest3.DIVERT_LINE_REED_SWITCH;

                        for (testRTYTest3 = 0; testRTYTest3 <= Enum.GetValues(typeof(RTYTest3)).Cast<RTYTest3>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest3.ToString();
                            string testRTYTest3String = testRTYTest3.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYTest3String);
                            testRTYTest3++;
                        }
                        break;
                    }
				case (int)ProductNums.RTYJSTest3:
					{
						RTYJSTest3 testRTYJSTest3 = RTYJSTest3.DIVERT_LINE_REED_SWITCH;

						for (testRTYJSTest3 = 0; testRTYJSTest3 <= Enum.GetValues(typeof(RTYJSTest3)).Cast<RTYJSTest3>().Last();)
						{
							if (can232Form.factTest.CurrentTestAborted)
							{
								can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
								return;
							}

							prodSpecificTest = testRTYJSTest3.ToString();
							string testRTYJSTest3String = testRTYJSTest3.ToString();

							performCommonLoomsTestNew(can232Form, testRTYJSTest3String);
							testRTYJSTest3++;
						}
						break;
					}
				case (int)ProductNums.RTYTest4:
                    {
                        RTYTest4 testRTYTest4 = RTYTest4.DIVERT_LINE_REED_SWITCH;

                        for (testRTYTest4 = 0; testRTYTest4 <= Enum.GetValues(typeof(RTYTest4)).Cast<RTYTest4>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest4.ToString();
                            string testRTYTest4String = testRTYTest4.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYTest4String);
                            testRTYTest4++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest4:
                    {
                        RTYJSTest4 testRTYJSTest4 = RTYJSTest4.DIVERT_LINE_REED_SWITCH;

                        for (testRTYJSTest4 = 0; testRTYJSTest4 <= Enum.GetValues(typeof(RTYJSTest4)).Cast<RTYJSTest4>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest4.ToString();
                            string testRTYJSTest4String = testRTYJSTest4.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYJSTest4String);
                            testRTYJSTest4++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest5:
                    {
                        RTYTest5 testRTYTest5 = RTYTest5.DIVERT_LINE_OP;

                        for (testRTYTest5 = 0; testRTYTest5 <= Enum.GetValues(typeof(RTYTest5)).Cast<RTYTest5>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest5.ToString();
                            string testRTYTest5String = testRTYTest5.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYTest5String);
                            testRTYTest5++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest5:
                    {
                        RTYJSTest5 testRTYJSTest5 = RTYJSTest5.DIVERT_LINE_OP;

                        for (testRTYJSTest5 = 0; testRTYJSTest5 <= Enum.GetValues(typeof(RTYJSTest5)).Cast<RTYJSTest5>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest5.ToString();
                            string testRTYJSTest5String = testRTYJSTest5.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYJSTest5String);
                            testRTYJSTest5++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest6:
                    {
                        RTYTest6 testRTYTest6 = RTYTest6.TEAT_SPRAY;

                        for (testRTYTest6 = 0; testRTYTest6 <= Enum.GetValues(typeof(RTYTest6)).Cast<RTYTest6>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest6.ToString();
                            string testRTYTest6String = testRTYTest6.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYTest6String);
                            testRTYTest6++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest6:
                    {
                        RTYJSTest6 testRTYJSTest6 = RTYJSTest6.TEAT_SPRAY;

                        for (testRTYJSTest6 = 0; testRTYJSTest6 <= Enum.GetValues(typeof(RTYJSTest6)).Cast<RTYJSTest6>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest6.ToString();
                            string testRTYJSTest6String = testRTYJSTest6.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYJSTest6String);
                            testRTYJSTest6++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest7:
                    {
                        RTYTest7 testRTYTest7 = RTYTest7.DIVERT_LINE_OP;

                        for (testRTYTest7 = 0; testRTYTest7 <= Enum.GetValues(typeof(RTYTest7)).Cast<RTYTest7>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest7.ToString();
                            string testRTYTest7String = testRTYTest7.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYTest7String);
                            testRTYTest7++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest7:
                    {
                        RTYJSTest7 testRTYJSTest7 = RTYJSTest7.DIVERT_LINE_OP;

                        for (testRTYJSTest7 = 0; testRTYJSTest7 <= Enum.GetValues(typeof(RTYJSTest7)).Cast<RTYJSTest7>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest7.ToString();
                            string testRTYJSTest7String = testRTYJSTest7.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYJSTest7String);
                            testRTYJSTest7++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest8:
                    {
                        RTYTest8 testRTYTest8 = RTYTest8.TEAT_SPRAY;

                        for (testRTYTest8 = 0; testRTYTest8 <= Enum.GetValues(typeof(RTYTest8)).Cast<RTYTest8>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest8.ToString();
                            string testRTYTest8String = testRTYTest8.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYTest8String);
                            testRTYTest8++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest8:
                    {
                        RTYJSTest8 testRTYJSTest8 = RTYJSTest8.TEAT_SPRAY;

                        for (testRTYJSTest8 = 0; testRTYJSTest8 <= Enum.GetValues(typeof(RTYJSTest8)).Cast<RTYJSTest8>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest8.ToString();
                            string testRTYJSTest8String = testRTYJSTest8.ToString();

                            performCommonLoomsTestNew(can232Form, testRTYJSTest8String);
                            testRTYJSTest8++;
                        }
                        break;
                    }

                default: break;
            }
        }

        public static void selectProductLoomsTestSimulation(Can232 can232Form)
        {
            if (can232Form.factTest.CurrentTestAborted)
            {
                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                return;
            }
            switch (can232Form.Selected_Test)
            {
                case (int)ProductNums.SSDUTest1:
                    {
                        SSDUTest1 testSSDUTest1 = SSDUTest1.MILK_METER_LEAD;

                        for (testSSDUTest1 = 0; testSSDUTest1 <= Enum.GetValues(typeof(SSDUTest1)).Cast<SSDUTest1>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest1.ToString();
                            string testSSDUTest1String = testSSDUTest1.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest1String);
                            testSSDUTest1++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest2:
                    {
                        SSDUTest2 testSSDUTest2 = SSDUTest2.MILK_METER_LEAD;

                        for (testSSDUTest2 = 0; testSSDUTest2 <= Enum.GetValues(typeof(SSDUTest2)).Cast<SSDUTest2>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest2.ToString();
                            string testSSDUTest2String = testSSDUTest2.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest2String);
                            testSSDUTest2++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest3:
                    {
                        SSDUTest3 testSSDUTest3 = SSDUTest3.DIVERT_LINE_OP;

                        for (testSSDUTest3 = 0; testSSDUTest3 <= Enum.GetValues(typeof(SSDUTest3)).Cast<SSDUTest3>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest3.ToString();
                            string testSSDUTest3String = testSSDUTest3.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest3String);
                            testSSDUTest3++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest4:
                    {
                        SSDUTest4 testSSDUTest4 = SSDUTest4.DIVERT_LINE_OP;

                        for (testSSDUTest4 = 0; testSSDUTest4 <= Enum.GetValues(typeof(SSDUTest4)).Cast<SSDUTest4>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest4.ToString();
                            string testSSDUTest4String = testSSDUTest4.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest4String);
                            testSSDUTest4++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest5:
                    {
                        SSDUTest5 testSSDUTest5 = SSDUTest5.DIVERT_LINE_REED_SWITCH;

                        for (testSSDUTest5 = 0; testSSDUTest5 <= Enum.GetValues(typeof(SSDUTest5)).Cast<SSDUTest5>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest5.ToString();
                            string testSSDUTest5String = testSSDUTest5.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest5String);
                            testSSDUTest5++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest6:
                    {
                        SSDUTest6 testSSDUTest6 = SSDUTest6.DIVERT_LINE_REED_SWITCH;

                        for (testSSDUTest6 = 0; testSSDUTest6 <= Enum.GetValues(typeof(SSDUTest6)).Cast<SSDUTest6>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest6.ToString();
                            string testSSDUTest6String = testSSDUTest6.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest6String);
                            testSSDUTest6++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest7:
                    {
                        SSDUTest7 testSSDUTest7 = SSDUTest7.MILK_METER_LEAD;

                        for (testSSDUTest7 = 0; testSSDUTest7 <= Enum.GetValues(typeof(SSDUTest7)).Cast<SSDUTest7>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest7.ToString();
                            string testSSDUTest7String = testSSDUTest7.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest7String);
                            testSSDUTest7++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest8:
                    {
                        SSDUTest8 testSSDUTest8 = SSDUTest8.MILK_METER_LEAD;

                        for (testSSDUTest8 = 0; testSSDUTest8 <= Enum.GetValues(typeof(SSDUTest8)).Cast<SSDUTest8>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest8.ToString();
                            string testSSDUTest8String = testSSDUTest8.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest8String);
                            testSSDUTest8++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest9:
                    {
                        SSDUTest9 testSSDUTest9 = SSDUTest9.DIVERT_LINE_OP;

                        for (testSSDUTest9 = 0; testSSDUTest9 <= Enum.GetValues(typeof(SSDUTest9)).Cast<SSDUTest9>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest9.ToString();
                            string testSSDUTest9String = testSSDUTest9.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest9String);
                            testSSDUTest9++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest10:
                    {
                        SSDUTest10 testSSDUTest10 = SSDUTest10.DIVERT_LINE_OP;

                        for (testSSDUTest10 = 0; testSSDUTest10 <= Enum.GetValues(typeof(SSDUTest10)).Cast<SSDUTest10>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest10.ToString();
                            string testSSDUTest10String = testSSDUTest10.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest10String);
                            testSSDUTest10++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest11:
                    {
                        SSDUTest11 testSSDUTest11 = SSDUTest11.DIVERT_LINE_REED_SWITCH;

                        for (testSSDUTest11 = 0; testSSDUTest11 <= Enum.GetValues(typeof(SSDUTest11)).Cast<SSDUTest11>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest11.ToString();
                            string testSSDUTest11String = testSSDUTest11.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest11String);
                            testSSDUTest11++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest12:
                    {
                        SSDUTest12 testSSDUTest12 = SSDUTest12.DIVERT_LINE_REED_SWITCH;

                        for (testSSDUTest12 = 0; testSSDUTest12 <= Enum.GetValues(typeof(SSDUTest12)).Cast<SSDUTest12>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest12.ToString();
                            string testSSDUTest12String = testSSDUTest12.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest12String);
                            testSSDUTest12++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest13:
                    {
                        SSDUTest13 testSSDUTest13 = SSDUTest13.DIVERT_LINE_OP;

                        for (testSSDUTest13 = 0; testSSDUTest13 <= Enum.GetValues(typeof(SSDUTest13)).Cast<SSDUTest13>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest13.ToString();
                            string testSSDUTest13String = testSSDUTest13.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest13String);
                            testSSDUTest13++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest14:
                    {
                        SSDUTest14 testSSDUTest14 = SSDUTest14.DIVERT_LINE_OP;

                        for (testSSDUTest14 = 0; testSSDUTest14 <= Enum.GetValues(typeof(SSDUTest14)).Cast<SSDUTest14>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest14.ToString();
                            string testSSDUTest14String = testSSDUTest14.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest14String);
                            testSSDUTest14++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest15:
                    {
                        SSDUTest15 testSSDUTest15 = SSDUTest15.DIVERT_LINE_OP;

                        for (testSSDUTest15 = 0; testSSDUTest15 <= Enum.GetValues(typeof(SSDUTest15)).Cast<SSDUTest15>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest15.ToString();
                            string testSSDUTest15String = testSSDUTest15.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest15String);
                            testSSDUTest15++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest16:
                    {
                        SSDUTest16 testSSDUTest16 = SSDUTest16.DIVERT_LINE_OP;

                        for (testSSDUTest16 = 0; testSSDUTest16 <= Enum.GetValues(typeof(SSDUTest16)).Cast<SSDUTest16>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest16.ToString();
                            string testSSDUTest16String = testSSDUTest16.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest16String);
                            testSSDUTest16++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest17:
                    {
                        SSDUTest17 testSSDUTest17 = SSDUTest17.DIVERT_LINE_OP;

                        for (testSSDUTest17 = 0; testSSDUTest17 <= Enum.GetValues(typeof(SSDUTest17)).Cast<SSDUTest17>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }
                            prodSpecificTest = testSSDUTest17.ToString();
                            string testSSDUTest17String = testSSDUTest17.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest17String);
                            testSSDUTest17++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest18:
                    {
                        SSDUTest18 testSSDUTest18 = SSDUTest18.DIVERT_LINE_OP;

                        for (testSSDUTest18 = 0; testSSDUTest18 <= Enum.GetValues(typeof(SSDUTest18)).Cast<SSDUTest18>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest18.ToString();
                            string testSSDUTest18String = testSSDUTest18.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest18String);
                            testSSDUTest18++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest19:
                    {
                        SSDUTest19 testSSDUTest19 = SSDUTest19.DIVERT_LINE_OP;

                        for (testSSDUTest19 = 0; testSSDUTest19 <= Enum.GetValues(typeof(SSDUTest19)).Cast<SSDUTest19>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest19.ToString();
                            string testSSDUTest19String = testSSDUTest19.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest19String);
                            testSSDUTest19++;
                        }
                        break;
                    }
                case (int)ProductNums.SSDUTest20:
                    {
                        SSDUTest20 testSSDUTest20 = SSDUTest20.DIVERT_LINE_OP;

                        for (testSSDUTest20 = 0; testSSDUTest20 <= Enum.GetValues(typeof(SSDUTest20)).Cast<SSDUTest20>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testSSDUTest20.ToString();
                            string testSSDUTest20String = testSSDUTest20.ToString();

                            performCommonLoomsTestSimulate(can232Form, testSSDUTest20String);
                            testSSDUTest20++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest1:
                    {
                        RTYTest1 testRTYTest1 = RTYTest1.MILK_METER_LEAD;

                        for (testRTYTest1 = 0; testRTYTest1 <= Enum.GetValues(typeof(RTYTest1)).Cast<RTYTest1>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest1.ToString();
                            string testRTYTest1String = testRTYTest1.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYTest1String);
                            testRTYTest1++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest1:
                    {
                        RTYJSTest1 testRTYJSTest1 = RTYJSTest1.MILK_METER_LEAD;

                        for (testRTYJSTest1 = 0; testRTYJSTest1 <= Enum.GetValues(typeof(RTYJSTest1)).Cast<RTYJSTest1>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest1.ToString();
                            string testRTYJSTest1String = testRTYJSTest1.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYJSTest1String);
                            testRTYJSTest1++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest2:
                    {
                        RTYTest2 testRTYTest2 = RTYTest2.TEAT_SPRAY;

                        for (testRTYTest2 = 0; testRTYTest2 <= Enum.GetValues(typeof(RTYTest2)).Cast<RTYTest2>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest2.ToString();
                            string testRTYTest2String = testRTYTest2.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYTest2String);
                            testRTYTest2++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest2:
                    {
                        RTYJSTest2 testRTYJSTest2 = RTYJSTest2.TEAT_SPRAY;

                        for (testRTYJSTest2 = 0; testRTYJSTest2 <= Enum.GetValues(typeof(RTYJSTest2)).Cast<RTYJSTest2>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest2.ToString();
                            string testRTYJSTest2String = testRTYJSTest2.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYJSTest2String);
                            testRTYJSTest2++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest3:
                    {
                        RTYTest3 testRTYTest3 = RTYTest3.DIVERT_LINE_REED_SWITCH;

                        for (testRTYTest3 = 0; testRTYTest3 <= Enum.GetValues(typeof(RTYTest3)).Cast<RTYTest3>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest3.ToString();
                            string testRTYTest3String = testRTYTest3.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYTest3String);
                            testRTYTest3++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest3:
                    {
                        RTYJSTest3 testRTYJSTest3 = RTYJSTest3.DIVERT_LINE_REED_SWITCH;

                        for (testRTYJSTest3 = 0; testRTYJSTest3 <= Enum.GetValues(typeof(RTYJSTest3)).Cast<RTYJSTest3>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest3.ToString();
                            string testRTYJSTest3String = testRTYJSTest3.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYJSTest3String);
                            testRTYJSTest3++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest4:
                    {
                        RTYTest4 testRTYTest4 = RTYTest4.DIVERT_LINE_REED_SWITCH;

                        for (testRTYTest4 = 0; testRTYTest4 <= Enum.GetValues(typeof(RTYTest4)).Cast<RTYTest4>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest4.ToString();
                            string testRTYTest4String = testRTYTest4.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYTest4String);
                            testRTYTest4++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest4:
                    {
                        RTYJSTest4 testRTYJSTest4 = RTYJSTest4.DIVERT_LINE_REED_SWITCH;

                        for (testRTYJSTest4 = 0; testRTYJSTest4 <= Enum.GetValues(typeof(RTYJSTest4)).Cast<RTYJSTest4>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest4.ToString();
                            string testRTYJSTest4String = testRTYJSTest4.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYJSTest4String);
                            testRTYJSTest4++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest5:
                    {
                        RTYTest5 testRTYTest5 = RTYTest5.DIVERT_LINE_OP;

                        for (testRTYTest5 = 0; testRTYTest5 <= Enum.GetValues(typeof(RTYTest5)).Cast<RTYTest5>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest5.ToString();
                            string testRTYTest5String = testRTYTest5.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYTest5String);
                            testRTYTest5++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest5:
                    {
                        RTYJSTest5 testRTYJSTest5 = RTYJSTest5.DIVERT_LINE_OP;

                        for (testRTYJSTest5 = 0; testRTYJSTest5 <= Enum.GetValues(typeof(RTYJSTest5)).Cast<RTYJSTest5>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest5.ToString();
                            string testRTYJSTest5String = testRTYJSTest5.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYJSTest5String);
                            testRTYJSTest5++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest6:
                    {
                        RTYTest6 testRTYTest6 = RTYTest6.TEAT_SPRAY;

                        for (testRTYTest6 = 0; testRTYTest6 <= Enum.GetValues(typeof(RTYTest6)).Cast<RTYTest6>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest6.ToString();
                            string testRTYTest6String = testRTYTest6.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYTest6String);
                            testRTYTest6++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest6:
                    {
                        RTYJSTest6 testRTYJSTest6 = RTYJSTest6.TEAT_SPRAY;

                        for (testRTYJSTest6 = 0; testRTYJSTest6 <= Enum.GetValues(typeof(RTYJSTest6)).Cast<RTYJSTest6>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest6.ToString();
                            string testRTYJSTest6String = testRTYJSTest6.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYJSTest6String);
                            testRTYJSTest6++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest7:
                    {
                        RTYTest7 testRTYTest7 = RTYTest7.DIVERT_LINE_OP;

                        for (testRTYTest7 = 0; testRTYTest7 <= Enum.GetValues(typeof(RTYTest7)).Cast<RTYTest7>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest7.ToString();
                            string testRTYTest7String = testRTYTest7.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYTest7String);
                            testRTYTest7++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest7:
                    {
                        RTYJSTest7 testRTYJSTest7 = RTYJSTest7.DIVERT_LINE_OP;

                        for (testRTYJSTest7 = 0; testRTYJSTest7 <= Enum.GetValues(typeof(RTYJSTest7)).Cast<RTYJSTest7>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest7.ToString();
                            string testRTYJSTest7String = testRTYJSTest7.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYJSTest7String);
                            testRTYJSTest7++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYTest8:
                    {
                        RTYTest8 testRTYTest8 = RTYTest8.TEAT_SPRAY;

                        for (testRTYTest8 = 0; testRTYTest8 <= Enum.GetValues(typeof(RTYTest8)).Cast<RTYTest8>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYTest8.ToString();
                            string testRTYTest8String = testRTYTest8.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYTest8String);
                            testRTYTest8++;
                        }
                        break;
                    }
                case (int)ProductNums.RTYJSTest8:
                    {
                        RTYJSTest8 testRTYJSTest8 = RTYJSTest8.TEAT_SPRAY;

                        for (testRTYJSTest8 = 0; testRTYJSTest8 <= Enum.GetValues(typeof(RTYJSTest8)).Cast<RTYJSTest8>().Last();)
                        {
                            if (can232Form.factTest.CurrentTestAborted)
                            {
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }

                            prodSpecificTest = testRTYJSTest8.ToString();
                            string testRTYJSTest8String = testRTYJSTest8.ToString();

                            performCommonLoomsTestSimulate(can232Form, testRTYJSTest8String);
                            testRTYJSTest8++;
                        }
                        break;
                    }

                default: break;
            }
        }
    }
}
