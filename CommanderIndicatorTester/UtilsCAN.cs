﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CAN232_Monitor
{
    public class CanMsg
    {
        public int Sid;
        public int Did;
        public int Cmd;
        public int DLC;
        public string Data;
        public string logFileTime = "";

        public void cnvtDataToBytes(out byte[] data)
        {
            int i = 0;
            if (DLC > 8) DLC = 8;
            data = new byte[8];
            for (i = 0; i < DLC; i++)
            {
                byte val = byte.Parse(Data.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                data[i] = val;
            }

        }

        public string cnvtDataToString()
        {
            int i = 0;
            if (DLC > 8) DLC = 8;
            string Text = "";
            for (i = 0; i < DLC; i++)
            {
                int ch = int.Parse(Data.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                Text += Convert.ToChar(ch);
            }
            return Text;
        }

        public bool valid()
        {
            if (Data.Length < DLC * 2)
                return false;

            return true;
        }

        public void parseCalibration(out int val, out int smoothed)
        {
            byte[] data = new byte[8];
            cnvtDataToBytes(out data);
            val = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 32);
            smoothed = data[4] | (data[5] << 8) | (data[6] << 16) | (data[7] << 32);
        }

        public void parseConductivityStream(out int offset, out int condValue1, out int condValue2, out int condValue3)
        {
            byte[] data = new byte[8];
            cnvtDataToBytes(out data);
            offset = data[0] | (data[1] << 8);
            condValue1 = data[2] | (data[3] << 8);
            condValue2 = data[4] | (data[5] << 8);
            condValue3 = data[6] | (data[7] << 8);
        }

        public bool parseConductivityStreamSmoothed(int DLC, out List<int> condValues)
        {
            byte[] data = new byte[8];

            condValues = new List<int>();

            if (DLC <= 0) return false;

            cnvtDataToBytes(out data);

            for (int i = 0; i < DLC; i++)
                condValues.Add(data[0]);

            return true;
        }

        public void parseCowID(out int CowID, out int Side)
        {
            byte[] data = new byte[8];
            cnvtDataToBytes(out data);
            CowID = data[0] | (data[1] << 8) | (data[2] << 16);
            Side = data[3];
        }

        public bool parseConductivity(out float Conductivity, out float rngDMA)
        {
            byte[] data = new byte[8];
            Conductivity = 0;
            rngDMA = 0;
            if (DLC != 4)
                return false;
            cnvtDataToBytes(out data);
            int iTemp = data[1];
            iTemp = (iTemp << 8) | data[0];
            Conductivity = iTemp;
            //Conductivity /= 100;
            int rTemp = data[3];
            rTemp = (rTemp << 8) | data[2];
            rngDMA = rTemp;
            return true;
        }


        public bool parseGrossWgt(out int adcCount, out int grosswgt)
        {
            byte[] data = new byte[8];
            adcCount = 0;
            grosswgt = 0;
            if (DLC != 8)
                return false;
            cnvtDataToBytes(out data);
            adcCount = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 32);
            grosswgt = data[4] | (data[5] << 8) | (data[6] << 16) | (data[7] << 32);
            return true;
        }



        public bool parseTemperature(out float Temperature)
        {
            byte[] data = new byte[8];
            Temperature = 0;
            if (DLC != 2)
                return false;
            cnvtDataToBytes(out data);
            int iTemp = data[1];
            iTemp = (iTemp << 8) | data[0];
            Temperature = iTemp;
            Temperature /= 100;
            return true;
        }

        /*public string parseYield(out int iYield, out int milking_time, out int iMaxFlowRate, out int side)
        {
            byte[] data = new byte[8];

            cnvtDataToBytes(out data);
            iYield = 0;
            int YldStatus;
            int dump_line_status = '?';
            side = 0;
            milking_time = 0;
            iMaxFlowRate = 0;

            iYield = data[1];
            iYield = (iYield << 8) | data[0];

            milking_time = data[3];
            milking_time = (milking_time << 8) | data[2];

            iMaxFlowRate = data[5];
            iMaxFlowRate = (iMaxFlowRate << 8) | data[4];

            YldStatus = data[6] & 0x03;
            side = (data[6] >> 2) & 0x01;
            string SideStr = "LEFT  ";
            if (side != 0) SideStr = "RIGHT ";
            dump_line_status = data[7];


            double Yield = iYield; Yield /= 10;
            double MaxFlowRate = iMaxFlowRate; MaxFlowRate /= 100;

            string YldTypeStr = "Unknown:" + YldStatus.ToString();

            if (YldStatus == (int)Constants.YIELD_TYPE.YIELD_TX_FROM_FLOW)
                YldTypeStr = "Flow";
            else if (YldStatus == (int)Constants.YIELD_TYPE.YIELD_TX_FROM_IP)
                YldTypeStr = "IP";
            else if (YldStatus == (int)Constants.YIELD_TYPE.YIELD_TX_FINAL)
                YldTypeStr = "Final";
            else if (YldStatus == (int)Constants.YIELD_TYPE.YIELD_TX_FROM_IP_START)
                YldTypeStr = "IP Start";

            string Msg = "Data(" + data[6].ToString() + ") " + SideStr + " iYield:" + iYield.ToString() + " Yield:" + Yield.ToString() + " " + YldTypeStr + " DvrtStatus:" + Convert.ToChar(dump_line_status) + " MTime:" + milking_time.ToString() + " MaxFlow:" + MaxFlowRate.ToString();

            return Msg;
        }*/

        public bool parseFlowProfileDataAcrCond(out int milkTime, out List<double> conductivityVals)
        {
            conductivityVals = new List<double>();
            milkTime = 0;

            if (DLC != 8) return false;

            byte[] data = new byte[8];

            cnvtDataToBytes(out data);

            milkTime = data[1];
            milkTime = (milkTime << 8) | data[0];

            for (int c = 0; c < 3; c++)
            {
                int coductivity;
                int Offset = 2 + (c * 2);
                coductivity = data[Offset + 1];
                coductivity = (coductivity << 8) | data[Offset];

                conductivityVals.Add(((double)coductivity) / 10);
            }


            return true;
        }

        public void encodeCowidDivertDraftGroup(out byte[] Data, int side, int CowID, int TagNo, int Group, char DivertStatus, int DraftDir, int isScheduledDraft)
        {
            Data = new byte[8];

            for (int i = 0; i < 8; i++)
                Data[i] = 0;

            // SEND CAN MESSAGE FOR COWS WITH NO COWID
            if ((CowID == 0) && (TagNo > 0))
            {
                Data[0] = (byte)(TagNo & 0xFF);
                Data[1] = (byte)((TagNo >> 8) & 0xFF);
                Data[2] = (byte)((TagNo >> 16) & 0xFF);
                Data[3] = (byte)(side | 0x04);
            }
            else
            {
                Data[0] = (byte)(CowID & 0xFF);
                Data[1] = (byte)((CowID >> 8) & 0xFF);
                Data[2] = (byte)((CowID >> 16) & 0xFF);
                Data[3] = (byte)side;
            }

            Data[4] = (byte)DivertStatus;
            Data[5] = (byte)DraftDir;
            Data[6] = (byte)Group;
            if (isScheduledDraft > 0)
                Data[7] = 1;
            else
                Data[7] = 0;
        }

        public void encodeStringToData(string str, out byte[] Data)
        {
            Data = new byte[8];

            int L = str.Length;
            if (L > 8) L = 8;

            for (int i = 0; i < L; i++)
                Data[i] = (byte)str[i];

        }

        public void decodeIO(out string outputs, out string inputs)
        {
            byte[] data;
            cnvtDataToBytes(out  data);

            outputs = "OP:";
            inputs = "IP:";


            int temp = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);

            for (int i = 0; i < 32; i++)
            {
                int bitSet = temp & (1 << i);

                if (bitSet > 0)
                    outputs += "1";
                else
                    outputs += "0";
            }

            temp = data[4] | (data[5] << 8) | (data[6] << 16) | (data[7] << 24);
            for (int i = 0; i < 32; i++)
            {
                int bitSet = temp & (1 << i);

                if (bitSet > 0)
                    inputs += "1";
                else
                    inputs += "0";
            }

        }

    }

    public class UtilsCAN
    {
        string Buffer = "";

        public List<CanMsg> canMsgList = new List<CanMsg>();

        public void DecodeID(int ID, out int Cmd, out int Did, out int Sid)
        {
            Sid = (int)(ID & 0x1FF);
            ID >>= 9;
            Did = (int)(ID & 0x1FF);
            ID >>= 9;
            Cmd = (int)ID;
        }

        public int EncodeID(int Cmd, int Did, int Sid)
        {
            int ExtID;
            ExtID = Cmd;
            ExtID <<= 9;
            ExtID |= Did;
            ExtID <<= 9;
            ExtID |= Sid;
            return ExtID;
        }

        public string SendCanMessage(int Cmd,int Did,int Sid,short Bytes,byte[] Data)
            {            
	        int ExtID = EncodeID(Cmd,Did,Sid);

	        string   output;
            
            byte[] outArray = new byte[21]; // enough to hold all numbers up to 64-bits            	                    
	        output = "T" + ExtID.ToString("X8") + Bytes.ToString();

            for (int i = 0; i < Bytes; i++)
		        {                
                output += Data[i].ToString("X2");
                }

            output += "\r\r\r";
            return output;
            }


        public string processMessage(string message)
        {
            int Len = message.Length;

            if (message == "Z\r") return "Ack" + Environment.NewLine;
            //if (message == "\a") return "Ack" + Environment.NewLine;
            
            Buffer += message;
            if (message[Len - 1] == '\r')
                return processFullMessage(Buffer);

            //return "Wait:" + message + " Full:" + Buffer + Environment.NewLine;
            return "";
        }

        public string processFullMessage(string message)
        {
            Buffer = "";

            try
            { 

                string[]parts = message.Split('T');

                string text = "";
                foreach (string p in parts)
                {
                    if (p.Length > 8)
                        {
                        text += processSingleMessage(p);
                        }
                }
                return text;
            }

            catch
            {
             
            }
            return "Error\r\n";
        }

        public string processSingleMessage(string message)
        {
            CanMsg cm = new CanMsg();

            String _hexId = message.Substring(0, 8);
            int decId = int.Parse(_hexId, System.Globalization.NumberStyles.HexNumber);

            DecodeID(decId, out cm.Cmd, out cm.Did, out cm.Sid);
            if (message.Length < 9) return "Invalid";
            cm.DLC = message[8] - '0';
            if ((cm.DLC < 0) || (cm.DLC > 8)) return "Invalid DLC";

            cm.Data = "";
            if (cm.DLC > 0) cm.Data = message.Substring(9, cm.DLC * 2);

            if (cm.valid())
                canMsgList.Add(cm);
            else
                return "INVALID DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString() + " CMD:" + cm.Cmd.ToString() + " DLC:" + cm.DLC.ToString() + " MSG:" + cm.Data + Environment.NewLine;

            if (cm.Cmd == (int)Constants.CAN_COMMANDS.CAN_CMD_CALIBRATION_ZERO_DATA) return "";
            if (cm.Cmd == (int)Constants.CAN_COMMANDS.CAN_CMD_CALIBRATION_SPAN_DATA) return "";

            return "DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString() + " CMD:" + cm.Cmd.ToString() + " DLC:" + cm.DLC.ToString() + " MSG:" + cm.Data + Environment.NewLine;
        }
    }
}
