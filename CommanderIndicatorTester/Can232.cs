﻿//
//
//  CAN232 Monitor Lite, a simple demo on how to use LAWICEL CAN232 together with C#
//  All commands and featuers are not implemented and it is a quick and dirty approach.
//  There is no check if commands can be sent or not, but result will be shown.
//
//  This demo will also work with CANUSB (not all commands though) when using the
//  FTDI VCP driver. CANUSB do not have the following commands:
//  Poll One        - P[CR]
//  Poll All        - A[CR]
//  Auto Poll Flag  - X0[CR] and X1[CR]
//
//  For more information we refer to the ASCII manual for each product.
//
//  (C) 2013 LAWICEL AB, Sweden, Lars Wictorsson
//
//  Version: 1.0.0, Released 27th of February 2013
//
//  http://www.can232.com
//
//  Disclaimer:
//  This software is provided "as is" and may be used and changed to suit your own
//  needs without royalties as long as it is used with products from LAWICEL only! 
//
//
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using CAN232_Monitor.Models.Repository;
using ProdTestingDomain.Queries;
using System.Text;
using ProdTestingModels;
using ProdTestingDomain.Commands.CreateTestSession;
using ProdTestingDomain.Commands.CreateUpdateTestSessionFailureReason;
using ProdTestingDomain.Commands.CreateUpdateTestSessionFixAction;
using MediatR;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Microsoft.Win32;
using System.Diagnostics;
/*using SimpleInjector;
using SimpleInjector.Diagnostics;*/



namespace CAN232_Monitor
{
    public partial class Can232 : Form
    {
        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private static Queue inputQueue;
        public UtilsCAN msgProc = new UtilsCAN();
        public CommanderTestForm commanderTestForm;
        private string FileName;
        public bool serialPortResponse = false;

        public int MinVersion = 2;
        public int MinMinorVersion = 52;
        public string appVersion = "1.0";

        public int DefaultAddr = Int32.Parse(Properties.Settings.Default.defaultAddr);
        public int coolContAddr = Int32.Parse(Properties.Settings.Default.coolContAddr);
        public bool performKeyTest = Properties.Settings.Default.performKeyTest;
        public bool autoPrintRpt = Properties.Settings.Default.autoPrintRpt;
        //public int DefaultSerial = Int32.Parse(Properties.Settings.Default.defaultSerial);
        public string DefaultSerial = getDefaultSerial();

        private readonly IMediator _mediator;
        private static List<TestResponse> _testList;
        private static List<TestPartCodeResponse> _testPartCodeList;
        private static List<TestStepResponse> _testStepsList;
        private static List<FailureReasonResponse> _failureReasonsList;
        private static List<FixActionResponse> _fixActionList;
        public static int _testCounterFail = 0;
        public static int _testCounterPass = 0;
        public static int _lastSavedTestSessionId;
        public static bool _testMode = false;

        DataGridViewComboBoxEditingControl cbec = null;

        public int keyPadCounter = 0;
        public CommanderFactoryTestResults factTest = new CommanderFactoryTestResults();
        bool factoryTestEnabled = false;
        public Constants.CommanderTestPhase testPhase = Constants.CommanderTestPhase.GET_VERSION;
        private string LongMessage = "";
        public CommanderLoomTests.CommanderCommonLoom testCommon = CommanderLoomTests.CommanderCommonLoom.MILKLINE_OP;
        public MilkLeadTest milkLeadTest;
        public bool milkLeadTestInitiated = false;
        public CommanderLoomTests.CommanderCommonRotLoom testCommonRot = CommanderLoomTests.CommanderCommonRotLoom.SMART_START_REED_SWITCH;
        public int Selected_Test;
        public KeyPadTest keyPadTest;
        public bool KeyPadTestInitiated = false;
        public int outputsCounter = 0;
        public List<byte> LongMsgData = new List<byte>();
        public static string LastDevID = "";
        public static string JigDevID = "";


        public Can232(IMediator mediator)
        {
            _mediator = mediator;

            InitializeComponent();
            inputQueue = Queue.Synchronized(new Queue());
            populateCan232Fields();

            //FileName = "c:\\temp\\" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_CanTestData.txt";
        }

        public void populateCan232Fields()
        {
            textBoxName.Text = EmployeeRepository.Employee.FullName;

            populateTestListComboBox();
            populateVertialDataGridColumnsNames();
            populatePCFields();
            textBoxSerialNumber.Text = DefaultSerial.ToString();
            //textBoxSerialNumber.Text = DefaultSerial > 0 ? DefaultSerial.ToString() : "";
            comboBoxFailureReason.Visible = false;
            lblFailureReason.Visible = false;
            comboBoxFixAction1.Visible = false;
            labelFixAction.Visible = false;

        }

        private void populatePCFields()
        {
            /*var computerNameHost = System.Net.Dns.GetHostName();
            var ipAddress = GetLocalIPAddress();
            string winVersion = GetOpSystemNameAndVersion();*/
            textBoxPCName.Text = System.Net.Dns.GetHostName();
            textBoxIPAddress.Text = GetLocalIPAddress();
            textBoxPCOpSystem.Text = GetOpSystemName();
            textBoxOpSystemVer.Text = GetOpSystemVersion();
        }

        public static string getDefaultSerial()
        {
            int num;
            string defaultNum = "";

            if (Int32.TryParse(Properties.Settings.Default.defaultSerial, out num))
            {
                return Int32.Parse(Properties.Settings.Default.defaultSerial).ToString();

            }
            else
            {
                return defaultNum;
            }
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        public static string GetOpSystemName()
        {
            string opSystemName = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName", "").ToString(); ;
            //string currentVersion = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CurrentVersion", "").ToString();
            return opSystemName;
        }

        public static string GetOpSystemVersion()
        {
            //string opSystemName = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName", "").ToString(); ;
            string currentVersion = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CurrentVersion", "").ToString();
            return currentVersion;
        }

        public async void populateTestListComboBox()
        {
            await getTestListAsync(GetTestCategoryId());

            var bsTests = new BindingSource();
            bsTests.DataSource = _testList;

            comboBoxTestList.DataSource = bsTests.DataSource;
            comboBoxTestList.DisplayMember = "TestName";
            comboBoxTestList.ValueMember = "TestName";
            var test = comboBoxTestList.SelectedItem;
            comboBoxTestList.SelectedIndex = -1;

        }

        public async void populateFailureReasonComboBox()
        {
            await getFailureReasonsAsync();

            var failureReasons = new BindingSource();
            failureReasons.DataSource = _failureReasonsList;

            comboBoxFailureReason.DataSource = failureReasons.DataSource;
            comboBoxFailureReason.DisplayMember = "FailureReasonName";
            comboBoxFailureReason.ValueMember = "FailureReasonName";
            var failure = comboBoxFailureReason.SelectedItem;
            comboBoxFailureReason.SelectedIndex = -1;

        }
        public async void populateFixActionComboBox(int failureReasonId)
        {
            await getFixActionAsync(failureReasonId);

            var fixActions = new BindingSource();
            fixActions.DataSource = _fixActionList;

            comboBoxFixAction1.DataSource = fixActions.DataSource;
            comboBoxFixAction1.DisplayMember = "FixActionName";
            comboBoxFixAction1.ValueMember = "FixActionName";
            var fixAction = comboBoxFixAction1.SelectedItem;
            comboBoxFixAction1.SelectedIndex = -1;

        }

        public async Task getTestListAsync(int testCategoryId)
        {
            var query = new GetTestsByCategoryQuery(testCategoryId);
            List<TestResponse> results = await Task.Run(() => _mediator.Send(query));
            populateTestList(results);

        }

        public static void populateTestList(List<TestResponse> testList)
        {
            _testList = new List<TestResponse>();

            foreach (var rec in testList)
            {
                _testList.Add(rec);
            }
        }

        private async void comboBoxTestList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (failureReasonRequired())
            {
                MessageBox.Show("Select Failure Reason.");
                return;
            }

            if (actionTakenRequired())
            {
                MessageBox.Show("Select Action Taken.");
                return;
            }

            if (comboBoxTestList.ValueMember.ToString() != "")
            {
                //Clear static lists
                //Clear datagrids
                var t = (TestResponse)comboBoxTestList.SelectedItem;
                Selected_Test = t != null ? t.TestId : 0;

                if (t != null)
                {
                    labelTestNameDisplay.Text = t.TestName.ToString();
                    if (dataGridViewVerticalTestResults.Columns.Count > 0)
                    {
                        clearDataGridViewRowsAndColumns(dataGridViewVerticalTestResults);

                    }
                    //populatePartCodesDropdown(t.TestId);
                    populateVertialDataGridRows(t.TestId);
                    clearTextFields();

                }

            }
        }

        private void clearDataGridViewRowsAndColumns(DataGridView dataGridViewToClear)
        {
            dataGridViewToClear.Rows.Clear();
            dataGridViewToClear.Columns.Clear();
        }

        private void clearTextFields()
        {
            textBoxStartTime.Clear();
            textBoxEndTime.Clear();
            rtboxReceive.Clear();
        }

        private async void populatePartCodesDropdown(int testId)
        {
            await getTestPartCodesAsync(testId);

            var bsTestPartCodes = new BindingSource();
            bsTestPartCodes.DataSource = _testPartCodeList;
            comboBoxTestPartCodes.DataSource = bsTestPartCodes.DataSource;
            comboBoxTestPartCodes.DisplayMember = "PartCode";
            comboBoxTestPartCodes.SelectedIndex = -1;

        }

        private void populateVertialDataGridColumnsNames()
        {
            dataGridViewVerticalTestResults.EnableHeadersVisualStyles = true;
            dataGridViewVerticalTestResults.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 11F,
                                               System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            //dataGridViewVerticalTestResults.ColumnHeadersDefaultCellStyle.BackColor = Color.White; //Equals(0, 51, 204);
            //dataGridViewVerticalTestResults.ColumnHeadersDefaultCellStyle.ForeColor = Color.Blue; //Equals(0, 51, 204);

            dataGridViewVerticalTestResults.ColumnHeadersDefaultCellStyle.BackColor = Color.DarkBlue;
            dataGridViewVerticalTestResults.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridViewVerticalTestResults.EnableHeadersVisualStyles = false;

            dataGridViewVerticalTestResults.Columns.Add("NAME", "NAME");
            dataGridViewVerticalTestResults.Columns.Add("RESULT", "RESULT");

            for (int i = 0; i < dataGridViewVerticalTestResults.Columns.Count; i++)
            {
                dataGridViewVerticalTestResults.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill; ;
            }

        }

        private async void populateVertialDataGridRows(int testId)
        {
            await getTestStepsAsync(testId);

            dataGridViewVerticalTestResults.Columns.Add("NAME", "NAME");
            dataGridViewVerticalTestResults.Columns.Add("RESULT", "RESULT");

            dataGridViewVerticalTestResults.Rows.Add();
            dataGridViewVerticalTestResults.Rows[0].Cells["NAME"].Value = "OVERALL";

            for (int i = 0; i < _testStepsList.Count; i++)
            {
                if (i < (_testStepsList.Count - 1))
                {
                    dataGridViewVerticalTestResults.Rows.Add();
                }

                var stepRec = _testStepsList[i];
                int j = i + 1;
                dataGridViewVerticalTestResults.Rows[j].Cells["NAME"].Value = stepRec.StepName;
            }

            for (int i = 0; i < dataGridViewVerticalTestResults.Columns.Count; i++)
            {
                dataGridViewVerticalTestResults.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill; ;
            }


            var height = 40;
            foreach (DataGridViewRow dr in dataGridViewVerticalTestResults.Rows)
            {
                height += dr.Height;
            }

            dataGridViewVerticalTestResults.Height = height;

            dataGridViewVerticalTestResults.RowHeadersDefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F,
                                               System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            populateFailureReasonComboBox();

        }
        public async Task getTestStepsAsync(int testId)
        {
            var query = new GetTestStepsQuery(GetTestCategoryId(), testId);
            List<TestStepResponse> results = await Task.Run(() => _mediator.Send(query));
            populateTestStepsList(results);

        }

        public async Task getFailureReasonsAsync()
        {
            var query = new GetFailureReasonsQuery(GetTestCategoryId());
            List<FailureReasonResponse> results = await Task.Run(() => _mediator.Send(query));
            populateFailureReasonList(results);

        }

        public static void populateTestStepsList(List<TestStepResponse> testStepsList)
        {
            _testStepsList = new List<TestStepResponse>();

            foreach (var rec in testStepsList)
            {
                _testStepsList.Add(rec);
            }

            int i = 1;
        }

        public static void populateFailureReasonList(List<FailureReasonResponse> failureReasonsList)
        {
            _failureReasonsList = new List<FailureReasonResponse>();

            foreach (var rec in failureReasonsList)
            {
                _failureReasonsList.Add(rec);
            }
        }

        public int GetTestCategoryId()
        {
            return (int)TestCategory.TestCategoryType.Commander;

        }
        private async void comboBoxTestList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int i = 0;
            /*var test = _testList.Where(x => x.TestName == comboBoxTestList.Text).FirstOrDefault();

            int testId = test.TestId;

            await getTestPartCodesAsync(testId);

            var bindingSource1 = new BindingSource();
            bindingSource1.DataSource = _testPartCodeList;

            comboBoxTestPartCodes.DataSource = bindingSource1.DataSource;
            comboBoxTestPartCodes.DisplayMember = "PartCode";
            comboBoxTestPartCodes.SelectedIndex = -1;*/
        }

        public async Task getTestPartCodesAsync(int testId)
        {
            var query = new GetTestPartCodesQuery(GetTestCategoryId(), testId);
            List<TestPartCodeResponse> results = await Task.Run(() => _mediator.Send(query));
            populateTestPartCodesList(results);

        }

        public static void populateTestPartCodesList(List<TestPartCodeResponse> testPartCodeList)
        {
            _testPartCodeList = new List<TestPartCodeResponse>();

            foreach (var rec in testPartCodeList)
            {
                _testPartCodeList.Add(rec);
            }
        }

        public async Task getFixActionAsync(int failureReasonId)
        {
            var query = new GetFixActionQuery(GetTestCategoryId(), failureReasonId);
            List<FixActionResponse> results = await Task.Run(() => _mediator.Send(query));
            populateFixActionList(results);

        }

        public static void populateFixActionList(List<FixActionResponse> fixActionListList)
        {
            _fixActionList = new List<FixActionResponse>();

            foreach (var rec in fixActionListList)
            {
                _fixActionList.Add(rec);
            }
        }

        private void dataGridViewTestResult_EditingControlShowing(object sender,
                           DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                cbec = e.Control as DataGridViewComboBoxEditingControl;
                cbec.SelectedIndexChanged -= Cbec_SelectedIndexChanged;
                cbec.SelectedIndexChanged += Cbec_SelectedIndexChanged;
            }
        }

        private void Cbec_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbec != null) Console.WriteLine(cbec.SelectedItem.ToString());
        }


        private void CheckHexValue(object sender, KeyPressEventArgs e, int maxLen)
        {
            if ((sender as TextBox).TextLength < maxLen)
            {
                if (char.IsDigit(e.KeyChar) || (e.KeyChar >= 'a' && e.KeyChar <= 'f') || (e.KeyChar >= 'A' && e.KeyChar <= 'F') || (e.KeyChar == '\b'))
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
            else
            {
                if (e.KeyChar == '\b')  // Backspace?
                {
                    e.Handled = false;
                }
                else if ((sender as TextBox).SelectionLength > 0)
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        private void UpdateDataBoxes()
        {
            if (numDlc.Value < 8)
            {
                tbxHex8.Visible = false;
            }
            else
            {
                tbxHex8.Visible = true;
            }

            if (numDlc.Value < 7)
            {
                tbxHex7.Visible = false;
            }
            else
            {
                tbxHex7.Visible = true;
            }

            if (numDlc.Value < 6)
            {
                tbxHex6.Visible = false;
            }
            else
            {
                tbxHex6.Visible = true;
            }

            if (numDlc.Value < 5)
            {
                tbxHex5.Visible = false;
            }
            else
            {
                tbxHex5.Visible = true;
            }

            if (numDlc.Value < 4)
            {
                tbxHex4.Visible = false;
            }
            else
            {
                tbxHex4.Visible = true;
            }

            if (numDlc.Value < 3)
            {
                tbxHex3.Visible = false;
            }
            else
            {
                tbxHex3.Visible = true;
            }

            if (numDlc.Value < 2)
            {
                tbxHex2.Visible = false;
            }
            else
            {
                tbxHex2.Visible = true;
            }

            if (numDlc.Value < 1)
            {
                tbxHex1.Visible = false;
            }
            else
            {
                tbxHex1.Visible = true;
            }
        }

        private void Can232_Load(object sender, EventArgs e)
        {
            string comportSetting = "COM" + Properties.Settings.Default.comport;

            int i = 0;

            //ccmbComPort.SelectedIndex = 0;

            foreach (string portName in System.IO.Ports.SerialPort.GetPortNames())
            {
                cmbComPort.Items.Add(portName);

                if (portName == comportSetting)
                {
                    cmbComPort.SelectedIndex = i;
                }

                i++;

            }

            if (cmbComPort.SelectedIndex < 0)
            {
                cmbComPort.SelectedIndex = 0;
            }

            cmbComSpeed.SelectedIndex = 5;
            cmbCanBitrate.SelectedIndex = 4;
            VersionLb.Text += " " + appVersion;

        }

        private void btnComOpen_Click(object sender, EventArgs e)
        {
            try
            {
                if (!serialPort.IsOpen)
                {
                    // Open Port
                    serialPort.PortName = cmbComPort.SelectedItem.ToString();
                    serialPort.BaudRate = int.Parse(cmbComSpeed.SelectedItem.ToString());
                    serialPort.Open();

                    cmbComPort.Enabled = false;
                    cmbComSpeed.Enabled = false;
                    grpboxCanCommands.Enabled = true;
                    grpboxCanFrameTransmit.Enabled = true;
                    btnComOpen.Enabled = false;
                    btnComClose.Enabled = true;

                    // Set status
                    toolStripStatusLabelComPort.Text = serialPort.PortName;
                    toolStripStatusLabelSpeed.Text = serialPort.BaudRate.ToString();
                }
                else
                {
                    rtboxReceive.Text = "Failed to open COM port";
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnComClose_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Close();
                toolStripStatusLabelComPort.Text = "Closed";
                toolStripStatusLabelSpeed.Text = "-";
                grpboxCanCommands.Enabled = false;
                grpboxCanFrameTransmit.Enabled = false;
                btnComClose.Enabled = false;
                btnComOpen.Enabled = true;
                cmbComPort.Enabled = true;
                cmbComSpeed.Enabled = true;
            }
        }

        public void closeComButton()
        {
            if (serialPort.IsOpen)
            {
                serialPort.Close();
                //toolStripStatusLabelComPort.Text = "Closed";
                //toolStripStatusLabelSpeed.Text = "-";
                grpboxCanCommands.Enabled = false;
                grpboxCanFrameTransmit.Enabled = false;
                btnComClose.Enabled = false;
                btnComOpen.Enabled = true;
                cmbComPort.Enabled = true;
                cmbComSpeed.Enabled = true;
            }
        }

        private void Can232_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Close();
            }
        }

        private void btnCanOpen_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("O\r");
            }
        }

        private void btnCanClose_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("C\r");
            }
        }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("S");
                serialPort.Write(cmbCanBitrate.SelectedIndex.ToString());
                serialPort.Write("\r");
            }
        }

        private void btnCanVersion_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("V\r");
            }
        }

        private void btnCanFlags_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("F\r");
            }
        }

        private void btnSerNo_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("N\r");
            }
        }


        public void serialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            try
            {
                inputQueue.Enqueue(serialPort.ReadExisting());

                this.Invoke(new EventHandler(DisplayText));

                serialPortResponse = true;
            }
            catch (System.TimeoutException ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void DisplayText(object s, EventArgs e)
        {
            if (inputQueue.Count == 0)
                return;

            string receiveBuffer = (string)inputQueue.Dequeue();
            string result = msgProc.processMessage(receiveBuffer);

            if ((result.Length > 0) && (commanderTestForm == null))
            {
                //rtboxReceive.AppendText(result);
            }

            processResponse();
        }

        public void processResponse()
        {
            int ct = msgProc.canMsgList.Count;
            if (ct > 0)
            {
                int i = 0;
                for (i = 0; i < ct; i++)
                {
                    CanMsg cm = msgProc.canMsgList[0];
                    msgProc.canMsgList.RemoveAt(0);

                    //logToFile(cm);

                    //if (commanderTestForm != null) commanderTestForm.processMsg(cm);
                    //{
                    //}

                    ProcessCommTestResp.processTestResponseNew(this, cm);
                }

            }
        }

        //private void logToFile(CanMsg cm)
        //{
        //    //12:35:25 CMD:241 SID: 11 DID:  1  STR:14 3294
        //    string text = String.Format("{0} CMD:{1} SID: {2} DID:{3} ", DateTime.Now.ToString("HH:mm:ss"), cm.Cmd, cm.Sid, cm.Did);

        //    /*if (cm.Cmd == (int)Constants.CAN_COMMANDS.CAN_CMD_TEXT)
        //    {
        //        text += "STR:" + cm.cnvtDataToString() + "\r\n";
        //    }
        //    else if (cm.Cmd == (int)Constants.CAN_COMMANDS.CAN_CMD_MILK_METER_YIELD)
        //    {
        //        int iYield, milking_time, iMaxFlowRate, side;
        //        string yldText = cm.parseYield(out iYield, out milking_time, out iMaxFlowRate, out side);
        //        text += "STR:" + yldText + "\r\n";
        //    }
        //    else if (cm.Cmd == (int)Constants.CAN_COMMANDS.CAN_CMD_MILK_METER_COWID)
        //    {
        //        int cowID = 0;
        //        int side;
        //        string sideStr;

        //        cm.parseCowID(out cowID, out side);

        //        if (side == 0)
        //            sideStr = "LEFT SIDE";
        //        else
        //            sideStr = "RIGHT SIDE";

        //        text += " CowID:" + cowID.ToString() + " " + sideStr + "\r\n";
        //    }
        //    else if (cm.Cmd == (int)Constants.CAN_COMMANDS.CAN_CMD_PULSATION_SYNCH)
        //    {
        //        text += " Pulstaion Synch\r\n";
        //    }
        //    else
        //        text += "DLC:" + cm.DLC.ToString() + " Data:" + cm.Data + "\r\n";*/

        //    // FileName = @"C:\temp\test.txt";
        //    File.AppendAllText(FileName, text);
        //}

        private void tbxID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (cboxExt.Checked == true)
            {
                CheckHexValue(sender, e, 8);
            }
            else
            {
                CheckHexValue(sender, e, 3);
            }
        }

        private void tbxHex1_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckHexValue(sender, e, 2);
        }

        private void tbxHex2_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckHexValue(sender, e, 2);
        }

        private void tbxHex3_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckHexValue(sender, e, 2);
        }

        private void tbxHex4_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckHexValue(sender, e, 2);
        }

        private void tbxHex5_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckHexValue(sender, e, 2);
        }

        private void tbxHex6_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckHexValue(sender, e, 2);
        }

        private void tbxHex7_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckHexValue(sender, e, 2);
        }

        private void tbxHex8_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckHexValue(sender, e, 2);
        }

        private void tbxID_Leave(object sender, EventArgs e)
        {
            if (cboxExt.Checked == true)
            {
                if (tbxID.TextLength == 0)
                {
                    tbxID.Text = "00000000";
                }
                else if (tbxID.TextLength == 1)
                {
                    tbxID.Text = "0000000" + tbxID.Text;
                }
                else if (tbxID.TextLength == 2)
                {
                    tbxID.Text = "000000" + tbxID.Text;
                }
                else if (tbxID.TextLength == 3)
                {
                    tbxID.Text = "00000" + tbxID.Text;
                }
                else if (tbxID.TextLength == 4)
                {
                    tbxID.Text = "0000" + tbxID.Text;
                }
                else if (tbxID.TextLength == 5)
                {
                    tbxID.Text = "000" + tbxID.Text;
                }
                else if (tbxID.TextLength == 6)
                {
                    tbxID.Text = "00" + tbxID.Text;
                }
                else if (tbxID.TextLength == 7)
                {
                    tbxID.Text = "0" + tbxID.Text;
                }
            }
            else
            {
                if (tbxID.TextLength == 0)
                {
                    tbxID.Text = "000";
                }
                else if (tbxID.TextLength == 1)
                {
                    tbxID.Text = "00" + tbxID.Text;
                }
                else if (tbxID.TextLength == 2)
                {
                    tbxID.Text = "0" + tbxID.Text;
                }
            }
            (sender as TextBox).Text = (sender as TextBox).Text.ToUpper();
        }

        private void tbxHex1_Leave(object sender, EventArgs e)
        {
            (sender as TextBox).Text = (sender as TextBox).Text.ToUpper();
        }

        private void tbxHex2_Leave(object sender, EventArgs e)
        {
            (sender as TextBox).Text = (sender as TextBox).Text.ToUpper();
        }

        private void tbxHex3_Leave(object sender, EventArgs e)
        {
            (sender as TextBox).Text = (sender as TextBox).Text.ToUpper();
        }

        private void tbxHex4_Leave(object sender, EventArgs e)
        {
            (sender as TextBox).Text = (sender as TextBox).Text.ToUpper();
        }

        private void tbxHex5_Leave(object sender, EventArgs e)
        {
            (sender as TextBox).Text = (sender as TextBox).Text.ToUpper();
        }

        private void tbxHex6_Leave(object sender, EventArgs e)
        {
            (sender as TextBox).Text = (sender as TextBox).Text.ToUpper();
        }

        private void tbxHex7_Leave(object sender, EventArgs e)
        {
            (sender as TextBox).Text = (sender as TextBox).Text.ToUpper();
        }

        private void tbxHex8_Leave(object sender, EventArgs e)
        {
            (sender as TextBox).Text = (sender as TextBox).Text.ToUpper();
        }

        private void numDlc_ValueChanged(object sender, EventArgs e)
        {
            UpdateDataBoxes();
        }

        private void cboxExt_CheckedChanged(object sender, EventArgs e)
        {
            if (cboxExt.Checked == true)
            {
                tbxID.Text = "00000" + tbxID.Text;
            }
            else
            {
                tbxID.Text = tbxID.Text.Substring(tbxID.Text.Length - 3);
            }
        }

        private void cboxRtr_CheckedChanged(object sender, EventArgs e)
        {
            if (cboxRtr.Checked == true)
            {
                tbxHex1.Visible = false;
                tbxHex2.Visible = false;
                tbxHex3.Visible = false;
                tbxHex4.Visible = false;
                tbxHex5.Visible = false;
                tbxHex6.Visible = false;
                tbxHex7.Visible = false;
                tbxHex8.Visible = false;
            }
            else
            {
                UpdateDataBoxes();
            }
        }

        private void btnSendFrame_Click(object sender, EventArgs e)
        {
            string canFrameData = "";

            if (cboxRtr.Checked == true)        // RTR Frame
            {
                if (cboxExt.Checked == true)    // 29bit Frame
                {
                    canFrameData += "R";
                }
                else                            // 11bit frame
                {
                    canFrameData += "r";
                }
                canFrameData += tbxID.Text + numDlc.Value.ToString();
            }
            else                                // Normal Frame
            {
                if (cboxExt.Checked == true)    // 29bit Frame
                {
                    canFrameData += "T";
                }
                else                            // 11bit frame
                {
                    canFrameData += "t";
                }
                canFrameData += tbxID.Text + numDlc.Value.ToString();
                if (numDlc.Value >= 1)
                {
                    canFrameData += tbxHex1.Text;
                }
                if (numDlc.Value >= 2)
                {
                    canFrameData += tbxHex2.Text;
                }
                if (numDlc.Value >= 3)
                {
                    canFrameData += tbxHex3.Text;
                }
                if (numDlc.Value >= 4)
                {
                    canFrameData += tbxHex4.Text;
                }
                if (numDlc.Value >= 5)
                {
                    canFrameData += tbxHex5.Text;
                }
                if (numDlc.Value >= 6)
                {
                    canFrameData += tbxHex6.Text;
                }
                if (numDlc.Value >= 7)
                {
                    canFrameData += tbxHex7.Text;
                }
                if (numDlc.Value >= 8)
                {
                    canFrameData += tbxHex8.Text;
                }
            }
            if (serialPort.IsOpen)
            {
                serialPort.Write(canFrameData);
                serialPort.Write("\r");
            }
            lblResult.Text = "Resulting command: " + canFrameData + "[CR]";
        }

        private void btnTimeStampOn_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("Z1\r");
            }
        }

        private void btnTimeStampOff_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("Z0\r");
            }
        }

        private void btnAutoOn_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("X1\r");
            }
        }

        private void btnAutoOff_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("X0\r");
            }
        }

        private void btnPollOne_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("P\r");
            }
        }

        private void btnPollAll_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("A\r");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //string text = msgProc.SendCanMessage(int Cmd,int Did,int Sid,short Bytes,byte[] Data)
            string text = msgProc.SendCanMessage(200, 0, 1, 0, null);
            serialPort.Write(text);
            //serialPort.Write("\r");
        }

        public void serialOut(string text)
        {

            serialPort.Write(text);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            rtboxReceive.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string text = msgProc.SendCanMessage(200, 0, 1, 0, null);
            serialPort.Write(text);
        }

        private void buttonCommanderTest_Click(object sender, EventArgs e)
        {
            try
            {
                if (!serialPort.IsOpen)
                {
                    rtboxReceive.AppendText("Please open COM port to proceed." + Environment.NewLine);
                    MessageBox.Show("Please open COM port to proceed.", "Alert");
                }
                else if ((textBoxName.TextLength < 5) || (textBoxJobTraveller.TextLength < 3))
                {
                    rtboxReceive.AppendText("Please enter Name and/or Job Traveller to proceed." + Environment.NewLine);
                    MessageBox.Show("Please enter Name and/or Job Traveller to proceed.", "Alert");
                }
                else if (comboBoxTestSelect.Text.Length < 3)
                {
                    rtboxReceive.AppendText("Please select a Test to proceed." + Environment.NewLine);
                    MessageBox.Show("Please select a Test to proceed.", "Alert");
                }
                else
                {
                    if (serialPortResponse)
                    {
                        rtboxReceive.AppendText(textBoxName.Text + Environment.NewLine);
                        rtboxReceive.AppendText(textBoxJobTraveller.Text + Environment.NewLine);
                        if (commanderTestForm == null)
                        {
                            commanderTestForm = new CommanderTestForm(this, textBoxName.Text, textBoxJobTraveller.Text, comboBoxTestSelect.SelectedIndex);
                        }
                        //if (commanderTestForm == null) commanderTestForm = new CommanderTestForm(this);
                        commanderTestForm.Show();
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Check Comport - no devices detected", "Alert");
                    }
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Settings(object sender, EventArgs e)
        {
            var settingsForm = new Settings();
            settingsForm.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxTestSelect_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, Color.White, ButtonBorderStyle.None);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Close();
                toolStripStatusLabelComPort.Text = "Closed";
                toolStripStatusLabelSpeed.Text = "-";
                grpboxCanCommands.Enabled = false;
                grpboxCanFrameTransmit.Enabled = false;
                btnComClose.Enabled = false;
                btnComOpen.Enabled = true;
                cmbComPort.Enabled = true;
                cmbComSpeed.Enabled = true;
            }

            this.Close();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoginForm login = new LoginForm();
            login.Show();

        }

        private void btnRunTest_Click(object sender, EventArgs e)
        {
            if (!serialPort.IsOpen)
            {
                rtboxReceive.AppendText("Please open COM port to proceed." + Environment.NewLine);
                MessageBox.Show("Please open COM port to proceed.", "Alert");
                return;
            }

			string canClose = "C\r";
			serialOut(canClose);
            Utils.delay(100);
            string canOpen = "O\r";
            serialOut(canOpen);

            if (failureReasonRequired())
            {
                MessageBox.Show("Select Failure Reason.");
                return;
            }

            if (actionTakenRequired())
            {
                MessageBox.Show("Select Action Taken.");
                return;
            }

            resetResultsSection();

            if (!validateUserInputs())
            {
                MessageBox.Show("Missing Test Details.");
                return;
            }

            if (!validateSerial())
            {
                MessageBox.Show("Invalid Serial Number.");
                return;
            }

            //if (serialPortResponse)
            //{
            //    rtboxReceive.AppendText(textBoxName.Text + Environment.NewLine);
            //    rtboxReceive.AppendText(textBoxJobTraveller.Text + Environment.NewLine);
            beginTest();
            //}
            //else
            //{
            //    MessageBox.Show("Check Comport - no devices detected", "Alert");
            //}

            if (_testMode)
            {
                if (_testCounterFail == 0)
                {
                    simulateTestResults("FAIL");
                    _testCounterFail++;
                }
                else
                {
                    if (comboBoxFailureReason.SelectedValue == null)
                    {
                        MessageBox.Show("Select Failure Reason");
                        return;
                    }

                    if (comboBoxFixAction1.SelectedValue == null)
                    {
                        MessageBox.Show("Select Action Taken");
                        return;
                    }

                    simulateTestResults("PASS");
                }
            }



            //         if (_testCounterFail == 0)
            //{
            //simulateTestResults("FAIL");

            //	_testCounterFail++;
            //}

            //else
            //{
            //             if (comboBoxFailureReason.SelectedValue == null)
            //             {
            //                 MessageBox.Show("Select Failure Reason");
            //                 return;
            //             }

            //             if (comboBoxFixAction1.SelectedValue == null)
            //             {
            //                 MessageBox.Show("Select Action Taken");
            //                 return;
            //             }

            //             simulateTestResults("PASS");



            //             //if Overall test result = Fail - displaY

            //             //comboBoxFailureReason.Visible = false;
            //             //lblFailureReason.Visible = false;
            //             //comboBoxFixAction1.Visible = true;
            //             //labelFixAction.Visible = true; 
            //         }

            SaveTestResult();
            //colourCodeResults();

            //iF OVERALL test = PASS 
            //check if any other test results exist and if so did the latest fail
            //if yes, display fix action check box
            if ((string)dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value == "FAIL" || (string)dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value == "ABORTED")
            {
                comboBoxFailureReason.Visible = true;
                lblFailureReason.Visible = true;
                comboBoxFixAction1.Visible = true;
                labelFixAction.Visible = true;
                setTestFieldInputsEditableStatus(false);
            }

            if ((string)dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value == "PASS")
            {
                //Check if fix action needs to be populated
                textBoxSerialNumber.Text = DefaultSerial.ToString();
            }

        }

        private void setTestFieldInputsEditableStatus(bool enableEdit)
        {
            textBoxJobTraveller.Enabled = enableEdit;
            textBoxSerialNumber.Enabled = enableEdit;
            comboBoxTestList.Enabled = enableEdit;
            //comboBoxTestPartCodes.Enabled = enableEdit;
            textBoxPartCode.Enabled = enableEdit;

        }


        private void simulateTestResults(string result)
        {
            textBoxStartTime.Text = System.DateTime.Now.ToString();

            for (int i = 0; i < dataGridViewVerticalTestResults.Rows.Count; i++)
            {
                var stepName = dataGridViewVerticalTestResults.Rows[i].Cells["NAME"].Value.ToString();

                if (stepName == "GET_VERSION")
                {
                    dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value = "Ver 3.25";
                }
                else if (stepName == "GET_DEVID")
                {
                    dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value = "24689111000";
                }
                else
                {
                    dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value = result;
                }
            }

            textBoxEndTime.Text = System.DateTime.Now.ToString();
        }

        public void resultToDisplay(string test) {

            switch(test)
			{
                case "CC_AIR":
					{
                        if (factTest.CCAirTestPassed)
                        {
                            setTestStepResult("CC_AIR", "PASS");
                        }
                        else
                        {
                            setTestStepResult("CC_AIR", "FAIL");
                            setTestStepResult("OVERALL", "FAIL");
                        }
                        break;
					}
                case "CC_WATER":
                    {
                        if (factTest.CCWaterTestPassed)
                        {
                            setTestStepResult("CC_WATER", "PASS");
                        }
                        else
                        {
                            setTestStepResult("CC_WATER", "FAIL");
                            setTestStepResult("OVERALL", "FAIL");
                        }
                        break;
                    }
				//case "PROX_SWITCH":
				//	{
				//		if (factTest.ProxSwitchFirstTestComplete && factTest.ProxSwitchSecondTestComplete)
				//		{
				//			break;
				//		}

				//		if (!factTest.ProxSwitchTestPassed)
				//		{
				//			setTestStepResult("PROX_SWITCH", "FAIL");
				//			setTestStepResult("OVERALL", "FAIL");
				//		}
				//		else
				//		{
				//			setTestStepResult("PROX_SWITCH", "PASS");
				//			setTestStepResult("OVERALL", "PASS");
				//		}

				//		break;
				//	}
				//case "SMART_START_REED_SWITCH":
				//	{
				//		if (!factTest.SmartStartReedSwitchTestComplete)
				//		{
				//			break;
				//		}
				//		if (factTest.SmartStartReedSwitchTestPassed)
				//		{
				//			setTestStepResult("SMART_START_REED_SWITCH", "PASS");
				//		}
				//		else
				//		{
				//			setTestStepResult("SMART_START_REED_SWITCH", "FAIL");
				//			setTestStepResult("OVERALL", "FAIL");
				//		}
				//		break;
				//	}
				//case "DIVERT_LINE_REED_SWITCH":
				//	{
				//		if (!factTest.DivertLineReedSwitchTestComplete)
				//		{
				//			break;
				//		}
				//		if (factTest.DivertLineReedSwitchTestPassed)
				//		{
				//			setTestStepResult("DIVERT_LINE_REED_SWITCH", "PASS");
				//		}
				//		else
				//		{
				//			setTestStepResult("DIVERT_LINE_REED_SWITCH", "FAIL");
				//			setTestStepResult("OVERALL", "FAIL");
				//		}
				//		break;
				//	}
				//case "MILK_METER_LEAD_WIRE":
				//	{
				//		if (!factTest.MilkMeterLeadWireTestComplete)
				//		{
				//			break;
				//		}
				//		if (factTest.MilkMeterLeadWireTestPassed)
				//		{
				//			setTestStepResult("MILK_METER_LEAD_WIRE", "PASS");
				//		}
				//		else
				//		{
				//			setTestStepResult("MILK_METER_LEAD_WIRE", "FAIL");
				//			setTestStepResult("OVERALL", "FAIL");
				//		}
				//		break;
				//	}
				//case "MILKLINE_OP":
				//	{
				//		if (!factTest.MilkLineOPTestComplete)
				//		{
				//			break;
				//		}
				//		if (factTest.MilkLineOPTestPassed)
				//		{
				//			setTestStepResult("MILKLINE_OP", "PASS");
				//		}
				//		else
				//		{
				//			setTestStepResult("MILKLINE_OP", "FAIL");
				//			setTestStepResult("OVERALL", "FAIL");
				//		}
				//		break;
				//	}
				//case "DIVERT_LINE_OP":
				//	{
				//		if (!factTest.DivertLineOPTestComplete)
				//		{
				//			break;
				//		}
				//		if (factTest.DivertLineOPTestPassed)
				//		{
				//			setTestStepResult("DIVERT_LINE_OP", "PASS");
				//		}
				//		else
				//		{
				//			setTestStepResult("DIVERT_LINE_OP", "FAIL");
				//			setTestStepResult("OVERALL", "FAIL");
				//		}
				//		break;
				//	}
				//case "PULSATION":
				//	{
				//		if (!factTest.PulsationTestComplete)
				//		{
				//			break;
				//		}
				//		if (factTest.PulsationTestPassed)
				//		{
				//			setTestStepResult("PULSATION", "PASS");
				//		}
				//		else
				//		{
				//			setTestStepResult("PULSATION", "FAIL");
				//			setTestStepResult("OVERALL", "FAIL");
				//		}
				//		break;
				//	}
				//case "ACR_RAM":
				//	{
				//		if (!factTest.AcrRamTestComplete)
				//		{
				//			break;
				//		}
				//		if (factTest.AcrRamTestPassed)
				//		{
				//			setTestStepResult("ACR_RAM", "PASS");
				//		}
				//		else
				//		{
				//			setTestStepResult("ACR_RAM", "FAIL");
				//			setTestStepResult("OVERALL", "FAIL");
				//		}
				//		break;
				//	}
				//case "JETSTREAM":
				//	{
				//		if (!factTest.JetStreamTestComplete)
				//		{
				//			break;
				//		}
				//		if (factTest.JetStreamTestPassed)
				//		{
				//			setTestStepResult("JETSTREAM", "PASS");
				//		}
				//		else
				//		{
				//			setTestStepResult("JETSTREAM", "FAIL");
				//			setTestStepResult("OVERALL", "FAIL");
				//		}
				//		break;
				//	}
				//case "RETENTION":
				//	{
				//		if (!factTest.RetentionTestComplete)
				//		{
				//			break;
				//		}
				//		if (factTest.RetentionTestPassed)
				//		{
				//			setTestStepResult("RETENTION", "PASS");
				//		}
				//		else
				//		{
				//			setTestStepResult("RETENTION", "FAIL");
				//			setTestStepResult("OVERALL", "FAIL");
				//		}
				//		break;
				//	}
				//case "TEAT_SPRAY":
				//	{
				//		if (!factTest.TeatSprayTestComplete)
				//		{
				//			break;
				//		}
				//		if (factTest.TeatSprayTestComplete)
				//		{
				//			setTestStepResult("TEAT_SPRAY", "PASS");
				//		}
				//		else
				//		{
				//			setTestStepResult("TEAT_SPRAY", "FAIL");
				//			setTestStepResult("OVERALL", "FAIL");
				//		}
				//		break;
				//	}
				default:
					{
                        break;
					}
            }
         }

        public void setTestStepResult(string step, string result)
        {
            int j = 0;
            
            for (int i = 0; i < dataGridViewVerticalTestResults.Rows.Count; i++)
            {
                var stepName = dataGridViewVerticalTestResults.Rows[i].Cells["NAME"].Value.ToString();

                if (stepName == step)
				{
                    dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value = result;
                    dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Style.BackColor = ((result == "FAIL") || result == ("ABORTED")) ? Color.OrangeRed : Color.White;

                }
            }
        }

        public void failOutstandingTests()
        {
            int j = 0;

            for (int i = 0; i < dataGridViewVerticalTestResults.Rows.Count; i++)
            {
                if (dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value.ToString() == "")
				{
                    dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value = "FAIL";
                    dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Style.BackColor = Color.OrangeRed;

                }

            }
        }

        public bool didAllTestsPass()
        {
            bool allPass = true;

            for (int i = 0; i < dataGridViewVerticalTestResults.Rows.Count; i++)
            {
                if (dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value.ToString() == "FAIL")
                {
                    allPass = false;

                }

            }

            return allPass;
        }

        private void resetResultsSection()
		{
            clearTextFields();
            _lastSavedTestSessionId = 0;
            clearCurrentResults();
            comboBoxFailureReason.Visible = false;
            lblFailureReason.Visible = false;
            comboBoxFixAction1.Visible = false;
            labelFixAction.Visible = false;
            comboBoxFailureReason.SelectedIndex = -1;
            comboBoxFixAction1.SelectedIndex = -1;


        }

        private void clearCurrentResults()
		{
            for (int i = 0; i < dataGridViewVerticalTestResults.Rows.Count; i++)
            {
                dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value = "";
                dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Style.BackColor = Color.White;
            }
        }
        private void SaveTestResult()
		{
            var t = (TestResponse)comboBoxTestList.SelectedItem;

            var createTestSessionCommand = new CreateTestSessionCommand
            {
                TestCategoryId = GetTestCategoryId(),
                TestId = t.TestId,
                //TestSessionTypeId = 3,
                //PartCode = comboBoxTestPartCodes.Text.ToString(),
                PartCode = textBoxPartCode.Text.ToString(),
                EmployeeBadge = EmployeeRepository.Employee.EmployeeReference,
                EmployeName = textBoxName.Text,
                EmployeeDept = EmployeeRepository.Employee.SubDepartment,
                SupervisorEmail = EmployeeRepository.Employee.SupervisorEmail,
                PCIPAddress = textBoxIPAddress.Text,
                PCName = textBoxPCName.Text,
                PCOpSystem = textBoxPCOpSystem.Text + " " + textBoxOpSystemVer.Text,
                Comport = cmbComPort.SelectedItem.ToString(),
                JobTraveller = textBoxJobTraveller.Text,
                SerialNo = textBoxSerialNumber.Text,
                JigDevID = factTest.JigDevID != "" ? factTest.JigDevID : "Unknown",
                StartDateTime = DateTime.Parse(textBoxStartTime.Text),
                EndDateTime = DateTime.Parse(textBoxEndTime.Text),
                OverallResult = (string)dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value == "PASS" ? true : false,
                IsAborted = factTest.CurrentTestAborted,
                TestSessionResultCommand = new List<TestSessionResultRequest>()

            };

   //         if (factTest.CurrentTestAborted)
			//{
   //             createTestSessionCommand.TestSessionTypeId = 3;
   //             createTestSessionCommand.IsAborted = true;
   //         }

            for (int i = 1; i < dataGridViewVerticalTestResults.Rows.Count; i++)
            {
                var stepCheck = dataGridViewVerticalTestResults.Rows[i].Cells["NAME"].Value.ToString();

                var step = _testStepsList.Where(s => s.StepName == stepCheck).FirstOrDefault();

                string textResult;
                bool failPassResult;

                if (stepCheck == "GET_VERSION" || stepCheck == "GET_DEVID")
                {

                    textResult = dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value.ToString() ?? "";
                    failPassResult = textResult == "FAIL" ? false : true;

                    if (stepCheck == "GET_DEVID")
                    {
                        createTestSessionCommand.DevId = textResult;
                    }

                    if (stepCheck == "GET_VERSION")
                    {
                        createTestSessionCommand.Version = textResult;
                    }
                }
                else
                {
                    string passOrFail = dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value.ToString();
                    failPassResult = passOrFail == "PASS" ? true : false;
                    textResult = "";
                }

                var testSessionResult = new TestSessionResultRequest
                {
                    StepId = step.StepId,
                    FailPassResult = failPassResult,
                    TextResult = textResult
                };

                createTestSessionCommand.TestSessionResultCommand.Add(testSessionResult);

            }

            saveTest(createTestSessionCommand);
        }

        public async void saveTest(CreateTestSessionCommand createTestSessionCommand)
        {
            var response = await Task.Run(() => _mediator.Send(createTestSessionCommand));
            _lastSavedTestSessionId = response;
        }

        private void colourCodeResults()
		{
            for (int i = 0; i < dataGridViewVerticalTestResults.Rows.Count; i++)
            {
               if ((string)dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value == "FAIL" || (string)dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value == "ABORTED")
				{
                    dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Style.BackColor = Color.OrangeRed;
                        //dataGridView1.Rows[i].Cells[7].Style.BackColor = Color.LightGreen;
                }

                if ((string)dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Value == "PASS")
                {
                    dataGridViewVerticalTestResults.Rows[i].Cells["RESULT"].Style.BackColor = Color.White;
                    //dataGridView1.Rows[i].Cells[7].Style.BackColor = Color.LightGreen;
                }
            }
        }

        private bool validateUserInputs()
        {
            bool valid = true;
            //if ((comboBoxTestList.SelectedValue == null) || (comboBoxTestPartCodes.SelectedValue == null) || (textBoxJobTraveller.Text == "") || (textBoxSerialNumber.Text == ""))
            if ((comboBoxTestList.SelectedValue == null) || (textBoxPartCode.Text == "") || (textBoxJobTraveller.Text == "") || (textBoxSerialNumber.Text == ""))
            {
                valid = false;
            }

            if (textBoxSerialNumber.Text == DefaultSerial.ToString())
			{
                valid = false;
            }
            return valid;
        }

        private bool validateSerial()
        {
            bool valid = true;

            if ((textBoxSerialNumber.Text == ""))
            {
                valid = false;
            }

            if (textBoxSerialNumber.Text == DefaultSerial.ToString())
            {
                valid = false;
            }

            if (textBoxSerialNumber.Text.Length < DefaultSerial.ToString().Length)
            {
                valid = false;
            }

            return valid;
        }

        private bool failureReasonRequired()
        {
            bool required = false;
            if ((string)dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value == "FAIL" || (string)dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value == "ABORTED")
            {
                if (comboBoxFailureReason.Visible == true && comboBoxFailureReason.SelectedValue == null)
                {
                    required = true;
                }

            }
            return required;
        }

        private bool actionTakenRequired()
        {
            bool required = false;
            if ((string)dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value == "FAIL" || (string)dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value == "ABORTED")
            {
                if (comboBoxFixAction1.Visible == true && comboBoxFixAction1.SelectedValue == null)
                {
                    required = true;
                }

            }
            return required;
        }

        private void comboBoxFailureReason_SelectedIndexChanged(object sender, EventArgs e)
		{
            if (!comboBoxFailureReason.Visible)
			{
                return;
			}

            if (comboBoxFailureReason.ValueMember.ToString() != "")
            {
                //Clear static lists
                //Clear datagrids
                var fr = (FailureReasonResponse)comboBoxFailureReason.SelectedItem;
                if (fr != null)
                {
                    if (_lastSavedTestSessionId > 0)
					{
                        var createUpdateTestSessionFailureCommand = new CreateUpdateTestSessionFailureCommand
                        {
                            TestSessionId = _lastSavedTestSessionId,
                            FailureReasonId = fr.FailureReasonId
                        };
                        saveTestSessionFailureReason(createUpdateTestSessionFailureCommand);

                    }
                    populateFixActionComboBox(fr.FailureReasonId);

                    //var fa = (FixActionResponse)comboBoxFailureReason.SelectedItem;
                    //if (comboBoxFixAction1.ValueMember.ToString() != "")
                    if(comboBoxFixAction1.SelectedIndex != -1)
                    {
                        setTestFieldInputsEditableStatus(true);
                    }

                    else
					{
                        setTestFieldInputsEditableStatus(false);
                    }

                }
            }
        }

        //private void comboBoxFixAction1_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (comboBoxFixAction1.ValueMember.ToString() != "")
        //    {
        //        //Clear static lists
        //        //Clear datagrids
        //        var fa = (FixActionResponse)comboBoxFixAction1.SelectedItem;
        //        if (fa != null)
        //        {
        //            if (_lastSavedTestSessionId > 0)
        //            {
        //                var createUpdateTestSessionFixActionCommand = new CreateUpdateTestSessionFixActionCommand
        //                {
        //                    TestSessionId = _lastSavedTestSessionId,
        //                    FixActionId = fa.FixActionId
        //                };
        //                saveTestSessionFixAction(createUpdateTestSessionFixActionCommand);

        //            }
        //            //populateFixActionComboBox(fa.FixActionId);

        //        }
        //    }
        //}

        public async void saveTestSessionFailureReason(CreateUpdateTestSessionFailureCommand createUpdateTestSessionFailureCommand)
        {
            var response = await Task.Run(() => _mediator.Send(createUpdateTestSessionFailureCommand));
            var affectedRows = response;
            
        }

        public async void saveTestSessionFixAction(CreateUpdateTestSessionFixActionCommand createUpdateTestSessionFixActionCommand)
        {
            var response = await Task.Run(() => _mediator.Send(createUpdateTestSessionFixActionCommand));
            var affectedRows = response;

        }

		private void comboBoxFixAction1_SelectedIndexChanged_1(object sender, EventArgs e)
		{
            if (comboBoxFixAction1.ValueMember.ToString() != "")
            {
                //Clear static lists
                //Clear datagrids
                var fa = (FixActionResponse)comboBoxFixAction1.SelectedItem;
                if (fa != null)
                {
                    if (_lastSavedTestSessionId > 0)
                    {
                        var createUpdateTestSessionFixActionCommand = new CreateUpdateTestSessionFixActionCommand
                        {
                            TestSessionId = _lastSavedTestSessionId,
                            FixActionId = fa.FixActionId
                        };
                       saveTestSessionFixAction(createUpdateTestSessionFixActionCommand);

                    }
                    //populateFixActionComboBox(fa.FixActionId);

                    if (comboBoxFailureReason.ValueMember.ToString() != "")
                    {
                        setTestFieldInputsEditableStatus(true);
                    }

                }
            }

        }

        /*private async void populateHorizontalDataGridColumnsNames(int testId)
        {
            await getTestStepsAsync(testId);

            foreach (var step in _testStepsList)
            {
                //int i = 0;
                dataGridViewTestResult.Columns.Add(step.StepName, step.StepName);
            }

            populateVertialDataGridColumnsNames(testId);
            populateFailureReasonColumn();

        }

        private async void populateFailureReasonColumn()
        {
            await getFailureReasonsAsync();

            DataGridViewComboBoxColumn frCmb = new DataGridViewComboBoxColumn();
            frCmb.HeaderText = "FAIL REASON";
            frCmb.Name = "FAIL REASON";

            foreach (var reason in _failureReasonsList)
            {
                frCmb.Items.Add(reason.FailureReasonName);
            }

            dataGridViewTestResult.Columns.Add(frCmb);

            dataGridViewTestResult.CellValueChanged += new DataGridViewCellEventHandler(dataGridViewTestResult_CellValueChanged);
            dataGridViewTestResult.CurrentCellDirtyStateChanged += new EventHandler(dataGridViewTestResult_CurrentCellDirtyStateChanged);


            DataGridViewComboBoxColumn faCmb = new DataGridViewComboBoxColumn();
            faCmb.HeaderText = "FIX ACTION";
            faCmb.Name = "FIX ACTION";
            dataGridViewTestResult.Columns.Add(faCmb);

            populateFailureReasonComboBox();

        }

        void dataGridViewTestResult_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridViewTestResult.IsCurrentCellDirty)
            {
                dataGridViewTestResult.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dataGridViewTestResult_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var colIndexFailReason = dataGridViewTestResult.Rows[e.RowIndex].Cells["FAIL REASON"].ColumnIndex;
            if (e.ColumnIndex == colIndexFailReason)
            {
                //ShowCellValue(e.RowIndex, e.ColumnIndex);
                if (((DataGridViewComboBoxColumn)dataGridViewTestResult.Columns["FIX ACTION"]).Items.Count > 0) ;
                {
                    ((DataGridViewComboBoxColumn)dataGridViewTestResult.Columns["FIX ACTION"]).Items.Clear();

                }
                var failureReason = dataGridViewTestResult.Rows[e.RowIndex].Cells["FAIL REASON"].Value.ToString();
                var failureReasonRec = _failureReasonsList.Where(x => x.FailureReasonName == failureReason).FirstOrDefault();
                populateFixActionColumn(failureReasonRec.FailureReasonId);
            }

            var colIndexFixAction = dataGridViewTestResult.Rows[e.RowIndex].Cells["FIX ACTION"].ColumnIndex;
            if (e.ColumnIndex == colIndexFixAction)
            {
                ShowCellValue(e.RowIndex, e.ColumnIndex);
            }
        }
        private async void populateFixActionColumn(int failureReasonId)
        {

            await getFixActionAsync(failureReasonId);

            foreach (var act in _fixActionList)
            {
                ((DataGridViewComboBoxColumn)dataGridViewTestResult.Columns["FIX ACTION"]).Items.Add(act.FixActionName);
            }

            dataGridViewTestResult.CellValueChanged += new DataGridViewCellEventHandler(dataGridViewTestResult_CellValueChanged);
            dataGridViewTestResult.CurrentCellDirtyStateChanged += new EventHandler(dataGridViewTestResult_CurrentCellDirtyStateChanged);

        }*/


        private void Can232_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Can232_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Can232_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }

        private void btnAbortTest_Click(object sender, EventArgs e)
        {
            testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
            //progressBar1.Value = (int)Constants.CommanderTestPhase.TEST_FINISHED;
            factTest.CurrentTestAborted = true;
            //processFactoryTest();
            btnAbortTest.Enabled = false;
        }

        public void beginTest() 
        {
            try
            {
                keyPadCounter = 0;
                btnRunTest.Enabled = false;
                btnSettings.Enabled = false;
                btnLogout.Enabled = false;
                btnExit.Enabled = false;
                btnAbortTest.Enabled = true;
                factTest.CurrentTestAborted = false;
                factoryTestEnabled = !factoryTestEnabled;
                logTest("Test Start Time: " + DateTime.Today.ToString("d") + " " + DateTime.Now.ToString("HH:mm:ss"));
                //progressBar1.Value = 0;
                //progressBar1.Maximum = Enum.GetNames(typeof(Constants.CommanderTestPhase)).Length - 1;
                //progressBar1Lb.ForeColor = Color.DarkBlue;
                //progressBar1Lb.Font = new Font(progressBar1Lb.Font, FontStyle.Bold);
                //progressBar1Lb.Text = "Starting Test";
                logTest("Starting Test");
                textBoxStartTime.Text = System.DateTime.Now.ToString();

                for (testPhase = 0; (int)testPhase < Enum.GetNames(typeof(Constants.CommanderTestPhase)).Length;)
                {
                    if (factTest.CurrentTestAborted) 
                    {
                        testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                        //progressBar1.Value = (int)Constants.CommanderTestPhase.TEST_FINISHED;
                    }

                    processFactoryTest();
                    //simulateFactoryTest();
                }

                //progressBar1Lb.ForeColor = Color.Black;
                //progressBar1Lb.Font = new Font(progressBar1Lb.Font, FontStyle.Regular);
                //progressBar1Lb.Text = "Progress";
                textBoxEndTime.Text = System.DateTime.Now.ToString();
                lblTestFeedback.Text = "Overall Result: ";
                //lblTestFeedbackText.Text = "PASS";
                lblTestFeedbackText.Text = (string)dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value;
                logTest("Progress");

                //Utils.delayThread(5000);

                btnRunTest.Text = "Run next Test";
                btnRunTest.Enabled = true;
                btnLogout.Enabled = true;
                btnExit.Enabled = true;
                btnSettings.Enabled = true;
                btnAbortTest.Enabled = false;

                //Utils.delay(300);

                //if (mainForm.autoPrintRpt)
                //{
                //    autoPrintTest();
                //}
            }

            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void simulateFactoryTest()
        {
            switch (testPhase)
            {
                case Constants.CommanderTestPhase.SET_FACTORY_MODE:
                    {
                        logTest("Entering Factory Mode");
                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "Entering Test Mode";
                        testPhase++;
                        break;

                    }
                case Constants.CommanderTestPhase.GET_VERSION:
                    {
                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "Get Software Version";
                        logTest("Get Software Version");
                        factTest.LastVersion = "2." + "34";
                        setTestStepResult("GET_VERSION", factTest.LastVersion);
                        
                        testPhase++;
                        break;
                    }
                case Constants.CommanderTestPhase.GET_DEVID:
                    {
                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "GET_DEVID";
                        logTest("GET_DEVID");
                        factTest.LastDevID = "";
                        factTest.LastDevID = "987654321";
                        setTestStepResult("GET_DEVID", factTest.LastDevID);
                        testPhase++;
                        testPhase++;
                        break;
                    }
                case Constants.CommanderTestPhase.TEST_IO:
                    {
                        logTest("Test_IO");
                        testPhase++;
                        break;
                    }

                case Constants.CommanderTestPhase.COMMON_LOOMS:
                    {
                        logTest("Common Loom Tests");
                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "COMMON_LOOMS";
                        for (testCommon = 0; (int)testCommon < Enum.GetNames(typeof(CommanderLoomTests.CommanderCommonLoom)).Length;)
                        {
                            if (factTest.CurrentTestAborted)
                            {
                                lblTestFeedback.Text = "Test Phase";
                                lblTestFeedbackText.Text = "TEST_ABORTED";
                                testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }
                            string testCommonString = testCommon.ToString();
                            //logTest(testCommonString);

                            CommanderLoomTests.performCommonLoomsTestSimulate(this, testCommonString);
                            Utils.delayThread(100);
                            testCommon++;
                        }
                        testPhase++;
                        break;

                        //lblTestFeedback.Text = "Test Phase";
                        //lblTestFeedbackText.Text = "COMMON_LOOMS";
                        //logTest("Common Loom Tests");                        
                        //testPhase++;
                        //break;
                    }
                case Constants.CommanderTestPhase.COMMON_ROT_LOOMS:
                    {
                        //lblTestFeedback.Text = "Test Phase";
                        //lblTestFeedbackText.Text = "COMMON_ROT_LOOMS";
                        //logTest("Common Rotary Loom Tests");
                        //testPhase++;
                        //break;

                        //if (Selected_Test > (int)CommanderLoomTests.ProductNums.SSDUTest20)
                        if (labelTestNameDisplay.Text.Contains("RTY"))
                        {
                            lblTestFeedback.Text = "Test Phase";
                            lblTestFeedbackText.Text = "COMMON_ROT_LOOMS";
                            logTest("Common Rotary Loom Tests");
                            for (testCommonRot = 0; (int)testCommonRot < Enum.GetNames(typeof(CommanderLoomTests.CommanderCommonRotLoom)).Length;)
                            {
                                if (factTest.CurrentTestAborted)
                                {
                                    lblTestFeedback.Text = "Test Phase";
                                    lblTestFeedbackText.Text = "TEST_ABORTED";
                                    testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                    return;
                                }
                                string testCommonRotString = testCommonRot.ToString();
                                logTest(testCommonRotString);
                                CommanderLoomTests.performCommonLoomsTestSimulate(this, testCommonRotString);
                                Utils.delayThread(100);
                                testCommonRot++;
                            }
                        }
                        testPhase++;
                        break;
                    }
                case Constants.CommanderTestPhase.PRODUCT_SPECIFIC_LOOMS:
                    {
                        //lblTestFeedback.Text = "Test Phase";
                        //lblTestFeedbackText.Text = "PRODUCT_SPECIFIC_LOOMS";
                        //logTest("Product Specific Loom Tests");
                        //testPhase++;
                        //break;

                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "PRODUCT_SPECIFIC_LOOMS";
                        logTest("Product Specific Loom Tests");
                        CommanderLoomTests.selectProductLoomsTestSimulation(this);
                        //Utils.delay(20);
                        Utils.delayThread(100);

                        if (factTest.CurrentTestAborted)
                        {
                            lblTestFeedback.Text = "Test Phase";
                            lblTestFeedbackText.Text = "TEST_ABORTED";
                            testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                            return;
                        }
                        testPhase++;
                        break;
                    }
                case Constants.CommanderTestPhase.KEYPAD_TEST:
                    {
                        setTestStepResult("KEYPAD", "PASS");
                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "KEYPAD_TEST";
                        logTest("KEYPAD TEST");
                        testPhase++;

                        break;
                    }
                case Constants.CommanderTestPhase.TEST_FINISHED:
                    {
                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "TEST_FINISHED";
                        logTest("*****************************************");
                        logTest("TEST FINISHED");
                        logTest("*****************************************");
                        testPhase++;
                        break;
                    }
            }
        }

        public void processFactoryTest()
        {
            if (factTest.CurrentTestAborted)
            {
                testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
            }

            logTest("TestPhase : " + testPhase.ToString());
            
            factTest.LastVersion = "";

            switch (testPhase)
            {
				//case Constants.CommanderTestPhase.GET_IAP_VERSION:
				//	{
    //                    btnAbortTest.Enabled = false;
    //                    factTest.IAPVersion = "";
    //                    factTest.IAPVersionTestComplete = false;
    //                    lblTestFeedback.Text = "Test Phase";
				//		lblTestFeedbackText.Text = "GET_IAP_VERSION";
				//		logTest("Get IAP Version");
				//		softReset(DefaultAddr);
				//		//Utils.delayThread(6000);
				//		//getVersion();
				//		//Utils.delay(100);                        
				//		//Utils.delayThread(3000);

				//		Stopwatch swIAP = new Stopwatch();
				//		swIAP.Start();

				//		while (factTest.IAPVersion == "")
				//		{
    //                        //Utils.delay(500);//Original
    //                        Utils.delay(250);
    //                        getVersion();

    //                        //if ((factTest.IAPVersion == "") && (swIAP.ElapsedMilliseconds > 5000)) //Original
    //                        if ((factTest.IAPVersion == "") && (swIAP.ElapsedMilliseconds > 10000))
    //                        {
    //                            //setTestStepResult(_testStepsList[0].StepName, "FAIL");
    //                            factTest.IAPVersion = "FAIL";
    //                            //setTestStepResult("GET_IAP_VERSION", "FAIL");
    //                            factTest.IAPVersionTestComplete = true;
    //                        }
							
				//		}

    //                    if (factTest.IAPVersion == "PASS")
    //                    {
    //                        //setTestStepResult("GET_IAP_VERSION", "PASS");
    //                        //Utils.delay(4000);//Original
    //                        factTest.IAPVersionTestComplete = true;
    //                        Utils.delay(10000);
                           
                            
    //                    }
    //                    setTestStepResult("GET_IAP_VERSION", factTest.IAPVersion);
    //                    testPhase++;
    //                    break;                        
				//	}
				case Constants.CommanderTestPhase.SET_FACTORY_MODE:
                    {

                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "Entering Test Mode";
                        //logTest("Entering Factory Mode");
                        factTest.Clear();
                        CommanderSecurityHandshake();
                        getProgType();
                        //Utils.delay(2000);
                        getWeighCalFactor();
                        //Utils.delay(2000);
                        FactoryTestMode();
                        //Utils.delay(2000);
                        testPhase++;
                        break;
                    }
                case Constants.CommanderTestPhase.GET_VERSION:
                    {
                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "GET_VERSION";
                        logTest("Get Software Version");
                        getVersion();
                        //Utils.delay(100);                        
                        Utils.delayThread(3000);

                        Stopwatch swVer = new Stopwatch();
                        swVer.Start();

                        while (factTest.LastVersion == "")
                        {
                            if ((factTest.LastVersion == "") && (swVer.ElapsedMilliseconds > 5000))
                            {
                                logTest("COMMS ISSUE - NO RESPONSE FROM DEVICE! ABORTING TEST");
                                factTest.CurrentTestAborted = true;
                                testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                logTest("TEST ABORTED");

                                
                                setTestStepResult(_testStepsList[0].StepName, "FAIL");
                                for (int i = 0; i < _testStepsList.Count; i++)
                                {
                                    setTestStepResult(_testStepsList[i].StepName, "FAIL");
                                }
                                dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value = "FAIL";

                                return;
                            }
                        }

                        if (factTest.LastVersion != "")
						{
                            setTestStepResult("GET_VERSION", factTest.LastVersion);
                            setTestStepResult("COMMS", "PASS");
                            if (factTest.IAPVersionPassed)
                            {
                                setTestStepResult("GET_IAP_VERSION", "PASS");

                            }
                            else
                            {
                                setTestStepResult("GET_IAP_VERSION", "FAIL");
                            }
                        }
                        

                        testPhase++;
                        break;
                    }
                case Constants.CommanderTestPhase.GET_DEVID:
                    {
                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "GET_DEVID";
                        logTest("Get Device ID");
                        factTest.LastDevID = "";
                        GetDevID();
                        //Utils.delay(300);
                        Utils.delayThread(1000);

                        testPhase++;

                        //Skip "Testing IO's" for the moment.
                        //Issue since we swaped from using the Retention lead for smart start reed switch
                        testPhase++;
                        break;
                    }
                case Constants.CommanderTestPhase.TEST_IO:
                    {
                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "TEST_IO";
                        logTest("Testing IO's");

                        //Skip "Testing IO's" for the moment.
                        //Issue since we swaped from using the Retention lead for smart start reed switch
                        testPhase++;

                        // FactoryTestMode();
                        // Utils.delayThread(10);
                        // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, mainForm.coolContAddr, 0);
                        // Utils.delayThread(10);
                        // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, mainForm.coolContAddr, 0);
                        // Utils.delayThread(10);
                        // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, mainForm.coolContAddr, 0);
                        // Utils.delayThread(10);
                        // allOutputs(0);
                        // //Utils.delay(50);
                        // Utils.delayThread(100);

                        // FactoryTestMode();
                        // Utils.delayThread(10);
                        // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, mainForm.coolContAddr, 0);
                        // Utils.delayThread(10);
                        // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, mainForm.coolContAddr, 0);
                        // Utils.delayThread(10);
                        // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, mainForm.coolContAddr, 0);
                        // Utils.delayThread(10);
                        // allOutputs(1);
                        // //Utils.delay(50);
                        // Utils.delayThread(100);

                        // ProcessCommTestResp.LastIO_inputs = "";
                        // ProcessCommTestResp.LastIO_outputs = "";
                        // RequestIO();
                        // //Utils.delay(200);
                        // Utils.delayThread(500);

                        // FactoryTestMode();
                        // Utils.delayThread(10);
                        // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, mainForm.coolContAddr, 0);
                        // Utils.delayThread(10);
                        // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, mainForm.coolContAddr, 0);
                        // Utils.delayThread(10);
                        // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, mainForm.coolContAddr, 0);
                        // Utils.delayThread(10);
                        // allOutputs(0);
                        //// Utils.delay(50);
                        // Utils.delayThread(500);

                        // ProcessCommTestResp.LastIO_inputs = "";
                        // ProcessCommTestResp.LastIO_outputs = "";
                        // RequestIO();
                        // //Utils.delay(200);
                        // Utils.delayThread(500);

                        // testPhase++;
                        // progressBar1.Value++;

                        // factTest.IOTestComplete = true;
                        // enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, mainForm.DefaultAddr, 1);
                        // enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_3A1, mainForm.DefaultAddr, 1);
                        // //Utils.delay(50);
                        // Utils.delayThread(10);

                        break;
                    }

                case Constants.CommanderTestPhase.COMMON_LOOMS:
                    {
                        btnAbortTest.Enabled = true;
                        logTest("Common Loom Tests");
                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "COMMON_LOOMS";
						for (testCommon = 0; (int)testCommon < Enum.GetNames(typeof(CommanderLoomTests.CommanderCommonLoom)).Length;)
						{
							if (factTest.CurrentTestAborted)
							{
								lblTestFeedback.Text = "Test Phase";
								lblTestFeedbackText.Text = "TEST_ABORTED";
                                dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value = "ABORTED";
                                dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Style.BackColor = Color.OrangeRed;
                                failOutstandingTests();
                                testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
								return;
							}
							string testCommonString = testCommon.ToString();

							CommanderLoomTests.performCommonLoomsTestNew(this, testCommonString);
							//Utils.delay(20);
							Utils.delayThread(100);

                            resultToDisplay(testCommonString);

							testCommon++;
						}
						testPhase++;
                        break;
                    }
                case Constants.CommanderTestPhase.COMMON_ROT_LOOMS:
                    {
                        //if (Selected_Test > (int)CommanderLoomTests.ProductNums.SSDUTest20)
                        if (labelTestNameDisplay.Text.Contains("RTY"))
                        {
                            lblTestFeedback.Text = "Test Phase";
                            lblTestFeedbackText.Text = "COMMON_ROT_LOOMS";
                            logTest("Common Rotary Loom Tests");
							for (testCommonRot = 0; (int)testCommonRot < Enum.GetNames(typeof(CommanderLoomTests.CommanderCommonRotLoom)).Length;)
							{
								if (factTest.CurrentTestAborted)
								{
									lblTestFeedback.Text = "Test Phase";
									lblTestFeedbackText.Text = "TEST_ABORTED";
                                    dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value = "ABORTED";
                                    dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Style.BackColor = Color.OrangeRed;
                                    failOutstandingTests();
                                    testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
									return;
								}
								string testCommonRotString = testCommonRot.ToString();
								CommanderLoomTests.performCommonLoomsTestNew(this, testCommonRotString);
								//Utils.delay(20);
								Utils.delayThread(100);
								testCommonRot++;
							}
						}
                        testPhase++;
                        break;
                    }
                case Constants.CommanderTestPhase.PRODUCT_SPECIFIC_LOOMS:
                    {
                        lblTestFeedback.Text = "Test Phase";
                        lblTestFeedbackText.Text = "PRODUCT_SPECIFIC_LOOMS";
                        logTest("Product Specific Loom Tests");
                        CommanderLoomTests.selectProductLoomsTestNew(this);
                        //Utils.delay(20);
                        Utils.delayThread(100);

						if (factTest.CurrentTestAborted)
						{
							lblTestFeedback.Text = "Test Phase";
							lblTestFeedbackText.Text = "TEST_ABORTED";
							testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                            dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value = "ABORTED";
                            dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Style.BackColor = Color.OrangeRed;
                            failOutstandingTests();
                            return;
						}
						testPhase++;
                        break;
                    }
                case Constants.CommanderTestPhase.KEYPAD_TEST:
                    {
                        if (factTest.CurrentTestAborted)
                        {
                            lblTestFeedback.Text = "Test Phase";
                            lblTestFeedbackText.Text = "TEST_ABORTED";
                            testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                            dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value = "ABORTED";
                            dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Style.BackColor = Color.OrangeRed;
                            failOutstandingTests();
                            return;
                        }

                        if (performKeyTest)
                        {
                            lblTestFeedback.Text = "Test Phase";
                            lblTestFeedbackText.Text = "KEYPAD_TEST";
                            ProcessCommTestResp.keyPadTestAborted = false;
                            logTest("Keypad Test");
                            //ProcessCommTestResp.checkKeyPadButton(this, keyPadCounter);
                            if (keyPadTest == null) keyPadTest = new KeyPadTest();
                            KeyPadTestInitiated = true;
                            keyPadTest.ShowDialog();

                            if (ProcessCommTestResp.keyPadTestAborted)
                            {
                                factTest.CurrentTestAborted = true;
                                logTest("KEYPAD TEST ABORTED.");
                                KeyPadTestInitiated = false;

                                if (keyPadTest != null)
                                {
                                    keyPadTest = null;
                                }

                                dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value = "ABORTED";
                                dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Style.BackColor = Color.OrangeRed;
                                failOutstandingTests();

                                testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                break;
                            }

                            testPhase++;
                        }
                        else
                        {
                            logTest("KEYPAD TEST SKIPPED.");
                            ProcessCommTestResp.keyPadTestFinished = true;
                            testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                            processFactoryTest();
                        }

                        break;
                    }
                case Constants.CommanderTestPhase.TEST_FINISHED:
                    {
                        factoryTestToggle();
                        if (factTest.CurrentTestAborted)
                        {
                            lblTestFeedback.Text = "Test Phase";
                            lblTestFeedbackText.Text = "TEST_ABORTED";
                            testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                            logTest("");
                            logTest("****************************************************************");
                            logTest("OVERALL TEST RESULT: TEST ABORTED!");
                            logTest("****************************************************************");
                            //return;

                            dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value = "ABORTED";
                            dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Style.BackColor = Color.OrangeRed;
                            failOutstandingTests();
                        }
                        else
                        {
                            if (ProcessCommTestResp.keyPadTestFinished)
                            {
                                lblTestFeedback.Text = "Test Phase";
                                lblTestFeedbackText.Text = "TEST_FINISHED";
                                logTest("Test Complete");
                                logTest("END OF TEST");
                                if (factTest.OverallTestPassed)
                                {
                                    logTest("");
                                    logTest("*****************************************");
                                    logTest("OVERALL TEST RESULT: PASSED");
                                    logTest("*****************************************");
                                }
                                else
                                {
                                    logTest("");
                                    logTest("***************************************");
                                    logTest("OVERALL TEST RESULT: FAILED");
                                    logTest("***************************************");
                                }
                            }

                            if (didAllTestsPass())
							{
                                dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value = "PASS";
                            }
                            else
							{
                                dataGridViewVerticalTestResults.Rows[0].Cells["RESULT"].Value = "FAIL";
                            }

                        }

                        btnAbortTest.Enabled = false;
                        SendNvrCmdSet(DefaultAddr, (int)Constants.NVR_COMMANDER.NVR_WEIGHALL_CAL_FACTOR, 521);
                        //Utils.delay(10);
                        Utils.delayThread(10);
                        //softReset(mainForm.coolContAddr);
                        //softReset(mainForm.DefaultAddr);
                        //allOutputs(0);
                        enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, DefaultAddr, 0);
                        Utils.delay(10);
                        enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, DefaultAddr, 0);
                        Utils.delay(10);
                        enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, DefaultAddr, 0);
                        Utils.delay(10);
                        enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_CCAIR, DefaultAddr, 0);
                        Utils.delay(10);
                        enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_RET, DefaultAddr, 0);
                        Utils.delay(10);
                        enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.PULS_1, DefaultAddr, 0);
                        Utils.delay(10);
                        enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, coolContAddr, 0);
                        Utils.delay(10);
                        enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, coolContAddr, 0);
                        Utils.delay(10);
                        enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, coolContAddr, 0);
                        Utils.delay(10);
                        enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, coolContAddr, 0);
                        Utils.delayThread(50);
                        softReset(0);

                        testPhase++;
                        break;
                    }
            }
        }

        public void getProgType()
        {
            int nvrAddr = 103;
            byte[] data = new byte[8]; // enough to hold all numbers up to 64-bits            	                    
            data[0] = (byte)(nvrAddr & 0xff);
            //data[0] = (byte)(103 & 0xff);
            data[1] = (byte)(nvrAddr >> 8);
            //data[1] = (byte)(103 >> 8);
            serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_NVR_GET, 250, 1, 2, data));
        }

        public void getWeighCalFactor()
        {
            int nvrAddr = 58;
            byte[] data = new byte[8]; // enough to hold all numbers up to 64-bits            	                    
            data[0] = (byte)(nvrAddr & 0xff);
            //data[0] = (byte)(103 & 0xff);
            data[1] = (byte)(nvrAddr >> 8);
            //data[1] = (byte)(103 >> 8);
            serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_NVR_GET, 250, 1, 2, data));
        }

        public void FactoryTestMode()
        {
            byte[] Data = new byte[8];

            Data[0] = 1;

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_FACTORY_TEST, 0, 1, 1, Data);
            serialOut(text);
        }

        public void CommanderSecurityHandshake()
        {
            /* Let Commander Version 4x.xx Know Who The Caller Is So That They Will Talk Back  */
            byte[] data = new byte[8];

            data[0] = (byte)7;
            data[1] = (byte)2;
            data[2] = (byte)4;
            data[3] = (byte)8;

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_CONFIRM_CALLER, 0, 1, 4, data); // CAN_CMD_CONFIRM_CALLER = 300
            serialOut(text);

        }

        private void getVersion()
        {
            try
            {
                string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_VERSION, DefaultAddr, 1, 0, null);
                //string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_VERSION, 0, 1, 0, null);
                serialOut(text);

                /*int milliseconds = 2000;
                Thread.Sleep(milliseconds);

                MessageBox.Show(mainForm.serialPortResponse);

                if (mainForm.serialPortResponse != "true")
                {
                    richTextBoxTestLogs.AppendText("COMMS TEST FAILURE: " + "Version Number not returned." + Environment.NewLine);
                }

                mainForm.serialPortResponse = "false"; */
            }

            catch (System.InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
                //richTextBoxTestLogs.AppendText("COMMS TEST FAILURE: " + ex.Message + Environment.NewLine);
                logTest("COMMS TEST FAILURE: " + ex.Message + Environment.NewLine);
            }
        }

        private void GetDevID()
        {

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_DEVID, DefaultAddr, 1, 0, null);
            //string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_DEVID, 0, 1, 0, null);
            LongMessage = "";
            serialOut(text);


        }

        private void GetJigDevID()
        {

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_DEVID, 201, 1, 0, null);
            //string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_DEVID, 0, 1, 0, null);
            LongMessage = "";
            serialOut(text);


        }

        public void allOutputs(byte mode)
        {
            byte i;
            for (i = 0; i < 20; i++)
            {
                setOutput(i, mode);
                //Utils.delay(2);
                Utils.delayThread(2);
            }
        }

        public void enableOP(Constants.CAN_COMMANDS canCommand, Constants.CommanderOutputs outPut, int deviceAddr, int enableOP)
        {
            //allOutputs(0);

            byte[] Data = new byte[8];

            Data[0] = (byte)outPut;
            Data[1] = (byte)enableOP;
            string text = msgProc.SendCanMessage((int)canCommand, deviceAddr, 1, 2, Data);
            serialOut(text);

        }

        private void setOutput(byte op, byte mode)
        {
            byte[] Data = new byte[8];

            Data[0] = op; //OP3a 4;?? //OP3 2;   //OP2 1;   //OP1 0;
            Data[1] = mode;
            //string text = msgProc.SendCanMessage(202, mainForm.DefaultAddr, 1, 2, Data);
            string text = msgProc.SendCanMessage(202, 0, 1, 2, Data);
            serialOut(text);
        }

        public void RequestIO(int didAddr)
        {
            //serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_IO, mainForm.DefaultAddr, 1, 0, null));
            serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_IO, didAddr, 1, 0, null));
        }

        public void SendNvrCmdSet(int did, int NvrAddr, int NvrValue)
        {
            byte[] data = new byte[8];
            data[0] = (byte)(NvrAddr & 0xff);
            data[1] = (byte)(NvrAddr >> 8);
            data[2] = (byte)(NvrValue & 0xff);
            data[3] = (byte)((NvrValue >> 8) & 0xff);
            data[4] = (byte)((NvrValue >> 16) & 0xff);
            data[5] = (byte)((NvrValue >> 32) & 0xff);
            serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_NVR_SET, did, 1, 6, data));

            //softReset(did);
        }

        public void softReset(int did)
        {
            byte[] data = new byte[8];
            data[0] = 2;

            serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_DO_SOFTWARE_RESET, did, 1, 1, data));

        }

        public void calibrateZero()
        {
            if (ProcessCommTestResp.milkMeterLeadTestAborted)
            {
                return;
            }

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_CALIBRATION_ZERO_DATA, 0, 1, 0, null);
            serialOut(text);


        }

        public void span()
        {
            if (ProcessCommTestResp.milkMeterLeadTestAborted)
            {
                return;
            }

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_CALIBRATION_SPAN_DATA, 0, 1, 0, null);
            serialOut(text);

        }

        public void getWeight()
        {
            if (ProcessCommTestResp.milkMeterLeadTestAborted)
            {
                return;
            }

            //string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_GROSS_WGT, 0, 1, 0, null);
            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_GROSS_WGT, 0, 1, 0, null);
            serialOut(text);

        }

        public void factoryTestToggle()
        {
            byte[] Data = new byte[8];
            //uncomment this
            if (cbFactTestMode.Checked)
                Data[0] = 1;
            else
                Data[1] = 0;

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_FACTORY_TEST, 0, 1, 1, Data);
            serialOut(text);
        }

        public void logTest(string text)
        {
            //if (richTextBoxTestLogs != null)
            //    richTextBoxTestLogs.AppendText(text + Environment.NewLine);

            //if (rtboxReceive != null)
                rtboxReceive.AppendText(text + Environment.NewLine);
        }

        public void factoryTestVersionReported(int Sid, int ProgID, int Version, int MinorVersion)
        {
     //       if (testPhase == Constants.CommanderTestPhase.GET_IAP_VERSION)
     //       {
     //           if (ProgID == 100) // IAP for Commander
     //           {
     //               //factTest.VersionMajorBootLoader = Version;
     //               //factTest.VersionMinorBootLoader = MinorVersion;
     //               if (Version == 1 && MinorVersion == 7)
					//{
     //                   factTest.IAPVersion = "PASS";
     //                   //setTestStepResult("GET_IAP_VERSION", "PASS");
     //                   //can232Form.setTestStepResult("OVERALL", "FAIL");
     //               }
     //               //  else
					////{
     //               //   setTestStepResult("GET_IAP_VERSION", "FAIL");
     //               //}
                    
     //           }

     //       }

            if (testPhase == Constants.CommanderTestPhase.GET_VERSION)
            {
                if (ProgID == 100) // IAP for Commander
                {
                    factTest.VersionMajorBootLoader = Version;
                    factTest.VersionMinorBootLoader = MinorVersion;
                }
                if (ProgID == 300) // IAP for Commander
                {
                    factTest.VersionMajorMainProgram = Version;
                    factTest.VersionMinorBootLoader = MinorVersion;
                }

            }
        }        

        public void processLongMessage(CanMsg cm)
        {
            byte[] data;
            cm.cnvtDataToBytes(out data);
            string msgStr = cm.cnvtDataToString();

            if (data[0] == 0)
            {
                LongMsgData.Clear();
                LongMessage = "";
            }
            else if (data[0] == 255)
            {
                int packetCheckSum = data[2];
                packetCheckSum <<= 8;
                packetCheckSum |= data[1];

                if ((uint)packetCheckSum == CalcCheckSum())
                {
                    logTest("Msg:" + LongMessage);
                    LastDevID = "";

                    string checkMsg = LongMessage.Substring(0, 5);
                    if (checkMsg.Equals("DevID"))
                    {
                        if (LongMessage.Length == 30)
                        {
                            LastDevID = LongMessage;
                            factTest.DEVIDTestPassed = true;
                            logTest(LongMessage);
                            string absDevId = LongMessage.Substring(6, 24);
                            //setTestStepResult("GET_DEVID", LastDevID);
                            setTestStepResult("GET_DEVID", absDevId);
                        }
                    }
                    else
                    {
                        logTest("CANNOT READ DEVID - POSSIBLE BOARD ISSUE!");
                        factTest.DEVIDTestPassed = false;
                        factTest.OverallTestPassed = false;
                        setTestStepResult("GET_DEVID", "FAIL");
                    }


                }
                else
                    logTest("Error");

                return;
            }
            else if (data[0] == 201)
            {
                int packetCheckSum = data[2];
                packetCheckSum <<= 8;
                packetCheckSum |= data[1];

                if ((uint)packetCheckSum == CalcCheckSum())
                {
                    logTest("Msg:" + LongMessage);
                    JigDevID = "";

                    string checkMsg = LongMessage.Substring(0, 5);
                    if (checkMsg.Equals("DevID"))
                    {
                        if (LongMessage.Length == 30)
                        {
                            JigDevID = LongMessage;
                            //factTest.DEVIDTestPassed = true;
                            logTest(LongMessage);
                            string absJigDevId = LongMessage.Substring(6, 24);
                            factTest.JigDevID = absJigDevId;
                            //setTestStepResult("GET_DEVID", LastDevID);
                            //setTestStepResult("GET_DEVID", absDevId);
                        }
                    }
                    //else
                    //{
                    //    logTest("CANNOT READ DEVID - POSSIBLE BOARD ISSUE!");
                    //    factTest.DEVIDTestPassed = false;
                    //    factTest.OverallTestPassed = false;
                    //    setTestStepResult("GET_DEVID", "FAIL");
                    //}


                }
                else
                    logTest("JIG DevID Error");

                return;
            }

            for (int i = 1; i < cm.DLC; i++)
            {
                LongMsgData.Add(data[i]);
                LongMessage += msgStr[i];// cm.Data[i];
            }


        }

        public uint CalcCheckSum()
        {
            List<uint> crcData = new List<uint>();
            int Ct = LongMsgData.Count;
            for (int i = 0; i < Ct;)
            {
                uint part = 0;
                for (int j = 0; j < 4; j++)
                {
                    if (i < Ct)
                        part = (part << 8) | LongMsgData[i];
                    i++;
                }
                crcData.Add(part);
            }
            return Crc.Calc(crcData.ToArray(), (uint)crcData.Count, (long)crcData.Count) & 0xFFFF;
        }

		private void cbFactTestMode_CheckedChanged(object sender, EventArgs e)
		{
            FactoryTestMode();

        }

		private void buttonDevId_Click(object sender, EventArgs e)
		{
            GetJigDevID();
            //Utils.delay(300);
            //Utils.delayThread(1000);
        }

		private void btnReset_Click(object sender, EventArgs e)
		{
            //enableIP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlInputs.I25, coolContAddr, 1);
            softReset(201);

        }

        public void enableOPCC(Constants.CAN_COMMANDS canCommand, Constants.CoolControlOutputs outPut, int deviceAddr, int enableOP)
        {
            //allOutputs(0);

            byte[] Data = new byte[8];

            Data[0] = (byte)outPut;
            Data[1] = (byte)enableOP;
            string text = msgProc.SendCanMessage((int)canCommand, deviceAddr, 1, 2, Data);
            serialOut(text);

        }

        public void enableIP(Constants.CAN_COMMANDS canCommand, Constants.CoolControlInputs outPut, int deviceAddr, int enableOP)
        {
            //allOutputs(0);

            byte[] Data = new byte[8];

            Data[0] = (byte)outPut;
            Data[1] = (byte)enableOP;
            string text = msgProc.SendCanMessage((int)canCommand, deviceAddr, 1, 2, Data);
            serialOut(text);

        }

		private void btnDevId_Click(object sender, EventArgs e)
		{
            //GetJigDevID();
            //GetDevID();
            RequestIO(250);
            RequestIO(201);
            RequestIO(200);
        }

		private void checkBoxShowOPLog_CheckedChanged(object sender, EventArgs e)
		{

		}
	}
}
