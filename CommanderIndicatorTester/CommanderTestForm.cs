﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing.Printing;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;


namespace CAN232_Monitor
{
    public partial class CommanderTestForm : Form
    {
        private bool isFormClosing = false;
        public Can232 mainForm = null;
        UtilsCAN msgProc = new UtilsCAN();
        bool factoryTestEnabled = false;
        public Constants.CommanderTestPhase testPhase = Constants.CommanderTestPhase.GET_VERSION;
        public CommanderLoomTests.CommanderCommonLoom testCommon = CommanderLoomTests.CommanderCommonLoom.MILKLINE_OP;
        public CommanderLoomTests.CommanderCommonRotLoom testCommonRot = CommanderLoomTests.CommanderCommonRotLoom.SMART_START_REED_SWITCH;
        int did = 11;
        private string FileName;
        public CommanderFactoryTestResults factTest = new CommanderFactoryTestResults();
        //richTextBox1.AppendText(Environment.NewLine + "Please enter Name and/or Job Traveller to proceed.");
        public int outputsCounter = 0;
        private int testCounter = 5;
        public int keyPadCounter = 0;
        public int Selected_Test;

        public static string LastDevID = "";

        //KeyPadTest frm2;

        public KeyPadTest keyPadTest;
        public bool KeyPadTestInitiated = false;

        public MilkLeadTest milkLeadTest;
        public bool milkLeadTestInitiated = false;
                

        public CommanderTestForm()
        {
            InitializeComponent();

        }

        public CommanderTestForm(Can232 mf, string testerName, string jobTraveller, int selectedTest)
        {
            InitializeComponent();            
            mainForm = mf;
            Selected_Test = selectedTest;
            CommanderLoomTests.ProductNums selected_test = (CommanderLoomTests.ProductNums)selectedTest;
            string SelectedTest = selected_test.ToString();
            textBox1.Text = testerName;
            textBox2.Text = jobTraveller;
            textBox3.Text = SelectedTest;
            button3.Enabled = false;
            abortTest.Enabled = false;
        }

        private void CommanderTestForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonCommanderVersion_Click(object sender, EventArgs e)
        {
            getVersion();            
            
        }


        private void getVersion()
        {
            try
            {
                string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_VERSION, mainForm.DefaultAddr, 1, 0, null);
                //string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_VERSION, 0, 1, 0, null);
                serialOut(text);

                /*int milliseconds = 2000;
                Thread.Sleep(milliseconds);

                MessageBox.Show(mainForm.serialPortResponse);

                if (mainForm.serialPortResponse != "true")
                {
                    richTextBoxTestLogs.AppendText("COMMS TEST FAILURE: " + "Version Number not returned." + Environment.NewLine);
                }

                mainForm.serialPortResponse = "false"; */
            }

            catch (System.InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
                richTextBoxTestLogs.AppendText("COMMS TEST FAILURE: " + ex.Message + Environment.NewLine);
            }
        }

        public void calibrateZero()
        {
            if (ProcessCommTestResp.milkMeterLeadTestAborted)
            {
                return;
            }

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_CALIBRATION_ZERO_DATA, 0, 1, 0, null);
            serialOut(text);
            

        }

        public void span()
        {
            if (ProcessCommTestResp.milkMeterLeadTestAborted)
            {
                return;
            }

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_CALIBRATION_SPAN_DATA, 0, 1, 0, null);
            serialOut(text);

        }

        public void getWeight()
        {
            if (ProcessCommTestResp.milkMeterLeadTestAborted)
            {
                return;
            }

            //string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_GROSS_WGT, 0, 1, 0, null);
            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_GROSS_WGT, 0, 1, 0, null);
            serialOut(text);

        }

        public void serialOut(string text)
        {
            if (mainForm != null) mainForm.serialOut(text);
        }


        // TODO NB with more devices need a Long Message per device
        private string LongMessage = "";
        public List<byte> LongMsgData = new List<byte>();

        public uint CalcCheckSum()
        {
            List<uint> crcData = new List<uint>();
            int Ct = LongMsgData.Count;
            for (int i = 0; i < Ct; )
            {
                uint part = 0;
                for (int j = 0; j < 4; j++)
                {
                    if (i < Ct)
                        part = (part << 8) | LongMsgData[i];
                    i++;
                }
                crcData.Add(part);
            }
            return Crc.Calc(crcData.ToArray(), (uint)crcData.Count, (long)crcData.Count) & 0xFFFF;
        }

        public void processLongMessage(CanMsg cm)
        {
            byte[] data;
            cm.cnvtDataToBytes(out data);
            string msgStr = cm.cnvtDataToString();

            if (data[0] == 0)
            {
                LongMsgData.Clear();
                LongMessage = "";
            }
            else if (data[0] == 255)
            {
                  int packetCheckSum = data[2];
                  packetCheckSum <<= 8;
                  packetCheckSum |= data[1];

                  if ((uint)packetCheckSum == CalcCheckSum())
                  {
                      log("Msg:" + LongMessage);
                      LastDevID = "";

                      string checkMsg = LongMessage.Substring(0, 5);
                      if (checkMsg.Equals("DevID"))
                      { 
                          if (LongMessage.Length == 30) 
                          {                      
                              LastDevID = LongMessage;
                              factTest.DEVIDTestPassed = true;
                              logTest(LongMessage);                              
                          }
                      }
                      else
                      {
                          logTest("CANNOT READ DEVID - POSSIBLE BOARD ISSUE!");
                          factTest.DEVIDTestPassed = false;
                          factTest.OverallTestPassed = false;
                      }

                     
                  }
                  else
                      log("Error");
               
                return;
            }

            for (int i = 1; i < cm.DLC; i++)
            {
                LongMsgData.Add(data[i]);
                LongMessage += msgStr[i];// cm.Data[i];
            }


        }


        public void processMsg(CanMsg cm)
        {
            ProcessCommTestResp.processTestResponse(this, cm);
            TestIO.processMessage(cm, this);
        }

        private bool checkInputs(string inputs, Constants.CoolControlInputs checkIP)
        {

            bool response = false;

            string checkInput = inputs.Substring(3, 32);

            int counter = 0;

            foreach (char io in checkInput)
            {
                if ((counter == (int)(checkIP)) && (io == '1'))
                {
                    response = true;
                }

                counter++;
            } 

            return response;
        }


        public void log(string text)
        {
            if (richTextBox1 != null)
                
                richTextBox1.AppendText(DateTime.Now.ToString("HH:mm:ss") + " " + text + Environment.NewLine);
        }

        public void factoryTestVersionReported(int Sid, int ProgID, int Version, int MinorVersion)
        {
            if (testPhase == Constants.CommanderTestPhase.GET_VERSION)
            {
                if (ProgID == 100) // IAP for Commander
                {
                    factTest.VersionMajorBootLoader = Version;
                    factTest.VersionMinorBootLoader = MinorVersion;
                }
                if (ProgID == 300) // IAP for Commander
                {
                    factTest.VersionMajorMainProgram = Version;
                    factTest.VersionMinorBootLoader = MinorVersion;
                }

            }
        }

       public void enableOP(Constants.CAN_COMMANDS canCommand, Constants.CommanderOutputs outPut, int deviceAddr, int enableOP)
       {
           //allOutputs(0);

           byte[] Data = new byte[8];

           Data[0] = (byte)outPut;
           Data[1] = (byte)enableOP;
           string text = msgProc.SendCanMessage((int)canCommand, deviceAddr, 1, 2, Data);
           serialOut(text);

       }

       public void enableIP(Constants.CAN_COMMANDS canCommand, Constants.CommanderInputs outPut, int deviceAddr, int enableOP)
       {
           //allOutputs(0);

           byte[] Data = new byte[8];

           Data[0] = (byte)outPut;
           Data[1] = (byte)enableOP;
           string text = msgProc.SendCanMessage((int)canCommand, deviceAddr, 1, 2, Data);
           serialOut(text);

       }

       public void enableOPCC(Constants.CAN_COMMANDS canCommand, Constants.CoolControlOutputs outPut, int deviceAddr, int enableOP)
       {
           //allOutputs(0);

           byte[] Data = new byte[8];

           Data[0] = (byte)outPut;
           Data[1] = (byte)enableOP;
           string text = msgProc.SendCanMessage((int)canCommand, deviceAddr, 1, 2, Data);
           serialOut(text);

       }


       public bool testMainLine()
       {
           allOutputs(0);

           byte[] Data = new byte[8];
           //Data[0] = (byte)Constants.CommanderOutputs.MAIN_LINE;
           //Data[0] = (byte)Constants.CommanderOutputs.OP_CCWAT;
           //Data[0] = (byte)Constants.CommanderOutputs.OP_CCAIR;
           //Data[0] = (byte)Constants.CommanderOutputs.PULS_1;
           //Data[0] = (byte)Constants.CommanderOutputs.OP_ACR;
           //Data[0] = (byte)Constants.CommanderOutputs.OP_RET;
           Data[0] = 0;
           Data[1] = 1;
           //string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, mainForm.DefaultAddr, 1, 2, Data);
           string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, mainForm.coolContAddr, 1, 2, Data);
           serialOut(text);

           Utils.delay(50);
           TestIO.LastIO_inputs = "";
           TestIO.LastIO_outputs = "";
           RequestIO();
           Utils.delay(50);

           string response = "";

           if (TestIO.LastIO_inputs.Length == 32)
           {
               //(IO) CMD:207 DID:1 SID:200 OP:00000000000000000000000000000000 IP:00000000000000000100000000000000

               if (TestIO.LastIO_inputs[(int)Constants.CoolControlInputs.OL3] == '1')
               {
                   response = "IP came on";

                   for (int i = 0; i < TestIO.LastIO_inputs.Length; i++)
                   {
                       if ((TestIO.LastIO_inputs[i] == '1') && (i != (int)Constants.CoolControlInputs.OL3))
                       { 
                           response = "Too Many IP's came on";
                       }
                   }
               }
               else
               {
                   response = "IP Did Not Come on";

                   for (int i = 0; i < TestIO.LastIO_inputs.Length; i++)
                   {
                       if ((TestIO.LastIO_inputs[i] == '1') && (i != (int)Constants.CoolControlInputs.OL1))
                       {
                           response = "Wrong IP came on";
                       }
                   }
               }
           }
           else
           {
               response = "IP response is corrupt";
           }

           logTest(response);
           return false;           
       }

       
        private void button1_Click(object sender, EventArgs e)
        {
            
            allOutputs(0);

            richTextBoxTestLogs.Text = "";
            //opstart = comboBoxOutputs1.SelectedIndex;
            //opstart = comboBox1CoolCtrlOps.SelectedIndex;
            logTest("OP:" + opstart.ToString());


            byte[] Data = new byte[8];

            // FactoryTestMode();

            

            //Data[0] = 5; //Top 19//Bottom 18//OP17 17//CCVent 16 //CCSan 15 //RET 11 //Puls2 9//OP3a 4;?? //OP3 2; //OP2 1;  //OP1 0;
            //Data[0] = (byte)Constants.CommanderOutputs.MAIN_LINE;
            Data[0] = (byte) opstart;
            opstart++;
            //Data[0] = (byte)testCounter;
            Data[1] = 1;
            
            //string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, mainForm.DefaultAddr, 1, 2, Data);
            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, mainForm.coolContAddr, 1, 2, Data);
            serialOut(text);
            //testCommon = CommanderLoomTests.CommanderCommonLoom.CLUST_CLEANSE_ACID;

            testCounter++;

           // allOutputs(1);
            
        }


        int opstart = 0;
        private void turnOnOPs(object sender, EventArgs e)
        {

            allOutputs(0);

            richTextBoxTestLogs.Text = "";
            //opstart = comboBoxOutputs1.SelectedIndex;
            //opstart = comboBox1CoolCtrlOps.SelectedIndex;
            logTest("OP:" + opstart.ToString());


            byte[] Data = new byte[8];

            // FactoryTestMode();



            //Data[0] = 5; //Top 19//Bottom 18//OP17 17//CCVent 16 //CCSan 15 //RET 11 //Puls2 9//OP3a 4;?? //OP3 2; //OP2 1;  //OP1 0;
            //Data[0] = (byte)Constants.CommanderOutputs.MAIN_LINE;
            Data[0] = (byte)opstart;
            opstart++;
            //Data[0] = (byte)testCounter;
            Data[1] = 1;

            //string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, mainForm.DefaultAddr, 1, 2, Data);
            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, mainForm.coolContAddr, 1, 2, Data);
            serialOut(text);
            //testCommon = CommanderLoomTests.CommanderCommonLoom.CLUST_CLEANSE_ACID;

            testCounter++;

            // allOutputs(1);

        }
        private void button2_Click(object sender, EventArgs e)
        {
            /*byte[] Data = new byte[8];
            Data[0] = 5; //Top 19//Bottom 18//OP17 17//CCVent 16 //CCSan 15 //RET 11 //Puls2 9//OP3a 4;?? //OP3 2; //OP2 1;  //OP1 0;
            Data[1] = 0;
            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, 0, 1, 2, Data);
            serialOut(text);*/
            allOutputs(0);
        }

        private void turnOffOPs(object sender, EventArgs e)
        {
            /*byte[] Data = new byte[8];
            Data[0] = 5; //Top 19//Bottom 18//OP17 17//CCVent 16 //CCSan 15 //RET 11 //Puls2 9//OP3a 4;?? //OP3 2; //OP2 1;  //OP1 0;
            Data[1] = 0;
            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, 0, 1, 2, Data);
            serialOut(text);*/
            allOutputs(0);
        }

        public void allOutputs(byte mode)
        {
            byte i;
            for (i = 0; i < 20; i++)
            {
                setOutput(i, mode);
                //Utils.delay(2);
                Utils.delayThread(2);
            }
        }

        private void setOutput(byte op, byte mode)
        {
            byte[] Data = new byte[8];

            Data[0] = op; //OP3a 4;?? //OP3 2;   //OP2 1;   //OP1 0;
            Data[1] = mode;
            //string text = msgProc.SendCanMessage(202, mainForm.DefaultAddr, 1, 2, Data);
            string text = msgProc.SendCanMessage(202, 0, 1, 2, Data);
            serialOut(text);
        }

        private void buttonRequestIO_Click(object sender, EventArgs e)
        {
            RequestIO();
        }

        public void RequestIO()
        {
            //serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_IO, mainForm.DefaultAddr, 1, 0, null));
            serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_IO, 0, 1, 0, null));
        }

        public void RequestIO(int didAddr)
        {
            //serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_IO, mainForm.DefaultAddr, 1, 0, null));
            serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_IO, didAddr, 1, 0, null));
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            GetDevID();
        }

        

        private void GetDevID()
        {

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_DEVID, mainForm.DefaultAddr, 1, 0, null);
            //string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_REQUEST_DEVID, 0, 1, 0, null);
            LongMessage = "";
            serialOut(text);

            
        }

        //private void factoryTestToggle(object sender, EventArgs e)
        public void factoryTestToggle()
        {
                byte[] Data = new byte[8];
                //uncomment this
                if (checkBox1.Checked)
                    Data[0] = 1;
                else
                    Data[1] = 0;

                string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_FACTORY_TEST, 0, 1, 1, Data);                
                serialOut(text);
        }

        public void FactoryTestMode()
        {
            byte[] Data = new byte[8];

            Data[0] = 1;

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_FACTORY_TEST, 0, 1, 1, Data);
            serialOut(text);
        }


        public void buttonStartCommanderTest_Click(object sender, EventArgs e)
        {
            try
            {
                keyPadCounter = 0;
                richTextBoxTestLogs.Clear();
                richTextBox1.Clear();
                //milkLeadTest.richTextBox1.Clear();
                buttonStartCommanderTest.Enabled = false;
                button3.Enabled = false;
                button2.Enabled = false;
                button1.Enabled = false;
                abortTest.Enabled = true;
                factTest.CurrentTestAborted = false;
                factoryTestEnabled = !factoryTestEnabled;
                //logTest("Test Start Time: " + DateTime.Now.ToString("HH:mm:ss"));
                logTest("Test Start Time: " + DateTime.Today.ToString("d") + " " + DateTime.Now.ToString("HH:mm:ss"));
                progressBar1.Value = 0;
                progressBar1.Maximum = Enum.GetNames(typeof(Constants.CommanderTestPhase)).Length - 1;
                progressBar1Lb.ForeColor = Color.DarkBlue;
                progressBar1Lb.Font = new Font(progressBar1Lb.Font, FontStyle.Bold);
                progressBar1Lb.Text = "Starting Test";
               
                for (testPhase = 0; (int)testPhase < Enum.GetNames(typeof(Constants.CommanderTestPhase)).Length; )
                {
                    if (factTest.CurrentTestAborted)
                    {
                        testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                        progressBar1.Value = (int)Constants.CommanderTestPhase.TEST_FINISHED;
                    }
                    processFactoryTest();
                }

                progressBar1Lb.ForeColor = Color.Black;
                progressBar1Lb.Font = new Font(progressBar1Lb.Font, FontStyle.Regular);
                progressBar1Lb.Text = "Progress";

                Utils.delayThread(5000);

                buttonStartCommanderTest.Text = "New Test";
                buttonStartCommanderTest.Enabled = true;
                button3.Enabled = true;
                button2.Enabled = true;
                button1.Enabled = true;
                abortTest.Enabled = false;

                //Utils.delay(300);

                if (mainForm.autoPrintRpt)
                {
                    autoPrintTest();
                }
                
                
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void logTest(string text)
        {
            if (richTextBoxTestLogs != null)
                richTextBoxTestLogs.AppendText(text + Environment.NewLine);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.ShowDialog();
        }

        private void printDocument1_PrintPage(object sender,
                                               System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            String message = richTextBoxTestLogs.Text;
            //String message = System.Environment.UserName;
            Font messageFont = new Font("Arial",
                     24, System.Drawing.GraphicsUnit.Point);
            g.DrawString(message, messageFont, Brushes.Black, 100, 100);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_COMMANDER_REQUEST_TEMPERATURE, 0, 1, 0, null);                
            serialOut(text);

        }

        public void checkBoxShowOPLog_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FactoryTestMode();
            //ProcessCommTestResp.checkKeyPadButton(this,keyPadCounter);
        }

        public void processFactoryTest()
        {
            if (factTest.CurrentTestAborted)
            {
                testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                progressBar1.Value = (int)Constants.CommanderTestPhase.TEST_FINISHED;
            }

            log("TestPhase : " + testPhase.ToString());
            factTest.LastVersion = "";

            switch (testPhase)
            {
                case Constants.CommanderTestPhase.SET_FACTORY_MODE:
                    {
                       progressBar1Lb.Text = "Entering Factory Mode";
                       factTest.Clear();
                       getProgType();
                       getWeighCalFactor();
                       FactoryTestMode();
                       testPhase++;
                       progressBar1.Value++;
                       break;
                       
                    }

                case Constants.CommanderTestPhase.GET_VERSION:
                    {
                        progressBar1Lb.Text = "Get Software Version";
                        getVersion();
                        //Utils.delay(100);                        
                        Utils.delayThread(3000);

                        Stopwatch sw = new Stopwatch();
                        sw.Start();
                       
                        while (factTest.LastVersion == "")
                        {
                            if ((factTest.LastVersion == "") && (sw.ElapsedMilliseconds > 5000))
                            {
                                logTest("COMMS ISSUE - NO RESPONSE FROM DEVICE! ABORTING TEST");
                                factTest.CurrentTestAborted = true;
                                testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                progressBar1.Value = (int)Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }
                        }

                        testPhase++;
                        progressBar1.Value++;
                        break;
                    }
                case Constants.CommanderTestPhase.GET_DEVID:
                    {
                        progressBar1Lb.Text = "Get Device ID";
                        factTest.LastDevID = "";
                        GetDevID();
                        //Utils.delay(300);
                        Utils.delayThread(1000);

                        testPhase++;
                        progressBar1.Value++;
                        
                        //Skip "Testing IO's" for the moment.
                        //Issue since we swaped from using the Retention lead for smart start reed switch
                        testPhase++;
                        progressBar1.Value++;

                        break;
                    }
                case Constants.CommanderTestPhase.TEST_IO:
                    {
                        progressBar1Lb.Text = "Testing IO's";

                        //Skip "Testing IO's" for the moment.
                        //Issue since we swaped from using the Retention lead for smart start reed switch
                        testPhase++;
                        progressBar1.Value++;                        

                       // FactoryTestMode();
                       // Utils.delayThread(10);
                       // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, mainForm.coolContAddr, 0);
                       // Utils.delayThread(10);
                       // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, mainForm.coolContAddr, 0);
                       // Utils.delayThread(10);
                       // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, mainForm.coolContAddr, 0);
                       // Utils.delayThread(10);
                       // allOutputs(0);
                       // //Utils.delay(50);
                       // Utils.delayThread(100);

                       // FactoryTestMode();
                       // Utils.delayThread(10);
                       // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, mainForm.coolContAddr, 0);
                       // Utils.delayThread(10);
                       // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, mainForm.coolContAddr, 0);
                       // Utils.delayThread(10);
                       // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, mainForm.coolContAddr, 0);
                       // Utils.delayThread(10);
                       // allOutputs(1);
                       // //Utils.delay(50);
                       // Utils.delayThread(100);

                       // ProcessCommTestResp.LastIO_inputs = "";
                       // ProcessCommTestResp.LastIO_outputs = "";
                       // RequestIO();
                       // //Utils.delay(200);
                       // Utils.delayThread(500);

                       // FactoryTestMode();
                       // Utils.delayThread(10);
                       // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, mainForm.coolContAddr, 0);
                       // Utils.delayThread(10);
                       // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, mainForm.coolContAddr, 0);
                       // Utils.delayThread(10);
                       // enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, mainForm.coolContAddr, 0);
                       // Utils.delayThread(10);
                       // allOutputs(0);
                       //// Utils.delay(50);
                       // Utils.delayThread(500);

                       // ProcessCommTestResp.LastIO_inputs = "";
                       // ProcessCommTestResp.LastIO_outputs = "";
                       // RequestIO();
                       // //Utils.delay(200);
                       // Utils.delayThread(500);

                       // testPhase++;
                       // progressBar1.Value++;

                       // factTest.IOTestComplete = true;
                       // enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, mainForm.DefaultAddr, 1);
                       // enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_3A1, mainForm.DefaultAddr, 1);
                       // //Utils.delay(50);
                       // Utils.delayThread(10);

                        break;
                    }

                case Constants.CommanderTestPhase.COMMON_LOOMS:
                    {
                        progressBar1Lb.Text = "Common Loom Tests";
                        for (testCommon = 0; (int)testCommon < Enum.GetNames(typeof(CommanderLoomTests.CommanderCommonLoom)).Length; )
                        {
                            if (factTest.CurrentTestAborted)
                            {
                                testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                return;
                            }
                            string testCommonString = testCommon.ToString();

                            CommanderLoomTests.performCommonLoomsTest(this, testCommonString);
                            //Utils.delay(20);
                            Utils.delayThread(100);
                            testCommon++;
                        }
                        testPhase++;
                        progressBar1.Value++;
                        break;
                    }
                case Constants.CommanderTestPhase.COMMON_ROT_LOOMS:
                    {
                        if (Selected_Test > (int)CommanderLoomTests.ProductNums.SSDUTest20)
                        {
                            progressBar1Lb.Text = "Common Rotary Loom Tests";
                            for (testCommonRot = 0; (int)testCommonRot < Enum.GetNames(typeof(CommanderLoomTests.CommanderCommonRotLoom)).Length; )
                            {
                                if (factTest.CurrentTestAborted)
                                {
                                    testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                    return;
                                }
                                string testCommonRotString = testCommonRot.ToString();
                                CommanderLoomTests.performCommonLoomsTest(this, testCommonRotString);
                                //Utils.delay(20);
                                Utils.delayThread(100);
                                testCommonRot++;
                            }
                        }
                        testPhase++;
                        progressBar1.Value++;
                        break;
                    }
                case Constants.CommanderTestPhase.PRODUCT_SPECIFIC_LOOMS:
                    {
                        progressBar1Lb.Text = "Product Specific Loom Tests";
                        CommanderLoomTests.selectProductLoomsTest(this);
                        //Utils.delay(20);
                        Utils.delayThread(100);

                        if (factTest.CurrentTestAborted)
                        {
                            testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                            return;
                        }
                        testPhase++;
                        progressBar1.Value++;
                        break;
                    }
                case Constants.CommanderTestPhase.KEYPAD_TEST:
                    {
                        if (factTest.CurrentTestAborted)
                        {
                            testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                            return;
                        }

                        if (mainForm.performKeyTest)
                        {
                            ProcessCommTestResp.keyPadTestAborted = false;
                            progressBar1Lb.Text = "Keypad Test";
                            //ProcessCommTestResp.checkKeyPadButton(this, keyPadCounter);
                            if (keyPadTest == null) keyPadTest = new KeyPadTest();
                            KeyPadTestInitiated = true;
                            keyPadTest.ShowDialog();

                            if (ProcessCommTestResp.keyPadTestAborted)
                            {
                                factTest.CurrentTestAborted = true;
                                logTest("KEYPAD TEST ABORTED.");
                                KeyPadTestInitiated = false;

                                if (keyPadTest != null)
                                {
                                    keyPadTest = null;
                                }

                                testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                break;
                            }

                            testPhase++;
                            progressBar1.Value++;
                        }
                        else
                        {
                            logTest("KEYPAD TEST SKIPPED.");
                            ProcessCommTestResp.keyPadTestFinished = true;
                            testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                            progressBar1.Value = (int)Constants.CommanderTestPhase.TEST_FINISHED; ;
                            processFactoryTest();
                        }

                        break;
                    }
                case Constants.CommanderTestPhase.TEST_FINISHED:
                    {
                        factoryTestToggle();
                        if (factTest.CurrentTestAborted)
                        {
                            testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                            logTest("");
                            logTest("****************************************************************");
                            logTest("OVERALL TEST RESULT: TEST ABORTED!");
                            logTest("****************************************************************");
                            //return;
                        }
                        else
                        { 
                            if (ProcessCommTestResp.keyPadTestFinished)
                            {
                                progressBar1Lb.Text = "Test Complete";
                                logTest("END OF TEST");
                                if (factTest.OverallTestPassed)
                                {
                                    logTest("");
                                    logTest("*****************************************");
                                    logTest("OVERALL TEST RESULT: PASSED");
                                    logTest("*****************************************");
                                }
                                else
                                {
                                    logTest("");
                                    logTest("***************************************");
                                    logTest("OVERALL TEST RESULT: FAILED");
                                    logTest("***************************************");
                                }
                            }
                        }

                        abortTest.Enabled = false;
                        SendNvrCmdSet(mainForm.DefaultAddr, (int)Constants.NVR_COMMANDER.NVR_WEIGHALL_CAL_FACTOR, 521);
                        //Utils.delay(10);
                        Utils.delayThread(10);
                        //softReset(mainForm.coolContAddr);
                        //softReset(mainForm.DefaultAddr);
                        //allOutputs(0);
                        enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, mainForm.DefaultAddr, 0);
                        Utils.delay(10);
                        enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, mainForm.DefaultAddr, 0);
                        Utils.delay(10);
                        enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, mainForm.DefaultAddr, 0);
                        Utils.delay(10);
                        enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_CCAIR, mainForm.DefaultAddr, 0);
                        Utils.delay(10);
                        enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_RET, mainForm.DefaultAddr, 0);
                        Utils.delay(10);
                        enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.PULS_1, mainForm.DefaultAddr, 0);
                        Utils.delay(10);
                        enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, mainForm.coolContAddr, 0);
                        Utils.delay(10);
                        enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, mainForm.coolContAddr, 0);
                        Utils.delay(10);
                        enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, mainForm.coolContAddr, 0);
                        Utils.delay(10);
                        enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, mainForm.coolContAddr, 0);
                        Utils.delayThread(50);
                        softReset(0);
                        
                        testPhase++;
                        break;
                    }
            }
        }

        private void testRetention()
        {
            allOutputs(0);
            enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_RET, mainForm.DefaultAddr, 1);
            Utils.delay(100);
        }

        private void testPulsation()
        {
            allOutputs(0);
            enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.PULS_1, mainForm.DefaultAddr, 1);
            Utils.delay(100);
        }

        private void getGrossWeight()
        {
            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_GROSS_WGT, 0, 1, 0, null);
            serialOut(text);
            Utils.delay(100);
            logTest("ADC:" + TestIO.adc.ToString() + " Gross:" + TestIO.grosswgt.ToString());

        }

        private void button5_Click(object sender, EventArgs e)
        {
            //testMainLine();
            //requestTemperature();
            //requestConductivity();
            testSmartStartReedSwitch(); //Commander IP_1 should be 1
            //testDivertLineReedSwitch(); //Commander IP_2 should be 1
            //testRetention();
            //testPulsation();
            //getGrossWeight();
        }

        private void checkTestsButton(object sender, EventArgs e)
        {
            //testMainLine();
            //requestTemperature();
            //requestConductivity();
            testSmartStartReedSwitch(); //Commander IP_1 should be 1
            //testDivertLineReedSwitch(); //Commander IP_2 should be 1
            //testRetention();
            //testPulsation();
            //getGrossWeight();
        }
        private void requestConductivity()
        {
            allOutputs(0);
            enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, mainForm.coolContAddr, 1);
            Utils.delay(100);            

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_CONDUCTIVITY, 0, 1, 0, null);
            serialOut(text);
            Utils.delay(100);
            if (TestIO.LastRawConductivityAdcRange > 0)
                logTest("Conductivity:" + TestIO.LastConductivity.ToString("0.00") + " RawAdcRange:" + TestIO.LastRawConductivityAdcRange.ToString("0"));
            else
                logTest("Conductivity request failed");


        }
        private void requestTemperature()
        {
            string text = msgProc.SendCanMessage((int) Constants.CAN_COMMANDS.CAN_CMD_COMMANDER_REQUEST_TEMPERATURE, 0, 1, 0, null);
            TestIO.LastTemperature = 0;
            serialOut(text);
            Utils.delay(100);
            if (TestIO.LastTemperature > 0) // Obvisouly will not work work if temp is 0
                logTest(TestIO.LastTemperature.ToString("0.00"));
            else 
                logTest("Temperature request failed");
        }

        private void testSmartStartReedSwitch()
        {
            allOutputs(0);
            enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, mainForm.DefaultAddr, 1);            
            enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, mainForm.DefaultAddr, 1);            
            //enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_RET, mainForm.DefaultAddr, 1);
            enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, mainForm.DefaultAddr, 1);
            enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_CCAIR, mainForm.DefaultAddr, 1);

            Utils.delay(100);
       
            //FOR SMART START
            enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, mainForm.coolContAddr, 1);
            
        }

        private void testDivertLineReedSwitch()
        {
            allOutputs(0);
            enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, mainForm.DefaultAddr, 1);
            enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, mainForm.DefaultAddr, 1);
            //enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_RET, mainForm.DefaultAddr, 1);
            enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, mainForm.DefaultAddr, 1);
            Utils.delay(10);

            //Turn off Prox switch
            enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, mainForm.coolContAddr, 1);

            Utils.delay(10);
            //FOR DIVERT LINE REED SWITCH
            enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, mainForm.coolContAddr, 1);
        }

        private void proxSwitch()
        {
             allOutputs(0);
             enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, mainForm.DefaultAddr, 1);
             enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, mainForm.coolContAddr, 1);
        //Utils.delay(10);
        }

        private void probes()
        {
            allOutputs(0);
            //enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, mainForm.DefaultAddr, 1);
            //enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.LINE_3, mainForm.DefaultAddr, 1);
            //enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O2, mainForm.coolContAddr, 1);
            //Utils.delayThread(500);

            string text = mainForm.msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_CONDUCTIVITY, mainForm.DefaultAddr, 1, 0, null);
            serialOut(text);
            //Utils.delay(10);
            Utils.delayThread(500);
           
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            testSmartStartReedSwitch();
            Utils.delay(100);
            enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, mainForm.coolContAddr, 0);
            Utils.delay(100);
            testDivertLineReedSwitch();
            Utils.delay(200);
            TestIO.LastIO_inputs = "";
            TestIO.LastIO_outputs = "";
            RequestIO(250);
            Utils.delay(200);
            logTest("testDivertLineReedSwitch " + TestIO.LastIO_outputs + " " + TestIO.LastIO_inputs);
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.progressBar1.Increment(1);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //frm2 = new KeyPadTest();
            //DialogResult dr = frm2.ShowDialog(this);
            //if (dr == DialogResult.Cancel)
            //{
            //    frm2.Close();
            //}
            //else if (dr == DialogResult.OK)
            //{
            //    //textBox1.Text = frm2.getText();
            //    //frm2.Close();
            //}

            FactoryTestMode();
            factTest.Clear();
            if (keyPadTest == null) keyPadTest = new KeyPadTest();
            KeyPadTestInitiated = true;
            keyPadTest.Show();
            
        }

        private void keyPadTestButton(object sender, EventArgs e)
        {

            FactoryTestMode();
            factTest.Clear();
            if (keyPadTest == null) keyPadTest = new KeyPadTest();
            KeyPadTestInitiated = true;
            keyPadTest.Show();

        }

        private void MilkLeadTest()
        {
            FactoryTestMode();
            factTest.Clear();

            if (milkLeadTest == null) milkLeadTest = new MilkLeadTest(this);
            milkLeadTestInitiated = true;
            milkLeadTest.ShowDialog();
            this.Hide();
        }

        private void DivertLineOPTest()
        {
            CommanderLoomTests.enableOutputCommander(this, Constants.CommanderOutputs.LINE_3);
                        
        }


        private void button3_Click_1(object sender, EventArgs e)
        {
            PrintDocument document = new System.Drawing.Printing.PrintDocument();
            document.OriginAtMargins = true;

            PrintPreviewDialog ppd = new PrintPreviewDialog();
            ppd.ClientSize = new System.Drawing.Size(292, 266);
            ppd.Location = new System.Drawing.Point(0, 0);

            Margins margins = new Margins(100, 100, 100, 100);
            document.PrintPage += new PrintPageEventHandler(Doc_PrintPage);
            document.DefaultPageSettings.Margins = margins;

            ppd.Document = document;
            ppd.ShowDialog();
            
        }

        private void Doc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            String testHeader = "Name: " + textBox1.Text + Environment.NewLine;
            testHeader += "Job Traveller: " + textBox2.Text + Environment.NewLine;
            testHeader += "Test: " + textBox3.Text + Environment.NewLine;
            
            string testResults = richTextBoxTestLogs.Text;
            
            Font printFont = new Font("Tahoma", 14, System.Drawing.FontStyle.Bold);
            e.Graphics.DrawString(testHeader, printFont, Brushes.Blue, 0, 0);            
           
            printFont = new Font("Tahoma", 12, System.Drawing.FontStyle.Regular);
            e.Graphics.DrawString(testResults, printFont, Brushes.Black, 0, 100);
        }

        private void autoPrintTest()
        {
            PrintDocument document = new System.Drawing.Printing.PrintDocument();
            document.OriginAtMargins = true;

            PrintPreviewDialog ppd = new PrintPreviewDialog();
            ppd.ClientSize = new System.Drawing.Size(292, 266);
            ppd.Location = new System.Drawing.Point(0, 0);

            Margins margins = new Margins(100, 100, 100, 100);
            document.PrintPage += new PrintPageEventHandler(Doc_PrintPage);
            document.DefaultPageSettings.Margins = margins;

            ppd.Document = document;
            //ppd.ShowDialog();
            ppd.Document.Print();

        }

        private void richTextBoxTestLogs_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            mainForm.commanderTestForm.Close();
            mainForm.commanderTestForm = null;
            mainForm.textBoxName.Clear();
            mainForm.textBoxJobTraveller.Clear();
            mainForm.comboBoxTestSelect.SelectedIndex = -1;
            mainForm.Show();


        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            mainForm.closeComButton();
            mainForm.commanderTestForm.Close();
            mainForm.commanderTestForm = null;
            mainForm.Close();
            mainForm = null;
        }

        public void SendNvrCmdSet(int did, int NvrAddr, int NvrValue)
        {
            byte[] data = new byte[8];
            data[0] = (byte)(NvrAddr & 0xff);
            data[1] = (byte)(NvrAddr >> 8);
            data[2] = (byte)(NvrValue & 0xff);
            data[3] = (byte)((NvrValue >> 8) & 0xff);
            data[4] = (byte)((NvrValue >> 16) & 0xff);
            data[5] = (byte)((NvrValue >> 32) & 0xff);
            serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_NVR_SET, did, 1, 6, data));

            //softReset(did);
        }

        public void softReset(int did)
        {
            byte[] data = new byte[8];
            data[0] = 2;

            serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_DO_SOFTWARE_RESET, did, 1, 1, data));
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            FactoryTestMode();
            Utils.delayThread(10);
            allOutputs(0);
            Utils.delayThread(10);
            //enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.MAIN_LINE, mainForm.DefaultAddr, 1);
            enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_BOTTOM, mainForm.DefaultAddr, 1);

            //enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_TOP, mainForm.DefaultAddr, 1);




            //Utils.delayThread(10);
            //enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, mainForm.coolContAddr, 0);
            //Utils.delayThread(10);
            //enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, mainForm.coolContAddr, 0);
            //Utils.delayThread(10);
            //enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, mainForm.coolContAddr, 0);
            //Utils.delayThread(10);
            //allOutputs(0);
                 
            //calibrateZero();
            //proxSwitch();
            //testSmartStartReedSwitch();
            //probes();
            //testDivertLineReedSwitch();
            //getVersion();
            //DivertLineOPTest();
            //MilkLeadTest();
            //getWeighCalFactor();

        }

        public void getWeighCalFactor()
        {
            int nvrAddr = 58;
            byte[] data = new byte[8]; // enough to hold all numbers up to 64-bits            	                    
            data[0] = (byte)(nvrAddr & 0xff);
            //data[0] = (byte)(103 & 0xff);
            data[1] = (byte)(nvrAddr >> 8);
            //data[1] = (byte)(103 >> 8);
            serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_NVR_GET, 250, 1, 2, data));
        }

        public void getProgType()
        {
            int nvrAddr = 103;
            byte[] data = new byte[8]; // enough to hold all numbers up to 64-bits            	                    
            data[0] = (byte)(nvrAddr & 0xff);
            //data[0] = (byte)(103 & 0xff);
            data[1] = (byte)(nvrAddr >> 8);
            //data[1] = (byte)(103 >> 8);
            serialOut(msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_NVR_GET, 250, 1, 2, data));
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            RequestIO();
        }

        private void checkBoxShowOPLog_CheckedChanged_1(object sender, EventArgs e)
        {

        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            //SendNvrCmdSet(250, 103, 1); //progType 1 for commander; 0 for indicator

            //SendNvrCmdSet(250, 58, 500); //cal fac set this to 500 for milk lead calibration test

            FactoryTestMode();
            Utils.delayThread(10);
            enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, mainForm.coolContAddr, 0);
            Utils.delayThread(10);
            enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, mainForm.coolContAddr, 0);
            Utils.delayThread(10);
            enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, mainForm.coolContAddr, 0);
            Utils.delayThread(10);
            allOutputs(1);
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            byte[] Data = new byte[8];
            //uncomment this
            if (checkBox1.Checked)
                Data[0] = 1;
            else
                Data[1] = 0;

            string text = msgProc.SendCanMessage((int)Constants.CAN_COMMANDS.CAN_CMD_FACTORY_TEST, 0, 1, 1, Data);
            serialOut(text);
        }

        private void abortTest_Click(object sender, EventArgs e)
        {
            //testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
            //progressBar1.Value = (int)Constants.CommanderTestPhase.TEST_FINISHED;
            factTest.CurrentTestAborted = true;
            //processFactoryTest();
            abortTest.Enabled = false;
        }

    }
}
