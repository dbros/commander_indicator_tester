﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Configuration;
using CAN232_Monitor.Models;

namespace CAN232_Monitor.Helper
{
	public class APIHelper
	{
		private HttpClient apiClient;

		public APIHelper()
		{
			InitializeClient();
		}

		private void InitializeClient()
		{
			string api = ConfigurationManager.AppSettings["api"];

			apiClient = new HttpClient();
			apiClient.BaseAddress = new Uri(api);
			apiClient.DefaultRequestHeaders.Accept.Clear();
			apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
		}

		public async Task<Employee> GetEmployeeData(string empBadge, string pin)
		{
			using (HttpResponseMessage response = await apiClient.GetAsync(string.Format("api/Employee?empBadge={0}&pin={1}", empBadge, pin)))
			{

				if (response.IsSuccessStatusCode)
				{
					var result = await response.Content.ReadAsAsync<Employee>();
					return result;
				}
				else
				{
					throw new Exception(response.ReasonPhrase);
				}

			}
		}

		public async Task<Employee> GetOverrideEmployeeData(string empBadge, string pin)
		{
			return new Employee{
				EmployeeReference = "000000",
				FirstName = "SYSTEM",
				LastName = "OVERRIDE",
				Department = "ADMIN",
				SubDepartment = "ADMIN",
				SupervisorEmail = "DBROSNAN@DAIRYMASTER.COM"

			};
		}

		public async Task<bool> IsEmployeeValid(string empBadge, string pin)
		{
			/*var data = new FormUrlEncodedContent(new[]
			{
				//new KeyValuePair<string, string>("grant_type", "Password"),
				new KeyValuePair<string, string>("empBadge", empBadge),
				new KeyValuePair<string, string>("pin", pin),
			});*/

			using (HttpResponseMessage response = await apiClient.GetAsync(string.Format("api/Test/GetValidEmployee?empBadge={0}&pin={1}", empBadge, pin)))
			{

				if (response.IsSuccessStatusCode)
				{
					return await response.Content.ReadAsAsync<bool>();
				}
				else
				{
					throw new Exception(response.ReasonPhrase);
				}

			}
		}
	}
}
