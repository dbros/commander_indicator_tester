﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Media;
using System.Windows.Forms;

namespace CAN232_Monitor
{
    public class ProcessCommTestResp
    {
        //static string[] KEY_WAV_FILES = {"Draft.wav","Draft.wav","feed.wav","test.wav","lock.wav","Up.Wav","down.wav",
        //                          "1.Wav","2.Wav","3.Wav","4.Wav","5.Wav","6.Wav","7.Wav","8.Wav","9.Wav","Zero.Wav",
        //                          "hold.wav","Cow.Wav","Divert.wav","A C R.Wav","function.wav","Enter.Wav","sound.wav","Milk.Wav"};

        static string[] KEY_PAD_BUTTONS = {"Draft","Sample","Down","7","5"};

        public static string LastIO_outputs = "";
        public static string LastIO_inputs = "";

        public static int adcCounts;
        public static int grossWeight;

        public static string logDLRS = "";

        public static bool keyPadTestFinished = false;
        public static bool keyPadTestAborted = false;
        public static bool milkMeterLeadTestAborted = false;


        public static void processTestResponse(CommanderTestForm commanderTestForm, CanMsg cm)
        {
            
            Constants.CAN_COMMANDS canCommand = (Constants.CAN_COMMANDS)cm.Cmd;
            byte[] data;
            cm.cnvtDataToBytes(out data);

            switch (canCommand)
            {
                case Constants.CAN_COMMANDS.CAN_CMD_NVR_REPORT_VALUE:
                    {
                        int NvrAddr = (data[1] << 8) | data[0];
                        int NvrValue = (data[3] << 8) | data[2];

                        if (cm.Sid == commanderTestForm.mainForm.DefaultAddr)
                        {
                            if (NvrAddr == 103)
                            { 
                                commanderTestForm.factTest.progType = NvrValue;

                                //commanderTestForm.logTest("ProgType: NvrAddr " + NvrAddr + ", NvrValue" + NvrValue);
                            }
                            else if (NvrAddr == 58)
                            {
                                commanderTestForm.factTest.weighCalFactor = NvrValue;

                                //commanderTestForm.logTest("WeighCalFactor: NvrAddr " + NvrAddr + ", NvrValue" + NvrValue);
                            }
                        }

                        break;
                    }
                case Constants.CAN_COMMANDS.CAN_CMD_TEXT:
                    {
                        string keyPadButton = cm.cnvtDataToString();

                        if ((keyPadButton.Substring(0, 3) == "KEY") & commanderTestForm.KeyPadTestInitiated)
                        {
                            keyPadButton = keyPadButton.Substring(4, 3).TrimStart('0');
                            int keyNum = Int32.Parse(keyPadButton);
                            int keyNumRtn = checkKeyPadButton(commanderTestForm, keyNum);

                            if (keyNumRtn == 7)
                            {
                                if ((commanderTestForm.mainForm.performKeyTest) && (commanderTestForm.factTest.KeyPadTestPassed))
                                {
                                    commanderTestForm.logTest("KEYPAD TEST PASSED");
                                }
                                else
                                {
                                    commanderTestForm.logTest("KEYPAD TEST FAILED.");
                                }

                                if (commanderTestForm.keyPadTest != null)
                                {
                                    commanderTestForm.KeyPadTestInitiated = false;
                                    commanderTestForm.keyPadTest.richTextBox1.Clear();                                    
                                    commanderTestForm.keyPadTest.Close();
                                    commanderTestForm.keyPadTest = null;
                                }

                                keyPadTestFinished = true;
                                commanderTestForm.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                commanderTestForm.processFactoryTest();
                            }
                        }

                        commanderTestForm.log(cm.cnvtDataToString());                        
                        break;
                    }

                case Constants.CAN_COMMANDS.CAN_CMD_REPORT_VERSION:
                    {

						if (commanderTestForm.factTest.LastVersion == "")
						{
							if (cm.Sid == commanderTestForm.mainForm.DefaultAddr)
							{
								int ProgID = (data[1] << 8) | data[0];
								int Version = (data[3] << 8) | data[2];
								int MinorVersion = (data[5] << 8) | data[4];
                                //LastVersion = "";


                    commanderTestForm.factoryTestVersionReported(cm.Sid, ProgID, Version, MinorVersion);

								commanderTestForm.factTest.LastVersion = Version.ToString() + MinorVersion.ToString();

								commanderTestForm.log("(REPORT VERSION) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
									+ " ProgID:" + ProgID.ToString()
									+ " Version:" + Version.ToString()
									+ " Minor Version:" + MinorVersion.ToString()
									);

								commanderTestForm.log("(REPORT VERSION) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
									+ " ProgID:" + ProgID.ToString()
									+ " Version:" + Version.ToString()
									+ " Minor Version:" + MinorVersion.ToString()
									);

								//commanderTestForm.logTest("(REPORT VERSION) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
								//    + " ProgID:" + ProgID.ToString()
								//    + " Version:" + Version.ToString()
								//    + " Minor Version:" + MinorVersion.ToString()
								//    );

								checkCommanderVersion(commanderTestForm, Version, MinorVersion);
							}
						}

						break;
                    }
 
                case Constants.CAN_COMMANDS.CAN_CMD_REPORT_IO:
                    {
                        if (commanderTestForm.checkBoxShowOPLog.Checked)
                        {
                            cm.decodeIO(out LastIO_outputs, out LastIO_inputs);

                            commanderTestForm.log("(IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                    + " " + LastIO_outputs + " " + LastIO_inputs
                                    );

                            commanderTestForm.FactoryTestMode();
                            Utils.delayThread(10);
                            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, commanderTestForm.mainForm.coolContAddr, 0);
                            Utils.delayThread(10);
                            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O2, commanderTestForm.mainForm.coolContAddr, 0);
                            Utils.delayThread(10);
                            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, commanderTestForm.mainForm.coolContAddr, 0);
                            Utils.delayThread(10);
                            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, commanderTestForm.mainForm.coolContAddr, 0);
                            Utils.delayThread(10);
                            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O5, commanderTestForm.mainForm.coolContAddr, 0);
                            Utils.delayThread(10);
                            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O6, commanderTestForm.mainForm.coolContAddr, 0);
                            Utils.delayThread(10);
                            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O7, commanderTestForm.mainForm.coolContAddr, 0);
                            Utils.delayThread(10);
                            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O8, commanderTestForm.mainForm.coolContAddr, 0);
                            Utils.delayThread(10);
                            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, commanderTestForm.mainForm.coolContAddr, 0);
                            Utils.delayThread(10);
                        }

                        if ((cm.Sid == commanderTestForm.mainForm.DefaultAddr) && (commanderTestForm.testPhase == Constants.CommanderTestPhase.TEST_IO))
                        {
                            //LastIO_outputs = "";
                            //LastIO_inputs = "";
                            
                            cm.decodeIO(out LastIO_outputs, out LastIO_inputs);
                            commanderTestForm.logTest("(IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                    + " " + LastIO_outputs + " " + LastIO_inputs
                                    );
                            checkOutputs(commanderTestForm, cm, LastIO_outputs, LastIO_inputs);

                            commanderTestForm.FactoryTestMode();
                            Utils.delayThread(10);
                            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, commanderTestForm.mainForm.coolContAddr, 0);
                            Utils.delayThread(10);
                            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, commanderTestForm.mainForm.coolContAddr, 0);
                            Utils.delayThread(10);
                            commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, commanderTestForm.mainForm.coolContAddr, 0);
                            Utils.delayThread(10);
                            
                        }

                        else if (cm.Sid == commanderTestForm.mainForm.coolContAddr)
                        {
                            string test = "";
                            if (CommanderLoomTests.ioRequestSent)
                            {
                                if (commanderTestForm.testPhase == Constants.CommanderTestPhase.COMMON_LOOMS)
                                {
                                    test = "testCommon";
                                }
                                else if (commanderTestForm.testPhase == Constants.CommanderTestPhase.COMMON_ROT_LOOMS)
                                {
                                    test = "testCommonRty";
                                }
                                else if (commanderTestForm.testPhase == Constants.CommanderTestPhase.PRODUCT_SPECIFIC_LOOMS)
                                {
                                    test = "prodSpecificTest";
                                }
                            }
                            LastIO_inputs = "";
                            LastIO_outputs = "";

                            cm.decodeIO(out LastIO_outputs, out LastIO_inputs);
                            processIOResponseCommon(commanderTestForm, cm, test);
                        }

                        else if (cm.Sid == commanderTestForm.mainForm.DefaultAddr)
                        {
                            string test = "";
                            if (CommanderLoomTests.ioRequestSent)
                            {
                                if (commanderTestForm.testPhase == Constants.CommanderTestPhase.COMMON_LOOMS)
                                {
                                    test = "testCommon";
                                }
                                else if (commanderTestForm.testPhase == Constants.CommanderTestPhase.COMMON_ROT_LOOMS)
                                {
                                    test = "testCommonRty";
                                }
                                else if (commanderTestForm.testPhase == Constants.CommanderTestPhase.PRODUCT_SPECIFIC_LOOMS)
                                {
                                    test = "prodSpecificTest";
                                }
                            }
                            logDLRS = "";
                            LastIO_inputs = "";
                            LastIO_outputs = "";

                            cm.decodeIO(out LastIO_outputs, out LastIO_inputs);
                            processIOResponseCommon(commanderTestForm, cm, test);                            
                        }

                        break;
                    }
                case Constants.CAN_COMMANDS.CAN_CMD_CONDUCTIVITY:
                    {
                        if (cm.Sid == commanderTestForm.mainForm.DefaultAddr)
                        {
                            checkConductivity(commanderTestForm, cm);                        

                        }
                      
                        break;
                    }
                case Constants.CAN_COMMANDS.CAN_CMD_LOG_TEMP_DATA:
                    {
                        if (cm.Sid == commanderTestForm.mainForm.DefaultAddr)
                        {
                            checkTemp(commanderTestForm, cm);
                        }

                        break;
                    }
                case Constants.CAN_COMMANDS.CAN_CMD_LONG_MSG:
                    {
                        commanderTestForm.processLongMessage(cm);
                        break;
                    }

                case Constants.CAN_COMMANDS.CAN_CMD_GROSS_WGT:
                    {
                        int adcCounts = 0;
                        int grossWeight = 0;

                        adcCounts = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 32);
                        grossWeight = data[4] | (data[5] << 8) | (data[6] << 16) | (data[7] << 32);

                        if (cm.Sid == commanderTestForm.mainForm.DefaultAddr && !commanderTestForm.factTest.MilkMeterLeadTestComplete)
                        {
                            //commanderTestForm.factTest.MilkMeterLeadTestComplete = true;

                            //log("Adc Counts:" + adcCounts.ToString() + " GrossWeight:" + grossWeight.ToString());
                            commanderTestForm.milkLeadTest.logMilkLeadTest(" DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString() +"Adc Counts:" + adcCounts.ToString() + " GrossWeight:" + grossWeight.ToString());
                            commanderTestForm.log(" DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString() + "Adc Counts:" + adcCounts.ToString() + " GrossWeight:" + grossWeight.ToString());

                            if (!commanderTestForm.factTest.Weight500gTestComplete)
                            { 
                            checkWeight(commanderTestForm, grossWeight, "500g");
                            }
                            else if (!commanderTestForm.factTest.Weight250gTestComplete)
                            {
                                checkWeight(commanderTestForm, grossWeight, "250g");
                            }
                        }

                        break;
                    }
                default: break;
            }
        }

        public static void processTestResponseNew(Can232 can232Form, CanMsg cm)
        {

            Constants.CAN_COMMANDS canCommand = (Constants.CAN_COMMANDS)cm.Cmd;
            byte[] data;
            cm.cnvtDataToBytes(out data);

            if (cm.Sid == 250)
			{
                can232Form.logTest("Commander Board message");
            }

            if (cm.Sid == 201)
            {
                can232Form.logTest("TANK Board message");
            }

            if (cm.Sid == 200)
            {
                can232Form.logTest("TANK Board message");
            }

            switch (canCommand)
            {
                case Constants.CAN_COMMANDS.CAN_CMD_NVR_REPORT_VALUE:
                    {
                        int NvrAddr = (data[1] << 8) | data[0];
                        int NvrValue = (data[3] << 8) | data[2];

                        if (cm.Sid == can232Form.DefaultAddr)
                        {
                            if (NvrAddr == 103)
                            {
                                can232Form.factTest.progType = NvrValue;

                                //commanderTestForm.logTest("ProgType: NvrAddr " + NvrAddr + ", NvrValue" + NvrValue);
                            }
                            else if (NvrAddr == 58)
                            {
                                can232Form.factTest.weighCalFactor = NvrValue;

                                //commanderTestForm.logTest("WeighCalFactor: NvrAddr " + NvrAddr + ", NvrValue" + NvrValue);
                            }
                        }

                        break;
                    }
                case Constants.CAN_COMMANDS.CAN_CMD_TEXT:
                    {
                        string keyPadButton = cm.cnvtDataToString();

                        if ((keyPadButton.Substring(0, 3) == "KEY") & can232Form.KeyPadTestInitiated)
                        {
                            keyPadButton = keyPadButton.Substring(4, 3).TrimStart('0');
                            int keyNum = Int32.Parse(keyPadButton);
                            int keyNumRtn = checkKeyPadButtonNew(can232Form, keyNum);

                            //if (keyNumRtn == 7)
                            if (keyNumRtn == 22)
                            {
                                if ((can232Form.performKeyTest) && (can232Form.factTest.KeyPadTestPassed))
                                {
                                    can232Form.setTestStepResult("KEYPAD", "PASS");
                                    can232Form.logTest("KEYPAD TEST PASSED");
                                }
                                else
                                {
                                    can232Form.setTestStepResult("KEYPAD", "FAIL");
                                    can232Form.setTestStepResult("OVERALL", "FAIL");
                                    can232Form.logTest("KEYPAD TEST FAILED.");
                                }

                                if (can232Form.keyPadTest != null)
                                {
                                    can232Form.KeyPadTestInitiated = false;
                                    can232Form.keyPadTest.richTextBox1.Clear();
                                    can232Form.keyPadTest.Close();
                                    can232Form.keyPadTest = null;
                                }

                                keyPadTestFinished = true;
                                can232Form.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
                                can232Form.processFactoryTest();
                            }
                        }

                        can232Form.logTest(cm.cnvtDataToString());
                        break;
                    }

                case Constants.CAN_COMMANDS.CAN_CMD_REPORT_VERSION:
                    {

						//                  if ((can232Form.factTest.IAPVersion == "") && (cm.Sid == can232Form.DefaultAddr))
						//{
						//                      int ProgID = (data[1] << 8) | data[0];
						//                      int Version = (data[3] << 8) | data[2];
						//                      int MinorVersion = (data[5] << 8) | data[4];

						//                      can232Form.logTest(" cm.Sid:" + cm.Sid.ToString());

						//                      can232Form.logTest(" ProgID:" + ProgID.ToString()
						//                                  + " Version:" + Version.ToString()
						//                                  + " Minor Version:" + MinorVersion.ToString()
						//                                  );
						//                      can232Form.factoryTestVersionReported(cm.Sid, ProgID, Version, MinorVersion);
						//                  }
						//                  else
						//{
						//	if (can232Form.factTest.LastVersion == "" && can232Form.factTest.IAPVersionTestComplete)
						//	{
						//		if (cm.Sid == can232Form.DefaultAddr)
						//		{
						//			int ProgID = (data[1] << 8) | data[0];
						//			int Version = (data[3] << 8) | data[2];
						//			int MinorVersion = (data[5] << 8) | data[4];
						//			//LastVersion = "";

						//			can232Form.factoryTestVersionReported(cm.Sid, ProgID, Version, MinorVersion);

						//			can232Form.factTest.LastVersion = Version.ToString() + MinorVersion.ToString();

						//			can232Form.logTest("(REPORT VERSION) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
						//				+ " ProgID:" + ProgID.ToString()
						//				+ " Version:" + Version.ToString()
						//				+ " Minor Version:" + MinorVersion.ToString()
						//				);

						//			can232Form.logTest("(REPORT VERSION) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
						//				+ " ProgID:" + ProgID.ToString()
						//				+ " Version:" + Version.ToString()
						//				+ " Minor Version:" + MinorVersion.ToString()
						//				);

						//			//commanderTestForm.logTest("(REPORT VERSION) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
						//			//    + " ProgID:" + ProgID.ToString()
						//			//    + " Version:" + Version.ToString()
						//			//    + " Minor Version:" + MinorVersion.ToString()
						//			//    );

						//			checkCommanderVersionNew(can232Form, Version, MinorVersion);
						//		}
						//	}
						//}



						if (can232Form.factTest.LastVersion == "")
						{
							if (cm.Sid == can232Form.DefaultAddr)
							{
								int ProgID = (data[1] << 8) | data[0];
								int Version = (data[3] << 8) | data[2];
								int MinorVersion = (data[5] << 8) | data[4];
                                int iapCRC = (data[7] << 8) | data[6];
                                //LastVersion = "";

                                if (iapCRC == 36947)
                                {
                                    can232Form.factTest.IAPVersionPassed = true;
                                }
                                else
								{
                                    can232Form.factTest.IAPVersionPassed = false;
                                }

                                can232Form.factoryTestVersionReported(cm.Sid, ProgID, Version, MinorVersion);

								can232Form.factTest.LastVersion = Version.ToString() + MinorVersion.ToString();

								can232Form.logTest("(REPORT VERSION) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
									+ " ProgID:" + ProgID.ToString()
									+ " Version:" + Version.ToString()
									+ " Minor Version:" + MinorVersion.ToString()
									);

								can232Form.logTest("(REPORT VERSION) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
									+ " ProgID:" + ProgID.ToString()
									+ " Version:" + Version.ToString()
									+ " Minor Version:" + MinorVersion.ToString()
									);

								//commanderTestForm.logTest("(REPORT VERSION) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
								//    + " ProgID:" + ProgID.ToString()
								//    + " Version:" + Version.ToString()
								//    + " Minor Version:" + MinorVersion.ToString()
								//    );

								checkCommanderVersionNew(can232Form, Version, MinorVersion);
							}
						}

						break;
                    }

                case Constants.CAN_COMMANDS.CAN_CMD_REPORT_IO:
                    {
                        if (can232Form.checkBoxShowOPLog.Checked)
                        {
                            cm.decodeIO(out LastIO_outputs, out LastIO_inputs);

                            can232Form.logTest("(IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                    + " " + LastIO_outputs + " " + LastIO_inputs
                                    );

                            can232Form.FactoryTestMode();
                            Utils.delayThread(10);
                            can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O1, can232Form.coolContAddr, 0);
                            Utils.delayThread(10);
                            can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O2, can232Form.coolContAddr, 0);
                            Utils.delayThread(10);
                            can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, can232Form.coolContAddr, 0);
                            Utils.delayThread(10);
                            can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, can232Form.coolContAddr, 0);
                            Utils.delayThread(10);
                            can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O5, can232Form.coolContAddr, 0);
                            Utils.delayThread(10);
                            can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O6, can232Form.coolContAddr, 0);
                            Utils.delayThread(10);
                            can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O7, can232Form.coolContAddr, 0);
                            Utils.delayThread(10);
                            can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O8, can232Form.coolContAddr, 0);
                            Utils.delayThread(10);
                            can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, can232Form.coolContAddr, 0);
                            Utils.delayThread(10);
                        }

                        if ((cm.Sid == can232Form.DefaultAddr) && (can232Form.testPhase == Constants.CommanderTestPhase.TEST_IO))
                        {
                            //LastIO_outputs = "";
                            //LastIO_inputs = "";

                            cm.decodeIO(out LastIO_outputs, out LastIO_inputs);
                            can232Form.logTest("(IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                    + " " + LastIO_outputs + " " + LastIO_inputs
                                    );
                            checkOutputsNew(can232Form, cm, LastIO_outputs, LastIO_inputs);

                            can232Form.FactoryTestMode();
                            Utils.delayThread(10);
                            can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, can232Form.coolContAddr, 0);
                            Utils.delayThread(10);
                            can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O3, can232Form.coolContAddr, 0);
                            Utils.delayThread(10);
                            can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O9, can232Form.coolContAddr, 0);
                            Utils.delayThread(10);

                        }

                        else if (cm.Sid == can232Form.coolContAddr)
                        {
                            string test = "";
                            if (CommanderLoomTests.ioRequestSent)
                            {
                                if (can232Form.testPhase == Constants.CommanderTestPhase.COMMON_LOOMS)
                                {
                                    test = "testCommon";
                                }
                                else if (can232Form.testPhase == Constants.CommanderTestPhase.COMMON_ROT_LOOMS)
                                {
                                    test = "testCommonRty";
                                }
                                else if (can232Form.testPhase == Constants.CommanderTestPhase.PRODUCT_SPECIFIC_LOOMS)
                                {
                                    test = "prodSpecificTest";
                                }
                            }
                            LastIO_inputs = "";
                            LastIO_outputs = "";

                            cm.decodeIO(out LastIO_outputs, out LastIO_inputs);
                            processIOResponseCommonNew(can232Form, cm, test);
                        }

                        else if (cm.Sid == can232Form.DefaultAddr)
                        {
                            string test = "";
                            if (CommanderLoomTests.ioRequestSent)
                            {
                                if (can232Form.testPhase == Constants.CommanderTestPhase.COMMON_LOOMS)
                                {
                                    test = "testCommon";
                                }
                                else if (can232Form.testPhase == Constants.CommanderTestPhase.COMMON_ROT_LOOMS)
                                {
                                    test = "testCommonRty";
                                }
                                else if (can232Form.testPhase == Constants.CommanderTestPhase.PRODUCT_SPECIFIC_LOOMS)
                                {
                                    test = "prodSpecificTest";
                                }
                            }
                            logDLRS = "";
                            LastIO_inputs = "";
                            LastIO_outputs = "";

                            cm.decodeIO(out LastIO_outputs, out LastIO_inputs);
                            processIOResponseCommonNew(can232Form, cm, test);
                        }

                        break;
                    }
                case Constants.CAN_COMMANDS.CAN_CMD_CONDUCTIVITY:
                    {
                        if (cm.Sid == can232Form.DefaultAddr)
                        {
                            checkConductivityNew(can232Form, cm);

                        }

                        break;
                    }
                case Constants.CAN_COMMANDS.CAN_CMD_LOG_TEMP_DATA:
                    {
                        if (cm.Sid == can232Form.DefaultAddr)
                        {
                            checkTempNew(can232Form, cm);
                        }

                        break;
                    }
                case Constants.CAN_COMMANDS.CAN_CMD_LONG_MSG:
                    {
                        can232Form.processLongMessage(cm);
                        break;
                    }

                case Constants.CAN_COMMANDS.CAN_CMD_GROSS_WGT:
                    {
                        int adcCounts = 0;
                        int grossWeight = 0;

                        adcCounts = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 32);
                        grossWeight = data[4] | (data[5] << 8) | (data[6] << 16) | (data[7] << 32);

                        if (cm.Sid == can232Form.DefaultAddr && !can232Form.factTest.MilkMeterLeadTestComplete)
                        {
                            //commanderTestForm.factTest.MilkMeterLeadTestComplete = true;

                            //log("Adc Counts:" + adcCounts.ToString() + " GrossWeight:" + grossWeight.ToString());
                            can232Form.milkLeadTest.logMilkLeadTest(" DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString() + "Adc Counts:" + adcCounts.ToString() + " GrossWeight:" + grossWeight.ToString());
                            can232Form.logTest(" DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString() + "Adc Counts:" + adcCounts.ToString() + " GrossWeight:" + grossWeight.ToString());

                            if (!can232Form.factTest.Weight500gTestComplete)
                            {
                                checkWeightNew(can232Form, grossWeight, "500g");
                            }
                            else if (!can232Form.factTest.Weight250gTestComplete)
                            {
                                checkWeightNew(can232Form, grossWeight, "250g");
                            }
                        }

                        break;
                    }
                default: break;
            }
        }
        private static void checkWeight(CommanderTestForm commanderTestForm, int weightToCheck, string weightValue)
        {
            if (weightValue == "500g")
            {

                if ((weightToCheck >= 4900) && (weightToCheck <= 5100))
                //if ((weightToCheck >= 4990) && (weightToCheck <= 5010))
                {
                    //commanderTestForm.factTest.MilkMeterLeadTestPassed = true;
                    commanderTestForm.factTest.Weight500gTestPassed = true;
                    commanderTestForm.milkLeadTest.logMilkLeadTest("500g TEST PASSED");
                    commanderTestForm.log("500g TEST PASSED");
                    
                }
                else
                {
                    //commanderTestForm.factTest.MilkMeterLeadTestPassed = false;
                    commanderTestForm.factTest.Weight500gTestPassed = false;
                    commanderTestForm.factTest.OverallTestPassed = false;
                    commanderTestForm.milkLeadTest.logMilkLeadTest("500g TEST FAILED");
                    commanderTestForm.log("500g TEST FAILED");                    
                }

                commanderTestForm.factTest.Weight500gTestComplete = true;
            }
            
            if (weightValue == "250g")
            {
                if ((weightToCheck >= 2450) && (weightToCheck <= 2550))
                //if ((weightToCheck >= 4990) && (weightToCheck <= 5010))
                {
                    //commanderTestForm.factTest.MilkMeterLeadTestPassed = true;
                    commanderTestForm.factTest.Weight250gTestPassed = true;
                    commanderTestForm.milkLeadTest.logMilkLeadTest("250g TEST PASSED");
                    commanderTestForm.log("250g TEST PASSED");

                }
                else
                {
                    //commanderTestForm.factTest.MilkMeterLeadTestPassed = false;
                    commanderTestForm.factTest.Weight250gTestPassed = false;
                    commanderTestForm.factTest.OverallTestPassed = false;
                    commanderTestForm.milkLeadTest.logMilkLeadTest("250g TEST FAILED");
                    commanderTestForm.log("250g TEST FAILED");
                }

                commanderTestForm.factTest.Weight250gTestComplete = true;
                commanderTestForm.factTest.MilkMeterLeadTestComplete = true;

                if (commanderTestForm.factTest.Weight500gTestPassed && commanderTestForm.factTest.Weight250gTestPassed)
                {
                    commanderTestForm.milkLeadTest.logMilkLeadTest("MILK LEAD TEST PASSED");
                    commanderTestForm.logTest("MILK LEAD TEST PASSED");
                }
                else
                {
                    commanderTestForm.milkLeadTest.logMilkLeadTest("MILK LEAD TEST FAILED");
                    commanderTestForm.logTest("MILK LEAD TEST FAILED");
                }

                if (commanderTestForm.milkLeadTest != null)
                {
                    commanderTestForm.milkLeadTest.richTextBox1.Clear();
                    commanderTestForm.milkLeadTest.Dispose();
                    commanderTestForm.milkLeadTest = null;
                }

                commanderTestForm.factTest.MilkMeterLeadTestComplete = true;
            }

        }

        private static void checkWeightNew(Can232 can232Form, int weightToCheck, string weightValue)
        {
            if (weightValue == "500g")
            {

                if ((weightToCheck >= 4900) && (weightToCheck <= 5100))
                //if ((weightToCheck >= 4990) && (weightToCheck <= 5010))
                {
                    //commanderTestForm.factTest.MilkMeterLeadTestPassed = true;
                    can232Form.factTest.Weight500gTestPassed = true;
                    can232Form.milkLeadTest.logMilkLeadTest("500g TEST PASSED");
                    can232Form.logTest("500g TEST PASSED");

                }
                else
                {
                    //commanderTestForm.factTest.MilkMeterLeadTestPassed = false;
                    can232Form.factTest.Weight500gTestPassed = false;
                    can232Form.factTest.OverallTestPassed = false;
                    can232Form.milkLeadTest.logMilkLeadTest("500g TEST FAILED");
                    can232Form.logTest("500g TEST FAILED");
                }

                can232Form.factTest.Weight500gTestComplete = true;
            }

            if (weightValue == "250g")
            {
                if ((weightToCheck >= 2450) && (weightToCheck <= 2550))
                //if ((weightToCheck >= 4990) && (weightToCheck <= 5010))
                {
                    //commanderTestForm.factTest.MilkMeterLeadTestPassed = true;
                    can232Form.factTest.Weight250gTestPassed = true;
                    can232Form.milkLeadTest.logMilkLeadTest("250g TEST PASSED");
                    can232Form.logTest("250g TEST PASSED");

                }
                else
                {
                    //commanderTestForm.factTest.MilkMeterLeadTestPassed = false;
                    can232Form.factTest.Weight250gTestPassed = false;
                    can232Form.factTest.OverallTestPassed = false;
                    can232Form.milkLeadTest.logMilkLeadTest("250g TEST FAILED");
                    can232Form.logTest("250g TEST FAILED");
                }

                can232Form.factTest.Weight250gTestComplete = true;
                can232Form.factTest.MilkMeterLeadTestComplete = true;

                if (can232Form.factTest.Weight500gTestPassed && can232Form.factTest.Weight250gTestPassed)
                {
                    can232Form.setTestStepResult("MILK_METER_LEAD", "PASS");
                    //can232Form.setTestStepResult("OVERALL", "PASS");
                    //can232Form.factTest.MilkMeterLeadTestPassed = true;
                    can232Form.milkLeadTest.logMilkLeadTest("MILK LEAD TEST PASSED");
                    can232Form.logTest("MILK LEAD TEST PASSED");
                }
                else
                {
                    can232Form.setTestStepResult("MILK_METER_LEAD", "FAIL");
                    can232Form.setTestStepResult("OVERALL", "FAIL");
                    //can232Form.factTest.MilkMeterLeadTestPassed = false;
                    can232Form.milkLeadTest.logMilkLeadTest("MILK LEAD TEST FAILED");
                    can232Form.logTest("MILK LEAD TEST FAILED");
                }

                if (can232Form.milkLeadTest != null)
                {
                    can232Form.milkLeadTest.richTextBox1.Clear();
                    can232Form.milkLeadTest.Dispose();
                    can232Form.milkLeadTest = null;
                }

                can232Form.factTest.MilkMeterLeadTestComplete = true;
            }

        }

        private static void checkCommanderVersion(CommanderTestForm commanderTestForm, int Version, int MinorVersion)
        {
            string softVer = Properties.Settings.Default.softVer;
            commanderTestForm.factTest.VersionTestPassed = false;

            if ((Version < commanderTestForm.mainForm.MinVersion) || ((Version == commanderTestForm.mainForm.MinVersion) && (MinorVersion < commanderTestForm.mainForm.MinMinorVersion)))
                {
                    commanderTestForm.factTest.OverallTestPassed = false;
                    commanderTestForm.logTest("FAIL - Software version must by 2.52 or later in order to perform test.");
                    MessageBox.Show("FAIL - Software version must by 2.52 or later in order to perform test.");
                    return;
                }

            string returnedSoftVer = Version.ToString() + MinorVersion.ToString();

            commanderTestForm.logTest("SOFTWARE VERSION: "
                    + Version.ToString()
                    + "." + MinorVersion.ToString()
                    );
            
            //if (returnedSoftVer == softVer) 
            //{
            //    commanderTestForm.logTest("SOFTWARE VERSION: "
            //        + Version.ToString()
            //        + "." + MinorVersion.ToString()
            //        );
            //}
            //else
            //{
            //    commanderTestForm.logTest("WARNING - SOFTWARE VERSION DO NOT MATCH SYSTEM SETTINGS "
            //        + softVer
            //        );
            //    commanderTestForm.logTest("SOFTWARE VERSION: "
            //        + Version.ToString()
            //        + "." + MinorVersion.ToString()
            //        );
            //}
            commanderTestForm.factTest.VersionTestPassed = true;
        }

        private static void checkCommanderVersionNew(Can232 can232Form, int Version, int MinorVersion)
        {
            string softVer = Properties.Settings.Default.softVer;
            can232Form.factTest.VersionTestPassed = false;

            if ((Version < can232Form.MinVersion) || ((Version == can232Form.MinVersion) && (MinorVersion < can232Form.MinMinorVersion)))
            {
                can232Form.factTest.OverallTestPassed = false;
                can232Form.logTest("FAIL - Software version must by 2.52 or later in order to perform test.");
                MessageBox.Show("FAIL - Software version must by 2.52 or later in order to perform test.");
                can232Form.setTestStepResult("GET_VERSION", "FAIL - version must be later than 2.52");
                can232Form.setTestStepResult("OVERALL", "FAIL");
                return;
            }

            string returnedSoftVer = Version.ToString() + MinorVersion.ToString();

            can232Form.logTest("SOFTWARE VERSION: "
                    + Version.ToString()
                    + "." + MinorVersion.ToString()
                    );

            //can232Form.setTestStepResult("GET_VERSION", Version.ToString()
            //        + "." + MinorVersion.ToString());
            //can232Form.setTestStepResult("COMMS", "PASS");

            //if (returnedSoftVer == softVer) 
            //{
            //    commanderTestForm.logTest("SOFTWARE VERSION: "
            //        + Version.ToString()
            //        + "." + MinorVersion.ToString()
            //        );
            //}
            //else
            //{
            //    commanderTestForm.logTest("WARNING - SOFTWARE VERSION DO NOT MATCH SYSTEM SETTINGS "
            //        + softVer
            //        );
            //    commanderTestForm.logTest("SOFTWARE VERSION: "
            //        + Version.ToString()
            //        + "." + MinorVersion.ToString()
            //        );
            //}
            can232Form.factTest.VersionTestPassed = true;
        }

        private static void checkOutputs(CommanderTestForm commanderTestForm, CanMsg cm, string outputs, string inputs)
        {
            if (!performOutputsTest(outputs, commanderTestForm.outputsCounter))
                {
                    commanderTestForm.factTest.IOTestPassed = false;
                    commanderTestForm.factTest.OverallTestPassed = false;

                    if (commanderTestForm.outputsCounter == 0)
                    {
                        commanderTestForm.logTest("OUTPUTS ENABLED RESPONSE TEST FAILURE: " + outputs);                    
                    }
                    else
                    {
                        commanderTestForm.logTest("OUTPUTS DISABLED RESPONSE TEST FAILURE: " + outputs);
                    }
                }
                else
                {
                    commanderTestForm.factTest.IOTestPassed = true;
                    if (commanderTestForm.outputsCounter == 1)
                    {
                        /*logTest("(IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                        + " " + outputs + " " + inputs
                        );*/

                        commanderTestForm.logTest("OUTPUTS RESPONSE TEST: PASSED");
                    }

                }

                if (commanderTestForm.outputsCounter == 1)
                {
                    commanderTestForm.outputsCounter = 0;
                }
                else
                {
                    commanderTestForm.outputsCounter++;
                }

                commanderTestForm.log("(IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                    + " " + outputs + " " + inputs
                    );
        }

        private static void checkOutputsNew(Can232 can232, CanMsg cm, string outputs, string inputs)
        {
            if (!performOutputsTest(outputs, can232.outputsCounter))
            {
                can232.factTest.IOTestPassed = false;
                can232.factTest.OverallTestPassed = false;

                if (can232.outputsCounter == 0)
                {
                    can232.logTest("OUTPUTS ENABLED RESPONSE TEST FAILURE: " + outputs);
                }
                else
                {
                    can232.logTest("OUTPUTS DISABLED RESPONSE TEST FAILURE: " + outputs);
                }
            }
            else
            {
                can232.factTest.IOTestPassed = true;
                if (can232.outputsCounter == 1)
                {
                    /*logTest("(IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                    + " " + outputs + " " + inputs
                    );*/

                    can232.logTest("OUTPUTS RESPONSE TEST: PASSED");
                }

            }

            if (can232.outputsCounter == 1)
            {
                can232.outputsCounter = 0;
            }
            else
            {
                can232.outputsCounter++;
            }

            can232.logTest("(IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                + " " + outputs + " " + inputs
                );
        }

        private static bool performOutputsTest(string outputs, int counter)
        {
            bool response = true;

            if (outputs.Length != 35)
            {
                response = false;
                return response;
            }

            string outputTest = outputs.Substring(3, 20);

            if (counter == 0)
            {
                if (outputTest != ("11111111111111111111"))
                {
                    response = false;
                }


            }
            else if (counter == 1)
            {
                if (outputTest != ("00000000000000000000"))
                {
                    response = false;
                }
            }
            else
            {
                response = false;
            }

            return response;

        }

        private static void checkConductivity(CommanderTestForm commanderTestForm, CanMsg cm)
        {
            float LastConductivity = 0;
            float LastRawConductivityAdcRange = 0;
            cm.parseConductivity(out LastConductivity, out LastRawConductivityAdcRange);

            Utils.delay(100);
            //if ((LastRawConductivityAdcRange < 690) || (LastRawConductivityAdcRange > 3300))
            if ((LastRawConductivityAdcRange < 250) || (LastRawConductivityAdcRange > 490))
            {
                commanderTestForm.log("Conductivity:" + LastConductivity.ToString("0.00") + " RawAdcRange:" + LastRawConductivityAdcRange.ToString("0"));
                commanderTestForm.logTest("PROBES TEST FAILED");
                commanderTestForm.factTest.CondProbesTestPassed = false;
                commanderTestForm.factTest.OverallTestPassed = false;

            }
            else
            {
                commanderTestForm.log("Conductivity:" + LastConductivity.ToString("0.00") + " RawAdcRange:" + LastRawConductivityAdcRange.ToString("0"));
                commanderTestForm.logTest("PROBES TEST PASSED");
                commanderTestForm.factTest.CondProbesTestPassed = true;

            }
            
        }

        private static void checkConductivityNew(Can232 can232Form, CanMsg cm)
        {
            //float LastConductivity = 0;
            float ADCTemperature = 0;
            //float LastRawConductivityAdcRange = 0;
            float ADCConductivity = 0;
            //cm.parseConductivity(out LastConductivity, out LastRawConductivityAdcRange);

            cm.parseConductivity(out ADCTemperature, out ADCConductivity);

            Utils.delay(100);
            //if ((LastRawConductivityAdcRange < 690) || (LastRawConductivityAdcRange > 3300))
            //if ((LastRawConductivityAdcRange < 250) || (LastRawConductivityAdcRange > 490))
            can232Form.logTest("ADCConductivity: " + ADCConductivity);
            if (ADCConductivity == 0)
            {
                //can232Form.logTest("Conductivity:" + LastConductivity.ToString("0.00") + " RawAdcRange:" + LastRawConductivityAdcRange.ToString("0"));
                can232Form.logTest("PROBES TEST FAILED");
                can232Form.factTest.CondProbesTestPassed = false;
                can232Form.factTest.OverallTestPassed = false;
                can232Form.setTestStepResult("PROBES", "FAIL");
                can232Form.setTestStepResult("OVERALL", "FAIL");

            }
            else
            {
                //can232Form.logTest("Conductivity:" + LastConductivity.ToString("0.00") + " RawAdcRange:" + LastRawConductivityAdcRange.ToString("0"));
                can232Form.logTest("PROBES TEST PASSED");
                can232Form.factTest.CondProbesTestPassed = true;
                can232Form.setTestStepResult("PROBES", "PASS");

            }

        }

        private static void checkTemp(CommanderTestForm commanderTestForm, CanMsg cm)
        {
            float LastTemperature = 0;
            cm.parseTemperature(out LastTemperature);

            Utils.delay(100);
            if ((LastTemperature >= 20) && (LastTemperature <= 30))// Obviously will not work work if temp is 0
            {
                commanderTestForm.factTest.TempTestPassed = true;
                commanderTestForm.logTest("TEMPERATURE TEST PASSED, TEMP:" + LastTemperature.ToString("0.00"));
            }
                
            else
            {
                commanderTestForm.factTest.TempTestPassed = false;
                commanderTestForm.factTest.OverallTestPassed = false;
                commanderTestForm.logTest("TEMPERATURE REQUEST FAILED");
                //commanderTestForm.logTest("DBROSNAN 13/08/2018 LATEST");
            }

        }

        private static void checkTempNew(Can232 can232Form, CanMsg cm)
        {
            float LastTemperature = 0;
            cm.parseTemperature(out LastTemperature);

            Utils.delay(100);
            if ((LastTemperature >= 20) && (LastTemperature <= 30))// Obviously will not work work if temp is 0
            {
                can232Form.factTest.TempTestPassed = true;
                can232Form.logTest("TEMPERATURE TEST PASSED, TEMP:" + LastTemperature.ToString("0.00"));
                can232Form.setTestStepResult("TEMP", "PASS");
            }

            else
            {
                can232Form.factTest.TempTestPassed = false;
                can232Form.factTest.OverallTestPassed = false;
                can232Form.logTest("TEMPERATURE REQUEST FAILED");
                can232Form.setTestStepResult("TEMP", "FAIL");
                can232Form.setTestStepResult("OVERALL", "FAIL");
                //commanderTestForm.logTest("DBROSNAN 13/08/2018 LATEST");
            }

        }

        private static bool checkInputsCC(Constants.CoolControlInputs checkIP)
        {
            string ipResponse = "";
            bool response = false;
            
            LastIO_outputs = LastIO_outputs.Substring(3, 32);
            LastIO_inputs = LastIO_inputs.Substring(3, 32);

            if (LastIO_inputs.Length != 32)
            {
                ipResponse = "IP response message for " + checkIP + " is corrupt";
                response = false;
                return response;
            }

            if (LastIO_inputs[(int)checkIP] == '1')
            {
                ipResponse = checkIP + " IP came on";
                response = true;

                for (int i = 0; i < LastIO_inputs.Length; i++)
                {
                    if ((LastIO_inputs[i] == '1') && (i != (int)checkIP))
                    {
                        ipResponse = "Too Many IP's came on for " + checkIP;
                        response = false;
                    }
                    
                }
            }
            else
            {
                ipResponse = checkIP + " IP Did Not Come on";
                response = false;

                for (int i = 0; i < LastIO_inputs.Length; i++)
                {
                    if ((LastIO_inputs[i] == '1') && (i != (int)checkIP))
                    {
                        ipResponse = "Wrong IP came on for " + checkIP;
                    }
                }
            }

            return response;
        }

        private static bool checkInputsCommander(Constants.CommanderInputs checkIP)
        {
           string ipResponse = "";
           bool response = false;

           LastIO_outputs = LastIO_outputs.Substring(3, 32);
           LastIO_inputs = LastIO_inputs.Substring(3, 32);

           if (LastIO_inputs.Length != 32)
           {
               ipResponse = "IP response message for " + checkIP + " is corrupt";
               response = false;
               return response;
           }

           if (LastIO_inputs[(int)checkIP] == '1')
           //if (LastIO_inputs[(int)checkIP] == checkValue)
           {
               ipResponse = checkIP + " IP came on";
               response = true;

               for (int i = 0; i < LastIO_inputs.Length; i++)
               {
                   if ((LastIO_inputs[i] == '1') && (i != (int)checkIP))
                   //if ((LastIO_inputs[i] == checkValue) && (i != (int)checkIP))
                   {
                       ipResponse = "Too Many IP's came on for " + checkIP;
                       response = false;
                   }
                    
               }
            }
            else
            {
                ipResponse = checkIP + " IP Did Not Come on";
                response = false;

                for (int i = 0; i < LastIO_inputs.Length; i++)
                {
                    if ((LastIO_inputs[i] == '1') && (i != (int)checkIP))
                    //if ((LastIO_inputs[i] == checkValue) && (i != (int)checkIP))
                    {
                        ipResponse = "Wrong IP came on for " + checkIP;
                    }
                }
            }

            return response;
        }

        private static bool checkInputsCommanderForOne(Constants.CommanderInputs checkIP)
        {
            string ipResponse = "";
            bool response = false;

            LastIO_outputs = LastIO_outputs.Substring(3, 32);
            LastIO_inputs = LastIO_inputs.Substring(3, 32);

            if (LastIO_inputs.Length != 32)
            {
                ipResponse = "IP response message for " + checkIP + " is corrupt";
                response = false;
                return response;
            }

            if (LastIO_inputs[(int)checkIP] == '1')
            //if (LastIO_inputs[(int)checkIP] == checkValue)
            {
                ipResponse = checkIP + " IP came on";
                response = true;

                for (int i = 0; i < LastIO_inputs.Length; i++)
                {
                    if ((LastIO_inputs[i] == '1') && (i != (int)checkIP))
                    //if ((LastIO_inputs[i] == checkValue) && (i != (int)checkIP))
                    {
                        ipResponse = "Too Many IP's came on for " + checkIP;
                        response = false;
                    }

                }
            }
            else
            {
                ipResponse = checkIP + " IP Did Not Come on";
                response = false;

                for (int i = 0; i < LastIO_inputs.Length; i++)
                {
                    if ((LastIO_inputs[i] == '1') && (i != (int)checkIP))
                    //if ((LastIO_inputs[i] == checkValue) && (i != (int)checkIP))
                    {
                        ipResponse = "Wrong IP came on for " + checkIP;
                    }
                }
            }

            return response;
        }

        private static bool checkInputsCommanderForZero(Constants.CommanderInputs checkIP)
        {
            string ipResponse = "";
            bool response = false;

            LastIO_outputs = LastIO_outputs.Substring(3, 32);

            LastIO_inputs = LastIO_inputs.Substring(3, 32);

            if (LastIO_inputs.Length != 32)
            {
                ipResponse = "IP response message for " + checkIP + " is corrupt";
                response = false;
                return response;
            }

            if (LastIO_inputs[(int)checkIP] == '0')
            //if (LastIO_inputs[(int)checkIP] == checkValue)
            {
                ipResponse = checkIP + " IP came on";
                response = true;

                for (int i = 0; i < LastIO_inputs.Length; i++)
                {
                    if ((LastIO_inputs[i] == '1') && (i != (int)checkIP))
                    //if ((LastIO_inputs[i] == checkValue) && (i != (int)checkIP))
                    {
                        ipResponse = "Too Many IP's came on for " + checkIP;
                        response = false;
                    }

                }
            }
            else
            {
                ipResponse = checkIP + " IP Did Not Come on";
                response = false;

                for (int i = 0; i < LastIO_inputs.Length; i++)
                {
                    if ((LastIO_inputs[i] == '1') && (i != (int)checkIP))
                    //if ((LastIO_inputs[i] == checkValue) && (i != (int)checkIP))
                    {
                        ipResponse = "Wrong IP came on for " + checkIP;
                    }
                }
            }

            return response;
        }

        public static void processIOResponseCommon(CommanderTestForm commanderTestForm, CanMsg cm, string test)
        {
            string testString = "";
            if (test == "testCommon")
            {
                testString = commanderTestForm.testCommon.ToString();
            }
            else if (test == "testCommonRty")
            {
                testString = commanderTestForm.testCommonRot.ToString();
            }
            else if (test == "prodSpecificTest")
            {
                testString = CommanderLoomTests.prodSpecificTest.ToString();
            }

           // switch (commanderTestForm.testCommon)
            switch (testString)
            {
                //case "CLUST_CLEANSE_AIR":
                case "CC_AIR":
                    {
                        if (!commanderTestForm.factTest.CCAirTestComplete)
                        {
                            commanderTestForm.factTest.CCAirTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.OL4))
                            {
                                commanderTestForm.factTest.CCAirTestPassed = true;
                                
                                commanderTestForm.logTest("CC_AIR TEST PASSED");
                            }
                            else
                            {
                                commanderTestForm.factTest.CCAirTestPassed = false;
                                commanderTestForm.factTest.OverallTestPassed = false;
                                commanderTestForm.logTest("CC_AIR TEST FAILED!");
                            }

                            commanderTestForm.log("CC_AIR CoolContr Input 14(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );

                        }
                        else
                        { 
                            commanderTestForm.log("CCAirTestComplete is true");
                        }
                        break;
                    }
                //case "CLUST_CLEANSE_WATER":
                case "CC_WATER":
                    {
                        if (!commanderTestForm.factTest.CCWaterTestComplete)
                        {
                            commanderTestForm.factTest.CCWaterTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.OL6))
                            {
                                commanderTestForm.factTest.CCWaterTestPassed = true;
                                commanderTestForm.logTest("CC_WATER TEST PASSED");
                            }
                            else
                            {
                                commanderTestForm.factTest.CCWaterTestPassed = false;
                                commanderTestForm.factTest.OverallTestPassed = false;
                                commanderTestForm.logTest("CC_WATER TEST FAILED!");
                            }
                            commanderTestForm.log("CC_WATER CoolContr Input 22(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }
                case "PROX_SWITCH":
                    {
                        if (!commanderTestForm.factTest.ProxSwitchTestComplete && cm.Sid == commanderTestForm.mainForm.DefaultAddr)
                        {
                            //commanderTestForm.factTest.ProxSwitchTestComplete = true;
                            if (!commanderTestForm.factTest.ProxSwitchFirstTestComplete)
                            {
                                commanderTestForm.factTest.ProxSwitchFirstTestComplete = true;
                                if (!checkInputsCommanderForOne(Constants.CommanderInputs.IP_1))
                                {
                                    commanderTestForm.factTest.ProxSwitchTestPassed = false;
                                    commanderTestForm.factTest.OverallTestPassed = false;
                                }
                            }
                            else if (!commanderTestForm.factTest.ProxSwitchSecondTestComplete)
                            {
                                commanderTestForm.factTest.ProxSwitchSecondTestComplete = true;
                                commanderTestForm.factTest.ProxSwitchTestComplete = true;
                                if (!checkInputsCommanderForZero(Constants.CommanderInputs.IP_1))
                                {
                                    commanderTestForm.factTest.ProxSwitchTestPassed = false;
                                    commanderTestForm.factTest.OverallTestPassed = false;                                
                                }

                                if (commanderTestForm.factTest.ProxSwitchTestPassed)
                                {
                                    commanderTestForm.logTest("PROX_SWITCH TEST PASSED!");
                                }
                                else
                                {
                                    commanderTestForm.logTest("PROX_SWITCH TEST FAILED!");
                                }
                                 
                            }

                            commanderTestForm.log("PROX_SWITCH: Commander Input 0(0) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        
                        break;
                    }
                case "SMART_START_REED_SWITCH":
                    {
                        if (!commanderTestForm.factTest.SmartStartReedSwitchTestComplete)
                        {
                            commanderTestForm.factTest.SmartStartReedSwitchTestComplete = true;
                            //if (checkInputsCommander(Constants.CommanderInputs.IP_1))
                            if (checkInputsCommanderForOne(Constants.CommanderInputs.IP_1))
                            {
                                commanderTestForm.factTest.SmartStartReedSwitchTestPassed = true;
                                commanderTestForm.logTest("SMART_START_REED_SWITCH TEST PASSED!");
                            }
                            else
                            {
                                commanderTestForm.factTest.SmartStartReedSwitchTestPassed = false;
                                commanderTestForm.factTest.OverallTestPassed = false;
                                commanderTestForm.logTest("SMART_START_REED_SWITCH TEST FAILED!");
                            }

                            commanderTestForm.log("SMART_START_REED_SWITCH Commander Input 0(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }

                        commanderTestForm.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, commanderTestForm.mainForm.coolContAddr, 0);
                        break;
                    }
                case "DIVERT_LINE_REED_SWITCH":
                    {
                        if (!commanderTestForm.factTest.DivertLineReedSwitchTestComplete && cm.Sid == commanderTestForm.mainForm.DefaultAddr)
                        {
                            if (!commanderTestForm.factTest.DivertLineReedSwitchFirstTestComplete)
                            {
                                commanderTestForm.factTest.DivertLineReedSwitchFirstTestComplete = true;
                                if (!checkInputsCommanderForZero(Constants.CommanderInputs.IP_2))
                                {
                                    commanderTestForm.factTest.DivertLineReedSwitchTestPassed = false;
                                    commanderTestForm.factTest.OverallTestPassed = false;
                                }
                            }
                            else if (!commanderTestForm.factTest.DivertLineReedSwitchSecondTestComplete)
                            {
                                commanderTestForm.factTest.DivertLineReedSwitchSecondTestComplete = true;
                                commanderTestForm.factTest.DivertLineReedSwitchTestComplete = true;
                                if (!checkInputsCommanderForOne(Constants.CommanderInputs.IP_2))
                                {
                                    commanderTestForm.factTest.DivertLineReedSwitchTestPassed = false;
                                    commanderTestForm.factTest.OverallTestPassed = false;
                                }

                                if (commanderTestForm.factTest.DivertLineReedSwitchTestPassed)
                                {
                                    commanderTestForm.logTest("DIVERT_LINE_REED_SWITCH TEST PASSED!");
                                }
                                else
                                {
                                    commanderTestForm.logTest("DIVERT_LINE_REED_SWITCH TEST FAILED!");
                                }

                            }

                            commanderTestForm.log("logDLRS" + " " + logDLRS);
                            commanderTestForm.log("DIVERT_LINE_REED_SWITCH Commander Input 1(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                   + " " + LastIO_outputs + " " + LastIO_inputs
                                   );
                        }

                        break;
                    }

                case "MILK_METER_LEAD_WIRE":
                    {
                        //if (!commanderTestForm.factTest.MilkMeterLeadWireTestComplete && cm.Sid == commanderTestForm.mainForm.DefaultAddr)
                        if (!commanderTestForm.factTest.MilkMeterLeadWireTestComplete && cm.Sid == commanderTestForm.mainForm.coolContAddr)
                        {
                            if (!commanderTestForm.factTest.MilkMeterLeadWireFirstTestComplete)
                            {
                                commanderTestForm.factTest.MilkMeterLeadWireFirstTestComplete = true;
                                if (!checkInputsCC(Constants.CoolControlInputs.OL9))
                                {
                                    commanderTestForm.factTest.MilkMeterLeadWireTestPassed = false;
                                    commanderTestForm.factTest.OverallTestPassed = false;
                                }
                            }
                            else if (!commanderTestForm.factTest.MilkMeterLeadWireSecondTestComplete)
                            {
                                commanderTestForm.factTest.MilkMeterLeadWireSecondTestComplete = true;
                                commanderTestForm.factTest.MilkMeterLeadWireTestComplete = true;
                                if (!checkInputsCC(Constants.CoolControlInputs.I1))
                                {
                                    commanderTestForm.factTest.MilkMeterLeadWireTestPassed = false;
                                    commanderTestForm.factTest.OverallTestPassed = false;
                                }

                                if (commanderTestForm.factTest.MilkMeterLeadWireTestPassed)
                                {
                                    commanderTestForm.logTest("MILK_METER_LEAD_WIRE TEST PASSED!");
                                }
                                else
                                {
                                    commanderTestForm.logTest("MILK_METER_LEAD_WIRE TEST FAILED!");
                                }

                            }                            

                            commanderTestForm.log("MILK_METER_LEAD_WIRE Commander Input 1(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                   + " " + LastIO_outputs + " " + LastIO_inputs
                                   );
                        }

                        break;
                    }

                case "MILKLINE_OP":
                    {
                        if (!commanderTestForm.factTest.MilkLineOPTestComplete)
                        {
                            commanderTestForm.factTest.MilkLineOPTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.OL1))
                            {
                                commanderTestForm.factTest.MilkLineOPTestPassed = true;
                                commanderTestForm.logTest("MILK LINE TEST PASSED");
                            }
                            else
                            {
                                commanderTestForm.factTest.MilkLineOPTestPassed = false;
                                commanderTestForm.factTest.OverallTestPassed = false;
                                commanderTestForm.logTest("MILK LINE TEST FAILED!");
                            }
                            commanderTestForm.log("MILKLINE_OP CoolContr Input 17(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }
                case "DIVERT_LINE_OP":
                    {
                        if (!commanderTestForm.factTest.DivertLineOPTestComplete)
                        {
                            commanderTestForm.factTest.DivertLineOPTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.OL2))
                            {
                                commanderTestForm.factTest.DivertLineOPTestPassed = true;
                                commanderTestForm.logTest("DIVERT LINE TEST PASSED");
                            }
                            else
                            {
                                commanderTestForm.factTest.DivertLineOPTestPassed = false;
                                commanderTestForm.factTest.OverallTestPassed = false;
                                commanderTestForm.logTest("DIVERT LINE TEST FAILED!");
                            }
                            commanderTestForm.log("DIVERT_LINE_OP CoolContr Input 16(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }
                case "PULSATION":
                    {
                        if (!commanderTestForm.factTest.PulsationTestComplete)
                        {
                            commanderTestForm.factTest.PulsationTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.I13))
                            {
                                commanderTestForm.factTest.PulsationTestPassed = true;
                                commanderTestForm.logTest("PULSATION TEST PASSED");
                            }
                            else
                            {
                                commanderTestForm.factTest.PulsationTestPassed = false;
                                commanderTestForm.factTest.OverallTestPassed = false;
                                commanderTestForm.logTest("PULSATION TEST FAILED!");
                            }
                            commanderTestForm.log("PULSATION (IO) CoolContr Input 12(1) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }
                case "ACR_RAM":
                    {
                        if (!commanderTestForm.factTest.AcrRamTestComplete)
                        {
                            commanderTestForm.factTest.AcrRamTestComplete = true;

                            //if (checkInputsCC(Constants.CoolControlInputs.I14))
                            if (checkInputsCC(Constants.CoolControlInputs.OL3))
                            {
                                commanderTestForm.factTest.AcrRamTestPassed = true;
                                commanderTestForm.logTest("ACR RAM TEST PASSED");
                            }
                            else
                            {
                                commanderTestForm.factTest.AcrRamTestPassed = false;
                                commanderTestForm.factTest.OverallTestPassed = false;
                                commanderTestForm.logTest("ACR RAM TEST FAILED!");
                            }
                            commanderTestForm.log("ACR_RAM CoolContr Input 15(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }

                        //Turn off ACR 
                        commanderTestForm.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, commanderTestForm.mainForm.DefaultAddr, 0);
                        break;
                    }
                case "JETSTREAM":
                    {
                        if (!commanderTestForm.factTest.JetStreamTestComplete)
                        {
                            commanderTestForm.factTest.JetStreamTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.I12))
                            {
                                commanderTestForm.factTest.JetStreamTestPassed = true;
                                commanderTestForm.logTest("JETSTREAM TEST PASSED");
                            }
                            else
                            {
                                commanderTestForm.factTest.JetStreamTestPassed = false;
                                commanderTestForm.factTest.OverallTestPassed = false;
                                commanderTestForm.logTest("JETSTREAM FAILED!");
                            }
                            commanderTestForm.log("JETSTREAM CoolContr Input 11(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }
                case "RETENTION":
                    {
                        if (!commanderTestForm.factTest.RetentionTestComplete)
                        {
                            commanderTestForm.factTest.RetentionTestComplete = true;

                            //if (checkInputsCC(Constants.CoolControlInputs.OL3))
                            if (checkInputsCC(Constants.CoolControlInputs.I14))
                            {
                                commanderTestForm.factTest.RetentionTestPassed = true;
                                commanderTestForm.logTest("RETENTION TEST PASSED");
                            }
                            else
                            {
                                commanderTestForm.factTest.RetentionTestPassed = false;
                                commanderTestForm.factTest.OverallTestPassed = false;
                                commanderTestForm.logTest("RETENTION TEST FAILED!");
                            }
                            commanderTestForm.log("RETENTION CoolContr Input 15(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }
                case "TEAT_SPRAY":
                    {
                        if (!commanderTestForm.factTest.TeatSprayTestComplete)
                        {
                            commanderTestForm.factTest.TeatSprayTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.I11))
                            {
                                commanderTestForm.factTest.TeatSprayTestPassed = true;
                                commanderTestForm.logTest("TEAT SPRAY TEST PASSED");
                            }
                            else
                            {
                                commanderTestForm.factTest.TeatSprayTestPassed = false;
                                commanderTestForm.factTest.OverallTestPassed = false;
                                commanderTestForm.logTest("TEAT SPRAY TEST FAILED!");
                            }
                            commanderTestForm.log("TEAT_SPRAY CoolContr Input 10(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }

                default: break;
            }
        }

        public static void processIOResponseCommonNew(Can232 can232Form, CanMsg cm, string test)
        {
            string testString = "";
            if (test == "testCommon")
            {
                testString = can232Form.testCommon.ToString();
            }
            else if (test == "testCommonRty")
            {
                testString = can232Form.testCommonRot.ToString();
            }
            else if (test == "prodSpecificTest")
            {
                testString = CommanderLoomTests.prodSpecificTest.ToString();
            }

            // switch (commanderTestForm.testCommon)
            switch (testString)
            {
                //case "CLUST_CLEANSE_AIR":
                case "CC_AIR":
                    {
                        if (!can232Form.factTest.CCAirTestComplete)
                        {
                            can232Form.factTest.CCAirTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.OL4))
                            {
                                can232Form.factTest.CCAirTestPassed = true;
                                can232Form.setTestStepResult("CC_AIR", "PASS");
                                can232Form.logTest("DEE CC_AIR TEST PASSED");
                            }
                            else
                            {
                                can232Form.factTest.CCAirTestPassed = false;
                                can232Form.factTest.OverallTestPassed = false;
								can232Form.setTestStepResult("CC_AIR", "FAIL");
								can232Form.setTestStepResult("OVERALL", "FAIL");
								can232Form.logTest("DEE CC_AIR TEST FAILED!");
                            }

                            can232Form.logTest("CC_AIR CoolContr Input 14(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );

                        }
                        else
                        {
                            can232Form.logTest("CCAirTestComplete is true");
                        }
                        break;
                    }
                //case "CLUST_CLEANSE_WATER":
                case "CC_WATER":
                    {
                        if (!can232Form.factTest.CCWaterTestComplete)
                        {
                            can232Form.factTest.CCWaterTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.OL6))
                            {
                                can232Form.factTest.CCWaterTestPassed = true;
                                can232Form.setTestStepResult("CC_WATER", "PASS");
                                can232Form.logTest("CC_WATER TEST PASSED");
                            }
                            else
                            {
                                can232Form.factTest.CCWaterTestPassed = false;
								can232Form.setTestStepResult("CC_WATER", "FAIL");
								can232Form.setTestStepResult("OVERALL", "FAIL");
								can232Form.factTest.OverallTestPassed = false;
                                can232Form.logTest("CC_WATER TEST FAILED!");
                            }
                            can232Form.logTest("CC_WATER CoolContr Input 22(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }
                case "PROX_SWITCH":
                    {
                        if (!can232Form.factTest.ProxSwitchTestComplete && cm.Sid == can232Form.DefaultAddr)
                        {
                            //commanderTestForm.factTest.ProxSwitchTestComplete = true;
                            if (!can232Form.factTest.ProxSwitchFirstTestComplete)
                            {
                                can232Form.factTest.ProxSwitchFirstTestComplete = true;
                                if (!checkInputsCommanderForOne(Constants.CommanderInputs.IP_1))
                                {
                                    can232Form.factTest.ProxSwitchTestPassed = false;
                                    can232Form.factTest.OverallTestPassed = false;
									can232Form.setTestStepResult("PROX_SWITCH", "FAIL");
									can232Form.setTestStepResult("OVERALL", "FAIL");
								}
                            }
                            else if (!can232Form.factTest.ProxSwitchSecondTestComplete)
                            {
                                can232Form.factTest.ProxSwitchSecondTestComplete = true;
                                can232Form.factTest.ProxSwitchTestComplete = true;
                                if (!checkInputsCommanderForZero(Constants.CommanderInputs.IP_1))
                                {
                                    can232Form.factTest.ProxSwitchTestPassed = false;
                                    can232Form.factTest.OverallTestPassed = false;
									can232Form.setTestStepResult("PROX_SWITCH", "FAIL");
									can232Form.setTestStepResult("OVERALL", "FAIL");

								}

                                if (can232Form.factTest.ProxSwitchTestPassed)
                                {
									can232Form.setTestStepResult("PROX_SWITCH", "PASS");
									can232Form.logTest("PROX_SWITCH TEST PASSED!");
                                }
                                else
                                {
									can232Form.setTestStepResult("PROX_SWITCH", "FAIL");
									can232Form.setTestStepResult("OVERALL", "FAIL");
									can232Form.logTest("PROX_SWITCH TEST FAILED!");
                                }

                            }

                            can232Form.logTest("PROX_SWITCH: Commander Input 0(0) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }

                        break;
                    }
                case "SMART_START_REED_SWITCH":
                    {
                        if (!can232Form.factTest.SmartStartReedSwitchTestComplete)
                        {
                            can232Form.factTest.SmartStartReedSwitchTestComplete = true;
                            //if (checkInputsCommander(Constants.CommanderInputs.IP_1))
                            if (checkInputsCommanderForOne(Constants.CommanderInputs.IP_1))
                            {
                                can232Form.factTest.SmartStartReedSwitchTestPassed = true;
								can232Form.setTestStepResult("SMART_START_REED_SWITCH", "PASS");
								can232Form.logTest("SMART_START_REED_SWITCH TEST PASSED!");
                            }
                            else
                            {
                                can232Form.factTest.SmartStartReedSwitchTestPassed = false;
                                can232Form.factTest.OverallTestPassed = false;
								can232Form.setTestStepResult("SMART_START_REED_SWITCH", "FAIL");
								can232Form.setTestStepResult("OVERALL", "FAIL");
								can232Form.logTest("SMART_START_REED_SWITCH TEST FAILED!");
                            }

                            can232Form.logTest("SMART_START_REED_SWITCH Commander Input 0(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }

                        can232Form.enableOPCC(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CoolControlOutputs.O4, can232Form.coolContAddr, 0);
                        break;
                    }
                case "DIVERT_LINE_REED_SWITCH":
                    {
                        if (!can232Form.factTest.DivertLineReedSwitchTestComplete && cm.Sid == can232Form.DefaultAddr)
                        {
                            if (!can232Form.factTest.DivertLineReedSwitchFirstTestComplete)
                            {
                                can232Form.factTest.DivertLineReedSwitchFirstTestComplete = true;
                                can232Form.logTest("DIVERT_LINE_REED_SWITCH 1st test check!");
                                if (!checkInputsCommanderForZero(Constants.CommanderInputs.IP_2))
                                {
                                    can232Form.logTest("DIVERT_LINE_REED_SWITCH 1st test FAILED!");
                                    can232Form.logTest(LastIO_inputs);                                    
                                    can232Form.factTest.DivertLineReedSwitchTestPassed = false;
                                    can232Form.factTest.OverallTestPassed = false;
									//can232Form.setTestStepResult("DIVERT_LINE_REED_SWITCH", "FAIL");
									//can232Form.setTestStepResult("OVERALL", "FAIL");
								}
                            }
                            else if (!can232Form.factTest.DivertLineReedSwitchSecondTestComplete)
                            {
                                can232Form.factTest.DivertLineReedSwitchSecondTestComplete = true;
                                can232Form.factTest.DivertLineReedSwitchTestComplete = true;
                                can232Form.logTest("DIVERT_LINE_REED_SWITCH 2nd test check!");
                                if (!checkInputsCommanderForOne(Constants.CommanderInputs.IP_2))
                                {
                                    can232Form.logTest("DIVERT_LINE_REED_SWITCH 2nd test FAILED!");
                                    can232Form.factTest.DivertLineReedSwitchTestPassed = false;
                                    can232Form.factTest.OverallTestPassed = false;
									//can232Form.setTestStepResult("DIVERT_LINE_REED_SWITCH", "FAIL");
									//can232Form.setTestStepResult("OVERALL", "FAIL");
								}

                                if (can232Form.factTest.DivertLineReedSwitchTestPassed)
                                {
                                    can232Form.logTest("DIVERT_LINE_REED_SWITCH TEST PASSED!");
									//can232Form.setTestStepResult("OVERALL", "PASS");
								}
                                else
                                {
                                    can232Form.logTest("DIVERT_LINE_REED_SWITCH TEST FAILED!");
									//can232Form.setTestStepResult("DIVERT_LINE_REED_SWITCH", "FAIL");
									//can232Form.setTestStepResult("OVERALL", "FAIL");
								}

                            }

                            can232Form.logTest("logDLRS" + " " + logDLRS);
                            can232Form.logTest("DIVERT_LINE_REED_SWITCH Commander Input 1(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                   + " " + LastIO_outputs + " " + LastIO_inputs
                                   );
                        }

                        break;
                    }

                case "MILK_METER_LEAD_WIRE":
                    {
                        //if (!commanderTestForm.factTest.MilkMeterLeadWireTestComplete && cm.Sid == commanderTestForm.mainForm.DefaultAddr)
                        if (!can232Form.factTest.MilkMeterLeadWireTestComplete && cm.Sid == can232Form.coolContAddr)
                        {
                            if (!can232Form.factTest.MilkMeterLeadWireFirstTestComplete)
                            {
                                can232Form.factTest.MilkMeterLeadWireFirstTestComplete = true;
                                if (!checkInputsCC(Constants.CoolControlInputs.OL9))
                                {
                                    can232Form.factTest.MilkMeterLeadWireTestPassed = false;
                                    can232Form.factTest.OverallTestPassed = false;
									can232Form.setTestStepResult("MILK_METER_LEAD_WIRE", "FAIL");
									can232Form.setTestStepResult("OVERALL", "FAIL");
								}
                            }
                            else if (!can232Form.factTest.MilkMeterLeadWireSecondTestComplete)
                            {
                                can232Form.factTest.MilkMeterLeadWireSecondTestComplete = true;
                                can232Form.factTest.MilkMeterLeadWireTestComplete = true;
                                if (!checkInputsCC(Constants.CoolControlInputs.I1))
                                {
                                    can232Form.factTest.MilkMeterLeadWireTestPassed = false;
                                    can232Form.factTest.OverallTestPassed = false;
									can232Form.setTestStepResult("MILK_METER_LEAD_WIRE", "FAIL");
									can232Form.setTestStepResult("OVERALL", "FAIL");
								}

                                if (can232Form.factTest.MilkMeterLeadWireTestPassed)
                                {
                                    can232Form.logTest("MILK_METER_LEAD_WIRE TEST PASSED!");
									can232Form.setTestStepResult("MILK_METER_LEAD_WIRE", "PASS");
								}
                                else
                                {
                                    can232Form.logTest("MILK_METER_LEAD_WIRE TEST FAILED!");
									can232Form.setTestStepResult("MILK_METER_LEAD_WIRE", "FAIL");
									can232Form.setTestStepResult("OVERALL", "FAIL");
								}

                            }

                            can232Form.logTest("MILK_METER_LEAD_WIRE Commander Input 1(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                   + " " + LastIO_outputs + " " + LastIO_inputs
                                   );
                        }

                        break;
                    }

                case "MILKLINE_OP":
                    {
                        if (!can232Form.factTest.MilkLineOPTestComplete)
                        {
                            can232Form.factTest.MilkLineOPTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.OL1))
                            {
                                can232Form.factTest.MilkLineOPTestPassed = true;
								can232Form.setTestStepResult("MILKLINE_OP", "PASS");
								can232Form.logTest("MILK LINE TEST PASSED");
                            }
                            else
                            {
                                can232Form.factTest.MilkLineOPTestPassed = false;
                                can232Form.factTest.OverallTestPassed = false;
								can232Form.setTestStepResult("MILKLINE_OP", "FAIL");
								can232Form.setTestStepResult("OVERALL", "FAIL");
								can232Form.logTest("MILK LINE TEST FAILED!");
                            }
                            can232Form.logTest("MILKLINE_OP CoolContr Input 17(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }
                case "DIVERT_LINE_OP":
                    {
                        if (!can232Form.factTest.DivertLineOPTestComplete)
                        {
                            can232Form.factTest.DivertLineOPTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.OL2))
                            {
                                can232Form.factTest.DivertLineOPTestPassed = true;
								can232Form.setTestStepResult("DIVERT_LINE_OP", "PASS");
								can232Form.logTest("DIVERT LINE TEST PASSED");
                            }
                            else
                            {
                                can232Form.factTest.DivertLineOPTestPassed = false;
                                can232Form.factTest.OverallTestPassed = false;
								can232Form.setTestStepResult("DIVERT_LINE_OP", "FAIL");
								can232Form.setTestStepResult("OVERALL", "FAIL");
								can232Form.logTest("DIVERT LINE TEST FAILED!");
                            }
                            can232Form.logTest("DIVERT_LINE_OP CoolContr Input 16(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }
                case "PULSATION":
                    {
                        if (!can232Form.factTest.PulsationTestComplete)
                        {
                            can232Form.factTest.PulsationTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.I13))
                            {
                                can232Form.factTest.PulsationTestPassed = true;
								can232Form.setTestStepResult("PULSATION", "PASS");
								can232Form.logTest("PULSATION TEST PASSED");
                            }
                            else
                            {
                                can232Form.factTest.PulsationTestPassed = false;
                                can232Form.factTest.OverallTestPassed = false;
								can232Form.setTestStepResult("PULSATION", "FAIL");
								can232Form.setTestStepResult("OVERALL", "FAIL");
								can232Form.logTest("PULSATION TEST FAILED!");
                            }
                            can232Form.logTest("PULSATION (IO) CoolContr Input 12(1) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }
                case "ACR_RAM":
                    {
                        if (!can232Form.factTest.AcrRamTestComplete)
                        {
                            can232Form.factTest.AcrRamTestComplete = true;

                            //if (checkInputsCC(Constants.CoolControlInputs.I14))
                            if (checkInputsCC(Constants.CoolControlInputs.OL3))
                            {
                                can232Form.factTest.AcrRamTestPassed = true;
								can232Form.setTestStepResult("ACR_RAM", "PASS");
								can232Form.logTest("ACR RAM TEST PASSED");
                            }
                            else
                            {
                                can232Form.factTest.AcrRamTestPassed = false;
                                can232Form.factTest.OverallTestPassed = false;
								can232Form.setTestStepResult("ACR_RAM", "FAIL");
								can232Form.setTestStepResult("OVERALL", "FAIL");
								can232Form.logTest("ACR RAM TEST FAILED!");
                            }
                            can232Form.logTest("ACR_RAM CoolContr Input 15(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }

                        //Turn off ACR 
                        can232Form.enableOP(Constants.CAN_COMMANDS.CAN_CMD_SET_SINGLE_OUTPUT, Constants.CommanderOutputs.OP_ACR, can232Form.DefaultAddr, 0);
                        break;
                    }
                case "JETSTREAM":
                    {
                        if (!can232Form.factTest.JetStreamTestComplete)
                        {
                            can232Form.factTest.JetStreamTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.I12))
                            {
                                can232Form.factTest.JetStreamTestPassed = true;
								can232Form.setTestStepResult("JETSTREAM", "PASS");
								can232Form.logTest("JETSTREAM TEST PASSED");
                            }
                            else
                            {
                                can232Form.factTest.JetStreamTestPassed = false;
                                can232Form.factTest.OverallTestPassed = false;
								can232Form.setTestStepResult("JETSTREAM", "FAIL");
								can232Form.setTestStepResult("OVERALL", "FAIL");
								can232Form.logTest("JETSTREAM FAILED!");
                            }
                            can232Form.logTest("JETSTREAM CoolContr Input 11(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }
                case "RETENTION":
                    {
                        if (!can232Form.factTest.RetentionTestComplete)
                        {
                            can232Form.factTest.RetentionTestComplete = true;

                            //if (checkInputsCC(Constants.CoolControlInputs.OL3))
                            if (checkInputsCC(Constants.CoolControlInputs.I14))
                            {
                                can232Form.factTest.RetentionTestPassed = true;
								can232Form.setTestStepResult("RETENTION", "PASS");
								can232Form.logTest("RETENTION TEST PASSED");
                            }
                            else
                            {
                                can232Form.factTest.RetentionTestPassed = false;
                                can232Form.factTest.OverallTestPassed = false;
								can232Form.setTestStepResult("RETENTION", "FAIL");
								can232Form.setTestStepResult("OVERALL", "FAIL");
								can232Form.logTest("RETENTION TEST FAILED!");
                            }
                            can232Form.logTest("RETENTION CoolContr Input 15(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }
                case "TEAT_SPRAY":
                    {
                        if (!can232Form.factTest.TeatSprayTestComplete)
                        {
                            can232Form.factTest.TeatSprayTestComplete = true;

                            if (checkInputsCC(Constants.CoolControlInputs.I11))
                            {
                                can232Form.factTest.TeatSprayTestPassed = true;
                                can232Form.logTest("TEAT SPRAY TEST PASSED");
								can232Form.setTestStepResult("TEAT_SPRAY", "PASS");
							}
							else
                            {
                                can232Form.factTest.TeatSprayTestPassed = false;
                                can232Form.factTest.OverallTestPassed = false;
								can232Form.setTestStepResult("TEAT_SPRAY", "FAIL");
								can232Form.setTestStepResult("OVERALL", "FAIL");
								can232Form.logTest("TEAT SPRAY TEST FAILED!");
                            }
                            can232Form.logTest("TEAT_SPRAY CoolContr Input 10(1) (IO) CMD:" + cm.Cmd.ToString() + " DID:" + cm.Did.ToString() + " SID:" + cm.Sid.ToString()
                                + " " + LastIO_outputs + " " + LastIO_inputs
                                );
                        }
                        break;
                    }

                default: break;
            }
        }

        public static int checkKeyPadButton(CommanderTestForm commanderTestForm, int playkeyPadWav)
        {
            //bool mybool = commanderTestForm.factTest.KeyPadTestPassed;

            if (!keyMatch(commanderTestForm, playkeyPadWav))
            {
                //playAudioFile("Error.wav");
                DialogResult result = MessageBox.Show("Incorrect Key returned. Skip this Key?", "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (result == DialogResult.Yes)
                {
                    commanderTestForm.factTest.KeyPadTestPassed = false;
                    commanderTestForm.factTest.OverallTestPassed = false;
                    commanderTestForm.logTest("KEY TEST FAILURE ON " + KEY_PAD_BUTTONS[commanderTestForm.keyPadCounter]);
                    commanderTestForm.keyPadTest.logKeyPadTest("KEY TEST FAILURE ON " + KEY_PAD_BUTTONS[commanderTestForm.keyPadCounter]);
                    commanderTestForm.keyPadCounter++;

                    if (commanderTestForm.keyPadCounter < 5)
                    { 
                        commanderTestForm.keyPadTest.logKeyPadTest("Press " + KEY_PAD_BUTTONS[commanderTestForm.keyPadCounter]);
                        
                    }
                    else
                    {
                        playkeyPadWav = 7;
                        return playkeyPadWav;
                    }
                    //playAudioFile(KEY_WAV_FILES[commanderTestForm.keyPadCounter]);
                }

            }
            else
            {
                if (commanderTestForm.keyPadCounter == 5)
                //if (commanderTestForm.keyPadCounter == (int)Enum.GetNames(typeof(Constants.KEYPAD_BUTTONS)).Length)
                {
                    commanderTestForm.keyPadCounter = 0;

                }
            }
            
            return playkeyPadWav;
        }

        public static int checkKeyPadButtonNew(Can232 can232Form, int playkeyPadWav)
        {
            //bool mybool = commanderTestForm.factTest.KeyPadTestPassed;

            if (!keyMatchNew(can232Form, playkeyPadWav))
            {
                //playAudioFile("Error.wav");
                DialogResult result = MessageBox.Show("Incorrect Key returned. Skip this Key?", "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (result == DialogResult.Yes)
                {
                    can232Form.factTest.KeyPadTestPassed = false;
                    can232Form.factTest.OverallTestPassed = false;
                    can232Form.setTestStepResult("KEYPAD", "FAIL");
                    can232Form.setTestStepResult("OVERALL", "FAIL");
                    can232Form.logTest("KEY TEST FAILURE ON " + KEY_PAD_BUTTONS[can232Form.keyPadCounter]);
                    can232Form.keyPadTest.logKeyPadTest("KEY TEST FAILURE ON " + KEY_PAD_BUTTONS[can232Form.keyPadCounter]);
                    can232Form.keyPadCounter++;

                    //if (can232Form.keyPadCounter < 5)
                    if (can232Form.keyPadCounter < 9)
                    {
                        can232Form.keyPadTest.logKeyPadTest("Press " + KEY_PAD_BUTTONS[can232Form.keyPadCounter]);

                    }
                    else
                    {
                        //playkeyPadWav = 7;
                        playkeyPadWav = 22;
                        return playkeyPadWav;
                    }
                    //playAudioFile(KEY_WAV_FILES[commanderTestForm.keyPadCounter]);
                }

            }
            else
            {
                if (can232Form.keyPadCounter == 10)
                //if (can232Form.keyPadCounter == 5)
                //if (commanderTestForm.keyPadCounter == (int)Enum.GetNames(typeof(Constants.KEYPAD_BUTTONS)).Length)
                {
                    can232Form.keyPadCounter = 0;

                }
            }

            return playkeyPadWav;
        }

        public static bool keyMatch(CommanderTestForm commanderTestForm, int playkeyPadWav)
        {
            bool response = false;

            switch ((Constants.KEYPAD_BUTTONS)playkeyPadWav)
            {
                //case Constants.KEYPAD_BUTTONS.KEY_Begin_Test:
                //    {
                //        if (commanderTestForm.keyPadCounter == 0)
                //        {
                //            playAudioFile("begin.wav");
                //            playAudioFile("key.wav");
                //            playAudioFile("test.wav");
                //            playAudioFile("silent.wav");
                //            commanderTestForm.keyPadCounter++;
                //            response = true;
                //        }

                //        return response;

                //    }
                case Constants.KEYPAD_BUTTONS.KEY_Draft:
                    {
                        if (commanderTestForm.keyPadCounter == 0)
                        {
                            if (commanderTestForm.keyPadTest != null)
                            {
                                commanderTestForm.keyPadTest.logKeyPadTest("Draft button detected.");
                                commanderTestForm.keyPadTest.logKeyPadTest("Press Sample Button: ");
                            }
                            commanderTestForm.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }

                //case Constants.KEYPAD_BUTTONS.KEY_Feed:
                //    {
                //        if (commanderTestForm.keyPadCounter == 2)
                //        {
                //            //playAudioFile("feed.wav");
                //            playAudioFile(KEY_WAV_FILES[commanderTestForm.keyPadCounter]);
                //            commanderTestForm.keyPadCounter++;
                //            response = true;
                //        }

                //        return response;

                //    }
                case Constants.KEYPAD_BUTTONS.KEY_Sample:
                    {
                        if (commanderTestForm.keyPadCounter == 1)
                        {
                            if (commanderTestForm.keyPadTest != null)
                            {
                                commanderTestForm.keyPadTest.logKeyPadTest("Sample button detected.");
                                commanderTestForm.keyPadTest.logKeyPadTest("Press Down Arrow Button: ");
                            }
                            commanderTestForm.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }
                case Constants.KEYPAD_BUTTONS.KEY_Down:
                    {
                        if (commanderTestForm.keyPadCounter == 2)
                        {
                            if (commanderTestForm.keyPadTest != null)
                            {
                                commanderTestForm.keyPadTest.logKeyPadTest("Down Arrow button detected.");
                                commanderTestForm.keyPadTest.logKeyPadTest("Press 7: ");
                            }
                            commanderTestForm.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }

                case Constants.KEYPAD_BUTTONS.KEY_5:
                    {
                        if (commanderTestForm.keyPadCounter == 4)
                        {
                            if (commanderTestForm.keyPadTest != null)
                            {
                                commanderTestForm.keyPadTest.logKeyPadTest("5 detected.");
                            }
                            commanderTestForm.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }
                
                case Constants.KEYPAD_BUTTONS.KEY_7:
                    {                       
                        if (commanderTestForm.keyPadCounter == 3)
                        {                            
                            if (commanderTestForm.keyPadTest != null)
                            {
                                commanderTestForm.keyPadTest.logKeyPadTest("7 detected.");
                                commanderTestForm.keyPadTest.logKeyPadTest("Press 5: ");
                            }
                            commanderTestForm.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }
                
                default: return false;
            }
        }

        //public static bool keyMatchNew(Can232 can232, int playkeyPadWav)
        //{
        //    bool response = false;

        //    switch ((Constants.KEYPAD_BUTTONS)playkeyPadWav)
        //    {
        //        //case Constants.KEYPAD_BUTTONS.KEY_Begin_Test:
        //        //    {
        //        //        if (commanderTestForm.keyPadCounter == 0)
        //        //        {
        //        //            playAudioFile("begin.wav");
        //        //            playAudioFile("key.wav");
        //        //            playAudioFile("test.wav");
        //        //            playAudioFile("silent.wav");
        //        //            commanderTestForm.keyPadCounter++;
        //        //            response = true;
        //        //        }

        //        //        return response;

        //        //    }
        //        case Constants.KEYPAD_BUTTONS.KEY_Draft:
        //            {
        //                if (can232.keyPadCounter == 0)
        //                {
        //                    if (can232.keyPadTest != null)
        //                    {
        //                        can232.keyPadTest.logKeyPadTest("Draft button detected.");
        //                        can232.keyPadTest.logKeyPadTest("Press Sample Button: ");
        //                    }
        //                    can232.keyPadCounter++;
        //                    response = true;
        //                }

        //                return response;
        //            }

        //        //case Constants.KEYPAD_BUTTONS.KEY_Feed:
        //        //    {
        //        //        if (commanderTestForm.keyPadCounter == 2)
        //        //        {
        //        //            //playAudioFile("feed.wav");
        //        //            playAudioFile(KEY_WAV_FILES[commanderTestForm.keyPadCounter]);
        //        //            commanderTestForm.keyPadCounter++;
        //        //            response = true;
        //        //        }

        //        //        return response;

        //        //    }
        //        case Constants.KEYPAD_BUTTONS.KEY_Sample:
        //            {
        //                if (can232.keyPadCounter == 1)
        //                {
        //                    if (can232.keyPadTest != null)
        //                    {
        //                        can232.keyPadTest.logKeyPadTest("Sample button detected.");
        //                        can232.keyPadTest.logKeyPadTest("Press Down Arrow Button: ");
        //                    }
        //                    can232.keyPadCounter++;
        //                    response = true;
        //                }

        //                return response;
        //            }
        //        case Constants.KEYPAD_BUTTONS.KEY_Down:
        //            {
        //                if (can232.keyPadCounter == 2)
        //                {
        //                    if (can232.keyPadTest != null)
        //                    {
        //                        can232.keyPadTest.logKeyPadTest("Down Arrow button detected.");
        //                        can232.keyPadTest.logKeyPadTest("Press 7: ");
        //                    }
        //                    can232.keyPadCounter++;
        //                    response = true;
        //                }

        //                return response;
        //            }

        //        case Constants.KEYPAD_BUTTONS.KEY_5:
        //            {
        //                if (can232.keyPadCounter == 4)
        //                {
        //                    if (can232.keyPadTest != null)
        //                    {
        //                        can232.keyPadTest.logKeyPadTest("5 detected.");
        //                    }
        //                    can232.keyPadCounter++;
        //                    response = true;
        //                }

        //                return response;
        //            }

        //        case Constants.KEYPAD_BUTTONS.KEY_7:
        //            {
        //                if (can232.keyPadCounter == 3)
        //                {
        //                    if (can232.keyPadTest != null)
        //                    {
        //                        can232.keyPadTest.logKeyPadTest("7 detected.");
        //                        can232.keyPadTest.logKeyPadTest("Press 5: ");
        //                    }
        //                    can232.keyPadCounter++;
        //                    response = true;
        //                }

        //                return response;
        //            }

        //        default: return false;
        //    }
        //}

        public static bool keyMatchNew(Can232 can232, int playkeyPadWav)
        {
            bool response = false;

            switch ((Constants.KEYPAD_BUTTONS)playkeyPadWav)
            {
                //case Constants.KEYPAD_BUTTONS.KEY_Begin_Test:
                //    {
                //        if (commanderTestForm.keyPadCounter == 0)
                //        {
                //            playAudioFile("begin.wav");
                //            playAudioFile("key.wav");
                //            playAudioFile("test.wav");
                //            playAudioFile("silent.wav");
                //            commanderTestForm.keyPadCounter++;
                //            response = true;
                //        }

                //        return response;

                //    }
                case Constants.KEYPAD_BUTTONS.KEY_Draft:
                    {
                        if (can232.keyPadCounter == 0)
                        {
                            if (can232.keyPadTest != null)
                            {
                                can232.keyPadTest.logKeyPadTest("Draft button detected.");
                                can232.keyPadTest.logKeyPadTest("Press 1: ");
                            }
                            can232.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }

                case Constants.KEYPAD_BUTTONS.KEY_1:
                    {
                        if (can232.keyPadCounter == 1)
                        {
                            if (can232.keyPadTest != null)
                            {
                                can232.keyPadTest.logKeyPadTest("1 detected.");
                                can232.keyPadTest.logKeyPadTest("Press 8: ");
                            }
                            can232.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }
                case Constants.KEYPAD_BUTTONS.KEY_8:
                    {
                        if (can232.keyPadCounter == 2)
                        {
                            if (can232.keyPadTest != null)
                            {
                                can232.keyPadTest.logKeyPadTest("8 detected.");
                                can232.keyPadTest.logKeyPadTest("Press 9: ");
                            }
                            can232.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }

                case Constants.KEYPAD_BUTTONS.KEY_9:
                    {
                        if (can232.keyPadCounter == 3)
                        {
                            if (can232.keyPadTest != null)
                            {
                                can232.keyPadTest.logKeyPadTest("9 detected.");
                                can232.keyPadTest.logKeyPadTest("Press 5: ");
                            }
                            can232.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }

                case Constants.KEYPAD_BUTTONS.KEY_5:
                    {
                        if (can232.keyPadCounter == 4)
                        {
                            if (can232.keyPadTest != null)
                            {
                                can232.keyPadTest.logKeyPadTest("5 detected.");
                                can232.keyPadTest.logKeyPadTest("Press Retention Button: ");
                            }
                            can232.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }

                case Constants.KEYPAD_BUTTONS.KEY_Retention:
                    {
                        if (can232.keyPadCounter == 5)
                        {
                            if (can232.keyPadTest != null)
                            {
                                can232.keyPadTest.logKeyPadTest("Retention Button detected.");
                                can232.keyPadTest.logKeyPadTest("Press CowID Button: ");
                            }
                            can232.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }

                case Constants.KEYPAD_BUTTONS.KEY_CowID:
                    {
                        if (can232.keyPadCounter == 6)
                        {
                            if (can232.keyPadTest != null)
                            {
                                can232.keyPadTest.logKeyPadTest("CowID Button detected.");
                                can232.keyPadTest.logKeyPadTest("Press Divert Button: ");
                            }
                            can232.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }

                case Constants.KEYPAD_BUTTONS.KEY_Divert:
                    {
                        if (can232.keyPadCounter == 7)
                        {
                            if (can232.keyPadTest != null)
                            {
                                can232.keyPadTest.logKeyPadTest("Divert Button detected.");
                                can232.keyPadTest.logKeyPadTest("Press ACR Override Button: ");
                            }
                            can232.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }

                case Constants.KEYPAD_BUTTONS.KEY_ACR:
                    {
                        if (can232.keyPadCounter == 8)
                        {
                            if (can232.keyPadTest != null)
                            {
                                can232.keyPadTest.logKeyPadTest("ACR Override Button detected.");
                                can232.keyPadTest.logKeyPadTest("Press Start Button: ");
                            }
                            can232.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }

                case Constants.KEYPAD_BUTTONS.KEY_Start:
                    {
                        if (can232.keyPadCounter == 9)
                        {
                            if (can232.keyPadTest != null)
                            {
                                can232.keyPadTest.logKeyPadTest("Start Button detected.");
                            }
                            can232.keyPadCounter++;
                            response = true;
                        }

                        return response;
                    }

                default: return false;
            }
        }

        public static void playAudioFile(string wavName)
        {
            SoundPlayer keySound = new SoundPlayer(@"C:\Program Files\Dairymaster\Dmmwin\WavFiles\" + wavName);
            keySound.Load();
            keySound.PlaySync(); //Plays the .wav file and loads the .wav file first if it has not been loaded.
        }

    }
}
