﻿using System;
using System.Windows.Forms;
using SimpleInjector;
using SimpleInjector.Diagnostics;
using SimpleInjector.Lifestyles;
using ProdTestingDomain;
using MediatR;
using ProdTestingDataAccess;
using ProdTestingDataAccess.Repositories;
using ProdTestingDataAccess.Interfaces;
using ProdTestingDomain.Queries;
using ProdTestingDomain.Commands;
using ProdTestingDomain.Mappers;
using ProdTestingModels;
using System.Threading.Tasks;
using System.Threading;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CAN232_Monitor
{
	public class DIContainer
	{
        /*public static Container Bootstrap()
         {
             // Create the container as usual.
             var container = new Container();
             //var assembli = GetAssemblies().ToArray();

             container.Options.AllowOverridingRegistrations = false;

             // Register your types, for instance:
             //container.Register<IMediator, Mediator>(Lifestyle.Singleton);
             container.RegisterSingleton<IMediator, Mediator>();
             //container.GetTypesToRegister(typeof(IRequestHandler<,>), typeof(GetTestsByCategoryQuery).Assembly);
             //container.Register(typeof(IRequestHandler<,>), typeof(GetTestsByCategoryQuery<,>));
             //container.GetTypesToRegister(typeof(IRequestHandler<,>), typeof(CreateTestSessionCommand).Assembly);
             container.Register(typeof(IRequestHandler<,>), typeof(GetTestsByCategoryQuery).Assembly);
             //container.Register(typeof(IRequestHandler<,>), assemblies);

             container.Register<IRequestHandler<List<TestResponse>>, GetTestsByCategoryQuery>();
             container.Register(typeof(IRequestHandler<,>), typeof(GetTestsByCategoryQuery<>).Handler);

             // Request instance
             ILogger logger = container.GetInstance<ILogger>();

             //container.Register<ITestSessionCommandsService, TestSessionCommandsService>();
             //container.Register<ITestSessionTransaction, TestSessionTransaction>();
             container.Register<IUnitOfWork, UnitOfWork>();
             //ontainer.Register<ITestSessionMapper, TestSessionMapper>();
             container.Register<ITestMapper, TestMapper>();

             //container.Collection.Register(typeof(IPipelineBehavior<,>), new[] { typeof(DefaultNoOpPipelineBehavior<,>) });
             container.Collection.Register(typeof(IPipelineBehavior<,>), Enumerable.Empty<Type>());
             //container.Collection.Register(typeof(IRequestPreProcessor<>), Enumerable.Empty<Type>());
            // container.Collection.Register(typeof(IRequestPostProcessor<,>), Enumerable.Empty<Type>());

             container.Register(() => new ServiceFactory(container.GetInstance), Lifestyle.Singleton);

             AutoRegisterWindowsForms(container);

             container.Verify();
             //container.GetInstance<IMediator>();

             return container;
         }

         public static void AutoRegisterWindowsForms(Container container)
         {
             /*var types = container.GetTypesToRegister<Form>(typeof(Program).Assembly);

             foreach (var type in types)
             {
                 var registration =
                     Lifestyle.Transient.CreateRegistration(type, container);

                 registration.SuppressDiagnosticWarning(
                     DiagnosticType.DisposableTransientComponent,
                     "Forms should be disposed by app code; not by the container.");

                 container.AddRegistration(type, registration);
             }*/

        //var type = container.GetTypesToRegister<Can232>(typeof(Form).Assembly);

        /*var registration =
                Lifestyle.Transient.CreateRegistration<Can232>(container);

        registration.SuppressDiagnosticWarning(
                DiagnosticType.DisposableTransientComponent,
                "Forms should be disposed by app code; not by the container.");

        container.AddRegistration<Can232>(registration);
    }*/
    }

    /*public class DefaultNoOpPipelineBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        public Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            return next();
        }
	}*/
}
