﻿/*
 Author Liam Mullane
 Date 26/1/2018
 Calculate CRC to match CRC in Commander 

*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CAN232_Monitor
{
    static public class Crc
    {
        static public uint RAM_BLANK = 0xFFFFFFFF;

        public static uint Calc(uint[] DataPtr, uint fileLengthIn32bits, long RamSize)
        {
            uint poly = 0x04C11DB7;
            uint crc = 0xffffffff, crc_temp;
            uint wdata;
            uint[] crctab = new uint[32];
            int i, j;
            uint RAM_Posn = 0;
            int Posn;
            for (j = 0; j < 32; j++)
                crctab[j] = (crc >> j) & 0x1;

            for (Posn = 0; Posn < fileLengthIn32bits; Posn++)
            {
                wdata = DataPtr[Posn];
                for (j = 0; j < 32; j++)
                {
                    crc_temp = (crctab[31] << 31) + (crctab[30] << 30) + (crctab[29] << 29) + (crctab[28] << 28) + (crctab[27] << 27) + (crctab[26] << 26) + (crctab[25] << 25) + (crctab[24] << 24) + (crctab[23] << 23) + (crctab[22] << 22) + (crctab[21] << 21) + (crctab[20] << 20) + (crctab[19] << 19) + (crctab[18] << 18) + (crctab[17] << 17) + (crctab[16] << 16) + (crctab[15] << 15) + (crctab[14] << 14) + (crctab[13] << 13) + (crctab[12] << 12) + (crctab[11] << 11) + (crctab[10] << 10) + (crctab[9] << 9) + (crctab[8] << 8) + (crctab[7] << 7) + (crctab[6] << 6) + (crctab[5] << 5) + (crctab[4] << 4) + (crctab[3] << 3) + (crctab[2] << 2) + (crctab[1] << 1) + (crctab[0]);
                    crctab[0] = ((wdata >> (31 - j)) & 0x1) ^ crctab[31];
                    for (i = 1; i < 32; i++)
                    {
                        crctab[i] = (crctab[0] & ((poly >> i) & 0x1)) ^ ((crc_temp >> (i - 1)) & 0x1);
                    }
                }
                crc = (crctab[31] << 31) + (crctab[30] << 30) + (crctab[29] << 29) + (crctab[28] << 28) + (crctab[27] << 27) + (crctab[26] << 26) + (crctab[25] << 25) + (crctab[24] << 24) + (crctab[23] << 23) + (crctab[22] << 22) + (crctab[21] << 21) + (crctab[20] << 20) + (crctab[19] << 19) + (crctab[18] << 18) + (crctab[17] << 17) + (crctab[16] << 16) + (crctab[15] << 15) + (crctab[14] << 14) + (crctab[13] << 13) + (crctab[12] << 12) + (crctab[11] << 11) + (crctab[10] << 10) + (crctab[9] << 9) + (crctab[8] << 8) + (crctab[7] << 7) + (crctab[6] << 6) + (crctab[5] << 5) + (crctab[4] << 4) + (crctab[3] << 3) + (crctab[2] << 2) + (crctab[1] << 1) + (crctab[0] << 0);
            }

            RAM_Posn = fileLengthIn32bits * 4;

            for (; RAM_Posn < RamSize; RAM_Posn += 4)
            {
                wdata = RAM_BLANK;

                for (j = 0; j < 32; j++)
                {
                    crc_temp = (crctab[31] << 31) + (crctab[30] << 30) + (crctab[29] << 29) + (crctab[28] << 28) + (crctab[27] << 27) + (crctab[26] << 26) + (crctab[25] << 25) + (crctab[24] << 24) + (crctab[23] << 23) + (crctab[22] << 22) + (crctab[21] << 21) + (crctab[20] << 20) + (crctab[19] << 19) + (crctab[18] << 18) + (crctab[17] << 17) + (crctab[16] << 16) + (crctab[15] << 15) + (crctab[14] << 14) + (crctab[13] << 13) + (crctab[12] << 12) + (crctab[11] << 11) + (crctab[10] << 10) + (crctab[9] << 9) + (crctab[8] << 8) + (crctab[7] << 7) + (crctab[6] << 6) + (crctab[5] << 5) + (crctab[4] << 4) + (crctab[3] << 3) + (crctab[2] << 2) + (crctab[1] << 1) + (crctab[0]);
                    crctab[0] = ((wdata >> (31 - j)) & 0x1) ^ crctab[31];
                    for (i = 1; i < 32; i++)
                    {
                        crctab[i] = (crctab[0] & ((poly >> i) & 0x1)) ^ ((crc_temp >> (i - 1)) & 0x1);
                    }
                }
                crc = (crctab[31] << 31) + (crctab[30] << 30) + (crctab[29] << 29) + (crctab[28] << 28) + (crctab[27] << 27) + (crctab[26] << 26) + (crctab[25] << 25) + (crctab[24] << 24) + (crctab[23] << 23) + (crctab[22] << 22) + (crctab[21] << 21) + (crctab[20] << 20) + (crctab[19] << 19) + (crctab[18] << 18) + (crctab[17] << 17) + (crctab[16] << 16) + (crctab[15] << 15) + (crctab[14] << 14) + (crctab[13] << 13) + (crctab[12] << 12) + (crctab[11] << 11) + (crctab[10] << 10) + (crctab[9] << 9) + (crctab[8] << 8) + (crctab[7] << 7) + (crctab[6] << 6) + (crctab[5] << 5) + (crctab[4] << 4) + (crctab[3] << 3) + (crctab[2] << 2) + (crctab[1] << 1) + (crctab[0] << 0);
            }


            return crc;
        }

    }
}
