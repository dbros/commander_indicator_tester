﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace CAN232_Monitor
{
    public partial class MilkLeadTest : Form
    {
        public CommanderTestForm ctf = null;
        public Can232 can = null;

        public MilkLeadTest(CommanderTestForm commanderTestForm)
        {
            ctf = commanderTestForm;
            InitializeComponent();

            this.ControlBox = false;

            Utils.delayThread(500);
            
            logMilkLeadTest("STARTING MILK LEAD TEST..");
            logMilkLeadTest("REMOVE WEIGHT IF ATTACHED..");

            runTimedProc(6000, "Calibrate", "PLEASE WAIT ... READING ZERO", commanderTestForm);
            runTimedProc(12000, "PLEASE ATTACH THE 500G TEST WEIGHT...");
            runTimedProc(19000, "Span", "SENDING SPAN CAN MESSAGE FOR 500G WEIGHT....", commanderTestForm);
            runTimedProc(26000, "GetWgt", "CHECKING 500G WEIGHT....", commanderTestForm);
            runTimedProc(28000, "PLEASE ATTACH THE 250G TEST WEIGHT...");
            runTimedProc(37000, "GetWgt", "CHECKING 250G WEIGHT....", commanderTestForm);
        }

        public MilkLeadTest(Can232 can232Form)
        {
            can = can232Form;
            InitializeComponent();

            this.ControlBox = false;

            Utils.delayThread(500);

            logMilkLeadTest("STARTING MILK LEAD TEST..");
            logMilkLeadTest("REMOVE WEIGHT IF ATTACHED..");

            runTimedProcNew(6000, "Calibrate", "PLEASE WAIT ... READING ZERO", can232Form);
            runTimedProc(12000, "PLEASE ATTACH THE 500G TEST WEIGHT...");
            runTimedProcNew(19000, "Span", "SENDING SPAN CAN MESSAGE FOR 500G WEIGHT....", can232Form);
            runTimedProcNew(26000, "GetWgt", "CHECKING 500G WEIGHT....", can232Form);
            runTimedProc(28000, "PLEASE ATTACH THE 250G TEST WEIGHT...");
            runTimedProcNew(37000, "GetWgt", "CHECKING 250G WEIGHT....", can232Form);
        }

        public void logMilkLeadTest(string text)
        {

            //if (!ctf.factTest.MilkMeterLeadTestComplete)
            if (!can.factTest.MilkMeterLeadTestComplete)
            {

                if (richTextBox1 != null)
                    richTextBox1.AppendText(text + Environment.NewLine);
            }
            
        }

        public void runTimedProc(int miliSecs, string proc, string msg, CommanderTestForm commanderTestForm)
        {
            var t = new Timer();
            t.Interval = miliSecs; // 1000 milisecs = 1 second
            t.Tick += (s, e) =>
            {                
                if (proc == "Calibrate")
                { 
                    commanderTestForm.calibrateZero();
                    logMilkLeadTest(msg);
                }
                else if (proc == "Span")
                {
                    commanderTestForm.span();
                    logMilkLeadTest(msg);
                }
                else if (proc == "GetWgt")
                {
                    commanderTestForm.getWeight();
                    logMilkLeadTest(msg);
                }
               
                t.Stop();
            };
            t.Start();

        }

        public void runTimedProcNew(int miliSecs, string proc, string msg, Can232 can232Form)
        {
            var t = new Timer();
            t.Interval = miliSecs; // 1000 milisecs = 1 second
            t.Tick += (s, e) =>
            {
                if (proc == "Calibrate")
                {
                    can232Form.calibrateZero();
                    logMilkLeadTest(msg);
                }
                else if (proc == "Span")
                {
                    can232Form.span();
                    logMilkLeadTest(msg);
                }
                else if (proc == "GetWgt")
                {
                    can232Form.getWeight();
                    logMilkLeadTest(msg);
                }

                t.Stop();
            };
            t.Start();

        }

        public void runTimedProc(int miliSecs, string msg)
        {
            var t = new Timer();
            t.Interval = miliSecs; // 1000 milisecs = 1 second
            t.Tick += (s, e) =>
            {
                logMilkLeadTest(msg);
                t.Stop();
            };
            t.Start();

        }

        private void button1_Click(object sender, EventArgs e)
        {
     
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            button1.Enabled = false;
            //ctf.factTest.Weight500gTestComplete = true;
            //ctf.factTest.Weight250gTestComplete = true;
            //ctf.factTest.MilkMeterLeadTestComplete = true;
            //ProcessCommTestResp.milkMeterLeadTestAborted = true;
            //ctf.factTest.CurrentTestAborted = true;
            //ctf.milkLeadTestInitiated = false;
            //ctf.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
            ////ctf.processFactoryTest();
            ////this.Dispose();
            //this.Close();

            //if (ctf.milkLeadTest != null)
            //{
            //    ctf.milkLeadTest = null;
            //}

            //Utils.delayThread(3000);

            //ctf.logTest("MILK LEAD TEST ABORTED.");

            can.factTest.Weight500gTestComplete = true;
            can.factTest.Weight250gTestComplete = true;
            can.factTest.MilkMeterLeadTestComplete = true;
			ProcessCommTestResp.milkMeterLeadTestAborted = true;
            can.factTest.CurrentTestAborted = true;
            can.milkLeadTestInitiated = false;
            can.testPhase = Constants.CommanderTestPhase.TEST_FINISHED;
			//ctf.processFactoryTest();
			//this.Dispose();
			this.Close();

			if (can.milkLeadTest != null)
			{
                can.milkLeadTest = null;
			}

			Utils.delayThread(3000);

            can.logTest("MILK LEAD TEST ABORTED.");

		}

    }
}
