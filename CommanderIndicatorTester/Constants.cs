﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CAN232_Monitor
{
    public static class Constants
    {
        public enum CommanderTestPhase
        {
            //GET_IAP_VERSION,
            SET_FACTORY_MODE,
            GET_VERSION,
            GET_DEVID,
            TEST_IO,
            COMMON_LOOMS,
            COMMON_ROT_LOOMS,
            PRODUCT_SPECIFIC_LOOMS,
            KEYPAD_TEST, //must perform this test last because waiting on user key press from comport
            TEST_FINISHED //
        };

        public enum CommanderOutputs
        {
            OP_1,   // 0
            OP_2,   // 1
            OP_3,   // 2
            OP_3A1,  // 3
            OP_3A2,  // 4
            MAIN_LINE, // 5 Line 1 Diversion Valve - Milk
            LINE_2,    // 6 Secondary line
            LINE_3,    // 7 Diversion Valve - Divert
            PULS_1,    // 8 Pulsation
            PULS_2,     // 9 Pulsation
            OP_ACR,    // 10 ACR Ram
            OP_RET,    // 11 Retention
            OP_TSPRAY, // 12 Teat Spray
            OP_CCAIR,  // 13 Cluster Cleanse Air
            OP_CCWAT,  // 14 Cluster Cleanse Water
            OP_CCSAN,  // 15 Cluster Cleanse Sanitiser
            OP_CCVENT, // 16 Cluster Cleanse Vent
            OP_17,     // 17 Jetstream
            OP_BOTTOM, // 18 Bottom Meter Valve
            OP_TOP     // 19 Top Meter Valve
        };

        public enum CoolControlInputs
        {
            OL1 = 17, //MAIN_LINE
            OL2 = 16, //LINE_3 DivertLine OP
            //OL3 = 15, //RETENTION
            OL3 = 15, //ACR_RAM
            OL4 = 14, //OP_CCAIR
            OL6 = 22, //OP_CCWAT
            OL9 = 0, //OP_BOTTOM
            I1 = 19, //OP_TOP
            I11 = 10, //TEAT_SPRAY
            I12 = 11, //JETSTREAM
            I13 = 12, //PULS_1
            //I14 = 13 //OP_ACR
            I14 = 13, //RETENTION
            I25 = 25
        };

        public enum CommanderInputs
        {
            IP_1,   // 0
            IP_2,   // 1
            IP_3,   // 2
            IP_4,  // 3
            IP_5,  // 4
            IP_6, // 5
           
        };

        public enum CoolControlOutputs
        {
            O1 = 0, //compressor op1 for CC OR Prox switch for commander
            O2 = 1, //for Conductivity rest
            O3 = 2, //for Divert Line Reed swith
            O4 = 3, // for Smart Start Reed Switch.
            O5 = 4,
            O6 = 5, 
            O7 = 6,
            O8 = 7,
            O9 = 8,
            O10 = 9,
            O11 = 10,
            O12 = 11,
            O13 = 12,
            O14 = 13,
            O15 = 14,
            O16 = 15,
            O17 = 16,
            O18 = 17,
            O19 = 18,
            O20 = 19,
            O21 = 20,
            O22 = 21,
            O23 = 22,
            O24 = 23,
            O25 = 24,
            O26 = 25,
            O27 = 26,
            O28 = 27           
        };

        public enum KEYPAD_BUTTONS
        {
            KEY_Begin_Test = 0,     //Begin Keypad Test
            KEY_Draft = 15,     //KEY:015 Draft
            KEY_Feed = 16,      //KEY:016 Feed
            KEY_Sample = 25,    //KEY:025 Sample
			KEY_Lock = 21,      //KEY:021  Lock
			KEY_Up = 6,         //KEY:006  Up Arrow
			KEY_Down = 11,      //KEY:011 Down Arrow
			KEY_1 = 4,          //KEY:004 1
			KEY_2 = 3,          //KEY:003 2
			KEY_3 = 9,          //KEY:009 3
			KEY_4 = 8,          //KEY:008 4
			KEY_5 = 7,          //KEY:007 5
            KEY_6 = 18,         //KEY:018 6
            KEY_7 = 17,         //KEY:017 7
			KEY_8 = 10,         //KEY:010 8
			KEY_9 = 5,          //KEY:005  9
			KEY_0 = 2,          //KEY:002  0
			KEY_Retention = 14, //KEY:014  Retention
			KEY_CowID = 13,     //KEY:013  CowID
			KEY_Divert = 20,    //KEY:020  Divert
			KEY_ACR = 19,       //KEY:019  ACR
			KEY_Function = 12,  //KEY:012  Function
			KEY_Enter = 1,      //KEY:001  Enter
			KEY_Audio = 24,     //KEY:024  Audio
			KEY_Start = 22,      //KEY:022  Start
			KEY_End_Test = 100      //End Keypad Test
		};

        public enum CAN_COMMANDS
        {
            CAN_CMD_REQUEST_ISP_STATUS = 100,
            CAN_CMD_ISP_START = 110,
            CAN_CMD_ISP_END = 125,
            CAN_CMD_POWER_UP = 150,
            CAN_CMD_REQUEST_VERSION = 200,
            CAN_CMD_REPORT_VERSION,                	//201
            CAN_CMD_SET_SINGLE_OUTPUT = 202,       	//202
            CAN_CMD_REQUEST_SINGLE_OUTPUT,         	//203
            CAN_CMD_REPORT_SINGLE_OUTPUT,          	//204
            CAN_CMD_SET_MULTI_OUTPUTS,             	//205
            CAN_CMD_REQUEST_IO,                    	//206
            CAN_CMD_REPORT_IO,                     	//207
            CAN_CMD_FUNCTION,                      	//208		
            CAN_CMD_KEY_STROKE,                    	//209
            CAN_CMD_REQUEST_PRESSURE,              	//210
            CAN_CMD_REPORT_PRESSURE,               	//211 
            CAN_CMD_REQUEST_TEMPERATURE,           	//212
            CAN_CMD_REPORT_TEMPERATURE,            	//213 
            CAN_CMD_REQUEST_ADC, 					//214
            CAN_CMD_REPORT_ADC, 					//215
            CAN_CMD_NO_RX_TIMEOUT_ALL_OUTPUTS_OFF, 	//216
            CAN_CMD_SET_TIMEOUTS,                  	//217
            CAN_CMD_DO_SOFTWARE_RESET,             	//218
            CAN_CMD_ERASE_FLASH,                   	//219
            CAN_CMD_PROGRAM_FLASH,                 	//220
            CAN_CMD_NVR_SET,                       	//221
            CAN_CMD_NVR_GET,                       	//222
            CAN_CMD_NVR_REPORT_VALUE,              	//223
            CAN_CMD_DEBUG_STR,						//224
            CAN_CMD_SET_CRC,                       	//225
            CAN_CMD_GET_CRC,
            CAN_CMD_REPORT_CRC,
            CAN_CMD_NVR_SET_U32,
            CAN_CMD_NVR_GET_U32,
            CAN_CMD_NVR_REPORT_WORD,               	//230
            CAN_CMD_MA_SET_MODE,
            CAN_CMD_MA_GET_MODE,
            CAN_CMD_MA_REPORT_MODE,
            CAN_CMD_ADDRESS_CONFIG,
            CAN_CMD_IO_TEST,		               	//235
            CAN_CMD_PULSATION_SYNCH,				//236
            CAN_CMD_START_MILKING,
            CAN_CMD_MILKING_STARTED,               	//238
            CAN_CMD_MILKING_ENDED,                 	//239
            CAN_CMD_ENERGY_PULSE_COUNT,				//240
            CAN_CMD_TEXT,							//241
            CAN_CMD_LCD_LINE,						//242
            //CAN_CMD_GENERIC_FUNCTION,				//243
            CAN_CMD_FACTORY_TEST,					//244
            CAN_CMD_LONG_MSG = 245,			//		245
            CAN_CMD_START_RECORDING = 246,
            CAN_CMD_STOP_RECORDING = 247,
            CAN_CMD_NVR_CHECK_CRC = 248,
            CAN_CMD_REQUEST_DEVID = 249,
            CAN_CMD_CONFIRM_CALLER = 300,
            CAN_CMD_MILK_METER_COWID = 500,
            CAN_CMD_MILK_METER_YIELD,				//501
            CAN_CMD_MILK_METER_BIT,					//502
            CAN_CMD_MILK_METER_SYMBOLS_ALL,			//503
            CAN_CMD_MILK_METER_FUNCTION,			//504	
            //CAN_CMD_REMOVED_AS_FUNCTION_CMD_WILL_DO_MILK_METER_LOCK,						//505
            CAN_CMD_KEYPAD_LOCK_MODE,
            CAN_CMD_PULSATION_BROADCAST_SYNCH,		//506
            CAN_CMD_BROADCAST_SETTINGS,				//507
            CAN_CMD_BROADCAST_CRC,					//508
            CAN_CMD_PRINT_FAIL, // Can be remapped	//509	
            CAN_CMD_UPDATE_NVR,						//510
            CAN_CMD_JET_SAVER_PULSATION,			//511
            CAN_CMD_COMMS_CHECK,					//512
            CAN_CMD_CLEAR_SIDE,						//513
            CAN_CMD_MILK_METER_CONFIGURE,			//514		
            CAN_CMD_MILK_METER_REMOVE_CLUSTERS,		//515
            CAN_CMD_MILK_METER_ROTARY_SYNCH_MODE,	//516
            CAN_CMD_FLOW_PROFILE_DATA,   			//517
            CAN_CMD_DIVERSION_DATA,					//518
            CAN_CMD_REPORT_CMT_DATA,				//519
            CAN_CMD_SCC_KETOSIS,					//520
            CAN_CMD_LOG_TEMP_DATA,					//521
            CAN_CMD_CHECK_MAGNETOMETER,				//522
            CAN_CMD_CABLE_TEST,						//523
            CAN_CMD_SHOW_DATE_DATA,					//524
            CAN_CMD_TIMES_AROUND,					//525
            CAN_CMD_MILK_DEVIATION,					//526
            CAN_CMD_RGB_SET_LEDS,					//527
            CAN_CMD_CONDUCTIVITY_MAX,				//528
            CAN_CMD_MILK_END_MESSAGE,
            CAN_CMD_CALIBRATION_ZERO_DATA = 530,	//530
            CAN_CMD_CALIBRATION_SPAN_DATA,			//531
            CAN_CMD_CONDUCTIVITY_STREAM,            //532
            CAN_CMD_CONDUCTIVITY_STREAM_SMOOTH,		//533
        	CAN_CMD_CONFIG_MODE,										//534
	        CAN_CMD_CLUSTER_CLENSE,									//535
            CAN_CMD_COMMANDER_REQUEST_TEMPERATURE, // 536
            CAN_CMD_CONDUCTIVITY, // 537
            CAN_CMD_GROSS_WGT, // 538

            CAN_CMD_DRAFTER_DRAFT_COW = 600,
            CAN_CMD_DRAFTER_LEAVE_COW_THROUGH,		//601
            CAN_CMD_DRAFTER_NEW_TAG,				//602
            CAN_CMD_DRAFTER_CONFIGS,				//603
            CAN_CMD_DRAFTER_MODE,					//604
            CAN_CMD_STRING_CMD,						//605
            CAN_CMD_DEBUG_TEST,						//606
            CAN_CMD_WEIR_LOG_DATA					//607

        };

        public enum NVR_COMMANDER
        {
            NVR_DEVICE_CAN_ID,									//0
            NVR_ENTER_ISP,											//1
            NVR_PROGRAM_SIZE_LOW, 							//2
            NVR_PROGRAM_SIZE_HIGH, 							//3
            NVR_CRC_LOW,												//4
            NVR_CRC_HIGH,												//5
            NVR_DEBUG_MODE,											//6
            NVR_SERIAL_LOW,											//7
            NVR_SERIAL_HIGH,										//8
            NVR_START_MODE,											//9
            NVR_FLOAT_DOWN_TIME,  							//10
            NVR_MIN_MILK_TIME,									//11
            NVR_MAX_MILK_TIME,									//12
            NVR_ACR_ONLY,												//13	- NB, see setting 93 also...This is used if the parlour is using ONLY FLOATS for ACR
            NVR_WASH_TIME_MAIN_LINE,						//14
            NVR_WASH_TIME_SECONDARY_LINE, 			//15
            NVR_WASH_TIME_DIVERT_LINE,					//16
            NVR_ACR_DELAY,											//17
            NVR_RATE,														//18
            NVR_RATIO,													//19
            NVR_DIVERT_LINE,										//20						
            NVR_NUM_UNITS,											//21
            NVR_CC_DELAY_TIME, 					 				//22			
            NVR_CC_SWEEP_DELAY_TIME,		 	 			//23
            NVR_CC_WATER_ON_TIME,								//24	
            NVR_CC_AIR_ON_TIME,									//25
            NVR_CC_CYCLES,											//26
            NVR_CC_OVERLAP_TIME,								//27
            NVR_CC_RAM_POS,											//28
            NVR_PARLOUR_TYPE,										//29
            NVR_SECOND_MILK_LINE,								//30
            NVR_SWAP_MODE,											//31	
            NVR_KEYLOCK_TIME,										//32
            NVR_ACR_ON,													//33
            NVR_CAL_FACTOR,											//34
            NVR_ACR_TIMER,											//35
            NVR_MILKING_COUNT_removed,					//36 TODO this setting can be replaced - REMOVED BECAUSE IT NEEDS TO BE 32-bit
            NVR_FLOW_RATE,											//ACR Flow rate 
            NVR_PULSATION_OVERRIDE,							//
            NVR_JET_STREAM,											//
            NVR_JET_ON_TIME,										//40
            NVR_JET_OFF_TIME,										//
            NVR_LOGGING,												//
            NVR_KEYLOCK_OVERRIDE,								//
            NVR_CC_PURE_WATER_ON_TIME,					//
            NVR_CC_PURE_OVERLAP_TIME,						//45
            NVR_3A_CC_TEST,											//
            NVR_RESTART_ACR_TIME,								//
            NVR_TURN_ON_TEMP,										//	
            NVR_JETTERS_DOWN_SWEEP_TIME,				//
            NVR_SOLENOID_DIAGNOSIS_THRESH,			//50
            NVR_ENABLE_CONDUCTIVITY,						//51
            NVR_DISPLAY_FLOAT,									//52
            NVR_DUMP_ERROR_WGT,									//53		ADDED THESE FROM HERE DOWN WEIGHALL PROGRAM
            NVR_ACR_PULL_TIME,									//54		
            NVR_DUMP_ACR_TIME,									//55
            NVR_LOW_YIELD_AMOUNT,								//56
            NVR_DEBUG_TIME,											//57
            NVR_WEIGHALL_CAL_FACTOR,						//58	
            NVR_PRESPRAY_ON_TIME,								//59	changed from NVR_SPRAY_ON_TIME to NVR_PRESPRAY_ON_TIME by request from insight dairies
            NVR_SPRAY_ENAB,											//60
            NVR_SPRAY_DELAY_TIME,								//
            NVR_VERBOSE,												//
            NVR_LOADCELL_OFFSET_COEFFICIENT,		//63	
            NVR_LOADCELL_OFFSET_COEFFICIENTB,		//		Required as the loadcell coefficients are 32-bit
            NVR_LOADCELL_GAIN_COEFFICIENT,			//
            NVR_LOADCELL_GAIN_COEFFICIENTB,			//		Required as the loadcell coefficients are 32-bit
            NVR_USE_RETENTION,									//
            NVR_LOG_TO_FILE,										//68	TODO this setting can be replaced as NVR_LOGGING does same job
            NVR_FLOW_PROFILE_TIME,							//69
            NVR_SIMULATE_MODE,									//70
            NVR_WASH_FILL_TOP,									//71
            NVR_WASH_FLOOD_CUP,									//72
            NVR_WASH_EMPTY_MTR,									//73	
            NVR_WASH_FILL_CUP,									//74
            NVR_ACR_RATE,												//75		TODO This can be replaced by new setting - NVR_FLOW_RATE does the same job as it currently
            NVR_TARE,														//76
            NVR_INFLIGHT_TIME,									//77
            NVR_EMPTY_TIME,											//78
            NVR_OPEN_TOP_VALVE_DELAY,						//79
            NVR_MAX_TARE,												//80
            NVR_PULSATION_GROUP_SIZE,  					//
            NVR_PULSATION_OFFSET,								//
            NVR_PULSATION_PERIOD,								//
            NVR_FEED_TOPUP,											//
            NVR_MAX_TRIP_TIME,									//
            NVR_MIN_TRIP_WEIGHT,								//
            NVR_USE_COWIDS,											//
            NVR_CAL_SETTLE_TIME,								//		fahrenheit or celcius
            NVR_YIELD_TYPE,											//		pounds/kgs
            NVR_TEST,														//90
            NVR_M_OFFSET_MSB, 									//91
            NVR_MAG_ENABLE,											//92
            NVR_ACR_ONLY_INDICATOR,							//93	-	This setting will turn off the Yield and use the indicator as an ACR only	
            NVR_TEMPERATURE_LOG,								//94	- 
            NVR_AUTO_START_OFFSET,							//95	
            NVR_FUNCTION_NO,										//96
            NVR_LOADCELL_SMOOTH,								//97
            NVR_CONDUCTIVITY_TIMER,							//98
            NVR_START_DELAY,										//99
            NVR_CONDUCTIVITY_LOGGING,						//100
            NVR_USE_INDICATOR_AS_ACR,						//101
            NVR_SMART_START_DELAY,							//102
            NVR_PROGRAM_TYPE,										//103
            NVR_DISPLAY_SCC,										//104
            NVR_POSTSPRAY_ON_TIME,							//105
            NVR_DISPLAY_COW_GROUP,							//106
            NVR_CONDUCTIVITY_THRESH,						//107
            NVR_M_THS_MSB,											//108
            NVR_M_THS_LSB,											//109
            NVR_M_OFFSET_VALUE,									//110	
            NVR_MAX_COND_PULSE_VALUE,						//111
            NVR_PULSATION_ROTARY,								//112
            NVR_CHECK_LINERS,										//113
            NVR_LINERS_CHANGE,									//114
            NVR_MILKING_COUNT_A,								//115
            NVR_MILKING_COUNT_B,								//116
            NVR_WASHING_LOGGING_TIME,						//117


            NVR_MAX_ADDRESS = 200

        };

    }
}
