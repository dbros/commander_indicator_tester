﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAN232_Monitor
{
	public class EmployeeData
	{
		public string EmployeeRef { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
	}
}
