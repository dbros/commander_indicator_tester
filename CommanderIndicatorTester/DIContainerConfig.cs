﻿using Autofac;
using ProdTestingDataAccess;
using ProdTestingDataAccess.Repositories;
using ProdTestingDataAccess.Interfaces;
using ProdTestingDomain.Queries;
using ProdTestingDomain.Commands.CreateTestSession;
using ProdTestingDomain.Commands.CreateUpdateTestSessionFailureReason;
using ProdTestingDomain.Commands.CreateUpdateTestSessionFixAction;
using ProdTestingDomain.Mappers;
using MediatR;
using System.Reflection;
using ProdTestingDomain;

namespace CAN232_Monitor
{
	public static class DIContainerConfig
	{
		public static IContainer Configure()
		{
			var builder = new ContainerBuilder();

			builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly)
			.AsImplementedInterfaces();
			/*builder.RegisterAssemblyTypes(typeof(GetTestStepsQuery).GetTypeInfo().Assembly)
				.AsClosedTypesOf(typeof(IRequestHandler<,>));*/
			builder.RegisterAssemblyTypes(typeof(GetTestPartCodesQuery).GetTypeInfo().Assembly)
				.AsClosedTypesOf(typeof(IRequestHandler<,>));
			builder.RegisterAssemblyTypes(typeof(GetTestStepsQuery).GetTypeInfo().Assembly)
				.AsClosedTypesOf(typeof(IRequestHandler<,>));
			builder.RegisterAssemblyTypes(typeof(GetFailureReasonsQuery).GetTypeInfo().Assembly)
				.AsClosedTypesOf(typeof(IRequestHandler<,>));
			builder.RegisterAssemblyTypes(typeof(GetFixActionQuery).GetTypeInfo().Assembly)
				.AsClosedTypesOf(typeof(IRequestHandler<,>));
			builder.RegisterAssemblyTypes(typeof(CreateTestSessionCommand).GetTypeInfo().Assembly)
				.AsClosedTypesOf(typeof(IRequestHandler<,>));
			builder.RegisterAssemblyTypes(typeof(CreateUpdateTestSessionFailureCommand).GetTypeInfo().Assembly)
				.AsClosedTypesOf(typeof(IRequestHandler<,>));
			builder.RegisterAssemblyTypes(typeof(CreateUpdateTestSessionFixActionCommand).GetTypeInfo().Assembly)
				.AsClosedTypesOf(typeof(IRequestHandler<,>));

			//builder.RegisterType<TestSessionCommandsService>().As<ITestSessionCommandsService>();
			builder.RegisterType<TestSessionTransaction>().As<ITestSessionTransaction>();
			builder.RegisterType<TestSessionFailureCommandsRepository>().As<ITestSessionFailureCommandsRepository>();
			builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
			builder.RegisterType<TestSessionMapper>().As<ITestSessionMapper>();
			builder.RegisterType<TestMapper>().As<ITestMapper>();
			builder.RegisterType<TestPartCodeMapper>().As<ITestPartCodeMapper>();
			builder.RegisterType<TestStepMapper>().As<ITestStepMapper>();
			builder.RegisterType<FailureReasonMapper>().As<IFailureReasonMapper>();

			//Automapper
			//builder.Register(c => AutoMapperConfig.RegisterAutoMapper()).As<IMapper>()
			//	.InstancePerLifetimeScope().PropertiesAutowired().PreserveExistingDefaults();

			builder.Register<ServiceFactory>(ctx =>
			{
				var c = ctx.Resolve<IComponentContext>();
				return t => c.Resolve(t);
			});

			return builder.Build();
		}

		//Automapper
		//public static IMapper RegisterAutoMapper()
		//{
		//	var config = new MapperConfiguration(cfg =>
		//	{
		//		cfg.AddProfile<ApiMappingProfile1>();
		//		cfg.AddProfile<ApiMappingProfile2>();
		//		// Add all your profiles
		//	});
		//	var mapper = config.CreateMapper();
		//	return mapper;
		//}
	}
}
