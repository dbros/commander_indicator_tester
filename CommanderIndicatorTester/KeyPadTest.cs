﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CAN232_Monitor
{
    public partial class KeyPadTest : Form
    {
        public bool CurrentTestAborted { get; set; }

        public KeyPadTest()
        {
            InitializeComponent();
            this.ControlBox = false;
            logKeyPadTest("Begin Keypad Test..");
            logKeyPadTest("Press Draft Button: ");

        }

        public void logKeyPadTest(string text)
        {
            if (richTextBox1 != null)
                richTextBox1.AppendText(text + Environment.NewLine);
        }

        public void button1_Click(object sender, EventArgs e)
        {
            this.richTextBox1.Clear();
            ProcessCommTestResp.keyPadTestFinished = true;                             
            this.Close();
        }

        private void abortTestbtn_Click(object sender, EventArgs e)
        {
            ProcessCommTestResp.keyPadTestAborted = true;
            this.richTextBox1.Clear();
            this.Close();

            //if (commanderTestForm.keyPadTest != null)
            //{
            //    keyPadTest = null;
            //}
        }

    }
}
