﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CAN232_Monitor
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();

            this.textBoxComport.Text = Properties.Settings.Default.comport;
            this.textBoxSoftVer.Text = Properties.Settings.Default.softVer;
            this.textBoxDefaultAddr.Text = Properties.Settings.Default.defaultAddr;
            this.checkBoxPerformKeyTest.Checked = Properties.Settings.Default.performKeyTest;
            this.tbDefaultSerial.Text = Properties.Settings.Default.defaultSerial;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.comport = this.textBoxComport.Text;
            Properties.Settings.Default.defaultAddr = this.textBoxDefaultAddr.Text;

            this.textBoxSoftVer.Text = this.textBoxSoftVer.Text.Replace(".", "");

            Properties.Settings.Default.softVer = this.textBoxSoftVer.Text;
            Properties.Settings.Default.performKeyTest = this.checkBoxPerformKeyTest.Checked;
            Properties.Settings.Default.autoPrintRpt = this.checkBoxAutoPrintRpt.Checked;
            Properties.Settings.Default.defaultSerial = this.tbDefaultSerial.Text;
            Properties.Settings.Default.Save(); 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
