﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CAN232_Monitor
{
    public class CommanderFactoryTestResults
    {
        public int VersionMajorBootLoader;
        public int VersionMinorBootLoader;
        public int VersionMajorMainProgram;
        public int VersionMinorMainProgram;

        public bool OverallTestPassed;

        public bool IAPVersionTestComplete;
        public string IAPVersion;
        public bool IAPVersionPassed;

        public bool VersionTestPassed;
        public string LastVersion;

        public bool DEVIDTestPassed;
        public string LastDevID;
        public string JigDevID;

        public bool IOTestPassed;
        public bool IOTestComplete;

        public bool CCAirTestPassed;
        public bool CCAirTestComplete;

        public bool CCWaterTestPassed;
        public bool CCWaterTestComplete;

        public bool ProxSwitchFirstTestComplete;
        public bool ProxSwitchSecondTestComplete;
        public bool ProxSwitchTestPassed;
        public bool ProxSwitchTestComplete;

        public bool SmartStartReedSwitchTestPassed;
        public bool SmartStartReedSwitchTestComplete;

        public bool DivertLineReedSwitchFirstTestComplete;
        public bool DivertLineReedSwitchSecondTestComplete;
        public bool DivertLineReedSwitchTestPassed;
        public bool DivertLineReedSwitchTestComplete;

        public bool MilkMeterLeadWireFirstTestComplete;
        public bool MilkMeterLeadWireSecondTestComplete;
        public bool MilkMeterLeadWireTestPassed;
        public bool MilkMeterLeadWireTestComplete;

        public int progType;
        public int weighCalFactor;
        public bool MilkMeterLeadTestPassed;
        public bool MilkMeterLeadTestComplete;
        public bool Weight500gTestPassed;
        public bool Weight500gTestComplete;
        public bool Weight250gTestPassed;
        public bool Weight250gTestComplete;

        public bool MilkLineOPTestPassed;
        public bool MilkLineOPTestComplete;

        public bool DivertLineOPTestPassed;
        public bool DivertLineOPTestComplete;

        public bool PulsationTestPassed;
        public bool PulsationTestComplete;

        public bool AcrRamTestPassed;
        public bool AcrRamTestComplete;

        public bool JetStreamTestPassed;
        public bool JetStreamTestComplete;

        public bool RetentionTestPassed;
        public bool RetentionTestComplete;

        public bool TeatSprayTestPassed;
        public bool TeatSprayTestComplete;

        public bool KeyPadTestPassed;

        public bool TempTestPassed;
        public bool CondProbesTestPassed;

        public bool CurrentTestAborted;

        public void Clear()
        {
            VersionMajorBootLoader = 0;
            VersionMinorBootLoader = 0;
            VersionMajorMainProgram = 0;
            VersionMinorMainProgram = 0;

            OverallTestPassed = true;

            VersionTestPassed = false;
            LastVersion = "";

            DEVIDTestPassed = false;
            LastDevID = "";

            IOTestPassed = false;
            IOTestComplete = false;

            CCAirTestPassed = false;
            CCAirTestComplete = false;

            CCWaterTestPassed = false;
            CCWaterTestComplete = false;

            ProxSwitchFirstTestComplete = false;
            ProxSwitchSecondTestComplete = false;
            ProxSwitchTestPassed = true;
            ProxSwitchTestComplete = false;

            SmartStartReedSwitchTestPassed = false;
            SmartStartReedSwitchTestComplete = false;

            DivertLineReedSwitchFirstTestComplete = false;
            DivertLineReedSwitchSecondTestComplete = false;
            DivertLineReedSwitchTestPassed = true;
            DivertLineReedSwitchTestComplete = false;

            MilkMeterLeadWireFirstTestComplete = false;
            MilkMeterLeadWireSecondTestComplete = false;
            MilkMeterLeadWireTestPassed = true;
            MilkMeterLeadWireTestComplete = false;

            progType = 0;
            weighCalFactor = 0;
            MilkMeterLeadTestPassed = false;
            MilkMeterLeadTestComplete = false;

            MilkLineOPTestPassed = false;
            MilkLineOPTestComplete = false;

            Weight500gTestPassed = false;
            Weight500gTestComplete = false;

            Weight250gTestPassed = false;
            Weight250gTestComplete = false;

            DivertLineOPTestPassed = false;
            DivertLineOPTestComplete = false;

            PulsationTestPassed = false;
            PulsationTestComplete = false;

            AcrRamTestPassed = false;
            AcrRamTestComplete = false;

            JetStreamTestPassed = false;
            JetStreamTestComplete = false;

            RetentionTestPassed = false;
            RetentionTestComplete = false;

            TeatSprayTestPassed = false;
            TeatSprayTestComplete = false;

            KeyPadTestPassed = true;

            TempTestPassed = false;
            CondProbesTestPassed = false;

            CurrentTestAborted = false;
        }
    }
}