﻿namespace CAN232_Monitor
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxComport = new System.Windows.Forms.TextBox();
			this.textBoxSoftVer = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxDefaultAddr = new System.Windows.Forms.TextBox();
			this.checkBoxPerformKeyTest = new System.Windows.Forms.CheckBox();
			this.checkBoxAutoPrintRpt = new System.Windows.Forms.CheckBox();
			this.tbDefaultSerial = new System.Windows.Forms.TextBox();
			this.lblDefaultSerial = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(12, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(67, 18);
			this.label1.TabIndex = 0;
			this.label1.Text = "Comport";
			// 
			// textBoxComport
			// 
			this.textBoxComport.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxComport.Location = new System.Drawing.Point(113, 20);
			this.textBoxComport.Name = "textBoxComport";
			this.textBoxComport.Size = new System.Drawing.Size(60, 24);
			this.textBoxComport.TabIndex = 1;
			// 
			// textBoxSoftVer
			// 
			this.textBoxSoftVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxSoftVer.Location = new System.Drawing.Point(113, 67);
			this.textBoxSoftVer.Name = "textBoxSoftVer";
			this.textBoxSoftVer.Size = new System.Drawing.Size(100, 24);
			this.textBoxSoftVer.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(12, 69);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(58, 18);
			this.label2.TabIndex = 3;
			this.label2.Text = "Version";
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.DarkBlue;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.ForeColor = System.Drawing.Color.GhostWhite;
			this.button1.Location = new System.Drawing.Point(41, 209);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 28);
			this.button1.TabIndex = 4;
			this.button1.Text = "Save";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.DarkBlue;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button2.ForeColor = System.Drawing.Color.GhostWhite;
			this.button2.Location = new System.Drawing.Point(154, 209);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 28);
			this.button2.TabIndex = 5;
			this.button2.Text = "Exit";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(12, 113);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(88, 18);
			this.label3.TabIndex = 6;
			this.label3.Text = "Default Addr";
			// 
			// textBoxDefaultAddr
			// 
			this.textBoxDefaultAddr.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxDefaultAddr.Location = new System.Drawing.Point(113, 113);
			this.textBoxDefaultAddr.Name = "textBoxDefaultAddr";
			this.textBoxDefaultAddr.Size = new System.Drawing.Size(100, 24);
			this.textBoxDefaultAddr.TabIndex = 7;
			// 
			// checkBoxPerformKeyTest
			// 
			this.checkBoxPerformKeyTest.AutoSize = true;
			this.checkBoxPerformKeyTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.checkBoxPerformKeyTest.Location = new System.Drawing.Point(281, 20);
			this.checkBoxPerformKeyTest.Name = "checkBoxPerformKeyTest";
			this.checkBoxPerformKeyTest.Size = new System.Drawing.Size(167, 22);
			this.checkBoxPerformKeyTest.TabIndex = 8;
			this.checkBoxPerformKeyTest.Text = "Perform Keypad Test";
			this.checkBoxPerformKeyTest.UseVisualStyleBackColor = true;
			// 
			// checkBoxAutoPrintRpt
			// 
			this.checkBoxAutoPrintRpt.AutoSize = true;
			this.checkBoxAutoPrintRpt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.checkBoxAutoPrintRpt.Location = new System.Drawing.Point(281, 65);
			this.checkBoxAutoPrintRpt.Name = "checkBoxAutoPrintRpt";
			this.checkBoxAutoPrintRpt.Size = new System.Drawing.Size(140, 22);
			this.checkBoxAutoPrintRpt.TabIndex = 9;
			this.checkBoxAutoPrintRpt.Text = "Auto Print Report";
			this.checkBoxAutoPrintRpt.UseVisualStyleBackColor = true;
			// 
			// tbDefaultSerial
			// 
			this.tbDefaultSerial.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tbDefaultSerial.Location = new System.Drawing.Point(113, 158);
			this.tbDefaultSerial.Name = "tbDefaultSerial";
			this.tbDefaultSerial.Size = new System.Drawing.Size(100, 24);
			this.tbDefaultSerial.TabIndex = 10;
			// 
			// lblDefaultSerial
			// 
			this.lblDefaultSerial.AutoSize = true;
			this.lblDefaultSerial.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDefaultSerial.Location = new System.Drawing.Point(12, 158);
			this.lblDefaultSerial.Name = "lblDefaultSerial";
			this.lblDefaultSerial.Size = new System.Drawing.Size(95, 18);
			this.lblDefaultSerial.TabIndex = 11;
			this.lblDefaultSerial.Text = "Default Serial";
			// 
			// Settings
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Silver;
			this.ClientSize = new System.Drawing.Size(494, 277);
			this.Controls.Add(this.lblDefaultSerial);
			this.Controls.Add(this.tbDefaultSerial);
			this.Controls.Add(this.checkBoxAutoPrintRpt);
			this.Controls.Add(this.checkBoxPerformKeyTest);
			this.Controls.Add(this.textBoxDefaultAddr);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.textBoxSoftVer);
			this.Controls.Add(this.textBoxComport);
			this.Controls.Add(this.label1);
			this.Name = "Settings";
			this.Text = "Settings";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxComport;
        private System.Windows.Forms.TextBox textBoxSoftVer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxDefaultAddr;
        private System.Windows.Forms.CheckBox checkBoxPerformKeyTest;
        private System.Windows.Forms.CheckBox checkBoxAutoPrintRpt;
		private System.Windows.Forms.TextBox tbDefaultSerial;
		private System.Windows.Forms.Label lblDefaultSerial;
	}
}