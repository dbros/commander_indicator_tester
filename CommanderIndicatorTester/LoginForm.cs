﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CAN232_Monitor.Helper;
using System.Threading.Tasks;
using CAN232_Monitor.Models.Repository;
using Autofac;
using ProdTestingDomain.Queries;
using ProdTestingDomain.Commands;
using MediatR;


namespace CAN232_Monitor
{
	public partial class LoginForm : Form
	{
		public LoginForm()
		{
			InitializeComponent();
			lblLoginError.Visible = false;
		}

		private void lbllClear_Click(object sender, EventArgs e)
		{
			txtUsername.Clear();
			txtPassword.Clear();
			txtUsername.Focus();
		}

		private void lblClear_Click(object sender, EventArgs e)
		{
			ClearLoginFields();
		}

		private void ClearLoginFields()
		{
			txtUsername.Clear();
			txtPassword.Clear();
			txtUsername.Focus();
		}

		private void lblExit_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void buttonLogIn_Click(object sender, EventArgs e)
		{
			LoginButton();
		}

		private async void LoginButton()
		{
			if (lblLoginError.Visible)
			{
				lblLoginError.Text = "";
				lblLoginError.Visible = false;

			}

			if ((txtUsername.Text.Length > 0) && (txtPassword.Text.Length > 0))
			{
				//if (await LogInRepository.isEmployeeValid(txtUsername.Text, txtPassword.Text))
				//{
				try
				{
					await EmployeeRepository.getEmployee(txtUsername.Text, txtPassword.Text);

					if (EmployeeRepository.Employee != null)
					{
						/*var container = DIContainer.Bootstrap();
						container.GetInstance<Can232>().Show();
						var mediator = container.GetInstance<IMediator>();*/
						var containiner = DIContainerConfig.Configure();
						var mediator = containiner.Resolve<IMediator>();
						/*var query = new GetTestsByCategoryQuery(1);
						var result = containiner.Resolve<IMediator>().Send(query);
						int i = 0;*/

						new Can232(mediator).Show();

						this.Hide();

					}

				} 
				catch (Exception ex)
				{
					lblLoginError.Visible = true;
					lblLoginError.Text = "Login Error: " + ex.Message.ToString();
					MessageBox.Show("Error: " + ex.Message.ToString());
					txtUsername.Clear();
					txtPassword.Clear();
					txtUsername.Focus();
				}
					
					
				//}

			}
			else
			{
				lblLoginError.Visible = true;
				lblLoginError.Text = "You must enter your Employee Badge Number and PIN";
				//MessageBox.Show("You must enter your Employee Badge Number and PIN");
				txtUsername.Clear();
				txtPassword.Clear();
				txtUsername.Focus();
			}

		}

		private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)Keys.Enter)
			{
				LoginButton();
			}

			if (e.KeyChar == (char)Keys.Escape)
			{
				ClearLoginFields();
			}
		}

		/*
		Constants in Windows API
		0x84 = WM_NCHITTEST - Mouse Capture Test
		0x1 = HTCLIENT - Application Client Area
		0x2 = HTCAPTION - Application Title Bar

		This function intercepts all the commands sent to the application. 
		It checks to see of the message is a mouse click in the application. 
		It passes the action to the base action by default. It reassigns 
		the action to the title bar if it occured in the client area
		to allow the drag and move behavior.
		*/

		protected override void WndProc(ref Message m)
		{
			switch (m.Msg)
			{
				case 0x84:
					base.WndProc(ref m);
					if ((int)m.Result == 0x1)
						m.Result = (IntPtr)0x2;
					return;
			}

			base.WndProc(ref m);
		}
	}
}
