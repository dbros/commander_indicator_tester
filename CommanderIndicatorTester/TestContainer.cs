﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using ProdTestingDomain.Queries;
using MediatR.SimpleInjector;
using SimpleInjector;
using SimpleInjector.Diagnostics;
using ProdTestingModels;
using System.Reflection;

namespace CAN232_Monitor
{
	public class TestContainer
	{
        private static void RegisterAssemblies(Container container)
        {
            var assemblies = GetAssemblies();
            container.RegisterSingleton<IMediator, Mediator>();
            container.Register(typeof(IRequestHandler<,>), assemblies);
            //container.Register(typeof(IRequestHandler<,>), typeof(GetTestsByCategoryQuery).Assembly);
            /*container.Register(typeof(IAsyncRequestHandler<,>), assemblies);
            container.Register(typeof(ICancellableAsyncRequestHandler<>), assemblies);
            container.RegisterCollection(typeof(INotificationHandler<>), assemblies);
            container.RegisterCollection(typeof(IAsyncNotificationHandler<>), assemblies);
            container.RegisterCollection(typeof(ICancellableAsyncNotificationHandler<>), assemblies);
            container.RegisterCollection(typeof(IPipelineBehavior<,>), assemblies);*/
            container.Collection.Register(typeof(IPipelineBehavior<,>), Enumerable.Empty<Type>());

            //container.Register(() => Mapper.Instance);
            //container.RegisterSingleton(new SingleInstanceFactory(container.GetInstance));
            //container.RegisterSingleton(new MultiInstanceFactory(container.GetAllInstances));
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            yield return typeof(IMediator).GetTypeInfo().Assembly;
            //yield return typeof(GetTestStepsQuery).GetTypeInfo().Assembly;
        }
    }
}
