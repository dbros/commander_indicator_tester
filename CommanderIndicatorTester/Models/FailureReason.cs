﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAN232_Monitor.Models
{
	public class FailureReason
	{
		public int FailureReasonId { get; set; }
		public string FailureReasonName { get; set; }
	}
}
