﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAN232_Monitor.Models
{
	public class FixAction
	{
		public int FixActionId { get; set; }
		public string FixActionName { get; set; }
	}
}
