﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAN232_Monitor.Helper;

namespace CAN232_Monitor.Models.Repository
{
	abstract class LogInRepository
	{
		static APIHelper _apiHelper = new APIHelper();

		public static async Task<bool> isEmployeeValid(string empBadge, string pin)
		{

			return await _apiHelper.IsEmployeeValid(empBadge, pin);

		}
	}
}
