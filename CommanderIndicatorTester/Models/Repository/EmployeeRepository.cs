﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAN232_Monitor.Helper;

namespace CAN232_Monitor.Models.Repository
{
	abstract class EmployeeRepository
	{
		static APIHelper _apiHelper = new APIHelper();
		public static Employee Employee;

		public async static Task getEmployee(string empBadge, string pin)
		{

			var task = _apiHelper.GetEmployeeData(empBadge, pin);
			//var task = _apiHelper.GetOverrideEmployeeData(empBadge, pin);
			Employee = await task;

		}


	}
}
