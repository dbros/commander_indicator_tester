﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAN232_Monitor.Models
{
	public class Employee
	{
        public string EmployeeReference { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Department { get; set; }
        public string SubDepartment { get; set; }
        public string SupervisorEmail { get; set; }
        //public decimal EntitlementThisYear { get; set; }
        //public decimal EntitlementNextYear { get; set; }
        //public decimal CarriedOver { get; set; }
        //public decimal DaysTaken { get; set; }
        //public decimal PlannedThisYear { get; set; }
        //public decimal PlannedNextYear { get; set; }
        //public decimal PendingApproval { get; set; }
        //public decimal Cancelled { get; set; }
        //public decimal Rejected { get; set; }
        //public string Address1 { get; set; }
        //public string Address2 { get; set; }
        //public string Address3 { get; set; }
        //public string Address4 { get; set; }
        //public string Address5 { get; set; }
        //public string Mobile { get; set; }
        //public string Landline { get; set; }
        //public decimal TotalEntitlementThisYear { get; set; }
        //public decimal RemainingThisYear { get; set; }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}
