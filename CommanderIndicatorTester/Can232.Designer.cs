﻿namespace CAN232_Monitor
{
    partial class Can232
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Can232));
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelComPort = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelSpeed = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnDevId = new DevExpress.XtraEditors.SimpleButton();
            this.checkBoxShowOPLog = new System.Windows.Forms.CheckBox();
            this.cbFactTestMode = new System.Windows.Forms.CheckBox();
            this.rtboxReceive = new System.Windows.Forms.RichTextBox();
            this.grpboxCanFrameReceive = new System.Windows.Forms.GroupBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.grpboxCanFrameTransmit = new System.Windows.Forms.GroupBox();
            this.lblDataBytes = new System.Windows.Forms.Label();
            this.lblDlc = new System.Windows.Forms.Label();
            this.lblCanId = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.tbxID = new System.Windows.Forms.TextBox();
            this.btnSendFrame = new System.Windows.Forms.Button();
            this.tbxHex1 = new System.Windows.Forms.TextBox();
            this.cboxRtr = new System.Windows.Forms.CheckBox();
            this.tbxHex2 = new System.Windows.Forms.TextBox();
            this.cboxExt = new System.Windows.Forms.CheckBox();
            this.numDlc = new System.Windows.Forms.NumericUpDown();
            this.tbxHex3 = new System.Windows.Forms.TextBox();
            this.tbxHex8 = new System.Windows.Forms.TextBox();
            this.tbxHex4 = new System.Windows.Forms.TextBox();
            this.tbxHex7 = new System.Windows.Forms.TextBox();
            this.tbxHex5 = new System.Windows.Forms.TextBox();
            this.tbxHex6 = new System.Windows.Forms.TextBox();
            this.grpboxCanCommands = new System.Windows.Forms.GroupBox();
            this.btnAutoOff = new System.Windows.Forms.Button();
            this.btnAutoOn = new System.Windows.Forms.Button();
            this.btnTimeStampOff = new System.Windows.Forms.Button();
            this.btnTimeStampOn = new System.Windows.Forms.Button();
            this.btnPollAll = new System.Windows.Forms.Button();
            this.btnPollOne = new System.Windows.Forms.Button();
            this.btnSerNo = new System.Windows.Forms.Button();
            this.btnCanVersion = new System.Windows.Forms.Button();
            this.btnCanFlags = new System.Windows.Forms.Button();
            this.btnCanClose = new System.Windows.Forms.Button();
            this.btnCanOpen = new System.Windows.Forms.Button();
            this.btnSetup = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbCanBitrate = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblScreenTitle = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.comboBoxTestPartCodes = new System.Windows.Forms.ComboBox();
            this.groupBoxResults = new System.Windows.Forms.GroupBox();
            this.lblTestFeedbackText = new System.Windows.Forms.Label();
            this.lblTestFeedback = new System.Windows.Forms.Label();
            this.btnAbortTest = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.labelTestNameDisplay = new System.Windows.Forms.Label();
            this.dataGridViewVerticalTestResults = new System.Windows.Forms.DataGridView();
            this.lblStartTime = new System.Windows.Forms.Label();
            this.lblTestName = new System.Windows.Forms.Label();
            this.btnRunTest = new System.Windows.Forms.Button();
            this.textBoxStartTime = new System.Windows.Forms.TextBox();
            this.lblFailureReason = new System.Windows.Forms.Label();
            this.textBoxEndTime = new System.Windows.Forms.TextBox();
            this.comboBoxFailureReason = new System.Windows.Forms.ComboBox();
            this.lblEndTime = new System.Windows.Forms.Label();
            this.labelFixAction = new System.Windows.Forms.Label();
            this.comboBoxFixAction1 = new System.Windows.Forms.ComboBox();
            this.buttonCommanderTest = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblOpSystemVer = new System.Windows.Forms.Label();
            this.textBoxOpSystemVer = new System.Windows.Forms.TextBox();
            this.textBoxPCOpSystem = new System.Windows.Forms.TextBox();
            this.lbLPCOpSystem = new System.Windows.Forms.Label();
            this.lblIPAddress = new System.Windows.Forms.Label();
            this.textBoxIPAddress = new System.Windows.Forms.TextBox();
            this.lblPCName = new System.Windows.Forms.Label();
            this.textBoxPCName = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.comboBoxFixAction = new System.Windows.Forms.ComboBox();
            this.lblFixAction = new System.Windows.Forms.Label();
            this.comboBoxTestSelect = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxPartCode = new System.Windows.Forms.TextBox();
            this.textBoxSerialNumber = new System.Windows.Forms.TextBox();
            this.lblSerialNumber = new System.Windows.Forms.Label();
            this.lblPartCodes = new System.Windows.Forms.Label();
            this.comboBoxTestList = new System.Windows.Forms.ComboBox();
            this.lblSelectTest = new System.Windows.Forms.Label();
            this.textBoxJobTraveller = new System.Windows.Forms.TextBox();
            this.labelJobTraveller = new System.Windows.Forms.Label();
            this.grpboxComPort = new System.Windows.Forms.GroupBox();
            this.btnComClose = new System.Windows.Forms.Button();
            this.btnComOpen = new System.Windows.Forms.Button();
            this.lblComSpeed = new System.Windows.Forms.Label();
            this.lblComPort = new System.Windows.Forms.Label();
            this.cmbComSpeed = new System.Windows.Forms.ComboBox();
            this.cmbComPort = new System.Windows.Forms.ComboBox();
            this.VersionLb = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.grpboxCanFrameTransmit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDlc)).BeginInit();
            this.grpboxCanCommands.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBoxResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewVerticalTestResults)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpboxComPort.SuspendLayout();
            this.SuspendLayout();
            // 
            // serialPort
            // 
            this.serialPort.ReadTimeout = 500;
            this.serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort_DataReceived);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.Lavender;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelComPort,
            this.toolStripStatusLabelSpeed});
            this.statusStrip1.Location = new System.Drawing.Point(0, 906);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1010, 25);
            this.statusStrip1.TabIndex = 16;
            this.statusStrip1.Text = "statusStrip";
            // 
            // toolStripStatusLabelComPort
            // 
            this.toolStripStatusLabelComPort.AutoSize = false;
            this.toolStripStatusLabelComPort.BackColor = System.Drawing.Color.Lavender;
            this.toolStripStatusLabelComPort.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabelComPort.Name = "toolStripStatusLabelComPort";
            this.toolStripStatusLabelComPort.Size = new System.Drawing.Size(100, 20);
            this.toolStripStatusLabelComPort.Text = "Closed";
            this.toolStripStatusLabelComPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabelSpeed
            // 
            this.toolStripStatusLabelSpeed.AutoSize = false;
            this.toolStripStatusLabelSpeed.BackColor = System.Drawing.Color.Lavender;
            this.toolStripStatusLabelSpeed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabelSpeed.Name = "toolStripStatusLabelSpeed";
            this.toolStripStatusLabelSpeed.Size = new System.Drawing.Size(100, 20);
            this.toolStripStatusLabelSpeed.Text = "-";
            this.toolStripStatusLabelSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(14, 784);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(814, 93);
            this.tabControl1.TabIndex = 19;
            this.tabControl1.UseWaitCursor = true;
            this.tabControl1.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage1.Controls.Add(this.btnDevId);
            this.tabPage1.Controls.Add(this.checkBoxShowOPLog);
            this.tabPage1.Controls.Add(this.cbFactTestMode);
            this.tabPage1.Controls.Add(this.rtboxReceive);
            this.tabPage1.Controls.Add(this.grpboxCanFrameReceive);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(806, 62);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Logs";
            this.tabPage1.UseWaitCursor = true;
            // 
            // btnDevId
            // 
            this.btnDevId.Location = new System.Drawing.Point(685, 89);
            this.btnDevId.Name = "btnDevId";
            this.btnDevId.Size = new System.Drawing.Size(75, 23);
            this.btnDevId.TabIndex = 34;
            this.btnDevId.Text = "simpleButton1";
            this.btnDevId.UseWaitCursor = true;
            this.btnDevId.Click += new System.EventHandler(this.btnDevId_Click);
            // 
            // checkBoxShowOPLog
            // 
            this.checkBoxShowOPLog.AutoSize = true;
            this.checkBoxShowOPLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxShowOPLog.Location = new System.Drawing.Point(685, 67);
            this.checkBoxShowOPLog.Name = "checkBoxShowOPLog";
            this.checkBoxShowOPLog.Size = new System.Drawing.Size(112, 17);
            this.checkBoxShowOPLog.TabIndex = 33;
            this.checkBoxShowOPLog.Text = "Display IO/OPLog";
            this.checkBoxShowOPLog.UseVisualStyleBackColor = true;
            this.checkBoxShowOPLog.UseWaitCursor = true;
            this.checkBoxShowOPLog.CheckedChanged += new System.EventHandler(this.checkBoxShowOPLog_CheckedChanged);
            // 
            // cbFactTestMode
            // 
            this.cbFactTestMode.AutoSize = true;
            this.cbFactTestMode.Location = new System.Drawing.Point(677, 17);
            this.cbFactTestMode.Name = "cbFactTestMode";
            this.cbFactTestMode.Size = new System.Drawing.Size(123, 22);
            this.cbFactTestMode.TabIndex = 17;
            this.cbFactTestMode.Text = "FactTestMode";
            this.cbFactTestMode.UseVisualStyleBackColor = true;
            this.cbFactTestMode.UseWaitCursor = true;
            this.cbFactTestMode.CheckedChanged += new System.EventHandler(this.cbFactTestMode_CheckedChanged);
            // 
            // rtboxReceive
            // 
            this.rtboxReceive.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtboxReceive.Location = new System.Drawing.Point(3, 4);
            this.rtboxReceive.Name = "rtboxReceive";
            this.rtboxReceive.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtboxReceive.Size = new System.Drawing.Size(658, 50);
            this.rtboxReceive.TabIndex = 1;
            this.rtboxReceive.Text = "";
            this.rtboxReceive.UseWaitCursor = true;
            // 
            // grpboxCanFrameReceive
            // 
            this.grpboxCanFrameReceive.Location = new System.Drawing.Point(3, 4);
            this.grpboxCanFrameReceive.Name = "grpboxCanFrameReceive";
            this.grpboxCanFrameReceive.Size = new System.Drawing.Size(507, 80);
            this.grpboxCanFrameReceive.TabIndex = 16;
            this.grpboxCanFrameReceive.TabStop = false;
            this.grpboxCanFrameReceive.UseWaitCursor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.grpboxCanFrameTransmit);
            this.tabPage2.Controls.Add(this.grpboxCanCommands);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(806, 215);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "System Logs";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.UseWaitCursor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(9, 228);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 26;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.UseWaitCursor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(107, 228);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 25;
            this.button3.Text = "Versions";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.UseWaitCursor = true;
            // 
            // grpboxCanFrameTransmit
            // 
            this.grpboxCanFrameTransmit.Controls.Add(this.lblDataBytes);
            this.grpboxCanFrameTransmit.Controls.Add(this.lblDlc);
            this.grpboxCanFrameTransmit.Controls.Add(this.lblCanId);
            this.grpboxCanFrameTransmit.Controls.Add(this.lblResult);
            this.grpboxCanFrameTransmit.Controls.Add(this.tbxID);
            this.grpboxCanFrameTransmit.Controls.Add(this.btnSendFrame);
            this.grpboxCanFrameTransmit.Controls.Add(this.tbxHex1);
            this.grpboxCanFrameTransmit.Controls.Add(this.cboxRtr);
            this.grpboxCanFrameTransmit.Controls.Add(this.tbxHex2);
            this.grpboxCanFrameTransmit.Controls.Add(this.cboxExt);
            this.grpboxCanFrameTransmit.Controls.Add(this.numDlc);
            this.grpboxCanFrameTransmit.Controls.Add(this.tbxHex3);
            this.grpboxCanFrameTransmit.Controls.Add(this.tbxHex8);
            this.grpboxCanFrameTransmit.Controls.Add(this.tbxHex4);
            this.grpboxCanFrameTransmit.Controls.Add(this.tbxHex7);
            this.grpboxCanFrameTransmit.Controls.Add(this.tbxHex5);
            this.grpboxCanFrameTransmit.Controls.Add(this.tbxHex6);
            this.grpboxCanFrameTransmit.Enabled = false;
            this.grpboxCanFrameTransmit.Location = new System.Drawing.Point(9, 81);
            this.grpboxCanFrameTransmit.Name = "grpboxCanFrameTransmit";
            this.grpboxCanFrameTransmit.Size = new System.Drawing.Size(335, 141);
            this.grpboxCanFrameTransmit.TabIndex = 24;
            this.grpboxCanFrameTransmit.TabStop = false;
            this.grpboxCanFrameTransmit.Text = "CAN Transmit Frame (HEX)";
            this.grpboxCanFrameTransmit.UseWaitCursor = true;
            // 
            // lblDataBytes
            // 
            this.lblDataBytes.AutoSize = true;
            this.lblDataBytes.Location = new System.Drawing.Point(106, 25);
            this.lblDataBytes.Name = "lblDataBytes";
            this.lblDataBytes.Size = new System.Drawing.Size(113, 18);
            this.lblDataBytes.TabIndex = 16;
            this.lblDataBytes.Text = "Databytes (1-8):";
            this.lblDataBytes.UseWaitCursor = true;
            // 
            // lblDlc
            // 
            this.lblDlc.AutoSize = true;
            this.lblDlc.Location = new System.Drawing.Point(69, 25);
            this.lblDlc.Name = "lblDlc";
            this.lblDlc.Size = new System.Drawing.Size(42, 18);
            this.lblDlc.TabIndex = 15;
            this.lblDlc.Text = "DLC:";
            this.lblDlc.UseWaitCursor = true;
            // 
            // lblCanId
            // 
            this.lblCanId.AutoSize = true;
            this.lblCanId.Location = new System.Drawing.Point(6, 25);
            this.lblCanId.Name = "lblCanId";
            this.lblCanId.Size = new System.Drawing.Size(61, 18);
            this.lblCanId.TabIndex = 14;
            this.lblCanId.Text = "CAN ID:";
            this.lblCanId.UseWaitCursor = true;
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(6, 120);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(342, 18);
            this.lblResult.TabIndex = 13;
            this.lblResult.Text = "Resulting command: t10080000000000000000[CR]";
            this.lblResult.UseWaitCursor = true;
            // 
            // tbxID
            // 
            this.tbxID.Location = new System.Drawing.Point(6, 41);
            this.tbxID.Name = "tbxID";
            this.tbxID.Size = new System.Drawing.Size(60, 24);
            this.tbxID.TabIndex = 0;
            this.tbxID.Text = "100";
            this.tbxID.UseWaitCursor = true;
            // 
            // btnSendFrame
            // 
            this.btnSendFrame.Location = new System.Drawing.Point(251, 84);
            this.btnSendFrame.Name = "btnSendFrame";
            this.btnSendFrame.Size = new System.Drawing.Size(75, 23);
            this.btnSendFrame.TabIndex = 12;
            this.btnSendFrame.Text = "Send Frame";
            this.btnSendFrame.UseVisualStyleBackColor = true;
            this.btnSendFrame.UseWaitCursor = true;
            // 
            // tbxHex1
            // 
            this.tbxHex1.Location = new System.Drawing.Point(108, 41);
            this.tbxHex1.Name = "tbxHex1";
            this.tbxHex1.Size = new System.Drawing.Size(22, 24);
            this.tbxHex1.TabIndex = 2;
            this.tbxHex1.Text = "00";
            this.tbxHex1.UseWaitCursor = true;
            // 
            // cboxRtr
            // 
            this.cboxRtr.AutoSize = true;
            this.cboxRtr.Location = new System.Drawing.Point(6, 90);
            this.cboxRtr.Name = "cboxRtr";
            this.cboxRtr.Size = new System.Drawing.Size(105, 22);
            this.cboxRtr.TabIndex = 11;
            this.cboxRtr.Text = "RTR Frame";
            this.cboxRtr.UseVisualStyleBackColor = true;
            this.cboxRtr.UseWaitCursor = true;
            // 
            // tbxHex2
            // 
            this.tbxHex2.Location = new System.Drawing.Point(136, 41);
            this.tbxHex2.Name = "tbxHex2";
            this.tbxHex2.Size = new System.Drawing.Size(22, 24);
            this.tbxHex2.TabIndex = 3;
            this.tbxHex2.Text = "00";
            this.tbxHex2.UseWaitCursor = true;
            // 
            // cboxExt
            // 
            this.cboxExt.AutoSize = true;
            this.cboxExt.Location = new System.Drawing.Point(6, 67);
            this.cboxExt.Name = "cboxExt";
            this.cboxExt.Size = new System.Drawing.Size(155, 22);
            this.cboxExt.TabIndex = 10;
            this.cboxExt.Text = "Extended ID (29 bit)";
            this.cboxExt.UseVisualStyleBackColor = true;
            this.cboxExt.UseWaitCursor = true;
            // 
            // numDlc
            // 
            this.numDlc.Location = new System.Drawing.Point(72, 42);
            this.numDlc.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numDlc.Name = "numDlc";
            this.numDlc.ReadOnly = true;
            this.numDlc.Size = new System.Drawing.Size(30, 24);
            this.numDlc.TabIndex = 1;
            this.numDlc.UseWaitCursor = true;
            this.numDlc.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // tbxHex3
            // 
            this.tbxHex3.Location = new System.Drawing.Point(164, 41);
            this.tbxHex3.Name = "tbxHex3";
            this.tbxHex3.Size = new System.Drawing.Size(22, 24);
            this.tbxHex3.TabIndex = 4;
            this.tbxHex3.Text = "00";
            this.tbxHex3.UseWaitCursor = true;
            // 
            // tbxHex8
            // 
            this.tbxHex8.Location = new System.Drawing.Point(304, 41);
            this.tbxHex8.Name = "tbxHex8";
            this.tbxHex8.Size = new System.Drawing.Size(22, 24);
            this.tbxHex8.TabIndex = 9;
            this.tbxHex8.Text = "00";
            this.tbxHex8.UseWaitCursor = true;
            // 
            // tbxHex4
            // 
            this.tbxHex4.Location = new System.Drawing.Point(192, 41);
            this.tbxHex4.Name = "tbxHex4";
            this.tbxHex4.Size = new System.Drawing.Size(22, 24);
            this.tbxHex4.TabIndex = 5;
            this.tbxHex4.Text = "00";
            this.tbxHex4.UseWaitCursor = true;
            // 
            // tbxHex7
            // 
            this.tbxHex7.Location = new System.Drawing.Point(276, 41);
            this.tbxHex7.Name = "tbxHex7";
            this.tbxHex7.Size = new System.Drawing.Size(22, 24);
            this.tbxHex7.TabIndex = 8;
            this.tbxHex7.Text = "00";
            this.tbxHex7.UseWaitCursor = true;
            // 
            // tbxHex5
            // 
            this.tbxHex5.Location = new System.Drawing.Point(220, 41);
            this.tbxHex5.Name = "tbxHex5";
            this.tbxHex5.Size = new System.Drawing.Size(22, 24);
            this.tbxHex5.TabIndex = 6;
            this.tbxHex5.Text = "00";
            this.tbxHex5.UseWaitCursor = true;
            // 
            // tbxHex6
            // 
            this.tbxHex6.Location = new System.Drawing.Point(248, 41);
            this.tbxHex6.Name = "tbxHex6";
            this.tbxHex6.Size = new System.Drawing.Size(22, 24);
            this.tbxHex6.TabIndex = 7;
            this.tbxHex6.Text = "00";
            this.tbxHex6.UseWaitCursor = true;
            // 
            // grpboxCanCommands
            // 
            this.grpboxCanCommands.Controls.Add(this.btnAutoOff);
            this.grpboxCanCommands.Controls.Add(this.btnAutoOn);
            this.grpboxCanCommands.Controls.Add(this.btnTimeStampOff);
            this.grpboxCanCommands.Controls.Add(this.btnTimeStampOn);
            this.grpboxCanCommands.Controls.Add(this.btnPollAll);
            this.grpboxCanCommands.Controls.Add(this.btnPollOne);
            this.grpboxCanCommands.Controls.Add(this.btnSerNo);
            this.grpboxCanCommands.Controls.Add(this.btnCanVersion);
            this.grpboxCanCommands.Controls.Add(this.btnCanFlags);
            this.grpboxCanCommands.Controls.Add(this.btnCanClose);
            this.grpboxCanCommands.Controls.Add(this.btnCanOpen);
            this.grpboxCanCommands.Controls.Add(this.btnSetup);
            this.grpboxCanCommands.Controls.Add(this.label1);
            this.grpboxCanCommands.Controls.Add(this.cmbCanBitrate);
            this.grpboxCanCommands.Enabled = false;
            this.grpboxCanCommands.Location = new System.Drawing.Point(350, 23);
            this.grpboxCanCommands.Name = "grpboxCanCommands";
            this.grpboxCanCommands.Size = new System.Drawing.Size(173, 301);
            this.grpboxCanCommands.TabIndex = 23;
            this.grpboxCanCommands.TabStop = false;
            this.grpboxCanCommands.Text = "CAN232 Commands";
            this.grpboxCanCommands.UseWaitCursor = true;
            this.grpboxCanCommands.Visible = false;
            // 
            // btnAutoOff
            // 
            this.btnAutoOff.Location = new System.Drawing.Point(92, 267);
            this.btnAutoOff.Name = "btnAutoOff";
            this.btnAutoOff.Size = new System.Drawing.Size(75, 23);
            this.btnAutoOff.TabIndex = 29;
            this.btnAutoOff.Text = "Auto Off";
            this.btnAutoOff.UseVisualStyleBackColor = true;
            this.btnAutoOff.UseWaitCursor = true;
            // 
            // btnAutoOn
            // 
            this.btnAutoOn.Location = new System.Drawing.Point(6, 267);
            this.btnAutoOn.Name = "btnAutoOn";
            this.btnAutoOn.Size = new System.Drawing.Size(75, 23);
            this.btnAutoOn.TabIndex = 28;
            this.btnAutoOn.Text = "Auto On";
            this.btnAutoOn.UseVisualStyleBackColor = true;
            this.btnAutoOn.UseWaitCursor = true;
            // 
            // btnTimeStampOff
            // 
            this.btnTimeStampOff.Location = new System.Drawing.Point(92, 223);
            this.btnTimeStampOff.Name = "btnTimeStampOff";
            this.btnTimeStampOff.Size = new System.Drawing.Size(75, 23);
            this.btnTimeStampOff.TabIndex = 27;
            this.btnTimeStampOff.Text = "Time Off";
            this.btnTimeStampOff.UseVisualStyleBackColor = true;
            this.btnTimeStampOff.UseWaitCursor = true;
            // 
            // btnTimeStampOn
            // 
            this.btnTimeStampOn.Location = new System.Drawing.Point(6, 223);
            this.btnTimeStampOn.Name = "btnTimeStampOn";
            this.btnTimeStampOn.Size = new System.Drawing.Size(75, 23);
            this.btnTimeStampOn.TabIndex = 26;
            this.btnTimeStampOn.Text = "Time On";
            this.btnTimeStampOn.UseVisualStyleBackColor = true;
            this.btnTimeStampOn.UseWaitCursor = true;
            // 
            // btnPollAll
            // 
            this.btnPollAll.Location = new System.Drawing.Point(92, 180);
            this.btnPollAll.Name = "btnPollAll";
            this.btnPollAll.Size = new System.Drawing.Size(75, 23);
            this.btnPollAll.TabIndex = 25;
            this.btnPollAll.Text = "Poll All";
            this.btnPollAll.UseVisualStyleBackColor = true;
            this.btnPollAll.UseWaitCursor = true;
            // 
            // btnPollOne
            // 
            this.btnPollOne.Location = new System.Drawing.Point(6, 180);
            this.btnPollOne.Name = "btnPollOne";
            this.btnPollOne.Size = new System.Drawing.Size(75, 23);
            this.btnPollOne.TabIndex = 24;
            this.btnPollOne.Text = "Poll One";
            this.btnPollOne.UseVisualStyleBackColor = true;
            this.btnPollOne.UseWaitCursor = true;
            // 
            // btnSerNo
            // 
            this.btnSerNo.Location = new System.Drawing.Point(6, 136);
            this.btnSerNo.Name = "btnSerNo";
            this.btnSerNo.Size = new System.Drawing.Size(75, 23);
            this.btnSerNo.TabIndex = 23;
            this.btnSerNo.Text = "S/No";
            this.btnSerNo.UseVisualStyleBackColor = true;
            this.btnSerNo.UseWaitCursor = true;
            // 
            // btnCanVersion
            // 
            this.btnCanVersion.Location = new System.Drawing.Point(6, 107);
            this.btnCanVersion.Name = "btnCanVersion";
            this.btnCanVersion.Size = new System.Drawing.Size(75, 23);
            this.btnCanVersion.TabIndex = 22;
            this.btnCanVersion.Text = "Version";
            this.btnCanVersion.UseVisualStyleBackColor = true;
            this.btnCanVersion.UseWaitCursor = true;
            // 
            // btnCanFlags
            // 
            this.btnCanFlags.Location = new System.Drawing.Point(92, 136);
            this.btnCanFlags.Name = "btnCanFlags";
            this.btnCanFlags.Size = new System.Drawing.Size(75, 23);
            this.btnCanFlags.TabIndex = 21;
            this.btnCanFlags.Text = "Flags";
            this.btnCanFlags.UseVisualStyleBackColor = true;
            this.btnCanFlags.UseWaitCursor = true;
            // 
            // btnCanClose
            // 
            this.btnCanClose.Location = new System.Drawing.Point(92, 63);
            this.btnCanClose.Name = "btnCanClose";
            this.btnCanClose.Size = new System.Drawing.Size(75, 23);
            this.btnCanClose.TabIndex = 20;
            this.btnCanClose.Text = "Close";
            this.btnCanClose.UseVisualStyleBackColor = true;
            this.btnCanClose.UseWaitCursor = true;
            // 
            // btnCanOpen
            // 
            this.btnCanOpen.Location = new System.Drawing.Point(6, 63);
            this.btnCanOpen.Name = "btnCanOpen";
            this.btnCanOpen.Size = new System.Drawing.Size(75, 23);
            this.btnCanOpen.TabIndex = 19;
            this.btnCanOpen.Text = "Open";
            this.btnCanOpen.UseVisualStyleBackColor = true;
            this.btnCanOpen.UseWaitCursor = true;
            // 
            // btnSetup
            // 
            this.btnSetup.Location = new System.Drawing.Point(92, 34);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(75, 23);
            this.btnSetup.TabIndex = 18;
            this.btnSetup.Text = "Setup";
            this.btnSetup.UseVisualStyleBackColor = true;
            this.btnSetup.UseWaitCursor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 18);
            this.label1.TabIndex = 17;
            this.label1.Text = "Bitrate";
            this.label1.UseWaitCursor = true;
            // 
            // cmbCanBitrate
            // 
            this.cmbCanBitrate.FormattingEnabled = true;
            this.cmbCanBitrate.Items.AddRange(new object[] {
            "10Kbit",
            "20Kbit",
            "50Kbit",
            "100Kbit",
            "125Kbit",
            "250Kbit",
            "500Kbit",
            "800Kbit",
            "1Mbit"});
            this.cmbCanBitrate.Location = new System.Drawing.Point(6, 36);
            this.cmbCanBitrate.Name = "cmbCanBitrate";
            this.cmbCanBitrate.Size = new System.Drawing.Size(75, 26);
            this.cmbCanBitrate.TabIndex = 16;
            this.cmbCanBitrate.UseWaitCursor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.lblScreenTitle);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.comboBoxTestPartCodes);
            this.panel2.Controls.Add(this.groupBoxResults);
            this.panel2.Controls.Add(this.buttonCommanderTest);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.comboBoxFixAction);
            this.panel2.Controls.Add(this.lblFixAction);
            this.panel2.Controls.Add(this.comboBoxTestSelect);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.grpboxComPort);
            this.panel2.Location = new System.Drawing.Point(0, -2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1462, 900);
            this.panel2.TabIndex = 27;
            // 
            // lblScreenTitle
            // 
            this.lblScreenTitle.AutoSize = true;
            this.lblScreenTitle.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScreenTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.lblScreenTitle.Location = new System.Drawing.Point(91, 20);
            this.lblScreenTitle.Name = "lblScreenTitle";
            this.lblScreenTitle.Size = new System.Drawing.Size(337, 25);
            this.lblScreenTitle.TabIndex = 56;
            this.lblScreenTitle.Text = "Commander + Indicator Tester";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::CAN232_Monitor.Properties.Resources.DMLogo;
            this.pictureBox2.Location = new System.Drawing.Point(13, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(76, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 55;
            this.pictureBox2.TabStop = false;
            // 
            // comboBoxTestPartCodes
            // 
            this.comboBoxTestPartCodes.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxTestPartCodes.FormattingEnabled = true;
            this.comboBoxTestPartCodes.Location = new System.Drawing.Point(877, 268);
            this.comboBoxTestPartCodes.Name = "comboBoxTestPartCodes";
            this.comboBoxTestPartCodes.Size = new System.Drawing.Size(115, 26);
            this.comboBoxTestPartCodes.TabIndex = 30;
            this.comboBoxTestPartCodes.Visible = false;
            // 
            // groupBoxResults
            // 
            this.groupBoxResults.Controls.Add(this.lblTestFeedbackText);
            this.groupBoxResults.Controls.Add(this.lblTestFeedback);
            this.groupBoxResults.Controls.Add(this.btnAbortTest);
            this.groupBoxResults.Controls.Add(this.btnExit);
            this.groupBoxResults.Controls.Add(this.btnLogout);
            this.groupBoxResults.Controls.Add(this.btnSettings);
            this.groupBoxResults.Controls.Add(this.labelTestNameDisplay);
            this.groupBoxResults.Controls.Add(this.dataGridViewVerticalTestResults);
            this.groupBoxResults.Controls.Add(this.lblStartTime);
            this.groupBoxResults.Controls.Add(this.lblTestName);
            this.groupBoxResults.Controls.Add(this.btnRunTest);
            this.groupBoxResults.Controls.Add(this.textBoxStartTime);
            this.groupBoxResults.Controls.Add(this.lblFailureReason);
            this.groupBoxResults.Controls.Add(this.textBoxEndTime);
            this.groupBoxResults.Controls.Add(this.comboBoxFailureReason);
            this.groupBoxResults.Controls.Add(this.lblEndTime);
            this.groupBoxResults.Controls.Add(this.labelFixAction);
            this.groupBoxResults.Controls.Add(this.comboBoxFixAction1);
            this.groupBoxResults.Location = new System.Drawing.Point(13, 247);
            this.groupBoxResults.Name = "groupBoxResults";
            this.groupBoxResults.Size = new System.Drawing.Size(818, 531);
            this.groupBoxResults.TabIndex = 52;
            this.groupBoxResults.TabStop = false;
            this.groupBoxResults.Tag = "";
            // 
            // lblTestFeedbackText
            // 
            this.lblTestFeedbackText.AutoSize = true;
            this.lblTestFeedbackText.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestFeedbackText.Location = new System.Drawing.Point(535, 29);
            this.lblTestFeedbackText.Name = "lblTestFeedbackText";
            this.lblTestFeedbackText.Size = new System.Drawing.Size(272, 18);
            this.lblTestFeedbackText.TabIndex = 57;
            this.lblTestFeedbackText.Text = "_________________________________";
            this.lblTestFeedbackText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTestFeedback
            // 
            this.lblTestFeedback.AutoSize = true;
            this.lblTestFeedback.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestFeedback.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.lblTestFeedback.Location = new System.Drawing.Point(423, 29);
            this.lblTestFeedback.Name = "lblTestFeedback";
            this.lblTestFeedback.Size = new System.Drawing.Size(94, 18);
            this.lblTestFeedback.TabIndex = 56;
            this.lblTestFeedback.Text = "Test Phase:";
            this.lblTestFeedback.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAbortTest
            // 
            this.btnAbortTest.BackColor = System.Drawing.Color.DarkBlue;
            this.btnAbortTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbortTest.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbortTest.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnAbortTest.Location = new System.Drawing.Point(700, 284);
            this.btnAbortTest.Name = "btnAbortTest";
            this.btnAbortTest.Size = new System.Drawing.Size(104, 29);
            this.btnAbortTest.TabIndex = 55;
            this.btnAbortTest.Text = "ABORT TEST";
            this.btnAbortTest.UseVisualStyleBackColor = false;
            this.btnAbortTest.Click += new System.EventHandler(this.btnAbortTest_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.DarkBlue;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnExit.Location = new System.Drawing.Point(706, 480);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(104, 29);
            this.btnExit.TabIndex = 26;
            this.btnExit.Text = "EXIT";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.DarkBlue;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnLogout.Location = new System.Drawing.Point(546, 480);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(154, 29);
            this.btnLogout.TabIndex = 27;
            this.btnLogout.Text = "SWITCH USER";
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.BackColor = System.Drawing.Color.DarkBlue;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnSettings.Location = new System.Drawing.Point(416, 480);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(127, 29);
            this.btnSettings.TabIndex = 18;
            this.btnSettings.Text = "SETTINGS";
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.Click += new System.EventHandler(this.button1_Settings);
            // 
            // labelTestNameDisplay
            // 
            this.labelTestNameDisplay.AutoSize = true;
            this.labelTestNameDisplay.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTestNameDisplay.Location = new System.Drawing.Point(121, 29);
            this.labelTestNameDisplay.Name = "labelTestNameDisplay";
            this.labelTestNameDisplay.Size = new System.Drawing.Size(272, 18);
            this.labelTestNameDisplay.TabIndex = 54;
            this.labelTestNameDisplay.Text = "_________________________________";
            this.labelTestNameDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridViewVerticalTestResults
            // 
            this.dataGridViewVerticalTestResults.AllowUserToOrderColumns = true;
            this.dataGridViewVerticalTestResults.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewVerticalTestResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewVerticalTestResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewVerticalTestResults.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewVerticalTestResults.Enabled = false;
            this.dataGridViewVerticalTestResults.GridColor = System.Drawing.Color.White;
            this.dataGridViewVerticalTestResults.Location = new System.Drawing.Point(5, 63);
            this.dataGridViewVerticalTestResults.Name = "dataGridViewVerticalTestResults";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewVerticalTestResults.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewVerticalTestResults.Size = new System.Drawing.Size(401, 448);
            this.dataGridViewVerticalTestResults.TabIndex = 34;
            // 
            // lblStartTime
            // 
            this.lblStartTime.AutoSize = true;
            this.lblStartTime.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.lblStartTime.Location = new System.Drawing.Point(423, 123);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(86, 18);
            this.lblStartTime.TabIndex = 48;
            this.lblStartTime.Text = "Start Time";
            // 
            // lblTestName
            // 
            this.lblTestName.AutoSize = true;
            this.lblTestName.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.lblTestName.Location = new System.Drawing.Point(2, 29);
            this.lblTestName.Name = "lblTestName";
            this.lblTestName.Size = new System.Drawing.Size(92, 18);
            this.lblTestName.TabIndex = 52;
            this.lblTestName.Text = "Test Name:";
            this.lblTestName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRunTest
            // 
            this.btnRunTest.BackColor = System.Drawing.Color.DarkBlue;
            this.btnRunTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRunTest.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRunTest.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnRunTest.Location = new System.Drawing.Point(426, 63);
            this.btnRunTest.Name = "btnRunTest";
            this.btnRunTest.Size = new System.Drawing.Size(378, 29);
            this.btnRunTest.TabIndex = 8;
            this.btnRunTest.Text = "RUN TEST";
            this.btnRunTest.UseVisualStyleBackColor = false;
            this.btnRunTest.Click += new System.EventHandler(this.btnRunTest_Click);
            // 
            // textBoxStartTime
            // 
            this.textBoxStartTime.Enabled = false;
            this.textBoxStartTime.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStartTime.Location = new System.Drawing.Point(426, 144);
            this.textBoxStartTime.Multiline = true;
            this.textBoxStartTime.Name = "textBoxStartTime";
            this.textBoxStartTime.ReadOnly = true;
            this.textBoxStartTime.Size = new System.Drawing.Size(178, 28);
            this.textBoxStartTime.TabIndex = 49;
            // 
            // lblFailureReason
            // 
            this.lblFailureReason.AutoSize = true;
            this.lblFailureReason.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFailureReason.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.lblFailureReason.Location = new System.Drawing.Point(423, 199);
            this.lblFailureReason.Name = "lblFailureReason";
            this.lblFailureReason.Size = new System.Drawing.Size(120, 18);
            this.lblFailureReason.TabIndex = 36;
            this.lblFailureReason.Text = "Failure Reason";
            // 
            // textBoxEndTime
            // 
            this.textBoxEndTime.Enabled = false;
            this.textBoxEndTime.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEndTime.Location = new System.Drawing.Point(618, 144);
            this.textBoxEndTime.Multiline = true;
            this.textBoxEndTime.Name = "textBoxEndTime";
            this.textBoxEndTime.ReadOnly = true;
            this.textBoxEndTime.Size = new System.Drawing.Size(184, 28);
            this.textBoxEndTime.TabIndex = 51;
            // 
            // comboBoxFailureReason
            // 
            this.comboBoxFailureReason.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxFailureReason.FormattingEnabled = true;
            this.comboBoxFailureReason.ItemHeight = 18;
            this.comboBoxFailureReason.Location = new System.Drawing.Point(426, 220);
            this.comboBoxFailureReason.Name = "comboBoxFailureReason";
            this.comboBoxFailureReason.Size = new System.Drawing.Size(157, 26);
            this.comboBoxFailureReason.TabIndex = 35;
            this.comboBoxFailureReason.SelectedIndexChanged += new System.EventHandler(this.comboBoxFailureReason_SelectedIndexChanged);
            // 
            // lblEndTime
            // 
            this.lblEndTime.AutoSize = true;
            this.lblEndTime.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.lblEndTime.Location = new System.Drawing.Point(617, 123);
            this.lblEndTime.Name = "lblEndTime";
            this.lblEndTime.Size = new System.Drawing.Size(75, 18);
            this.lblEndTime.TabIndex = 50;
            this.lblEndTime.Text = "End Time";
            // 
            // labelFixAction
            // 
            this.labelFixAction.AutoSize = true;
            this.labelFixAction.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFixAction.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.labelFixAction.Location = new System.Drawing.Point(595, 199);
            this.labelFixAction.Name = "labelFixAction";
            this.labelFixAction.Size = new System.Drawing.Size(104, 18);
            this.labelFixAction.TabIndex = 37;
            this.labelFixAction.Text = "Action Taken";
            // 
            // comboBoxFixAction1
            // 
            this.comboBoxFixAction1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxFixAction1.FormattingEnabled = true;
            this.comboBoxFixAction1.ItemHeight = 18;
            this.comboBoxFixAction1.Location = new System.Drawing.Point(598, 220);
            this.comboBoxFixAction1.Name = "comboBoxFixAction1";
            this.comboBoxFixAction1.Size = new System.Drawing.Size(204, 26);
            this.comboBoxFixAction1.TabIndex = 38;
            this.comboBoxFixAction1.SelectedIndexChanged += new System.EventHandler(this.comboBoxFixAction1_SelectedIndexChanged_1);
            // 
            // buttonCommanderTest
            // 
            this.buttonCommanderTest.BackColor = System.Drawing.Color.DarkBlue;
            this.buttonCommanderTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCommanderTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCommanderTest.ForeColor = System.Drawing.Color.GhostWhite;
            this.buttonCommanderTest.Location = new System.Drawing.Point(922, 139);
            this.buttonCommanderTest.Name = "buttonCommanderTest";
            this.buttonCommanderTest.Size = new System.Drawing.Size(70, 32);
            this.buttonCommanderTest.TabIndex = 17;
            this.buttonCommanderTest.Text = "Submit";
            this.buttonCommanderTest.UseVisualStyleBackColor = false;
            this.buttonCommanderTest.Visible = false;
            this.buttonCommanderTest.Click += new System.EventHandler(this.buttonCommanderTest_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblOpSystemVer);
            this.groupBox3.Controls.Add(this.textBoxOpSystemVer);
            this.groupBox3.Controls.Add(this.textBoxPCOpSystem);
            this.groupBox3.Controls.Add(this.lbLPCOpSystem);
            this.groupBox3.Controls.Add(this.lblIPAddress);
            this.groupBox3.Controls.Add(this.textBoxIPAddress);
            this.groupBox3.Controls.Add(this.lblPCName);
            this.groupBox3.Controls.Add(this.textBoxPCName);
            this.groupBox3.Controls.Add(this.textBoxName);
            this.groupBox3.Controls.Add(this.nameLabel);
            this.groupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.groupBox3.Location = new System.Drawing.Point(12, 78);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(819, 81);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "SYSTEM INFO";
            // 
            // lblOpSystemVer
            // 
            this.lblOpSystemVer.AutoSize = true;
            this.lblOpSystemVer.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpSystemVer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.lblOpSystemVer.Location = new System.Drawing.Point(739, 19);
            this.lblOpSystemVer.Name = "lblOpSystemVer";
            this.lblOpSystemVer.Size = new System.Drawing.Size(65, 18);
            this.lblOpSystemVer.TabIndex = 31;
            this.lblOpSystemVer.Text = "Version";
            // 
            // textBoxOpSystemVer
            // 
            this.textBoxOpSystemVer.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOpSystemVer.Location = new System.Drawing.Point(742, 40);
            this.textBoxOpSystemVer.Multiline = true;
            this.textBoxOpSystemVer.Name = "textBoxOpSystemVer";
            this.textBoxOpSystemVer.ReadOnly = true;
            this.textBoxOpSystemVer.Size = new System.Drawing.Size(62, 30);
            this.textBoxOpSystemVer.TabIndex = 30;
            // 
            // textBoxPCOpSystem
            // 
            this.textBoxPCOpSystem.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPCOpSystem.Location = new System.Drawing.Point(545, 41);
            this.textBoxPCOpSystem.Multiline = true;
            this.textBoxPCOpSystem.Name = "textBoxPCOpSystem";
            this.textBoxPCOpSystem.ReadOnly = true;
            this.textBoxPCOpSystem.Size = new System.Drawing.Size(185, 30);
            this.textBoxPCOpSystem.TabIndex = 29;
            // 
            // lbLPCOpSystem
            // 
            this.lbLPCOpSystem.AutoSize = true;
            this.lbLPCOpSystem.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLPCOpSystem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.lbLPCOpSystem.Location = new System.Drawing.Point(542, 18);
            this.lbLPCOpSystem.Name = "lbLPCOpSystem";
            this.lbLPCOpSystem.Size = new System.Drawing.Size(142, 18);
            this.lbLPCOpSystem.TabIndex = 28;
            this.lbLPCOpSystem.Text = "Operating System";
            // 
            // lblIPAddress
            // 
            this.lblIPAddress.AutoSize = true;
            this.lblIPAddress.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIPAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.lblIPAddress.Location = new System.Drawing.Point(391, 16);
            this.lblIPAddress.Name = "lblIPAddress";
            this.lblIPAddress.Size = new System.Drawing.Size(89, 18);
            this.lblIPAddress.TabIndex = 27;
            this.lblIPAddress.Text = "IP Address";
            // 
            // textBoxIPAddress
            // 
            this.textBoxIPAddress.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIPAddress.Location = new System.Drawing.Point(394, 40);
            this.textBoxIPAddress.Multiline = true;
            this.textBoxIPAddress.Name = "textBoxIPAddress";
            this.textBoxIPAddress.ReadOnly = true;
            this.textBoxIPAddress.Size = new System.Drawing.Size(145, 30);
            this.textBoxIPAddress.TabIndex = 26;
            // 
            // lblPCName
            // 
            this.lblPCName.AutoSize = true;
            this.lblPCName.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPCName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.lblPCName.Location = new System.Drawing.Point(203, 16);
            this.lblPCName.Name = "lblPCName";
            this.lblPCName.Size = new System.Drawing.Size(75, 18);
            this.lblPCName.TabIndex = 25;
            this.lblPCName.Text = "PC Name";
            // 
            // textBoxPCName
            // 
            this.textBoxPCName.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPCName.Location = new System.Drawing.Point(206, 40);
            this.textBoxPCName.Multiline = true;
            this.textBoxPCName.Name = "textBoxPCName";
            this.textBoxPCName.ReadOnly = true;
            this.textBoxPCName.Size = new System.Drawing.Size(181, 30);
            this.textBoxPCName.TabIndex = 24;
            // 
            // textBoxName
            // 
            this.textBoxName.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxName.Location = new System.Drawing.Point(3, 40);
            this.textBoxName.Multiline = true;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.ReadOnly = true;
            this.textBoxName.Size = new System.Drawing.Size(197, 30);
            this.textBoxName.TabIndex = 21;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.nameLabel.Location = new System.Drawing.Point(2, 16);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(85, 18);
            this.nameLabel.TabIndex = 20;
            this.nameLabel.Text = "Operator :";
            this.nameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.nameLabel.Click += new System.EventHandler(this.label2_Click);
            // 
            // comboBoxFixAction
            // 
            this.comboBoxFixAction.BackColor = System.Drawing.Color.White;
            this.comboBoxFixAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFixAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxFixAction.FormattingEnabled = true;
            this.comboBoxFixAction.Location = new System.Drawing.Point(962, 92);
            this.comboBoxFixAction.Name = "comboBoxFixAction";
            this.comboBoxFixAction.Size = new System.Drawing.Size(40, 26);
            this.comboBoxFixAction.TabIndex = 31;
            this.comboBoxFixAction.Visible = false;
            // 
            // lblFixAction
            // 
            this.lblFixAction.AutoSize = true;
            this.lblFixAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFixAction.Location = new System.Drawing.Point(850, 95);
            this.lblFixAction.Name = "lblFixAction";
            this.lblFixAction.Size = new System.Drawing.Size(104, 18);
            this.lblFixAction.TabIndex = 33;
            this.lblFixAction.Text = "Part Code   :";
            this.lblFixAction.Visible = false;
            // 
            // comboBoxTestSelect
            // 
            this.comboBoxTestSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTestSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxTestSelect.FormattingEnabled = true;
            this.comboBoxTestSelect.Items.AddRange(new object[] {
            "SSDUTest1",
            "SSDUTest2",
            "SSDUTest3",
            "SSDUTest4",
            "SSDUTest5",
            "SSDUTest6",
            "SSDUTest7",
            "SSDUTest8",
            "SSDUTest9",
            "SSDUTest10",
            "SSDUTest11",
            "SSDUTest12",
            "SSDUTest13",
            "SSDUTest14",
            "SSDUTest15",
            "SSDUTest16",
            "SSDUTest17",
            "SSDUTest18",
            "SSDUTest19",
            "SSDUTest20",
            "RTYTest1",
            "RTYJSTest1",
            "RTYTest2",
            "RTYJSTest2",
            "RTYTest3",
            "RTYJSTest3",
            "RTYTest4",
            "RTYJSTest4",
            "RTYTest5",
            "RTYJSTest5",
            "RTYTest6",
            "RTYJSTest6",
            "RTYTest7",
            "RTYJSTest7",
            "RTYTest8",
            "RTYJSTest8"});
            this.comboBoxTestSelect.Location = new System.Drawing.Point(944, 50);
            this.comboBoxTestSelect.Name = "comboBoxTestSelect";
            this.comboBoxTestSelect.Size = new System.Drawing.Size(48, 26);
            this.comboBoxTestSelect.TabIndex = 25;
            this.comboBoxTestSelect.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxPartCode);
            this.groupBox1.Controls.Add(this.textBoxSerialNumber);
            this.groupBox1.Controls.Add(this.lblSerialNumber);
            this.groupBox1.Controls.Add(this.lblPartCodes);
            this.groupBox1.Controls.Add(this.comboBoxTestList);
            this.groupBox1.Controls.Add(this.lblSelectTest);
            this.groupBox1.Controls.Add(this.textBoxJobTraveller);
            this.groupBox1.Controls.Add(this.labelJobTraveller);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.groupBox1.Location = new System.Drawing.Point(13, 165);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(818, 76);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TEST DETAILS";
            // 
            // textBoxPartCode
            // 
            this.textBoxPartCode.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPartCode.Location = new System.Drawing.Point(611, 37);
            this.textBoxPartCode.Multiline = true;
            this.textBoxPartCode.Name = "textBoxPartCode";
            this.textBoxPartCode.Size = new System.Drawing.Size(193, 27);
            this.textBoxPartCode.TabIndex = 7;
            // 
            // textBoxSerialNumber
            // 
            this.textBoxSerialNumber.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSerialNumber.Location = new System.Drawing.Point(206, 38);
            this.textBoxSerialNumber.Multiline = true;
            this.textBoxSerialNumber.Name = "textBoxSerialNumber";
            this.textBoxSerialNumber.Size = new System.Drawing.Size(196, 27);
            this.textBoxSerialNumber.TabIndex = 5;
            // 
            // lblSerialNumber
            // 
            this.lblSerialNumber.AutoSize = true;
            this.lblSerialNumber.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSerialNumber.Location = new System.Drawing.Point(215, 15);
            this.lblSerialNumber.Name = "lblSerialNumber";
            this.lblSerialNumber.Size = new System.Drawing.Size(125, 18);
            this.lblSerialNumber.TabIndex = 33;
            this.lblSerialNumber.Text = "Serial Number :";
            this.lblSerialNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPartCodes
            // 
            this.lblPartCodes.AutoSize = true;
            this.lblPartCodes.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPartCodes.Location = new System.Drawing.Point(617, 15);
            this.lblPartCodes.Name = "lblPartCodes";
            this.lblPartCodes.Size = new System.Drawing.Size(98, 18);
            this.lblPartCodes.TabIndex = 31;
            this.lblPartCodes.Text = "Part Code   :";
            // 
            // comboBoxTestList
            // 
            this.comboBoxTestList.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxTestList.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxTestList.FormattingEnabled = true;
            this.comboBoxTestList.Location = new System.Drawing.Point(408, 39);
            this.comboBoxTestList.Name = "comboBoxTestList";
            this.comboBoxTestList.Size = new System.Drawing.Size(196, 26);
            this.comboBoxTestList.TabIndex = 6;
            this.comboBoxTestList.SelectedIndexChanged += new System.EventHandler(this.comboBoxTestList_SelectedIndexChanged);
            // 
            // lblSelectTest
            // 
            this.lblSelectTest.AutoSize = true;
            this.lblSelectTest.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectTest.Location = new System.Drawing.Point(410, 15);
            this.lblSelectTest.Name = "lblSelectTest";
            this.lblSelectTest.Size = new System.Drawing.Size(108, 18);
            this.lblSelectTest.TabIndex = 24;
            this.lblSelectTest.Text = "Select Test   :";
            // 
            // textBoxJobTraveller
            // 
            this.textBoxJobTraveller.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxJobTraveller.Location = new System.Drawing.Point(3, 37);
            this.textBoxJobTraveller.Multiline = true;
            this.textBoxJobTraveller.Name = "textBoxJobTraveller";
            this.textBoxJobTraveller.Size = new System.Drawing.Size(197, 27);
            this.textBoxJobTraveller.TabIndex = 4;
            // 
            // labelJobTraveller
            // 
            this.labelJobTraveller.AutoSize = true;
            this.labelJobTraveller.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelJobTraveller.Location = new System.Drawing.Point(5, 16);
            this.labelJobTraveller.Name = "labelJobTraveller";
            this.labelJobTraveller.Size = new System.Drawing.Size(116, 18);
            this.labelJobTraveller.TabIndex = 22;
            this.labelJobTraveller.Text = "Job Traveller :";
            this.labelJobTraveller.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpboxComPort
            // 
            this.grpboxComPort.Controls.Add(this.btnComClose);
            this.grpboxComPort.Controls.Add(this.btnComOpen);
            this.grpboxComPort.Controls.Add(this.lblComSpeed);
            this.grpboxComPort.Controls.Add(this.lblComPort);
            this.grpboxComPort.Controls.Add(this.cmbComSpeed);
            this.grpboxComPort.Controls.Add(this.cmbComPort);
            this.grpboxComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpboxComPort.Location = new System.Drawing.Point(461, 4);
            this.grpboxComPort.Name = "grpboxComPort";
            this.grpboxComPort.Size = new System.Drawing.Size(370, 68);
            this.grpboxComPort.TabIndex = 1;
            this.grpboxComPort.TabStop = false;
            // 
            // btnComClose
            // 
            this.btnComClose.BackColor = System.Drawing.Color.DarkBlue;
            this.btnComClose.Enabled = false;
            this.btnComClose.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComClose.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnComClose.Location = new System.Drawing.Point(265, 35);
            this.btnComClose.Name = "btnComClose";
            this.btnComClose.Size = new System.Drawing.Size(75, 28);
            this.btnComClose.TabIndex = 3;
            this.btnComClose.Text = "CLOSE";
            this.btnComClose.UseVisualStyleBackColor = false;
            this.btnComClose.Click += new System.EventHandler(this.btnComClose_Click);
            // 
            // btnComOpen
            // 
            this.btnComOpen.BackColor = System.Drawing.Color.DarkBlue;
            this.btnComOpen.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComOpen.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnComOpen.Location = new System.Drawing.Point(88, 34);
            this.btnComOpen.Name = "btnComOpen";
            this.btnComOpen.Size = new System.Drawing.Size(75, 29);
            this.btnComOpen.TabIndex = 1;
            this.btnComOpen.Text = "OPEN";
            this.btnComOpen.UseVisualStyleBackColor = false;
            this.btnComOpen.Click += new System.EventHandler(this.btnComOpen_Click);
            // 
            // lblComSpeed
            // 
            this.lblComSpeed.AutoSize = true;
            this.lblComSpeed.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComSpeed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.lblComSpeed.Location = new System.Drawing.Point(186, 14);
            this.lblComSpeed.Name = "lblComSpeed";
            this.lblComSpeed.Size = new System.Drawing.Size(54, 18);
            this.lblComSpeed.TabIndex = 16;
            this.lblComSpeed.Text = "Speed";
            // 
            // lblComPort
            // 
            this.lblComPort.AutoSize = true;
            this.lblComPort.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComPort.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
            this.lblComPort.Location = new System.Drawing.Point(4, 16);
            this.lblComPort.Name = "lblComPort";
            this.lblComPort.Size = new System.Drawing.Size(71, 18);
            this.lblComPort.TabIndex = 15;
            this.lblComPort.Text = "Comport";
            // 
            // cmbComSpeed
            // 
            this.cmbComSpeed.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbComSpeed.FormattingEnabled = true;
            this.cmbComSpeed.Items.AddRange(new object[] {
            "2400",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.cmbComSpeed.Location = new System.Drawing.Point(188, 35);
            this.cmbComSpeed.Name = "cmbComSpeed";
            this.cmbComSpeed.Size = new System.Drawing.Size(75, 24);
            this.cmbComSpeed.TabIndex = 2;
            // 
            // cmbComPort
            // 
            this.cmbComPort.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbComPort.FormattingEnabled = true;
            this.cmbComPort.Location = new System.Drawing.Point(7, 39);
            this.cmbComPort.Name = "cmbComPort";
            this.cmbComPort.Size = new System.Drawing.Size(75, 24);
            this.cmbComPort.TabIndex = 0;
            // 
            // VersionLb
            // 
            this.VersionLb.AutoSize = true;
            this.VersionLb.BackColor = System.Drawing.Color.Lavender;
            this.VersionLb.Location = new System.Drawing.Point(1232, 1048);
            this.VersionLb.Name = "VersionLb";
            this.VersionLb.Size = new System.Drawing.Size(42, 13);
            this.VersionLb.TabIndex = 28;
            this.VersionLb.Text = "Version";
            // 
            // Can232
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1010, 931);
            this.Controls.Add(this.VersionLb);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Can232";
            this.Text = "Dairymaster Commander Tester";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Can232_FormClosed);
            this.Load += new System.EventHandler(this.Can232_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.grpboxCanFrameTransmit.ResumeLayout(false);
            this.grpboxCanFrameTransmit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDlc)).EndInit();
            this.grpboxCanCommands.ResumeLayout(false);
            this.grpboxCanCommands.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBoxResults.ResumeLayout(false);
            this.groupBoxResults.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewVerticalTestResults)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpboxComPort.ResumeLayout(false);
            this.grpboxComPort.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelComPort;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSpeed;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox grpboxCanFrameReceive;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox grpboxCanFrameTransmit;
        private System.Windows.Forms.Label lblDataBytes;
        private System.Windows.Forms.Label lblDlc;
        private System.Windows.Forms.Label lblCanId;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.TextBox tbxID;
        private System.Windows.Forms.Button btnSendFrame;
        private System.Windows.Forms.TextBox tbxHex1;
        private System.Windows.Forms.CheckBox cboxRtr;
        private System.Windows.Forms.TextBox tbxHex2;
        private System.Windows.Forms.CheckBox cboxExt;
        private System.Windows.Forms.NumericUpDown numDlc;
        private System.Windows.Forms.TextBox tbxHex3;
        private System.Windows.Forms.TextBox tbxHex8;
        private System.Windows.Forms.TextBox tbxHex4;
        private System.Windows.Forms.TextBox tbxHex7;
        private System.Windows.Forms.TextBox tbxHex5;
        private System.Windows.Forms.TextBox tbxHex6;
        private System.Windows.Forms.GroupBox grpboxCanCommands;
        private System.Windows.Forms.Button btnAutoOff;
        private System.Windows.Forms.Button btnAutoOn;
        private System.Windows.Forms.Button btnTimeStampOff;
        private System.Windows.Forms.Button btnTimeStampOn;
        private System.Windows.Forms.Button btnPollAll;
        private System.Windows.Forms.Button btnPollOne;
        private System.Windows.Forms.Button btnSerNo;
        private System.Windows.Forms.Button btnCanVersion;
        private System.Windows.Forms.Button btnCanFlags;
        private System.Windows.Forms.Button btnCanClose;
        private System.Windows.Forms.Button btnCanOpen;
        private System.Windows.Forms.Button btnSetup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbCanBitrate;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonCommanderTest;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label lblSelectTest;
        private System.Windows.Forms.Label labelJobTraveller;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Label VersionLb;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.ComboBox comboBoxTestSelect;
        public System.Windows.Forms.TextBox textBoxJobTraveller;
        public System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Button btnExit;
		private System.Windows.Forms.Button btnLogout;
		public System.Windows.Forms.ComboBox comboBoxTestList;
		private System.Windows.Forms.Button btnRunTest;
		private System.Windows.Forms.Label lblPartCodes;
		public System.Windows.Forms.ComboBox comboBoxTestPartCodes;
		private System.Windows.Forms.Label lblFixAction;
		public System.Windows.Forms.ComboBox comboBoxFixAction;
		private System.Windows.Forms.ComboBox comboBoxFixAction1;
		private System.Windows.Forms.Label labelFixAction;
		private System.Windows.Forms.Label lblFailureReason;
		private System.Windows.Forms.ComboBox comboBoxFailureReason;
		public System.Windows.Forms.TextBox textBoxSerialNumber;
		private System.Windows.Forms.Label lblSerialNumber;
		private System.Windows.Forms.GroupBox grpboxComPort;
		private System.Windows.Forms.Button btnComClose;
		private System.Windows.Forms.Button btnComOpen;
		private System.Windows.Forms.Label lblComSpeed;
		private System.Windows.Forms.Label lblComPort;
		private System.Windows.Forms.ComboBox cmbComSpeed;
		private System.Windows.Forms.ComboBox cmbComPort;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label lbLPCOpSystem;
		private System.Windows.Forms.Label lblIPAddress;
		private System.Windows.Forms.TextBox textBoxIPAddress;
		private System.Windows.Forms.Label lblPCName;
		private System.Windows.Forms.TextBox textBoxPCName;
		private System.Windows.Forms.TextBox textBoxPCOpSystem;
		private System.Windows.Forms.TextBox textBoxEndTime;
		private System.Windows.Forms.Label lblEndTime;
		private System.Windows.Forms.TextBox textBoxStartTime;
		private System.Windows.Forms.Label lblStartTime;
		private System.Windows.Forms.Label lblOpSystemVer;
		private System.Windows.Forms.TextBox textBoxOpSystemVer;
		private System.Windows.Forms.GroupBox groupBoxResults;
		private System.Windows.Forms.Label labelTestNameDisplay;
		private System.Windows.Forms.DataGridView dataGridViewVerticalTestResults;
		private System.Windows.Forms.Label lblTestName;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Label lblScreenTitle;
		private System.Windows.Forms.Button btnAbortTest;
		private System.Windows.Forms.RichTextBox rtboxReceive;
		private System.Windows.Forms.CheckBox cbFactTestMode;
		public System.Windows.Forms.CheckBox checkBoxShowOPLog;
		private System.Windows.Forms.Label lblTestFeedbackText;
		private System.Windows.Forms.Label lblTestFeedback;
		public System.Windows.Forms.TextBox textBoxPartCode;
		private DevExpress.XtraEditors.SimpleButton btnDevId;
	}
}

