﻿namespace CAN232_Monitor
{
	partial class LoginForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblLogIn = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.buttonLogIn = new System.Windows.Forms.Button();
			this.lblExit = new System.Windows.Forms.Label();
			this.txtUsername = new System.Windows.Forms.TextBox();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.lblClear = new System.Windows.Forms.Label();
			this.lblLoginError = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// lblLogIn
			// 
			this.lblLogIn.AutoSize = true;
			this.lblLogIn.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblLogIn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
			this.lblLogIn.Location = new System.Drawing.Point(94, 151);
			this.lblLogIn.Name = "lblLogIn";
			this.lblLogIn.Size = new System.Drawing.Size(101, 29);
			this.lblLogIn.TabIndex = 1;
			this.lblLogIn.Text = "LOG IN";
			// 
			// panel1
			// 
			this.panel1.Location = new System.Drawing.Point(77, 209);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(236, 1);
			this.panel1.TabIndex = 3;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(117)))), ((int)(((byte)(214)))));
			this.panel2.Location = new System.Drawing.Point(23, 240);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(236, 1);
			this.panel2.TabIndex = 4;
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
			this.panel3.Location = new System.Drawing.Point(23, 287);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(236, 1);
			this.panel3.TabIndex = 6;
			// 
			// pictureBox3
			// 
			this.pictureBox3.Image = global::CAN232_Monitor.Properties.Resources.lock_icon;
			this.pictureBox3.Location = new System.Drawing.Point(23, 256);
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new System.Drawing.Size(25, 25);
			this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox3.TabIndex = 5;
			this.pictureBox3.TabStop = false;
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = global::CAN232_Monitor.Properties.Resources.User_blue_icon;
			this.pictureBox2.Location = new System.Drawing.Point(23, 209);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(25, 25);
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox2.TabIndex = 2;
			this.pictureBox2.TabStop = false;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::CAN232_Monitor.Properties.Resources.DMLogo;
			this.pictureBox1.Location = new System.Drawing.Point(98, 64);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(87, 71);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// buttonLogIn
			// 
			this.buttonLogIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
			this.buttonLogIn.FlatAppearance.BorderSize = 0;
			this.buttonLogIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonLogIn.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonLogIn.ForeColor = System.Drawing.Color.White;
			this.buttonLogIn.Location = new System.Drawing.Point(23, 323);
			this.buttonLogIn.Name = "buttonLogIn";
			this.buttonLogIn.Size = new System.Drawing.Size(236, 33);
			this.buttonLogIn.TabIndex = 7;
			this.buttonLogIn.Text = "LOG IN";
			this.buttonLogIn.UseVisualStyleBackColor = false;
			this.buttonLogIn.Click += new System.EventHandler(this.buttonLogIn_Click);
			// 
			// lblExit
			// 
			this.lblExit.AutoSize = true;
			this.lblExit.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
			this.lblExit.Location = new System.Drawing.Point(117, 370);
			this.lblExit.Name = "lblExit";
			this.lblExit.Size = new System.Drawing.Size(37, 18);
			this.lblExit.TabIndex = 9;
			this.lblExit.Text = "Exit";
			this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
			// 
			// txtUsername
			// 
			this.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtUsername.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
			this.txtUsername.Location = new System.Drawing.Point(54, 209);
			this.txtUsername.Multiline = true;
			this.txtUsername.Name = "txtUsername";
			this.txtUsername.Size = new System.Drawing.Size(205, 24);
			this.txtUsername.TabIndex = 10;
			// 
			// txtPassword
			// 
			this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtPassword.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
			this.txtPassword.HideSelection = false;
			this.txtPassword.Location = new System.Drawing.Point(54, 256);
			this.txtPassword.Multiline = true;
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(205, 24);
			this.txtPassword.TabIndex = 11;
			this.txtPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPassword_KeyPress);
			// 
			// lblClear
			// 
			this.lblClear.AutoSize = true;
			this.lblClear.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblClear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(204)))));
			this.lblClear.Location = new System.Drawing.Point(179, 302);
			this.lblClear.Name = "lblClear";
			this.lblClear.Size = new System.Drawing.Size(97, 18);
			this.lblClear.TabIndex = 12;
			this.lblClear.Text = "Clear Fields";
			this.lblClear.Click += new System.EventHandler(this.lblClear_Click);
			// 
			// lblLoginError
			// 
			this.lblLoginError.AutoSize = true;
			this.lblLoginError.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblLoginError.ForeColor = System.Drawing.Color.Red;
			this.lblLoginError.Location = new System.Drawing.Point(20, 183);
			this.lblLoginError.Name = "lblLoginError";
			this.lblLoginError.Size = new System.Drawing.Size(44, 18);
			this.lblLoginError.TabIndex = 13;
			this.lblLoginError.Text = "Error:";
			// 
			// LoginForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(288, 447);
			this.Controls.Add(this.lblLoginError);
			this.Controls.Add(this.lblClear);
			this.Controls.Add(this.txtPassword);
			this.Controls.Add(this.txtUsername);
			this.Controls.Add(this.lblExit);
			this.Controls.Add(this.buttonLogIn);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.pictureBox3);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.lblLogIn);
			this.Controls.Add(this.pictureBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "LoginForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "LoginForm";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label lblLogIn;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.Button buttonLogIn;
		private System.Windows.Forms.Label lblExit;
		private System.Windows.Forms.TextBox txtUsername;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Label lblClear;
		private System.Windows.Forms.Label lblLoginError;
	}
}