﻿namespace CAN232_Monitor
{
    partial class CommanderTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommanderTestForm));
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.buttonStartCommanderTest = new System.Windows.Forms.Button();
			this.printDocument1 = new System.Drawing.Printing.PrintDocument();
			this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.richTextBoxTestLogs = new System.Windows.Forms.RichTextBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.button6 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.checkBoxShowOPLog = new System.Windows.Forms.CheckBox();
			this.button4 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.progressBar1Lb = new System.Windows.Forms.Label();
			this.printDialog1 = new System.Windows.Forms.PrintDialog();
			this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.abortTest = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// richTextBox1
			// 
			this.richTextBox1.Location = new System.Drawing.Point(6, 6);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.Size = new System.Drawing.Size(449, 478);
			this.richTextBox1.TabIndex = 0;
			this.richTextBox1.Text = "";
			// 
			// buttonStartCommanderTest
			// 
			this.buttonStartCommanderTest.BackColor = System.Drawing.Color.DarkBlue;
			this.buttonStartCommanderTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonStartCommanderTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonStartCommanderTest.ForeColor = System.Drawing.Color.GhostWhite;
			this.buttonStartCommanderTest.Location = new System.Drawing.Point(420, 7);
			this.buttonStartCommanderTest.Name = "buttonStartCommanderTest";
			this.buttonStartCommanderTest.Size = new System.Drawing.Size(86, 32);
			this.buttonStartCommanderTest.TabIndex = 1;
			this.buttonStartCommanderTest.Text = "Begin Test";
			this.buttonStartCommanderTest.UseVisualStyleBackColor = false;
			this.buttonStartCommanderTest.Click += new System.EventHandler(this.buttonStartCommanderTest_Click);
			// 
			// printDocument1
			// 
			this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
			// 
			// printPreviewDialog1
			// 
			this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
			this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
			this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
			this.printPreviewDialog1.Document = this.printDocument1;
			this.printPreviewDialog1.Enabled = true;
			this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
			this.printPreviewDialog1.Name = "printPreviewDialog1";
			this.printPreviewDialog1.Visible = false;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabControl1.Location = new System.Drawing.Point(7, 156);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(593, 521);
			this.tabControl1.TabIndex = 11;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.richTextBoxTestLogs);
			this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabPage1.Location = new System.Drawing.Point(4, 27);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(585, 490);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Test Logs";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// richTextBoxTestLogs
			// 
			this.richTextBoxTestLogs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.richTextBoxTestLogs.Location = new System.Drawing.Point(7, 7);
			this.richTextBoxTestLogs.Name = "richTextBoxTestLogs";
			this.richTextBoxTestLogs.ReadOnly = true;
			this.richTextBoxTestLogs.Size = new System.Drawing.Size(572, 471);
			this.richTextBoxTestLogs.TabIndex = 0;
			this.richTextBoxTestLogs.Text = "";
			this.richTextBoxTestLogs.TextChanged += new System.EventHandler(this.richTextBoxTestLogs_TextChanged);
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.checkBox1);
			this.tabPage2.Controls.Add(this.button6);
			this.tabPage2.Controls.Add(this.button5);
			this.tabPage2.Controls.Add(this.richTextBox1);
			this.tabPage2.Controls.Add(this.checkBoxShowOPLog);
			this.tabPage2.Controls.Add(this.button4);
			this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabPage2.Location = new System.Drawing.Point(4, 27);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(585, 490);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "System Logs";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.checkBox1.Location = new System.Drawing.Point(472, 12);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(95, 17);
			this.checkBox1.TabIndex = 35;
			this.checkBox1.Text = "FactTestMode";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// button6
			// 
			this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button6.Location = new System.Drawing.Point(488, 99);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(75, 23);
			this.button6.TabIndex = 34;
			this.button6.Text = "Set";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.button6_Click_1);
			// 
			// button5
			// 
			this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button5.Location = new System.Drawing.Point(472, 151);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(102, 23);
			this.button5.TabIndex = 33;
			this.button5.Text = "Request IO\'s";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button5_Click_1);
			// 
			// checkBoxShowOPLog
			// 
			this.checkBoxShowOPLog.AutoSize = true;
			this.checkBoxShowOPLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.checkBoxShowOPLog.Location = new System.Drawing.Point(461, 128);
			this.checkBoxShowOPLog.Name = "checkBoxShowOPLog";
			this.checkBoxShowOPLog.Size = new System.Drawing.Size(112, 17);
			this.checkBoxShowOPLog.TabIndex = 32;
			this.checkBoxShowOPLog.Text = "Display IO/OPLog";
			this.checkBoxShowOPLog.UseVisualStyleBackColor = true;
			this.checkBoxShowOPLog.CheckedChanged += new System.EventHandler(this.checkBoxShowOPLog_CheckedChanged_1);
			// 
			// button4
			// 
			this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button4.Location = new System.Drawing.Point(488, 59);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(75, 23);
			this.button4.TabIndex = 31;
			this.button4.Text = "SmartStart";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click_1);
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.DarkBlue;
			this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button3.ForeColor = System.Drawing.Color.GhostWhite;
			this.button3.Location = new System.Drawing.Point(516, 6);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(86, 32);
			this.button3.TabIndex = 20;
			this.button3.Text = "Print";
			this.button3.UseVisualStyleBackColor = false;
			this.button3.Click += new System.EventHandler(this.button3_Click_1);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(3, 12);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(110, 18);
			this.label3.TabIndex = 21;
			this.label3.Text = "Tester Name:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(2, 48);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(111, 18);
			this.label4.TabIndex = 22;
			this.label4.Text = "Job Traveller:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(3, 83);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(116, 18);
			this.label5.TabIndex = 23;
			this.label5.Text = "Selected Test:";
			// 
			// textBox1
			// 
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox1.Location = new System.Drawing.Point(127, 10);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.textBox1.Size = new System.Drawing.Size(258, 24);
			this.textBox1.TabIndex = 24;
			this.textBox1.WordWrap = false;
			// 
			// textBox2
			// 
			this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox2.Location = new System.Drawing.Point(127, 46);
			this.textBox2.Name = "textBox2";
			this.textBox2.ReadOnly = true;
			this.textBox2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.textBox2.Size = new System.Drawing.Size(258, 24);
			this.textBox2.TabIndex = 25;
			this.textBox2.WordWrap = false;
			// 
			// textBox3
			// 
			this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox3.Location = new System.Drawing.Point(127, 80);
			this.textBox3.Name = "textBox3";
			this.textBox3.ReadOnly = true;
			this.textBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.textBox3.Size = new System.Drawing.Size(258, 24);
			this.textBox3.TabIndex = 26;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(7, 125);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(595, 23);
			this.progressBar1.TabIndex = 27;
			// 
			// progressBar1Lb
			// 
			this.progressBar1Lb.AutoSize = true;
			this.progressBar1Lb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.progressBar1Lb.Location = new System.Drawing.Point(4, 107);
			this.progressBar1Lb.Name = "progressBar1Lb";
			this.progressBar1Lb.Size = new System.Drawing.Size(48, 13);
			this.progressBar1Lb.TabIndex = 28;
			this.progressBar1Lb.Text = "Progress";
			// 
			// printDialog1
			// 
			this.printDialog1.Document = this.printDocument1;
			this.printDialog1.UseEXDialog = true;
			// 
			// pageSetupDialog1
			// 
			this.pageSetupDialog1.Document = this.printDocument1;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.DarkBlue;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.ForeColor = System.Drawing.Color.GhostWhite;
			this.button1.Location = new System.Drawing.Point(516, 72);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(86, 32);
			this.button1.TabIndex = 29;
			this.button1.Text = "Exit All";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.button1_Click_2);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.DarkBlue;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button2.ForeColor = System.Drawing.Color.GhostWhite;
			this.button2.Location = new System.Drawing.Point(420, 72);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(86, 32);
			this.button2.TabIndex = 30;
			this.button2.Text = "Switch Test";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.button2_Click_2);
			// 
			// abortTest
			// 
			this.abortTest.BackColor = System.Drawing.Color.DarkBlue;
			this.abortTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
			this.abortTest.ForeColor = System.Drawing.Color.GhostWhite;
			this.abortTest.Location = new System.Drawing.Point(518, 149);
			this.abortTest.Name = "abortTest";
			this.abortTest.Size = new System.Drawing.Size(86, 32);
			this.abortTest.TabIndex = 31;
			this.abortTest.Text = "Abort Test";
			this.abortTest.UseVisualStyleBackColor = false;
			this.abortTest.Click += new System.EventHandler(this.abortTest_Click);
			// 
			// CommanderTestForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Silver;
			this.ClientSize = new System.Drawing.Size(796, 688);
			this.Controls.Add(this.abortTest);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.progressBar1Lb);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.textBox3);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.buttonStartCommanderTest);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "CommanderTestForm";
			this.Text = "Commander Test Form";
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        public System.Windows.Forms.Button buttonStartCommanderTest;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RichTextBox richTextBoxTestLogs;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        protected System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label progressBar1Lb;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        public System.Windows.Forms.CheckBox checkBoxShowOPLog;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button abortTest;
    }
}