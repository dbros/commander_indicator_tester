﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
//using System.Diagnostics;

namespace CAN232_Monitor
{
    static class Utils
    {
        static public void delay(int ms)
        {
            do
            {
                Thread.Sleep(1);// Not very reliable
                Application.DoEvents();
            }
            while ((ms--) > 0);
        }


        static public bool askYesNo(string text)
        {
            DialogResult result = MessageBox.Show(text, "Confirmation", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes) return true;
            return false;
        }

        /*static public void delayStopwatch()
         {
             Stopwatch sw = new Stopwatch(); // sw cotructor
             sw.Start(); // starts the stopwatch
             for (int i = 0; ; i++) 
             {
                 if (i % 100000 == 0) // if in 100000th iteration (could be any other large number
             // depending on how often you want the time to be checked) 
             {
                 sw.Stop(); // stop the time measurement
                 if (sw.ElapsedMilliseconds > 5000) // check if desired period of time has elapsed
                 {
                     break; // if more than 5000 milliseconds have passed, stop looping and return
                     // to the existing code
                 }
                 else
                 {
                     sw.Start(); // if less than 5000 milliseconds have elapsed, continue looping
                     // and resume time measurement
                 }
             }
             }
         }*/

        static public void delayThread(int delayMiliSecs)
        {
          DateTime Tthen = DateTime.Now;

          do
            {
                Application.DoEvents();
            } 
          //while (Tthen.AddSeconds(delaySecs) > DateTime.Now);
          while (Tthen.AddMilliseconds(delayMiliSecs) > DateTime.Now) ;
        }

    }

}
