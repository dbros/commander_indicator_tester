﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MediatR;
using ProdTestingDomain.Queries;
using MediatR.SimpleInjector;
using SimpleInjector;
using ProdTestingModels;
using System.Reflection;
using Autofac;

namespace CAN232_Monitor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Can232());
            /*var container = DIContainer.Bootstrap();
            var mediator = container.GetInstance<IMediator>();

            var query = new GetTestsByCategoryQuery(1);
            var result = mediator.Send(query);*/

            /*var containiner = DIContainerConfig.Configure();
            using (var scope = containiner.BeginLifetimeScope())
            {
                var query = new GetTestsByCategoryQuery(1);
                var result = containiner.Resolve<IMediator>().Send(query);
                int i = 0;
            }*/


            Application.Run(new LoginForm());
        }

       /* public static Container RegisterMediator(this Container container)
        {
            var assemblies = GetAssemblies();
            return container.BuildMediator(assemblies);
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            yield return typeof(TestCommand).GetTypeInfo().Assembly;
        }*/

        /*private static void RegisterAssemblies(Container container)
        {
            var assemblies = GetAssemblies();
            container.RegisterSingleton<IMediator, Mediator>();
            container.Register(typeof(IRequestHandler<,>), assemblies);
            //container.Register(typeof(IRequestHandler<,>), typeof(GetTestsByCategoryQuery).Assembly);
            /*container.Register(typeof(IAsyncRequestHandler<,>), assemblies);
            container.Register(typeof(ICancellableAsyncRequestHandler<>), assemblies);
            container.RegisterCollection(typeof(INotificationHandler<>), assemblies);
            container.RegisterCollection(typeof(IAsyncNotificationHandler<>), assemblies);
            container.RegisterCollection(typeof(ICancellableAsyncNotificationHandler<>), assemblies);
            container.RegisterCollection(typeof(IPipelineBehavior<,>), assemblies);*/
           /* container.Collection.Register(typeof(IPipelineBehavior<,>), Enumerable.Empty<Type>());

            container.Register(() => Mapper.Instance);
            container.RegisterSingleton(new SingleInstanceFactory(container.GetInstance));
            container.RegisterSingleton(new MultiInstanceFactory(container.GetAllInstances));
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            yield return typeof(IMediator).GetTypeInfo().Assembly;
            yield return typeof(GetTestsByCategoryQuery).GetTypeInfo().Assembly;
        }*/
    }
}
