﻿using ProdTestingModels;
using System.Collections.Generic;

namespace ProdTestingDataAccess.Interfaces
{
	public interface IFailureReasonsQueryRepository
	{
		List<FailureReason> Get(int TestCategoryId);
		int GetByIdCount(int TestSessionId);
	}
}