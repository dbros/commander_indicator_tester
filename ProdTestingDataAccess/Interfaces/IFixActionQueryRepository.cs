﻿using ProdTestingModels;
using System.Collections.Generic;

namespace ProdTestingDataAccess.Interfaces
{
	public interface IFixActionQueryRepository
	{
		List<FixActionResponse> Get(int TestCategoryId, int FailureReasonId);
		int GetByIdCount(int TestSessionId);
	}
}