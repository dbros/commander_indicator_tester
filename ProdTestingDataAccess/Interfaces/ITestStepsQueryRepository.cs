﻿using ProdTestingModels;
using System.Collections.Generic;

namespace ProdTestingDataAccess.Interfaces
{
	public interface ITestStepsQueryRepository
	{
		List<TestStep> Get(int TestCategoryId, int TestId);
	}
}