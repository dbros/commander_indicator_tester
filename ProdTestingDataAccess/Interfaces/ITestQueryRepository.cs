﻿using ProdTestingModels;
using System.Collections.Generic;

namespace ProdTestingDataAccess.Interfaces
{
	public interface ITestQueryRepository
	{
		List<Test> GetAll (int Id);
	}
}