﻿using System;
using System.Collections.Generic;
using System.Text;
using ProdTestingDataAccess.Repositories;

namespace ProdTestingDataAccess.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ITestSessionCommandsRepository TestSessionCommandsRepository { get; }
        ITestSessionResultCommandsRepository TestSessionResultCommandsRepository { get; }
        ITestQueryRepository TestQueryRepository { get; }
        ITestPartCodesQueryRepository TestPartCodesQueryRepository { get; }
        ITestStepsQueryRepository TestStepsQueryRepository { get; }
        IFailureReasonsQueryRepository FailureReasonsQueryRepository { get; }
        IFixActionQueryRepository FixActionQueryRepository { get; }
        ITestSessionFailureCommandsRepository TestSessionFailureCommandsRepository { get; }
        ITestSessionFixActionCommandsRepository TestSessionFixActionCommandsRepository { get; }
        ITestSessionExistForDevIdRepository TestSessionExistForDevIdRepository { get; }

        void Commit();
    }
}
