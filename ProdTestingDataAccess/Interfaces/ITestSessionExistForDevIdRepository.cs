﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdTestingDataAccess.Interfaces
{
	public interface ITestSessionExistForDevIdRepository
	{
		bool CheckDevIdExists(string TestSessionId);
	}
}
