﻿using ProdTestingModels;

namespace ProdTestingDataAccess.Interfaces
{
	public interface ITestSessionCommandsRepository
	{
		TestSession Create(TestSession testSession);
	}
}