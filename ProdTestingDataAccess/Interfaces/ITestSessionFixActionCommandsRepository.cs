﻿using ProdTestingModels;

namespace ProdTestingDataAccess.Interfaces
{
	public interface ITestSessionFixActionCommandsRepository
	{
		int Create(int TestSessionId, int FixActionId);
		int Update(int TestSessionId, int FixActionId);
	}
}