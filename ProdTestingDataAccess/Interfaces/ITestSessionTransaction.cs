﻿using ProdTestingModels;

namespace ProdTestingDataAccess.Interfaces
{
	public interface ITestSessionTransaction
	{
		int Create(TestSession testSession);
	}
}