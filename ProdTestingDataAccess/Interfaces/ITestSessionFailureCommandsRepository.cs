﻿using ProdTestingModels;

namespace ProdTestingDataAccess.Interfaces
{
	public interface ITestSessionFailureCommandsRepository
	{
		int Create(int TestSessionId, int FailureReasonId);
		int Update(int TestSessionId, int FailureReasonId);
	}
}