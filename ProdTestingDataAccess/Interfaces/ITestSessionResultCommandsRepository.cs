﻿using ProdTestingModels;

namespace ProdTestingDataAccess.Interfaces
{
	public interface ITestSessionResultCommandsRepository
	{
		TestSessionResult Create(TestSessionResult testSessionResult);
	}
}