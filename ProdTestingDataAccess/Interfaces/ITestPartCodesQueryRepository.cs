﻿using ProdTestingModels;
using System.Collections.Generic;

namespace ProdTestingDataAccess.Interfaces
{
	public interface ITestPartCodesQueryRepository
	{
		List<TestPartCode> Get(int CategoryId, int TestId);
	}
}