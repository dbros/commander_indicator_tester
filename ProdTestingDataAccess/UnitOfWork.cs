﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using ProdTestingDataAccess.Repositories;
using ProdTestingDataAccess.Interfaces;

namespace ProdTestingDataAccess
{
	public class UnitOfWork : IUnitOfWork
	{
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private ITestSessionCommandsRepository _testSessionCommandsRepository;
        private ITestSessionResultCommandsRepository _testSessionResultCommandsRepository;
        private ITestQueryRepository _testQueryRepository;
        private ITestPartCodesQueryRepository _testPartCodesQueryRepository;
        private ITestStepsQueryRepository _testStepsQueryRepository;
        private IFailureReasonsQueryRepository _failureReasonsQueryRepository;
        private IFixActionQueryRepository _fixActionQueryRepository;
        private ITestSessionFailureCommandsRepository _testSessionFailureCommandsRepository;
        private ITestSessionFixActionCommandsRepository _testSessionFixActionCommandsRepository;
        private ITestSessionExistForDevIdRepository _testSessionExistForDevIdRepository;
        private bool _disposed;

        //private string _connectionString = @"Data Source=10.0.0.16;Initial Catalog=ProductQCTesting;persist security info=True;Integrated Security=SSPI;";
        private string _connectionString = @"Data Source = 10.0.0.16; Initial Catalog = ProductQCTesting; User ID = ProductQCTesting; Password=snak3.Rattle.ggE39?;";
        //public UnitOfWork(string connectionString)
        public UnitOfWork()
        {
            _connection = new SqlConnection(_connectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public ITestSessionCommandsRepository TestSessionCommandsRepository
        {
            get { return _testSessionCommandsRepository ?? (_testSessionCommandsRepository = new TestSessionCommandsRepository(_transaction)); }
        }

        public ITestSessionResultCommandsRepository TestSessionResultCommandsRepository
        {
            get { return _testSessionResultCommandsRepository ?? (_testSessionResultCommandsRepository = new TestSessionResultCommandsRepository(_transaction)); }
        }

        public ITestQueryRepository TestQueryRepository
        {
            get { return _testQueryRepository ?? (_testQueryRepository = new TestQueryRepository(_transaction)); }
        }

        public ITestPartCodesQueryRepository TestPartCodesQueryRepository
        {
            get { return _testPartCodesQueryRepository ?? (_testPartCodesQueryRepository = new TestPartCodesQueryRepository(_transaction)); }
        }
        public ITestStepsQueryRepository TestStepsQueryRepository
        {
            get { return _testStepsQueryRepository ?? (_testStepsQueryRepository = new TestStepsQueryRepository(_transaction)); }
        }

        public IFailureReasonsQueryRepository FailureReasonsQueryRepository
        {
            get { return _failureReasonsQueryRepository ?? (_failureReasonsQueryRepository = new FailureReasonsQueryRepository(_transaction)); }
        }

        public IFixActionQueryRepository FixActionQueryRepository
        {
            get { return _fixActionQueryRepository ?? (_fixActionQueryRepository = new FixActionQueryRepository(_transaction)); }
        }

        public ITestSessionFailureCommandsRepository TestSessionFailureCommandsRepository
        {
            get { return _testSessionFailureCommandsRepository ?? (_testSessionFailureCommandsRepository = new TestSessionFailureCommandsRepository(_transaction)); }
        }

        public ITestSessionFixActionCommandsRepository TestSessionFixActionCommandsRepository
        {
            get { return _testSessionFixActionCommandsRepository ?? (_testSessionFixActionCommandsRepository = new TestSessionFixActionCommandsRepository(_transaction)); }
        }

        public ITestSessionExistForDevIdRepository TestSessionExistForDevIdRepository
        {
            get { return _testSessionExistForDevIdRepository ?? (_testSessionExistForDevIdRepository = new TestSessionExistForDevIdRepository(_transaction)); }
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                resetRepositories();
            }
        }

        private void resetRepositories()
        {
            _testSessionCommandsRepository = null;
            _testSessionResultCommandsRepository = null;
            _testQueryRepository = null;
            _testPartCodesQueryRepository = null;
            _testStepsQueryRepository = null;
            _failureReasonsQueryRepository = null;
            _fixActionQueryRepository = null;
            _testSessionFailureCommandsRepository = null;
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~UnitOfWork()
        {
            dispose(false);
        }
    }
}
