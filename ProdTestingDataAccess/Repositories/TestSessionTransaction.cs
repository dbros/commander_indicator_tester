﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using ProdTestingModels;
using ProdTestingDataAccess.Interfaces;

namespace ProdTestingDataAccess.Repositories
{
	public class TestSessionTransaction : ITestSessionTransaction
	{
		/*private readonly string _connectionString; //from StartUp.cs

		public TestSessionTransaction(string ConnectionString)
		{
			_connectionString = ConnectionString;
		}*/
		public int Create(TestSession testSession)
		{
			int i = 0;
			using (IDbConnection cnn = new SqlConnection(Connection.GetConnectionString()))
			{
				var p = new DynamicParameters();
				p.Add("@TestCategoryId", testSession.TestCategoryId);
				p.Add("@TestId", testSession.TestId);
				p.Add("@TestSessionTypeId", testSession.TestSessionTypeId);
				p.Add("@DevId", testSession.DevId);
				p.Add("@Version", testSession.Version);
				p.Add("@PartCode", testSession.PartCode);
				p.Add("@EmpBadge", testSession.EmpBadge);
				p.Add("@EmpName", testSession.EmpName);
				p.Add("@EmpDept", testSession.EmpDept);
				p.Add("@SupervisorEmail", testSession.SupervisorEmail);
				p.Add("@PCIPAddress", testSession.PCIPAddress);
				p.Add("@PCName", testSession.PCName);
				p.Add("@PCOpSystem", testSession.PCOpSystem);
				p.Add("@Comport", testSession.Comport);
				p.Add("@JobTraveller", testSession.JobTraveller);
				p.Add("@SerialNo", testSession.SerialNo);
				p.Add("@StartDateTime", testSession.StartDateTime);
				p.Add("@EndDateTime", testSession.EndDateTime);
				p.Add("@OverallResult", testSession.OverallResult);
				p.Add("@IsAborted", testSession.IsAborted);

				string sqlSession = $@"insert into dbo.TestSession(TestCategoryId,TestId,TestSessionTypeId,DevId,Version,PartCode,EmpBadge,EmpName,EmpDept,SupervisorEmail,PCIPAddress,PCName,PCOpSystem,Comport,JobTraveller,SerialNo,StartDateTime,EndDateTime,OverallResult,IsAborted)
								       values(@TestCategoryId,@TestId,@TestSessionTypeId,@DevId,@Version,@PartCode,@EmpBadge,@EmpName,@EmpDept,@SupervisorEmail,@PCIPAddress,@PCName,@PCOpSystem,@Comport,@JobTraveller,@SerialNo,@StartDateTime,@EndDateTime,@OverallResult,@IsAborted);
								       SELECT CAST(SCOPE_IDENTITY() as int)";
				cnn.Open();
				using (var trans = cnn.BeginTransaction())
				{
					try
					{
						//testSession.TestSessionId = Convert.ToInt32(Connection.Insert(testSession, transaction: Transaction));
						testSession.TestSessionId = cnn.QuerySingle<int>(sqlSession, p, trans);   //Execute(sqlSession, p, trans);

					    foreach (TestSessionResult item in testSession.TestSessionResult)
						{
							var paramDetails = new DynamicParameters();
							paramDetails.Add("@TestSessionId", testSession.TestSessionId);
							paramDetails.Add("@StepId", item.StepId);
							paramDetails.Add("@FailPassResult", item.FailPassResult);
							paramDetails.Add("@TextResult", item.TextResult);

							string sqlResult = $@"insert into dbo.TestSessionResult(TestSessionId,StepId,FailPassResult,TextResult)
											  values(@TestSessionId,@StepId,@FailPassResult,@TextResult);";

							//Insert record in detail table. Pass transaction parameter to Dapper.
							var affectedRows = cnn.Execute(sqlResult, paramDetails, trans);

						}
						trans.Commit();
						i = testSession.TestSessionId;

					}
					catch(Exception ex)
					{
						trans.Rollback();
						i = 0;
					}

				}
			}

			return i;

		}
	}
}
