﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ProdTestingModels;
using Dapper;
using ProdTestingDataAccess.Interfaces;

namespace ProdTestingDataAccess.Repositories
{
	public class TestQueryRepository : RepositoryBase, ITestQueryRepository
	{
		public TestQueryRepository(IDbTransaction transaction)
			: base(transaction)
		{ }

		public List<Test> GetAll(int Id)
		{
			var tests = new List<Test>();

			var sqlQuery = @"select TestID, TestName, TestCategoryId from Test where TestCategoryId = @Id";

			var p = new { Id = Id };

			tests = (List<Test>)Connection.Query<Test>(sqlQuery, p, transaction: Transaction);

			return tests;
		}
	}
}
