﻿using System;
using System.Collections.Generic;
using System.Text;
using ProdTestingDataAccess.Interfaces;
using System.Data;
using ProdTestingModels;
using Dapper;

namespace ProdTestingDataAccess.Repositories
{
	public class TestSessionExistForDevIdRepository : RepositoryBase, ITestSessionExistForDevIdRepository
	{
		public TestSessionExistForDevIdRepository(IDbTransaction transaction)
			: base(transaction)
		{ }

		public bool CheckDevIdExists(string DevId)
		{
			var count = 0;

			var sqlQuery = @"SELECT Count(*) FROM TestSession WHERE DevId = @DevId and IsDeleted <> 1";

			var p = new { DevId = DevId };

			count = Connection.QueryFirst<int>(sqlQuery, p, transaction: Transaction);

			return count > 0 ? true : false;
		}
	}
}
