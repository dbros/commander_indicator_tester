﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ProdTestingDataAccess.Repositories
{
    public abstract class RepositoryBase
    {
        internal IDbTransaction Transaction { get; private set; }
        internal IDbConnection Connection { get { return Transaction.Connection; } }

        public RepositoryBase(IDbTransaction transaction)
        {
            Transaction = transaction;
        }
    }
}
