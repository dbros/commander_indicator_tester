﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ProdTestingModels;
using Dapper;
using ProdTestingDataAccess.Interfaces;


namespace ProdTestingDataAccess.Repositories
{
	public class FixActionQueryRepository : RepositoryBase, IFixActionQueryRepository
	{
		public FixActionQueryRepository(IDbTransaction transaction)
			: base(transaction)
		{ }

		public List<FixActionResponse> Get(int TestCategoryId, int FailureReasonId)
		{
			var recs = new List<FixActionResponse>();

			var sqlQuery = @"SELECT ffa.FixActionId, a.FixActionName
							 FROM FailureFixAction ffa
						     LEFT JOIN FixAction a
							 ON ffa.FixActionId = a.FixActionId
							 WHERE ffa.TestCategoryId = @TestCategoryId
							 AND ffa.FailureReasonId = @FailureReasonId;";

			var p = new { TestCategoryId = TestCategoryId, FailureReasonId = FailureReasonId };

			recs = (List<FixActionResponse>)Connection.Query<FixActionResponse>(sqlQuery, p, transaction: Transaction);

			return recs;
		}

		public int GetByIdCount(int TestSessionId)
		{
			var recs = 0;

			var sqlQuery = @"SELECT Count(*) FROM TestSessionFixAction WHERE TestSessionId = @TestSessionId";

			var p = new { TestSessionId = TestSessionId };

			//recs = (int)Connection.Query<int>(sqlQuery, p);//, transaction: Transaction);
			recs = Connection.QueryFirst<int>(sqlQuery, p, transaction: Transaction);//, transaction: Transaction);

			return recs;
		}
	}
}
