﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ProdTestingModels;
using Dapper;
using ProdTestingDataAccess.Interfaces;


namespace ProdTestingDataAccess.Repositories
{
	public class FailureReasonsQueryRepository : RepositoryBase, IFailureReasonsQueryRepository
	{
		public FailureReasonsQueryRepository(IDbTransaction transaction)
			: base(transaction)
		{ }

		public List<FailureReason> Get(int TestCategoryId)
		{
			var recs = new List<FailureReason>();

			var sqlQuery = @"SELECT FailureReasonId, FailureReasonName, TestCategoryId from FailureReason 
                             WHERE TestCategoryId = @TestCategoryId and FailureReasonId <> 0 and IsEnabled <> 0";

			var p = new { TestCategoryId = TestCategoryId };

			recs = (List<FailureReason>)Connection.Query<FailureReason>(sqlQuery, p, transaction: Transaction);

			return recs;
		}

		public int GetByIdCount(int TestSessionId)
		{
			var recs = 0;

			var sqlQuery = @"SELECT Count(*) FROM TestSessionFailure WHERE TestSessionId = @TestSessionId";

			var p = new { TestSessionId = TestSessionId };

			//recs = (int)Connection.Query<int>(sqlQuery, p);//, transaction: Transaction);
			recs = Connection.QueryFirst<int>(sqlQuery, p, transaction: Transaction);//, transaction: Transaction);

			return recs;
		}
	}
}
