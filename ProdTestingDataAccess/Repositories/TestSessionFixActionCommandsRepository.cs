﻿using System;
using System.Collections.Generic;
using System.Text;
using ProdTestingModels;
using Dapper;
using System.Data;
using Dapper.Contrib.Extensions;
using System.Data.SqlClient;
using ProdTestingDataAccess.Interfaces;

namespace ProdTestingDataAccess.Repositories
{
	public class TestSessionFixActionCommandsRepository : RepositoryBase, ITestSessionFixActionCommandsRepository
	//public class TestSessionFailureCommandsRepository : ITestSessionFailureCommandsRepository
	{ 
		public TestSessionFixActionCommandsRepository(IDbTransaction transaction)
			: base(transaction)
		{ }

		public int Create(int testSessionId, int fixActionId)
		{
			var affectedRows = 0;

			var p = new { TestSessionid = testSessionId, FixActionId = fixActionId };
			string sql = "INSERT INTO TestSessionFixAction (TestSessionId, FixActionId) Values (@TestSessionId, @FixActionId);";

			affectedRows = Connection.Execute(sql, p, transaction: Transaction);

			return affectedRows;
		}

		public int Update(int testSessionId, int fixActionId)
		{
			var affectedRows = 0;

			var p = new { TestSessionid = testSessionId, FixActionId = fixActionId };
			string sql = "UPDATE TestSessionFixAction SET FixActionId = @FixActionId WHERE TestSessionId = @TestSessionId;";

			affectedRows = Connection.Execute(sql, p, transaction: Transaction);

			return affectedRows;
		}
	}
}
