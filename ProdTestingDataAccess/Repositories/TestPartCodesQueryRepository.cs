﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ProdTestingModels;
using Dapper;
using ProdTestingDataAccess.Interfaces;


namespace ProdTestingDataAccess.Repositories
{
	public class TestPartCodesQueryRepository : RepositoryBase, ITestPartCodesQueryRepository
	{
		public TestPartCodesQueryRepository(IDbTransaction transaction)
			: base(transaction)
		{ }

		public List<TestPartCode> Get(int TestCategoryId, int TestId)
		{
			var testPartCodes = new List<TestPartCode>();

			var sqlQuery = @"select TestCategoryId, TestId, PartCode from TestPartCodes where TestCategoryId = @TestCategoryId and TestId = @TestId";

			var p = new { TestCategoryId = TestCategoryId, TestId = TestId };

			testPartCodes = (List<TestPartCode>)Connection.Query<TestPartCode>(sqlQuery, p, transaction: Transaction);

			return testPartCodes;
		}
	}
}
