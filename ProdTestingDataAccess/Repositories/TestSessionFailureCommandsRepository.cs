﻿using System;
using System.Collections.Generic;
using System.Text;
using ProdTestingModels;
using Dapper;
using System.Data;
using Dapper.Contrib.Extensions;
using System.Data.SqlClient;
using ProdTestingDataAccess.Interfaces;

namespace ProdTestingDataAccess.Repositories
{
	public class TestSessionFailureCommandsRepository : RepositoryBase, ITestSessionFailureCommandsRepository
	//public class TestSessionFailureCommandsRepository : ITestSessionFailureCommandsRepository
	{ 
		public TestSessionFailureCommandsRepository(IDbTransaction transaction)
			: base(transaction)
		{ }

		/*public int Create(int testSessionId, int failureReasonId)
		{
			var affectedRows = 0;
			using (IDbConnection cnn = new SqlConnection(Connection.GetConnectionString()))
			{

				var p = new { TestSessionid = testSessionId, FailureReasonId = failureReasonId };
				string sql = "INSERT INTO TestSessionFailure (TestSessionId, FailureReasonId) Values (@TestSessionId, @FailureReasonId);";

				affectedRows = cnn.Execute(sql, p);
				//Connection.Insert(testSessionFailure, transaction: Transaction);
			}

			return affectedRows;
		}*/

		public int Create(int testSessionId, int failureReasonId)
		{
			var affectedRows = 0;

			var p = new { TestSessionid = testSessionId, FailureReasonId = failureReasonId };
			string sql = "INSERT INTO TestSessionFailure (TestSessionId, FailureReasonId) Values (@TestSessionId, @FailureReasonId);";

			affectedRows = Connection.Execute(sql, p, transaction: Transaction);
			//Connection.Insert(testSessionFailure, transaction: Transaction);

			return affectedRows;
		}

		public int Update(int testSessionId, int failureReasonId)
		{
			var affectedRows = 0;

			var p = new { TestSessionid = testSessionId, FailureReasonId = failureReasonId };
			string sql = "UPDATE TestSessionFailure SET FailureReasonId = @FailureReasonId WHERE TestSessionId = @TestSessionId;";

			affectedRows = Connection.Execute(sql, p, transaction: Transaction);
			//Connection.Insert(testSessionFailure, transaction: Transaction);

			return affectedRows;
		}
	}
}
