﻿using System;
using System.Collections.Generic;
using System.Text;
using ProdTestingModels;
using Dapper;
using System.Data;
using Dapper.Contrib.Extensions;
using System.Data.SqlClient;
using ProdTestingDataAccess.Interfaces;

namespace ProdTestingDataAccess.Repositories
{
	public class TestSessionCommandsRepository : RepositoryBase, ITestSessionCommandsRepository
	{
		public TestSessionCommandsRepository(IDbTransaction transaction)
			: base(transaction)
		{ }

		public TestSession Create(TestSession testSession)
		{
			testSession.TestSessionId = Convert.ToInt32(Connection.Insert(testSession, transaction: Transaction));

			return testSession;
		}
		
	}
}
