﻿using System;
using System.Collections.Generic;
using System.Text;
using ProdTestingModels;
using System.Data;
using Dapper.Contrib.Extensions;
using ProdTestingDataAccess.Interfaces;

namespace ProdTestingDataAccess.Repositories
{
	public class TestSessionResultCommandsRepository : RepositoryBase, ITestSessionResultCommandsRepository
	{
		public TestSessionResultCommandsRepository(IDbTransaction transaction)
			: base(transaction)
		{ }

		public TestSessionResult Create(TestSessionResult testSessionResult)
		{
			testSessionResult.Id = Convert.ToInt32(Connection.Insert(testSessionResult, transaction: Transaction));

			return testSessionResult;
		}
	}
}
