﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ProdTestingModels;
using Dapper;
using ProdTestingDataAccess.Interfaces;


namespace ProdTestingDataAccess.Repositories
{
	public class TestStepsQueryRepository : RepositoryBase, ITestStepsQueryRepository
	{
		public TestStepsQueryRepository(IDbTransaction transaction)
			: base(transaction)
		{ }

		public List<TestStep> Get(int TestCategoryId, int TestId)
		{
			var testSteps = new List<TestStep>();

			var sqlQuery = @"SELECT t.TestCategoryId, t.TestId, t.TestName, ts.StepId, s.StepName, ts.TestSequence from TestSteps ts 
                             INNER JOIN Step s on ts.StepId = s.StepId
                             INNER JOIN Test t on ts.TestId = t.TestId
                             WHERE ts.TestCategoryId = @TestCategoryId
							 AND ts.TestId = @TestId
							 AND ts.TestStepEnabled = 1
                             AND t.TestEnabled = 1
							 ORDER BY ts.StepId asc";

			var p = new { TestCategoryId = TestCategoryId, TestId = TestId };

			testSteps = (List<TestStep>)Connection.Query<TestStep>(sqlQuery, p, transaction: Transaction);

			return testSteps;
		}
	}
}
