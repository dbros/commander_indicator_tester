﻿using ProdTestingModels;

namespace ProdTestingDomain
{
	public interface ITestSessionCommandsService
	{
		TestSessionResponse Create(TestSessionRequest request);
	}
}