﻿using System;
using System.Collections.Generic;
using System.Text;
using ProdTestingModels;

namespace ProdTestingDomain.Mappers
{
	public class TestStepMapper : ITestStepMapper
	{
		public List<TestStepResponse> MapEntityToModel(List<TestStep> testPartCodes, List<TestStepResponse> testStepsResponse)
		{
			var response = new List<TestStepResponse>();

			foreach (var rec in testPartCodes)
			{
				var testStep = new TestStepResponse
				{
					TestName = rec.TestName,
					StepId = rec.StepId,
					StepName = rec.StepName,
					TestSequence = rec.TestSequence
				};

				response.Add(testStep);
			}

			return response;
		}
	}
}
