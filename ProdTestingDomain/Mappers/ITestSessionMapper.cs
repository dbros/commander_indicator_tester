﻿using ProdTestingDomain.Commands.CreateTestSession;
using ProdTestingModels;
using System.Collections.Generic;

namespace ProdTestingDomain.Mappers
{
	public interface ITestSessionMapper
	{
		TestSession MapCreateTestSessionCommandToEntity(CreateTestSessionCommand request);
		TestSessionResult mapTestSessionResultCommand(TestSessionResultRequest request);
		TestSessionResponse MapEntityToModel(TestSession testSession, List<TestSessionResult> testSessionResult);
	}
}