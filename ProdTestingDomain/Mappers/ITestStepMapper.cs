﻿using ProdTestingModels;
using System.Collections.Generic;

namespace ProdTestingDomain.Mappers
{
	public interface ITestStepMapper
	{
		List<TestStepResponse> MapEntityToModel(List<TestStep> testPartCodes, List<TestStepResponse> testStepsResponse);
	}
}