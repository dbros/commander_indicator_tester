﻿using System;
using System.Collections.Generic;
using System.Text;
using ProdTestingModels;
using ProdTestingDomain.Commands.CreateTestSession;

namespace ProdTestingDomain.Mappers
{
	public class TestSessionMapper : ITestSessionMapper
	{
		public TestSessionResponse MapEntityToModel(TestSession testSession, List<TestSessionResult> testSessionResult)
		{
			var response = new TestSessionResponse
			{
				TestSessionId = testSession.TestSessionId,
				TestCategoryId = testSession.TestCategoryId,
				TestId = testSession.TestId,
				TestSessionTypeId = testSession.TestSessionTypeId,
				DevId = testSession.DevId,
				Version = testSession.Version,
				PartCode = testSession.PartCode,
				EmployeeBadge = testSession.EmpBadge,
				JobTraveller = testSession.JobTraveller,
				SerialNo = testSession.SerialNo,
				StartDateTime = testSession.StartDateTime,
				EndDateTime = testSession.EndDateTime,
				OverallResult = testSession.OverallResult,
				/*GetVersion = testSession.GetVersion,
				IAPVersion = testSession.IAPVersion,
				GetDevID = testSession.GetDevID,
				TestIO = testSession.TestIO,
				ClustCleanseAir = testSession.ClustCleanseAir,
				ClustCleanseWater = testSession.ClustCleanseWater,
				Pulsation = testSession.Pulsation,
				AcrRam = testSession.AcrRam,
				MilklineOP = testSession.MilklineOP,
				Retention = testSession.Retention,
				SmartStartReedSwitch = testSession.SmartStartReedSwitch,
				ProxSwitch = testSession.ProxSwitch,
				Temp = testSession.Temp,
				Probes = testSession.Probes,
				Jetstream = testSession.Jetstream,
				DivertLineReedSwitch = testSession.DivertLineReedSwitch,
				TeatSpray = testSession.TeatSpray,
				MilkMeterLeadWire = testSession.MilkMeterLeadWire,
				MilkMeterLead = testSession.MilkMeterLead,*/
				TestSessionResultResponse = new List<TestSessionResultResponse>()
			};

			foreach (var line in testSessionResult)
			{
				response.TestSessionResultResponse.Add(mapTestSessionResult(line));
			}

			return response;
		}

		private TestSessionResultResponse mapTestSessionResult(TestSessionResult result)
		{
			var testSessionResultResponse = new TestSessionResultResponse
			{
				Id = result.Id,
				StepId = result.StepId,
				FailPassResult = result.FailPassResult,
				TextResult = result.TextResult
			};

			return testSessionResultResponse;
		}

		public TestSession MapCreateTestSessionCommandToEntity(CreateTestSessionCommand request)
		{
			var testSession = new TestSession
			{
				TestCategoryId = request.TestCategoryId,
				TestSessionTypeId = request.TestSessionTypeId ?? -1,
				TestId = request.TestId,
				DevId = request.DevId,
				Version = request.Version,
				PartCode = request.PartCode,
				EmpBadge = request.EmployeeBadge,
				EmpName = request.EmployeName,
				EmpDept = request.EmployeeDept,
				SupervisorEmail = request.SupervisorEmail,
				PCIPAddress = request.PCIPAddress,
				PCName = request.PCName,
				PCOpSystem = request.PCOpSystem,
				Comport = request.Comport,
				JobTraveller = request.JobTraveller,
				SerialNo = request.SerialNo,
				StartDateTime = request.StartDateTime,
				EndDateTime = request.EndDateTime,
				OverallResult = request.OverallResult,
				IsAborted = request.IsAborted,
				/*GetVersion = request.GetVersion,
				IAPVersion = request.IAPVersion,
				GetDevID = request.GetDevID,
				TestIO = request.TestIO,
				ClustCleanseAir = request.ClustCleanseAir,
				ClustCleanseWater = request.ClustCleanseWater,
				Pulsation = request.Pulsation,
				AcrRam = request.AcrRam,
				MilklineOP = request.MilklineOP,
				Retention = request.Retention,
				SmartStartReedSwitch = request.SmartStartReedSwitch,
				ProxSwitch = request.ProxSwitch,
				Temp = request.Temp,
				Probes = request.Probes,
				Jetstream = request.Jetstream,
				DivertLineReedSwitch = request.DivertLineReedSwitch,
				TeatSpray = request.TeatSpray,
				MilkMeterLeadWire = request.MilkMeterLeadWire,
				MilkMeterLead = request.MilkMeterLead,
				FailureId = request.FailId,
				FixActionId = request.FixActionId,*/
				TestSessionResult = new List<TestSessionResult>()
			};

			foreach(var line in request.TestSessionResultCommand)
			{
				testSession.TestSessionResult.Add(mapTestSessionResultCommand(line));
			}

			return testSession;
		}

		public TestSessionResult mapTestSessionResultCommand(TestSessionResultRequest request)
		{
			var testSessionResult = new TestSessionResult
			{
				StepId = request.StepId,
				FailPassResult = request.FailPassResult,
				TextResult = request.TextResult
			};

			return testSessionResult;
		}
	}
}
