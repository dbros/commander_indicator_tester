﻿using System;
using System.Collections.Generic;
using System.Text;
using ProdTestingModels;

namespace ProdTestingDomain.Mappers
{
	public class FailureReasonMapper : IFailureReasonMapper
	{
		public List<FailureReasonResponse> MapEntityToModel(List<FailureReason> failureReasons, List<FailureReasonResponse> failureReasonsResponse)
		{
			var response = new List<FailureReasonResponse>();

			foreach (var rec in failureReasons)
			{
				var failureReason = new FailureReasonResponse
				{
					FailureReasonId = rec.FailureReasonId,
					FailureReasonName = rec.FailureReasonName
				};

				response.Add(failureReason);
			}

			return response;
		}
	}
}
