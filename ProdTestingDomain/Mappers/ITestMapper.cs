﻿using ProdTestingModels;
using System.Collections.Generic;

namespace ProdTestingDomain.Mappers
{
	public interface ITestMapper
	{
		List<TestResponse> MapEntityToModel(List<Test> tests, List<TestResponse> testResponse);
	}
}