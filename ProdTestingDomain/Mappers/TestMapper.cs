﻿using System;
using System.Collections.Generic;
using System.Text;
using ProdTestingModels;

namespace ProdTestingDomain.Mappers
{
	public class TestMapper : ITestMapper
	{
		public List<TestResponse> MapEntityToModel(List<Test> tests, List<TestResponse> testResponse)
		{
			var response = new List<TestResponse>();

			foreach (var rec in tests)
			{
				var test = new TestResponse
				{
					TestId = rec.TestId,
					TestName = rec.TestName
				};

				response.Add(test);
			}

			return response;
		}
	}
}
