﻿using System;
using System.Collections.Generic;
using System.Text;
using ProdTestingModels;

namespace ProdTestingDomain.Mappers
{
	public class TestPartCodeMapper : ITestPartCodeMapper
	{
		public List<TestPartCodeResponse> MapEntityToModel(List<TestPartCode> testPartCodes, List<TestPartCodeResponse> testPartCodesResponse)
		{
			var response = new List<TestPartCodeResponse>();

			foreach (var rec in testPartCodes)
			{
				var testPartCode = new TestPartCodeResponse
				{
					TestId = rec.TestId,
					PartCode = rec.PartCode
				};

				response.Add(testPartCode);
			}

			return response;
		}
	}
}
