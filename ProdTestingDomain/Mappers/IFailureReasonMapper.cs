﻿using ProdTestingModels;
using System.Collections.Generic;

namespace ProdTestingDomain.Mappers
{
	public interface IFailureReasonMapper
	{
		List<FailureReasonResponse> MapEntityToModel(List<FailureReason> failureReasons, List<FailureReasonResponse> failureReasonsResponse);
	}
}