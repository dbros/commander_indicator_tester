﻿using ProdTestingModels;
using System.Collections.Generic;

namespace ProdTestingDomain.Mappers
{
	public interface ITestPartCodeMapper
	{
		List<TestPartCodeResponse> MapEntityToModel(List<TestPartCode> testPartCodes, List<TestPartCodeResponse> testPartCodesResponse);
	}
}