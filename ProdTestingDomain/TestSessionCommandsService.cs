﻿using ProdTestingDataAccess.Interfaces;
using ProdTestingDomain.Mappers;
using ProdTestingModels;
using System.Collections.Generic;

namespace ProdTestingDomain
{
	public class TestSessionCommandsService : ITestSessionCommandsService
	{
		//private readonly ITestSessionRepository _testSessionResultRepository;
		private readonly IUnitOfWork _uow;
		private readonly ITestSessionMapper _testSessionMapper;

		//public TestSessionService(ITestSessionRepository repository, ITestSessionMapper mapper)
		public TestSessionCommandsService(IUnitOfWork Uow, ITestSessionMapper mapper)
		{
			//_testSessionResultRepository = repository;
			_uow = Uow;
			_testSessionMapper = mapper;
		}
		public TestSessionResponse Create(TestSessionRequest request)
		{
			/*var testSession = _testSessionMapper.MapCreateTestSessionCommandToEntity(request);
			var tsEntity = _uow.TestSessionCommandsRepository.Create(testSession);
			var tsResultsEntity = new List<TestSessionResult>();

			foreach (var line in request.TestSessionResultCommand)
			{
				var testSessionResult = _testSessionMapper.MapCreateTestSessionCommandToEntity(line);
				testSessionResult.TestSessionId = tsEntity.TestSessionId;
				var tsrEntity = _uow.TestSessionResultCommandsRepository.Create(testSessionResult);
				tsResultsEntity.Add(tsrEntity);
			}
			_uow.Commit();

			return _testSessionMapper.MapEntityToModel(tsEntity, tsResultsEntity);*/
			return null;
		}
	}
}
