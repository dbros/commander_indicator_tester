﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using ProdTestingModels;
using System.Threading;
using System.Threading.Tasks;
using ProdTestingDomain.Mappers;
using ProdTestingDataAccess.Interfaces;

namespace ProdTestingDomain.Queries
{
	public class GetTestStepsQuery : IRequest<List<TestStepResponse>>
	{
		public int TestCategoryId { get; }
		public int TestId { get; }

		public GetTestStepsQuery(int testCategoryid, int testId)
		{
			TestCategoryId = testCategoryid;
			TestId = testId;
		}	
	}

	public class GetTestStepsQueryHandler : IRequestHandler<GetTestStepsQuery, List<TestStepResponse>>
	{
		private readonly IUnitOfWork _uow;
		private readonly ITestStepMapper _testStepMapper;
		public GetTestStepsQueryHandler(IUnitOfWork Uow, ITestStepMapper mapper)
		{
			_uow = Uow;
			_testStepMapper = mapper;
		}
		public async Task<List<TestStepResponse>> Handle(GetTestStepsQuery request, CancellationToken cancellationToken)
		{
			var testStepsEntity = _uow.TestStepsQueryRepository.Get(request.TestCategoryId, request.TestId);
			var tsStepsResultsEntity = new List<TestStepResponse>();

			return _testStepMapper.MapEntityToModel(testStepsEntity, tsStepsResultsEntity);
		}
	}

}
