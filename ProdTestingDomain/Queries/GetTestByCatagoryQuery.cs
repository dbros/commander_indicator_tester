﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using ProdTestingModels;
using System.Threading;
using System.Threading.Tasks;
using ProdTestingDomain.Mappers;
using ProdTestingDataAccess.Interfaces;

namespace ProdTestingDomain.Queries
{
	public class GetTestsByCategoryQuery : IRequest<List<TestResponse>>
	{
		public int TestCategoryId { get; }

		public GetTestsByCategoryQuery(int id)
		{
			TestCategoryId = id;
		}	
	}

	public class GetTestsByCategoryHandler : IRequestHandler<GetTestsByCategoryQuery, List<TestResponse>>
	{
		private readonly IUnitOfWork _uow;
		private readonly ITestMapper _testMapper;
		public GetTestsByCategoryHandler(IUnitOfWork Uow, ITestMapper mapper)
		{
			_uow = Uow;
			_testMapper = mapper;
		}
		public async Task<List<TestResponse>> Handle(GetTestsByCategoryQuery request, CancellationToken cancellationToken)
		{
			var testEntity = _uow.TestQueryRepository.GetAll(request.TestCategoryId);
			var tsResultsEntity = new List<TestResponse>();

			return _testMapper.MapEntityToModel(testEntity, tsResultsEntity);
		}
	}

}
