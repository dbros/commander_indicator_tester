﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using ProdTestingModels;
using System.Threading;
using System.Threading.Tasks;
using ProdTestingDomain.Mappers;
using ProdTestingDataAccess.Interfaces;

namespace ProdTestingDomain.Queries
{
	public class GetFixActionQuery : IRequest<List<FixActionResponse>>
	{
		public int TestCategoryId { get; }
		public int FailureReasonId { get; set; }

		public GetFixActionQuery(int testCategoryid, int failureReasonId)
		{
			TestCategoryId = testCategoryid;
			FailureReasonId = failureReasonId;
		}	
	}

	public class GetFixActionQueryHandler : IRequestHandler<GetFixActionQuery, List<FixActionResponse>>
	{
		private readonly IUnitOfWork _uow;
		//private readonly IFailureReasonMapper _mapper;
		public GetFixActionQueryHandler(IUnitOfWork Uow, IFailureReasonMapper mapper)
		{
			_uow = Uow;
			//_mapper = mapper;
		}
		public async Task<List<FixActionResponse>> Handle(GetFixActionQuery request, CancellationToken cancellationToken)
		{
			var entity = _uow.FixActionQueryRepository.Get(request.TestCategoryId, request.FailureReasonId);
			var resultsEntity = new List<FixActionResponse>();

			//return _mapper.MapEntityToModel(entity, resultsEntity);

			return entity;
		}
	}

}
