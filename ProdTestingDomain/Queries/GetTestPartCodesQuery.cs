﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using ProdTestingModels;
using System.Threading;
using System.Threading.Tasks;
using ProdTestingDomain.Mappers;
using ProdTestingDataAccess.Interfaces;

namespace ProdTestingDomain.Queries
{
	public class GetTestPartCodesQuery : IRequest<List<TestPartCodeResponse>>
	{
		public int TestCategoryId { get; }
		public int TestId { get; }

		public GetTestPartCodesQuery(int testCategoryid, int testId)
		{
			TestCategoryId = testCategoryid;
			TestId = testId;
		}	
	}

	public class GetTestPartCodesQueryHandler : IRequestHandler<GetTestPartCodesQuery, List<TestPartCodeResponse>>
	{
		private readonly IUnitOfWork _uow;
		private readonly ITestPartCodeMapper _testPartCodeMapper;
		public GetTestPartCodesQueryHandler(IUnitOfWork Uow, ITestPartCodeMapper mapper)
		{
			_uow = Uow;
			_testPartCodeMapper = mapper;
		}
		public async Task<List<TestPartCodeResponse>> Handle(GetTestPartCodesQuery request, CancellationToken cancellationToken)
		{
			var testPartCodesEntity = _uow.TestPartCodesQueryRepository.Get(request.TestCategoryId, request.TestId);
			var tsPartCodesResultsEntity = new List<TestPartCodeResponse>();

			return _testPartCodeMapper.MapEntityToModel(testPartCodesEntity, tsPartCodesResultsEntity);
		}
	}

}
