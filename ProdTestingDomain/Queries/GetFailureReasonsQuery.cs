﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using ProdTestingModels;
using System.Threading;
using System.Threading.Tasks;
using ProdTestingDomain.Mappers;
using ProdTestingDataAccess.Interfaces;

namespace ProdTestingDomain.Queries
{
	public class GetFailureReasonsQuery : IRequest<List<FailureReasonResponse>>
	{
		public int TestCategoryId { get; }

		public GetFailureReasonsQuery(int testCategoryid)
		{
			TestCategoryId = testCategoryid;
		}	
	}

	public class GetFailureReasonsQueryHandler : IRequestHandler<GetFailureReasonsQuery, List<FailureReasonResponse>>
	{
		private readonly IUnitOfWork _uow;
		private readonly IFailureReasonMapper _mapper;
		public GetFailureReasonsQueryHandler(IUnitOfWork Uow, IFailureReasonMapper mapper)
		{
			_uow = Uow;
			_mapper = mapper;
		}
		public async Task<List<FailureReasonResponse>> Handle(GetFailureReasonsQuery request, CancellationToken cancellationToken)
		{
			var entity = _uow.FailureReasonsQueryRepository.Get(request.TestCategoryId);
			var resultsEntity = new List<FailureReasonResponse>();

			return _mapper.MapEntityToModel(entity, resultsEntity);
		}
	}

}
