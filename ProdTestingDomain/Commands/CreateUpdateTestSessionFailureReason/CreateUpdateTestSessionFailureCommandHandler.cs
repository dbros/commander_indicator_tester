﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProdTestingDataAccess.Interfaces;
using ProdTestingDomain.Mappers;


namespace ProdTestingDomain.Commands.CreateUpdateTestSessionFailureReason
{
	public class CreateUpdateTestSessionFailureCommandHandler : IRequestHandler<CreateUpdateTestSessionFailureCommand, int>
    {
        private readonly IUnitOfWork _uow;
        public CreateUpdateTestSessionFailureCommandHandler(IUnitOfWork Uow)//, ITestSessionMapper mapper)
        {
            _uow = Uow;
            //_mapper = mapper;
        }

        public async Task<int> Handle(CreateUpdateTestSessionFailureCommand request, CancellationToken cancellationToken)
        {
           
            int response = 0;

            try
			{
                var recExists = _uow.FailureReasonsQueryRepository.GetByIdCount(request.TestSessionId);
                _uow.Commit();
                if (recExists < 1)
                {
                    response = _uow.TestSessionFailureCommandsRepository.Create(request.TestSessionId, request.FailureReasonId);
                    _uow.Commit();
                }

                if (recExists == 1)
                {
                    response = _uow.TestSessionFailureCommandsRepository.Update(request.TestSessionId, request.FailureReasonId);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
			{
                int i = 0;
			}   

            return response;
        }
    }
}
