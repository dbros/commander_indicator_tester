﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace ProdTestingDomain.Commands.CreateUpdateTestSessionFailureReason
{
	public class CreateUpdateTestSessionFailureCommand : IRequest<int>
	{
		public int TestSessionId { get; set; }
		public int FailureReasonId { get; set; }		
	}
}
