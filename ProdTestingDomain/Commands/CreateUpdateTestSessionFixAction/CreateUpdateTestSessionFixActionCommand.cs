﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace ProdTestingDomain.Commands.CreateUpdateTestSessionFixAction
{
	public class CreateUpdateTestSessionFixActionCommand : IRequest<int>
	{
		public int TestSessionId { get; set; }
		public int FixActionId { get; set; }		
	}
}
