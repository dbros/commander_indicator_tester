﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProdTestingDataAccess.Interfaces;
using ProdTestingDomain.Mappers;


namespace ProdTestingDomain.Commands.CreateUpdateTestSessionFixAction
{
	public class CreateUpdateTestSessionFixActionCommandHandler : IRequestHandler<CreateUpdateTestSessionFixActionCommand, int>
    {
        private readonly IUnitOfWork _uow;
        //private readonly ITestSessionMapper _mapper;
        public CreateUpdateTestSessionFixActionCommandHandler(IUnitOfWork Uow)//, ITestSessionMapper mapper)
        {
            _uow = Uow;
            //_mapper = mapper;
        }

        public async Task<int> Handle(CreateUpdateTestSessionFixActionCommand request, CancellationToken cancellationToken)
        {
            int response = 0;

            try
			{
                var recExists = _uow.FixActionQueryRepository.GetByIdCount(request.TestSessionId);
                _uow.Commit();
                if (recExists < 1)
                {
                    response = _uow.TestSessionFixActionCommandsRepository.Create(request.TestSessionId, request.FixActionId);
                    _uow.Commit();
                }

                if (recExists == 1)
                {
                    response = _uow.TestSessionFixActionCommandsRepository.Update(request.TestSessionId, request.FixActionId);
                    _uow.Commit();
                }
            }
            catch (Exception ex)
			{
                int i = 0;
			}      

            return response;
        }
    }
}
