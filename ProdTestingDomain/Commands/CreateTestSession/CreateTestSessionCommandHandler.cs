﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProdTestingDataAccess.Interfaces;
using ProdTestingDomain.Mappers;

namespace ProdTestingDomain.Commands.CreateTestSession
{
    public class CreateTestSessionCommandHandler : IRequestHandler<CreateTestSessionCommand, int>
    {
        private readonly IUnitOfWork _uow;
        private readonly ITestSessionTransaction _context;
        private readonly ITestSessionMapper _mapper;

        public CreateTestSessionCommandHandler(IUnitOfWork Uow, ITestSessionTransaction Context, ITestSessionMapper mapper)
        {
            _uow = Uow;
            _context = Context;
            _mapper = mapper;
        }

        public async Task<int> Handle(CreateTestSessionCommand request, CancellationToken cancellationToken)
        {
            var testSession = _mapper.MapCreateTestSessionCommandToEntity(request);

            if (testSession.TestSessionTypeId == -1 && testSession.DevId != null)
			{
                bool testLoggedForDevId = _uow.TestSessionExistForDevIdRepository.CheckDevIdExists(testSession.DevId);

                testSession.TestSessionTypeId = testLoggedForDevId ? 2 : 1;
            }

            var testSessionId = _context.Create(testSession);

            return testSessionId;
        }
	}
}
