﻿using ProdTestingDomain;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestingConsole
{
	public class Application
	{
		ITestSessionCommandsService _testSessionResultService;
		public Application(ITestSessionCommandsService testSessionResultService)
		{
			_testSessionResultService = testSessionResultService;
		}

		public void Run()
		{
			var result = _testSessionResultService.Create(Testing.MockTestSessionResult());
			Console.WriteLine(result.TestSessionId);
		}
	}
}
