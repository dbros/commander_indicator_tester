﻿using System;
using System.Collections.Generic;
using System.Text;
using ProdTestingModels;
using ProdTestingDomain;
using ProdTestingDomain.Commands.CreateTestSession;

namespace TestingConsole
{
	public class Testing
	{
		//public static void SaveTestSessionResult()
		public static TestSessionRequest MockTestSessionResult()
		{

			var testSession = new TestSessionRequest
			{
				TestId = 21,
				TestSessionTypeId = 3,
				PartCode = "10401669",
				EmployeeBadge = "900052",
				EmployeName = "Deirdre Brosnan",
				EmployeeDept = "ADMIN",
				SupervisorEmail = "jdaly@dairymaster.com",
				PCIPAddress = "10.50.102.76",
				PCName = "DESIGN27",
				PCOpSystem = "Windows 10 Pro",
				Comport = "2",
				JobTraveller = "12345678",
				SerialNo = "635746YT",
				StartDateTime = DateTime.UtcNow.AddMinutes(-1),
				EndDateTime = DateTime.UtcNow,
				OverallResult = true,
				GetVersion = "Ver2.55",
				IAPVersion = "IAP 1.07",
				GetDevID = "2233445566",
				//TestIO = ,
				ClustCleanseAir = true,
				ClustCleanseWater = true,
				Pulsation = true,
				AcrRam = true,
				MilklineOP = true,
				Retention = true,
				SmartStartReedSwitch = true,
				ProxSwitch = true,
				Temp = true,
				Probes = true,
				Jetstream = true,
				DivertLineReedSwitch = true,
				TeatSpray = true,
				MilkMeterLeadWire = true,
				MilkMeterLead = true,
				//FailId = request.FailId,
				//FixActionId = request.FixActionId
				TestSessionResultRequest = new List<TestSessionResultRequest>()

			};
			var testSessionResult1 = new TestSessionResultRequest
			{
				StepId = 1,
				FailPassResult = true
			};

			var testSessionResult2 = new TestSessionResultRequest
			{
				StepId = 2,
				FailPassResult = true
			};

			testSession.TestSessionResultRequest.Add(testSessionResult1);
			testSession.TestSessionResultRequest.Add(testSessionResult2);

			/*var testSessionResultService = new TestSessionResultService();

			var response = testSessionResultService.Create(testSessionResult);

			Console.WriteLine(response.TestSessionId);*/

			return testSession;

		}

		public static CreateTestSessionCommand CreateTestSessionCommand()
		{
			var createTestSessionCommand = new CreateTestSessionCommand
			{
				TestId = 21,
				TestSessionTypeId = 3,
				PartCode = "10401669",
				EmployeeBadge = "900052",
				EmployeName = "Deirdre Brosnan",
				EmployeeDept = "ADMIN",
				SupervisorEmail = "jdaly@dairymaster.com",
				PCIPAddress = "10.50.102.76",
				PCName = "DESIGN27",
				PCOpSystem = "Windows 10 Pro",
				Comport = "2",
				JobTraveller = "12345678",
				SerialNo = "635746YT",
				StartDateTime = DateTime.UtcNow.AddMinutes(-1),
				EndDateTime = DateTime.UtcNow,
				OverallResult = true,
				TestSessionResultCommand = new List<TestSessionResultRequest>()

			};
			var testSessionResult1 = new TestSessionResultRequest
			{
				StepId = 1,
				FailPassResult = true
			};

			var testSessionResult2 = new TestSessionResultRequest
			{
				StepId = 2,
				FailPassResult = true
			};

			createTestSessionCommand.TestSessionResultCommand.Add(testSessionResult1);
			createTestSessionCommand.TestSessionResultCommand.Add(testSessionResult2);

			return createTestSessionCommand;
		}

	}
}
