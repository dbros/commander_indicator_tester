﻿using Autofac;
using ProdTestingDomain.Queries;
using ProdTestingDomain.Commands;
using System;
using MediatR;

namespace TestingConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			//Testing.SaveTestSessionResult();
			var containiner = ContainerConfig.Configure();
			//var mediator = containiner.Resolve<IMediator>();

			//using (var scope = containiner.BeginLifetimeScope())
			//{

				//Create Test
				/*var command = Testing.CreateTestSessionCommand();
				var response = scope.Resolve<IMediator>().Send(command);
				Console.WriteLine(response.Result.ToString());*/

				//Get list of tests
				
				var query = new GetTestsByCategoryQuery(1);
				//var result = scope.Resolve<IMediator>().Send(query);
				var result = containiner.Resolve<IMediator>().Send(query);

			int i = 0;

			//}

			Console.WriteLine("Press and key to exit.");
			Console.ReadLine();
;		}
	}
}
