﻿using Autofac;
using ProdTestingDataAccess;
using ProdTestingDataAccess.Repositories;
using ProdTestingDataAccess.Interfaces;
using ProdTestingDomain.Queries;
using ProdTestingDomain.Commands.CreateTestSession;
using ProdTestingDomain.Mappers;
using MediatR;
using System.Reflection;
using ProdTestingDomain;

namespace TestingConsole
{
	public static class ContainerConfig
	{
		public static IContainer Configure()
		{
			var builder = new ContainerBuilder();

			builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly)
			.AsImplementedInterfaces();
			builder.RegisterAssemblyTypes(typeof(GetTestStepsQuery).GetTypeInfo().Assembly)
				.AsClosedTypesOf(typeof(IRequestHandler<,>));
			builder.RegisterAssemblyTypes(typeof(CreateTestSessionCommand).GetTypeInfo().Assembly)
				.AsClosedTypesOf(typeof(IRequestHandler<,>));

			//builder.RegisterType<TestSessionCommandsService>().As<ITestSessionCommandsService>();
			builder.RegisterType<TestSessionTransaction>().As<ITestSessionTransaction>();
			builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
			builder.RegisterType<TestSessionMapper>().As<ITestSessionMapper>();
			builder.RegisterType<TestMapper>().As<ITestMapper>();

			builder.Register<ServiceFactory>(ctx =>
			{
				var c = ctx.Resolve<IComponentContext>();
				return t => c.Resolve(t);
			});

			return builder.Build();
		}
	}
}
